//
//  AppDelegate.swift
//  Songa
//
//  Created by Collins Korir on 5/15/18.
//  Copyright © 2018 Kip. All rights reserved.
//

import UIKit
import RealmSwift
import AVKit
import AVFoundation
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
import FirebaseMessaging
import Sentry

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate
{

       var window: UIWindow?
      let gcmMessageIDKey = "gcm.message_id"
    var appLaunchedFromNotification = false
    
    //If a background donwload task completes when the app isn’t running, the app will relaunch in the background. This event needs to be handledt from the app delegate. see Downloadanager.swift

    var backgroundSessionCompletionHandler: (() -> Void)?
    
    func application(
      _ application: UIApplication,
      handleEventsForBackgroundURLSession
        handleEventsForBackgroundURLSessionidentifier: String,
      completionHandler: @escaping () -> Void) {
        backgroundSessionCompletionHandler = completionHandler
    }


  //  private private func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOpUIApplication,.LaunchOptionsKeyptionsKey: Any]?) -> Bool {
        
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
   
        // Override point for customization after application launch.
 
  //  init a audio session to enable background play
  //  let audioSession = AVAudioSession.sharedInstance()
        do {
        try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
        try AVAudioSession.sharedInstance().setActive(true)
    } catch {
    print(error)
    print("Setting category to AVAudioSessionCategoryPlayback failed.")
    }
        
      

        // Create a Sentry client and start crash handler
        do {
            Client.shared = try Client(dsn: "https://9c518b6dd40e4ada856b04feb21d069b@sentry.io/1467971")
            try Client.shared?.startCrashHandler()
        } catch let error {
            print("\(error)")
        }
     
        print("Realm location")
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
        FirebaseApp.configure()
        
 
        Messaging.messaging().delegate = self
        
        UNUserNotificationCenter.current().delegate = self
        
        registerForPushNotifications()
       
        
        //get application instance ID
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        application.registerForRemoteNotifications()
       
        
        var config = Realm.Configuration()
        config.deleteRealmIfMigrationNeeded = true
        
        Realm.Configuration.defaultConfiguration = config
       
    // Check if app is launched from notification
    let notificationOption = launchOptions?[.remoteNotification]

        if let notification = notificationOption as? [AnyHashable: Any] {
    
        //let type = notification["type"] as? String ?? ""
            
//            switch type {
//                       case "artist":
//                (window?.rootViewController as? UITabBarController)?.selectedIndex = 1
//
//                       case "album":
//                (window?.rootViewController as? UITabBarController)?.selectedIndex = 1
//
//                       case "chart":
//                (window?.rootViewController as? UITabBarController)?.selectedIndex = 1
//
//                       case "playlist":
//                (window?.rootViewController as? UITabBarController)?.selectedIndex = 1
//
//                       case "track":
//                (window?.rootViewController as? UITabBarController)?.selectedIndex = 1
//
//
//                       default:
//                           print("unknown")
//                       }
//            }
            
            self.appLaunchedFromNotification = true
            saveNotificationToRealm(notificationInfo: notification)
            
            print("aahaja")
            print(appLaunchedFromNotification)
        }
        return true
    }
    
    
       func registerForPushNotifications() {
        
        //Solicit permission from the user to receive notifications
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
              
             UNUserNotificationCenter.current()
               .requestAuthorization(options: authOptions) {
                [weak self] granted, error in
                   print("Permission granted: \(granted)")
                   guard granted else { return }
                self?.getNotificationSettings()
               }
    }
    func getNotificationSettings() {
      UNUserNotificationCenter.current().getNotificationSettings { settings in
        print("Notification settings: \(settings)")
        guard settings.authorizationStatus == .authorized else { return }
        DispatchQueue.main.async {
          UIApplication.shared.registerForRemoteNotifications()
        }
      }
    }
        
        func saveNotificationToRealm(notificationInfo : [AnyHashable: Any]) {
            let notification: SongaNotification = SongaNotification()
                      
                      
                      notification.message =  notificationInfo["message"] as? String ?? ""
                      notification.title = notificationInfo["title"] as? String ?? ""
                      notification.itemType = notificationInfo["type"] as? String ?? ""
                      notification.imageUrl = notificationInfo["image"] as? String ?? ""
                      notification.itemId = notificationInfo["id"] as? String ?? ""
                      
                      let dateFormatter = DateFormatter()
                      dateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ssZZZ"
                      
                      notification.timeStamp =  dateFormatter.string(from: Date())
                      notification.isRead = false
                      
                      
                      do {
                          let realm = try Realm()
                          try realm.write {
                              realm.add(notification,update: .modified)
                          }
                          
                          
                      } catch let error as NSError {
                          
                          print(error)
                          
                      }
        }
    
    //TODO Go to notifications screeen when noti
    func instantiateNotificationsScreen(){
 
    }
}

//Messaging extension

extension AppDelegate :  UNUserNotificationCenterDelegate, MessagingDelegate {
    //TODO: - Hanld eopening noificationn when app is in background
      // This function will be called right after user tap on the notification
    func application(_ application: UIApplication, didReceiveRemoteNotification notificationInfo: [AnyHashable: Any]) {
           //save message to realm
           
           NotificationCenter.default.post(name: Notification.Name.notificationReceived,
                                           object: nil)
        self.saveNotificationToRealm(notificationInfo: notificationInfo)
            
       }
       
       func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
           print("Unable to register for remote notifications: \(error.localizedDescription)")
       }
       
       func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
           print("Firebase registration token: \(fcmToken)")
           
           let dataDict:[String: String] = ["token": fcmToken]
           NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
           // TODO: If necessary send token to application server.
           // Note: This callback is fired at each app startup and whenever a new token is generated.
       }
       
       func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
           print("Received data message: \(remoteMessage.appData)")
       }
    
}
