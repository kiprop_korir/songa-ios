//
//  SongaSecondaryText.swift
//  Songa
//
//  Created by Kiprop Korir on 31/10/2019.
//  Copyright © 2019 Collins Korir. All rights reserved.
//


import UIKit
class SongaSecondaryTextLabel:UILabel {
    override func layoutSubviews() {
           super.layoutSubviews()
        self.textColor = ThemeManager.currentTheme().textColorSecondary
    }
}
