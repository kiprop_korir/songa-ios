//
//  NotificationNames.swift
//  Songa
//
//  Created by Collins Korir on 8/15/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let pauseClicked = Notification.Name("pauseClicked")
    static let playClicked = Notification.Name("playClicked")
    static let playNextClicked = Notification.Name("playNextClicked")
    static let playPreviousClicked = Notification.Name("playPreviousClicked")
    static let PlaylistHasLoaded = Notification.Name("playlistHasLoaded")
    static let ArtistHasLoaded = Notification.Name("artistHasLoaded")
    static let playerStartedPlaying = Notification.Name("playerStartedPlaying")
    static let playerTimeElapsed = Notification.Name("playerTimeElapsed")
    static let playerFinishedPlaying = Notification.Name("playerFinishedPlaying")
    static let playbackError = Notification.Name("playbackError")
    static let sliderValueChanged = Notification.Name("sliderValueChanged")
    static let trackDownloadComplete = Notification.Name("trackDownloadComplete")
    static let startPlay = Notification.Name("startPlay")
    static let startOfflinePlay = Notification.Name("startOfflinePlay")
    static let startRadioPlay = Notification.Name("startRadioPlay")
    static let resetStream = Notification.Name("resetStream")
    static let dataHasBeenFetched = Notification.Name("dataHasBeenFetched")
    static let menuItemClicked = Notification.Name("menuItemClicked")
    static let searchButtonClicked = Notification.Name("searchButtonClicked")
    static let notificationsViewClicked = Notification.Name("notificationsViewClicked")
    static let retryClicked = Notification.Name("retryClicked")
    static let notificationReceived = Notification.Name("notificationReceived")
    static let trackFailedToLoad = Notification.Name("songFailedToLoad")
    static let togglePlayerRepeat = Notification.Name("toggleRepeat")
    static let togglePlayerShuffle = Notification.Name("toggleShuffle")
    static let startPodcastPlay = Notification.Name("startPodcastPlay")
    static let artistFollowCount = Notification.Name("artistFollowCount")
    static let playlistFollowCount = Notification.Name("playlistFollowCount")
    static let playlistFollowCountModified = Notification.Name("playlistFollowCountModified")
    static let artistFollowCountModified = Notification.Name("artistFollowCountModified")
    static let themeChanged = Notification.Name("themeChanged")
    static let downloadRequested = Notification.Name("downloadRequested")
    static let trackDownloadProgress = Notification.Name("trackDownloadProgress")
}
