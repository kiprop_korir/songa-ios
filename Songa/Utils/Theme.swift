//
//  Theme.swift
//  Songa
//
//  Created by Collins Korir on 8/20/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import UIKit
import MarqueeLabel
import XLPagerTabStrip

enum Theme: Int {

    case darkTheme, lightTheme
    
    var primaryColor: UIColor {
        switch self {
        case .darkTheme:
            return .colorPrimaryDark
        case .lightTheme:
            return .colorPrimary
        }
    }
    
//    //Customizing the Navigation Bar
//    var barStyle: UIBarStyle {
//        switch self {
//        case .darkTheme:
//            return .default
//        case .lightTheme:
//            return .black
//        }
//    }
//
    
    var barTint: UIColor? {
        switch self {
        case .darkTheme:
            return .colorPrimary
        case .lightTheme:
            return .colorPrimaryDark
        }
    }
    var tabBarUnselectedItemTint: UIColor? {
        switch self {
        case .darkTheme:
            return .gray
        case .lightTheme:
            return .colorPrimaryDark
        }
    }
  
//    var navigationBackgroundImage: UIImage? {
//        return self == .darkTheme ? UIImage(named: "navBackground") : nil
//    }
//    
//    var tabBarBackgroundImage: UIImage? {
//        return self == .darkTheme ? UIImage(named: "tabBarBackground") : nil
//    }
   
    var secondaryColor: UIColor {
        switch self {
        case .darkTheme:
            return .colorSecondaryDark
        case .lightTheme:
            return .colorSecondary
        }
    }
   
    var textColor: UIColor? {
        switch self {
        case .darkTheme:
            return .textColorPrimaryDark
        case .lightTheme:
            return .textColorPrimary
        }
    }
    
    var textColorDisabled: UIColor? {
        switch self {
        case .darkTheme:
            return .textColorDisabledDark
        case .lightTheme:
            return .textColorDisabled
        }
    }
    var loadingIndicatorColor: UIColor {
        switch self {
        case .darkTheme:
            return .white
        case .lightTheme:
            return .colorAccent
        }
    }
    
    var textColorSecondary: UIColor? {
        switch self {
        case .darkTheme:
            return .textColorSecondaryDark
        case .lightTheme:
            return .textColorSecondary
        }
    }
    
    var colorCardBackground: UIColor? {
        switch self {
        case .darkTheme:
            return .colorCardBackground
        case .lightTheme:
            return .white
        }
    }
    
    var statusBarStyle: UIStatusBarStyle? {
        switch self {
        case .darkTheme:
            return .lightContent
        case .lightTheme:
            return .default
        }
    }
    

 
}

// Enum declaration
let selectedThemeKey = "selectedTheme"

// This will let you use a theme in the app.
class ThemeManager {
    
    // ThemeManager
    static func currentTheme() -> Theme {
        if let storedTheme = Defaults.getSelectedTheme() {
            return Theme(rawValue: storedTheme)!
        } else {
            return .darkTheme
        }
    }
    
    static func applyTheme(theme: Theme) {
        // First persist the selected theme using NSUserDefaults.
        
        Defaults.setTheme(theme: theme.rawValue)
        
        print("selected theme \(theme.primaryColor )")
          
        
        // You get your current (selected) theme and apply the main color to the tintColor property of your application’s window.
        let sharedApplication = UIApplication.shared
        sharedApplication.delegate?.window??.backgroundColor = theme.primaryColor
        
        UINavigationBar.appearance().tintColor = theme.barTint
        UINavigationBar.appearance().barTintColor  = theme.primaryColor
        UIApplication.shared.statusBarStyle = theme.statusBarStyle!
        
        //SearchTextField.appearance().backgroundColor = theme.secondaryColor
        //SearchTextField.appearance().colo
        
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: ThemeManager.currentTheme().textColor!]
        
        UITabBar.appearance().barTintColor = theme.primaryColor
        UITabBar.appearance().tintColor = UIColor.colorAccent
        
        
        UITabBar.appearance().unselectedItemTintColor = theme.tabBarUnselectedItemTint
        UICollectionView.appearance().backgroundColor = theme.secondaryColor
        UICollectionViewCell.appearance().backgroundColor = theme.colorCardBackground
        
        UITableView.appearance().backgroundColor = theme.secondaryColor
        UITableViewCell.appearance().backgroundColor = theme.secondaryColor
        
        UIActivityIndicatorView.appearance().color = theme.loadingIndicatorColor
        
        UIImageView.appearance().tintColor = theme.textColor
        
        UILabel.appearance().textColor = theme.textColor
        MarqueeLabel.appearance().textColor = theme.textColor
        
        BackgroundView.appearance().backgroundColor = theme.secondaryColor
        
        
    }
}
