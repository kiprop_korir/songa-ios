//
//  Error.swift
//  Songa
//
//  Created by Collins Korir on 8/20/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation


class  SongaError {
    
    var description:String = ""
    var errorCode:String = ""
    
    init(description: String) {
        self.description = description;
    }
}
