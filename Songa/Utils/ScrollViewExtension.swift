//
//  ScrollViewExtension.swift
//  Songa
//
//  Created by Collins Korir on 8/8/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import UIKit

extension UIScrollView {
    func updateContentViewSize() {
        var newHeight: CGFloat = 0
        for view in subviews {
            let ref = view.frame.origin.y + view.frame.height
            if ref > newHeight {
                newHeight = ref
            }
        }
        let oldSize = contentSize
        let newSize = CGSize(width: oldSize.width, height: newHeight + 1000)
        contentSize = newSize
    }
}
