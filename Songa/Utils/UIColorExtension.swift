import Foundation
import UIKit
extension UIColor {
    
    // Setup custom colours we can use throughout the app using hex values
    // 0x indicates its the start of a hex number, the values after this are the hex value
    static let colorAccent = UIColor(named: "colorAccent")!
    
    
    
//
//    static let colorSecondary = UIColor().colorFromHexString("eef1f4")
//    static let colorSecondaryDark = UIColor().colorFromHexString("151728")
//    static let colorPrimary = UIColor().colorFromHexString("ffffff")
//    static let colorPrimaryDark = UIColor().colorFromHexString("1c1e32")
//    static let textColorPrimary = UIColor().colorFromHexString("0a1849")
//    static let textColorPrimaryDark = UIColor().colorFromHexString("FFFFFF")
//    static let textColorSecondary = UIColor().colorFromHexString("52576d")
//    static let textColorSecondaryDisabled = UIColor().colorFromHexString("63677b")
//    static let textColorDisabled = UIColor().colorFromHexString("b6b6b6")
//    static let textColorHeader = UIColor().colorFromHexString("ed215d")
//    static let fadedRed = UIColor().colorFromHexString("70ed215d")
  
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
    
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
        
    }
    
    func colorFromHexString (_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
