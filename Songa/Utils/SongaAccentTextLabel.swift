//
//  SongaAccentTextLabel.swift
//  Songa
//
//  Created by Kiprop Korir on 27/10/2019.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
class SongaAccentTextLabel:UILabel {
    override func layoutSubviews() {
           super.layoutSubviews()
        self.textColor = .colorAccent
    }
}
