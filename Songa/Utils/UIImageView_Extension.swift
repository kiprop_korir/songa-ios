//
//  UIImageView_Extension.swift
//  Songa
//
//  Created by Collins Korir on 6/13/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit



extension UIImageView{
    func blurImage()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        self.addSubview(blurEffectView)
    }
    

        func addGradientLayer(frame: CGRect, colors:[UIColor]){
            //remove current sub layers
            self.layer.sublayers = nil
            let gradient = CAGradientLayer()
            gradient.frame = frame
            gradient.colors = colors.map{$0.cgColor}
            self.layer.addSublayer(gradient)
        }

}

