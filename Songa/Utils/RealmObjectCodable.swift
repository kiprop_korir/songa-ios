//
//  RealmObjectCodable.swift
//  Songa
//
//  Created by Collins Korir on 2/19/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
//
//  RealmSwift+Codable.swift
//
//  Created by Anson Yao on 7/25/18.
//
//Adding this file can make your classes inherited from Realm Object comfirm to Codable easily
//Inspired by @mishagray https://gist.github.com/mishagray/3ee82a3a82f357bfbf8ff3b3d9eca5cd



extension RealmOptional : Encodable where Value: Encodable  {
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        if let v = self.value {
            try v.encode(to: encoder)
        } else {
            try container.encodeNil()
        }
    }
}

extension RealmOptional : Decodable where Value: Decodable {
    public convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            self.value = try Value(from: decoder)
        }
    }
}
extension List : Decodable where Element : Decodable {
    public convenience init(from decoder: Decoder) throws {
        self.init()
        var container = try decoder.unkeyedContainer()
        while !container.isAtEnd {
            let element = try container.decode(Element.self)
            self.append(element)
        }
    }
}

extension List : Encodable where Element : Encodable {
    public func encode(to encoder: Encoder) throws {
        var container = encoder.unkeyedContainer()
        for element in self {
            try element.encode(to: container.superEncoder())
        }
    }
}
