//
//  BackgroundView.swift
//  Songa
//
//  Created by Collins Korir on 9/10/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import UIKit
class BackgroundView: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Set your background color
        self.backgroundColor = ThemeManager.currentTheme().secondaryColor
        
//        // Or, if your background is an image
//        self.layer.contents = UIImage(named: "background")!.cgImage
    }
}
