//
//  NotificationBadge.swift
//  Songa
//
//  Created by Collins Korir on 8/15/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import UIKit

class NotificationBadge : UILabel {
    
    override init(frame: CGRect){
        super.init(frame: frame)
        
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.red
        self.font = UIFont.systemFont(ofSize: 12)
        self.textColor =  UIColor.white
        self.textAlignment = .center
        
}
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
}
}
