//
//  CollectionViewExtension.swift
//  Songa
//
//  Created by Collins Korir on 6/17/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

import UIKit
extension UICollectionView{
    
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = ThemeManager.currentTheme().textColor
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        //messageLabel.font = UIFont(name: "WorkSans", size: 15)
        messageLabel.sizeToFit()
        
        self.backgroundView = messageLabel;
        //self.indicatorStyle = .none;
    }
    
}
