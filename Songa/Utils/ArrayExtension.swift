//
//  ArrayExtension.swift
//  Songa
//
//  Created by Collins Korir on 8/17/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation

extension Collection {
    
    // Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
