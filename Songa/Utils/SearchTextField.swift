//
//  SearchTextField.swift
//  Songa
//
//  Created by Collins Korir on 9/18/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import UIKit

class SearchTextField: UITextField {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        

        
        for view in subviews {
            if let button = view as? UIButton {
                button.setImage(button.image(for: .normal)?.withRenderingMode(.alwaysTemplate), for: .normal)
                button.tintColor = .white
            
            }
        }
    }
}

extension SearchTextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    func setLeftImage(imageName:String) {
        
         let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 32.0, height: 30.0))

        let imageView = UIImageView(frame: CGRect(x: 8, y: 7.5, width: 16, height: 16))
        imageView.image = UIImage(named: imageName)
        imageView.tintColor = ThemeManager.currentTheme().textColorSecondary
        leftView.addSubview(imageView)
        self.leftView = leftView;
        self.leftViewMode = .always
    }
}
