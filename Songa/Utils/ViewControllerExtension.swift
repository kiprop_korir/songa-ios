//
//  ViewControllerExtension.swift
//  Songa
//
//  Created by Collins Korir on 9/26/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import MarqueeLabel

extension UIViewController {
    
    
    /// Call this once to dismiss open keyboards by tapping anywhere in the view controller
    func setupHideKeyboardOnTap() {
        self.view.addGestureRecognizer(self.endEditingRecognizer())
        self.navigationController?.navigationBar.addGestureRecognizer(self.endEditingRecognizer())
    }
    
    /// Dismisses the keyboard from self.view
    private func endEditingRecognizer() -> UIGestureRecognizer {
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
        tap.cancelsTouchesInView = false
        return tap
    }

    func  showLoadingView() {
        let customView = UIView(frame: CGRect(x: 0, y: -20    , width: self.navigationController!.view.frame.width , height: self.navigationController!.view.frame.height))
        customView.backgroundColor = ThemeManager.currentTheme().secondaryColor
        customView.tag = -888754
       // customView.addConstraint(MainViewController.botto)
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.startAnimating()
        activityIndicator.style = UIActivityIndicatorView.Style.white
        activityIndicator.center = CGPoint(x: (navigationController!.view.bounds.width)/2, y: (navigationController!.view.bounds.height)/2)
        customView.addSubview(activityIndicator)
        self.view.addSubview(customView)
    }
    
    func showErrorView(errorCode: Int ){
        // TODO: bug when retry is clicked, it has to be clicked twice
        let view = ErrorView(frame: self.view.bounds)
        view.tag = -888755
        view.tvError.text =  getErrorText(errorCode: errorCode)
        self.view.addSubview(view)
    }
    
    func getErrorText(errorCode: Int) -> String {
        
        switch errorCode {
            case 1:
                //network error
            return "A network error occurred. Please check your connection."
        case 400:
            //bad request
            return "Oops, something went wrong."
        case 403:
            //forbidden
            return "Oops, something went wrong."
        case 404:
            //not found
            return "Sorry, requested resource is unavailable."
        case 422:
            //unprocessable entity
            return "Malformed request"
        case 502:
            //bad gateway
            return "The service could not complete your request."
        case 503:
            //service unavailable
            return "The service is not available, please try again."
         
        default:
            return "An error occurred while you were trying to access the service."
        }
    }

    func  dismissErrorView() {
       self.navigationController?.view.viewWithTag(-888755)?.removeFromSuperview()
    }
    
    func  dismissLoadingView() {
        self.reloadInputViews()
        self.navigationController?.view.viewWithTag(-888754)?.removeFromSuperview()
    }
   
    func showOptionsController(track:Track) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
            
            let headerView = UIView()
            alertController.view.addSubview(headerView)
            headerView.translatesAutoresizingMaskIntoConstraints = false
            headerView.topAnchor.constraint(equalTo: alertController.view.topAnchor, constant: 8).isActive = true
            headerView.rightAnchor.constraint(equalTo: alertController.view.rightAnchor, constant: -10).isActive = true
            headerView.leftAnchor.constraint(equalTo: alertController.view.leftAnchor, constant: 10).isActive = true
            headerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
            
            let rect = CGRect(x: 8, y: 8, width: 80, height: 80)
            let trackCover = UIImageView(frame: rect)
            
            let url : String
            if ( track.artwork.count > 0){
                url = track.artwork[0].small
            }
            else
            {    url = "no_cover"
            }
            trackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            
            
            let lblTrackTitle = MarqueeLabel(frame: CGRect(x: 100, y: 22, width: alertController.view.bounds.size.width - 150 , height: 22))
            lblTrackTitle.font = UIFont.boldSystemFont(ofSize: 20)
            lblTrackTitle.speed = MarqueeLabel.SpeedLimit.duration(10)
            lblTrackTitle.animationDelay = 2
            lblTrackTitle.fadeLength = 10
            lblTrackTitle.textColor = .black
            
            
            let lblArtist = MarqueeLabel(frame: CGRect(x: 100 , y: 53, width: alertController.view.bounds.size.width - 150 , height: 22))
            lblArtist.font = UIFont.systemFont(ofSize: 20)
            lblArtist.speed = MarqueeLabel.SpeedLimit.duration(10)
            lblArtist.animationDelay = 2
            lblArtist.fadeLength = 10
            
            lblTrackTitle.text = track.title
            lblArtist.text = track.artistName
            lblArtist.textColor  = UIColor.darkGray
            headerView.addSubview(trackCover)
            headerView.addSubview(lblTrackTitle)
            headerView.addSubview(lblArtist)
            
            
            alertController.view.translatesAutoresizingMaskIntoConstraints = false
            alertController.view.heightAnchor.constraint(equalToConstant: 410).isActive = true
            
            let downloadAction = UIAlertAction(title: "Download", style: .default) { (action) in
                self.startDownload(track: track)
            }
            let shareAction = UIAlertAction(title: "Share", style: .default) { (action) in
                self.share(artist:track.artistName , link: "https//open.songamusic.com/track/\(track.id)")
            }
            
            let viewArtistAction = UIAlertAction(title: "View Artist", style: .default) { (action) in
                
                print("artist is")
                print(track.artistId)
                self.viewArtist(artistID: track.artistId)
            }
            let addToPlaylistAction = UIAlertAction(title: "Add to Playlist", style: .default) { (action) in
                self.addToPlaylist(track: track)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(downloadAction)
            alertController.addAction(shareAction)
            alertController.addAction(viewArtistAction)
            alertController.addAction(addToPlaylistAction)
      
            
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showShareController(id:String, title:String , artist: String, artwork:String ) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
            
            let headerView = UIView()
            alertController.view.addSubview(headerView)
            headerView.translatesAutoresizingMaskIntoConstraints = false
            headerView.topAnchor.constraint(equalTo: alertController.view.topAnchor, constant: 8).isActive = true
            headerView.rightAnchor.constraint(equalTo: alertController.view.rightAnchor, constant: -10).isActive = true
            headerView.leftAnchor.constraint(equalTo: alertController.view.leftAnchor, constant: 10).isActive = true
            headerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
            
            let rect = CGRect(x: 8, y: 8, width: 80, height: 80)
            let trackCover = UIImageView(frame: rect)
            
          
            trackCover.sd_setImage(with: URL(string: artwork), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            
            
            let lblTrackTitle = MarqueeLabel(frame: CGRect(x: 100, y: 22, width: alertController.view.bounds.size.width - 150 , height: 22))
            lblTrackTitle.font = UIFont.boldSystemFont(ofSize: 20)
            lblTrackTitle.speed = MarqueeLabel.SpeedLimit.duration(10)
            lblTrackTitle.animationDelay = 2
            lblTrackTitle.fadeLength = 10
            lblTrackTitle.textColor = .black
            
            
            let lblArtist = MarqueeLabel(frame: CGRect(x: 100 , y: 53, width: alertController.view.bounds.size.width - 150 , height: 22))
            lblArtist.font = UIFont.systemFont(ofSize: 20)
            lblArtist.textColor = ThemeManager.currentTheme().textColor
            lblArtist.speed = MarqueeLabel.SpeedLimit.duration(10)
            lblArtist.animationDelay = 2
            lblArtist.fadeLength = 10
            
            lblTrackTitle.text = title
            lblArtist.text = artist
            lblArtist.textColor  = UIColor.darkGray
            headerView.addSubview(trackCover)
            headerView.addSubview(lblTrackTitle)
            headerView.addSubview(lblArtist)
            
            
            alertController.view.translatesAutoresizingMaskIntoConstraints = false
            alertController.view.heightAnchor.constraint(equalToConstant: 250).isActive = true
            
            let shareAction = UIAlertAction(title: "Share", style: .default) { (action) in
                self.share(artist: artist , link: "https//open.songamusic.com/track/\(id)")
            }
            
           
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(shareAction)
      
            
            
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func share(artist:String,link:String){
        DispatchQueue.main.async {
            //Set the default sharing message.
            
            
            let message = "Listening to \(artist) on Songa. Click here to listen: \n"
            //Set the link to share.
            if let link = NSURL(string: link)
            {
                let objectsToShare = [message + String(describing: link),link] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
                self.present(activityVC, animated: true, completion: nil)
            }
        }
    }
    func viewArtist(artistID:String){
  
        performSegue(withIdentifier: "segueArtistPage", sender: artistID)
    }
    
  
    func startDownload(track:Track){
        NotificationCenter.default.post(name: Notification.Name.downloadRequested,
                                        object: nil,
                                        userInfo:["track": track ])
    }
    
    
    
    func addToPlaylist(track:Track){
        
             performSegue(withIdentifier: "segueAddToPlaylist", sender: track)
    }

    func setViewColors(){
              self.view.backgroundColor = ThemeManager.currentTheme().secondaryColor
        
        self.navigationController?.navigationBar.tintColor = ThemeManager.currentTheme().barTint

         self.navigationController?.navigationBar.barTintColor = ThemeManager.currentTheme().primaryColor

        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: ThemeManager.currentTheme().textColor!]
        
         self.navigationController?.navigationItem.rightBarButtonItem?.tintColor = ThemeManager.currentTheme().barTint
        
        self.navigationController?.navigationItem.leftBarButtonItem?.tintColor = ThemeManager.currentTheme().barTint
        
        
        print("setting up shit \(ThemeManager.currentTheme())" )
    }
    
//    func getHowLongAgo(dateString:String) -> String {
//        let dateFormatter = DateFormatter()
//               dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//               dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//            let date = dateFormatter.date(from: dateString)
//           return (date?.timeAgoDisplay())!
//       }
    
   

    func timeInterval(timeAgo:String) -> String
        {
            
             let dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let df = DateFormatter()

            df.dateFormat = dateFormat
            let dateWithTime = df.date(from: timeAgo)

            let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: dateWithTime!, to: Date())

            if let year = interval.year, year > 0 {
                return year == 1 ? "\(year)" + " " + "year ago" : "\(year)" + " " + "years ago"
            } else if let month = interval.month, month > 0 {
                return month == 1 ? "\(month)" + " " + "month ago" : "\(month)" + " " + "months ago"
            } else if let day = interval.day, day > 0 {
                return day == 1 ? "\(day)" + " " + "day ago" : "\(day)" + " " + "days ago"
            }else if let hour = interval.hour, hour > 0 {
                return hour == 1 ? "\(hour)" + " " + "hour ago" : "\(hour)" + " " + "hours ago"
            }else if let minute = interval.minute, minute > 0 {
                return minute == 1 ? "\(minute)" + " " + "minute ago" : "\(minute)" + " " + "minutes ago"
            }else if let second = interval.second, second > 0 {
                return second == 1 ? "\(second)" + " " + "second ago" : "\(second)" + " " + "seconds ago"
            } else {
                return "a moment ago"

            }
        }
   
}
