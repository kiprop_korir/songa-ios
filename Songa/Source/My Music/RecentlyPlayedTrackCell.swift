//
//  RecentlyPlayedTrackCell.swift
//  Songa
//
//  Created by Collins Korir on 8/28/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import UIKit

class RecentlyPlayedTrackCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var ivDownloaded: UIImageView!
    @IBOutlet weak var lblArtist: UILabel!
    @IBOutlet weak var lblIndex: UILabel!
}
