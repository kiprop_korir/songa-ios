//
//  MyPlaylistsViewController.swift
//  Songa
//
//  Created by Collins Korir on 8/30/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import SDWebImage
import RealmSwift

class MyPlaylistsViewController: BaseViewController {

   
    @IBOutlet weak var cvPlaylists: UICollectionView!
    var playlists : [UserPlaylist] = []
    var playlistID : String = ""
    let realm = try! Realm()
    var selectedPlaylist = 0
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.navigationItem.title = "My Playlists"
        
        cvPlaylists.delegate =  self
        cvPlaylists.dataSource =  self
        
        getPlaylistsFromRealm()
        
    }
    
    
    func getPlaylistsFromRealm(){
        
        playlists.removeAll()
        
        let downloadedPlaylistsResults: Results<UserPlaylist>? = realm.objects(UserPlaylist.self)
        
        for downloadedPlaylistResult in downloadedPlaylistsResults! {
            
            playlists.append(downloadedPlaylistResult)
            
        }
        
        //get only first 25
        //playlists =  Array (playlists.prefix(25))
        self.cvPlaylists.reloadData()
        
        if(playlists.count == 0){
            cvPlaylists.setEmptyMessage("You have no playlists on Songa")
        }
        
    }
    
    func getUserProfile(){
        
        APIClient.getUserProfile(userID: (Defaults.getUser()?.id)!){ result in
            
            switch result.result {
            case .success(let value):
                
                for playlist in value.playlist ?? []{
                    let userPlaylist = UserPlaylist()
                    userPlaylist.createdAt = playlist.updatedAt
                    userPlaylist.updatedAt = playlist.updatedAt
                    userPlaylist.creator = playlist.creator
                    userPlaylist.id = playlist.id
                    userPlaylist.name = playlist.name
                    userPlaylist.playlistDescription = playlist.playlistDescription
                    userPlaylist.shared = playlist.shared
                    userPlaylist.tracks = playlist.tracks
                    
                    self.playlists.append(userPlaylist)
                    
                    do {
                        let realm = try Realm()
                        try realm.write {
                            
                            realm.add(userPlaylist,update: true)
                            
                        }
                        
                    } catch let error as NSError {
                        
                        print(error)
                        
                    }
                }
                
//                for favourite in value.favourite ?? []{
//
//                    if(favourite)
//
//                    do {
//                        let realm = try Realm()
//                        try realm.write {
//
//                            realm.add(userPlaylist,update: true)
//
//                        }
//
//                    } catch let error as NSError {
//
//                        print(error)
//
//                    }
           
                
            
            case .failure:
                
                print("fail")
            }
         }
     }
    
    
    class cellTapGesture: UITapGestureRecognizer {
        var playlist = UserPlaylist()
        var currrentPlayingIndex = Int()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
        //go to playlist page
        performSegue(withIdentifier: "seguePlaylistPage", sender: sender.playlist)
         selectedPlaylist = sender.currrentPlayingIndex
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PlaylistViewController {
         vc.userPlaylist = playlists[selectedPlaylist]
    }
    }
}

extension MyPlaylistsViewController :UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    //MARL: - Delegate datasource methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playlists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //handle playlists cellForItemAt
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "playlistCell", for: indexPath) as! PlaylistCell
        
        let url : String
        
        if ( playlists.count > 0){
            
            url =  playlists[0].tracks[0].artwork[0].medium
        }
        else
        {
            //might be a blank array
            url = "no_cover"
        }
        
        //set the genre image using sd web image library asycnhronously
        
        cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = playlists[indexPath.row].name
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
        
        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        
        tappy.playlist =  playlists[indexPath.row]
        tappy.currrentPlayingIndex = indexPath.row
        
        cell.addGestureRecognizer(tappy)
        
        return cell
        
        
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size : CGSize? = nil
        
        if collectionView == self.cvPlaylists {
            
            let padding: CGFloat =  20
            let collectionViewWidth = collectionView.frame.size.width - padding
            
            size = CGSize(width: collectionViewWidth/2, height: collectionViewWidth/2 + 34)
        }
        return size!
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }


}
