//
//  MyMusicViewController.swift
//  Songa
//
//  Created by Collins Korir on 8/14/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import RealmSwift
import MarqueeLabel

class MyMusicViewController: UIViewController {

    @IBOutlet weak var viewNotifications: UIView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var viewFavourites: UIView!
    @IBOutlet weak var viewPlaylists: UIView!
    @IBOutlet weak var viewDownloads: UIView!
    @IBOutlet weak var tvRecentlyPlayedTracks: UITableView!
    @IBOutlet weak var ivPlaylists: UIImageView!
    @IBOutlet weak var ivDownloads: UIImageView!
    @IBOutlet weak var ivFavourites: UIImageView!
    @IBOutlet weak var lblRecentlyPlayedDes: UILabel!
    
    var loggedInUsers: [RAGUser] = []
    let realm = try! Realm()
    var tracks : [Track] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()

        tvRecentlyPlayedTracks.delegate = self
        tvRecentlyPlayedTracks.dataSource = self
        
        //remove separator form tableview
        tvRecentlyPlayedTracks.separatorStyle = UITableViewCell.SeparatorStyle.none
        tvRecentlyPlayedTracks.allowsSelection = false

        
        loadPage()
        
        getRecentlyPlayedTracks()
        
        setUpNavBar()

    }

    override func viewDidAppear(_ animated: Bool) {
        setUpNavBar()
        loadPage()
        tvRecentlyPlayedTracks.reloadData()
    }
  
    @objc func profileViewClicked(){
        performSegue(withIdentifier: "segueProfile", sender: self)
    }
    
    @objc func searchClicked(){
        performSegue(withIdentifier: "segueSearch", sender: self)
    }


    func loadPage(){
        
        //add tap gestures to the views
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.showFavourites(sender:)))
        viewFavourites.addGestureRecognizer(tap)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.showPlaylists(sender:)))
        viewPlaylists.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.showDownloads(sender:)))
        viewDownloads.addGestureRecognizer(tap3)
        
        ivPlaylists.tintColor = ThemeManager.currentTheme().textColor
        ivDownloads.tintColor = ThemeManager.currentTheme().textColor
        ivFavourites.tintColor = ThemeManager.currentTheme().textColor
        
    }
    
    @objc func showFavourites(sender : UITapGestureRecognizer) {
        
        self.performSegue(withIdentifier: "showFavourites", sender:sender)
        
    }
    @objc func showPlaylists(sender : UITapGestureRecognizer) {
        
        self.performSegue(withIdentifier: "showPlaylists", sender:sender)
        
    }
    @objc func showDownloads(sender : UITapGestureRecognizer) {
        
        
        self.performSegue(withIdentifier: "showDownloads", sender:sender)
        
        
    }
     func getRecentlyPlayedTracks(){
        
        tracks.removeAll()

        
        let recentlyPlayedResults: Results<RecentlyPlayed>? = realm.objects(RecentlyPlayed.self)
        
      
        for recentlyPlayed in recentlyPlayedResults! {
            
            tracks.append(recentlyPlayed.track!)
       
        
        }
        
        //get only first 25
        //tracks =  Array (tracks.prefix(25))
            self.tvRecentlyPlayedTracks.reloadData()
  
        if(tracks.count == 0){
            lblRecentlyPlayedDes.isHidden = true
        }
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var tracks = Array<Track>()
        var currrentPlayingIndex = Int()
    }
    
    class optionsTapGesture: UITapGestureRecognizer {
        var track = Track()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
        
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
        
    }
    
    @objc func optionsTapped(_ sender: optionsTapGesture) {
        showOptionsController(track: sender.track)
    }
    
    @objc func setUpNavBar(){
           let view = UIView(frame: CGRect(x: 0, y: 2, width: 40, height: 40))
               
               let logo = UIImageView(frame: CGRect(x: 0 , y: 10, width: 25, height: 25))
               logo.image = #imageLiteral(resourceName: "ic_profile")
               view.addSubview(logo)
               
               let count = RoundEdgesButton(frame: CGRect(x: 20, y: 0, width: 20, height: 20))
               count.backgroundColor = UIColor.red
               count.cornerRadius = 10
               
               let notificationsCount = realm.objects(SongaNotification.self).filter("isRead == false").count
               
               if(notificationsCount>0){
                   count.setTitle(String(notificationsCount), for: .normal)
                   count.titleLabel?.font = .systemFont(ofSize: 12)
                   view.addSubview(count)
               }
               
               let barButtonItem = UIBarButtonItem(customView: view)
               self.navigationItem.leftBarButtonItem = barButtonItem
               
               let tap = UITapGestureRecognizer(target: self, action: #selector(self.profileViewClicked))
               barButtonItem.customView?.addGestureRecognizer(tap)
               
               let searchButton = UIBarButtonItem(image: UIImage(imageLiteralResourceName: "ic_search").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(self.searchClicked))
        
               self.navigationItem.rightBarButtonItem = searchButton
               self.navigationItem.rightBarButtonItem?.tintColor = ThemeManager.currentTheme().barTint
               
           
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
      super.viewWillAppear(false)
      self.tvRecentlyPlayedTracks.reloadData()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setUpNavBar),
                                               name: Notification.Name.notificationReceived,
                                               object: nil)
    }
    
    func isTrackDownloaded (trackID:String) -> Bool {
        let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(trackID)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
        else {
            return false
        }
    }
    

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if( segue.identifier == "segueArtistPage") {
         
                let vc = segue.destination as! ArtistPageViewController
                //get the current selected cell
            
            if let artistID = sender as? String{
               
                vc.artistID = artistID
            }
        }
    }
}

extension MyMusicViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //handle albums cellForItemAt
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as! TrackTableCell
        
        let url : String
        
        if ( tracks[indexPath.row].artwork.count > 0){
            
            url = tracks[indexPath.row].artwork[0].small
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
        }
        //set the genre image using sd web image library asycnhronously
        
        cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = tracks[indexPath.row].title
        cell.lblArtist.text = tracks[indexPath.row].artistName
        cell.lblTrackIndex.text = String(indexPath.row + 1)
        cell.lblTrackIndex.textColor = ThemeManager.currentTheme().textColorSecondary
        
        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        
        tappy.tracks =  tracks
        tappy.currrentPlayingIndex =  indexPath.row
        
        //cell.btnPlay.addGestureRecognizer(tappy)
        cell.addGestureRecognizer(tappy)
        
        
        let tapOptions = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
        tapOptions.track = tracks[indexPath.row]
        
        cell.btnOptions.addGestureRecognizer(tapOptions)
        cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
         if(!isTrackDownloaded(trackID: tracks[indexPath.row].id)){
                 cell.ivDownloaded.isHidden = true
             }
         else {
            cell.ivDownloaded.isHidden = false
        }
        
        // Add a separator below each cell
        let horizontalGap = 15.0 as CGFloat
        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
        cell.addSubview(seperatorView)
        
        return cell
    }
      
}
