//
//  TracksViewController.swift
//  Songa
//
//  Created by Collins Korir on 8/30/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Foundation
import  Alamofire
import AlamofireObjectMapper
import SDWebImage
import RealmSwift

class FavouriteTracksViewController: BaseViewController ,IndicatorInfoProvider {
    
    
    
    let headers = APIurls.Headers
    
    let playlistTracksAPI = APIurls.Playlist
 
    @IBOutlet weak var tvTracks: UITableView!
    var tracks : [Track] = []
    let realm = try! Realm()
    
    var source : String = ""
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "TRACKS")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get instance of PlaylistViewController
        
        tvTracks.delegate =  self
        tvTracks.dataSource =  self
        
        //remove separator form tableview
        tvTracks.separatorStyle = UITableViewCell.SeparatorStyle.none
        tvTracks.allowsSelection = false
   
        getTracksFromRealm()
       
        
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var tracks = Array<Track>()
        var currrentPlayingIndex = Int()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
        
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": sender.tracks , "currentPlayingIndex": sender.currrentPlayingIndex])
        
    }
    class optionsTapGesture: UITapGestureRecognizer {
        var track = Track()
    }
    
    @objc func optionsTapped(_ sender: optionsTapGesture) {
        showOptionsController(track: sender.track)
    }
    
    func isTrackDownloaded (trackID:String) -> Bool {
        let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(trackID)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
        else {
            return false
        }
    }
    
    func getTracksFromRealm(){
    

    
        tracks.removeAll()
        
        
        let favouriteTracksResults: Results<FavouriteTrack>? = realm.objects(FavouriteTrack.self)
        
        
        for favouriteTracksResult in favouriteTracksResults! {
            
            tracks.append(favouriteTracksResult.track!)
            
            
        }
        
        //get only first 25
        //tracks =  Array (tracks.prefix(25))
        self.tvTracks.reloadData()
        
        if(tracks.count == 0){
            tvTracks.setEmptyMessage("Your favourited tracks will appear here")
        }


    
    }
    

}

extension FavouriteTracksViewController :UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //handle albums cellForItemAt
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as! TrackTableCell
        
        let url : String
        
        if ( tracks[indexPath.row].artwork.count > 0){
            
            url = tracks[indexPath.row].artwork[0].small
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        //set the genre image using sd web image library asycnhronously
        
        
        cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = tracks[indexPath.row].title
        cell.lblArtist.text = tracks[indexPath.row].artistName
        cell.lblTrackIndex.text = String(indexPath.row + 1)
        cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        
        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        
        tappy.tracks =  tracks
        tappy.currrentPlayingIndex =  indexPath.row
        
        //cell.btnPlay.addGestureRecognizer(tappy)
        cell.addGestureRecognizer(tappy)
        
        
        let tapOptions = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
        tapOptions.track = tracks[indexPath.row]
        
        cell.btnOptions.addGestureRecognizer(tapOptions)
        cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
        if(!isTrackDownloaded(trackID: tracks[indexPath.row].id)){
            cell.ivDownloaded.isHidden = true
        }
        
        // Add a separator below each cell
        let horizontalGap = 15.0 as CGFloat
        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
        cell.addSubview(seperatorView)
        
        return cell
    }
    
    
    
    
}
