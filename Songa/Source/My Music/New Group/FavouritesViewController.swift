//
//  FavouritesViewController.swift
//  Songa
//
//  Created by Collins Korir on 2/5/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

class FavouritesViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       self.navigationItem.title = "Favourites"
        // Do any additional setup after loading the view.
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
