//
//  AlbumViewController.swift
//  Songa
//
//  Created by Collins Korir on 8/30/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SDWebImage
import RealmSwift

class FavouriteAlbumsViewController: BaseViewController ,IndicatorInfoProvider {
    
    
    @IBOutlet weak var cvAlbums: UICollectionView!
    var albums : [Album] = []
    var albumID : String = ""
    let realm = try! Realm()
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "ALBUMS")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cvAlbums.delegate =  self
        cvAlbums.dataSource =  self
    
        getAlbumsFromRealm()
        
    }
    
    func getAlbumsFromRealm(){
        
        albums.removeAll()
        
        let favouriteAlbumsResults: Results<FavouriteAlbum>? = realm.objects(FavouriteAlbum.self)
        
        for favouriteAlbumResult in favouriteAlbumsResults! {
            
            albums.append(favouriteAlbumResult.album!)
            
            print(favouriteAlbumResult.album!.artwork[0].original)
            
        }
        
        //get only first 25
        //albums =  Array (albums.prefix(25))
        self.cvAlbums.reloadData()
        
        if(albums.count == 0){
            cvAlbums.setEmptyMessage("Your favourited albums will appear here")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vc = segue.destination as! AlbumPageViewController
        if let cell = sender as? UICollectionViewCell,
            let indexPath = self.cvAlbums.indexPath(for: cell) {
            vc.albumID = self.albums[indexPath.row].id
        }
    }
}


extension FavouriteAlbumsViewController :UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    //MARL: - Delegate datasource methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //handle albums cellForItemAt
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "albumCell", for: indexPath) as! AlbumCell
        
        let url : String
        
        if ( albums[indexPath.row].artwork.count > 0){
            
            url =  albums[indexPath.row].artwork[0].medium
        }
        else
        {
            //might be a blank array
            url = "no_cover"
        }
        
        //set the genre image using sd web image library asycnhronously
        
        cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = albums[indexPath.row].title
        
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
        
        return cell
        
        
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size : CGSize? = nil
        
        if collectionView == self.cvAlbums {
            
            let padding: CGFloat =  20
            let collectionViewWidth = collectionView.frame.size.width - padding
            
            size = CGSize(width: collectionViewWidth/2, height: 194)
            
        }
        return size!
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }
}
