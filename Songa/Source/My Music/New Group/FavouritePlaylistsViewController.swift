//
//  PlaylistViewController.swift
//  Songa
//
//  Created by Collins Korir on 8/30/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SDWebImage
import RealmSwift

class FavouritePlaylistsViewController: BaseViewController ,IndicatorInfoProvider {
    
    
    @IBOutlet weak var cvPlaylists: UICollectionView!
    var playlists : [Playlist] = []
    var playlistID : String = ""
    let realm = try! Realm()
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "PLAYLISTS")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cvPlaylists.delegate =  self
        cvPlaylists.dataSource =  self
        
        getPlaylistsFromRealm()
        
    }
    
    func getPlaylistsFromRealm(){
        
        playlists.removeAll()
        
        let favouritePlaylistsResults: Results<FavouritePlaylist>? = realm.objects(FavouritePlaylist.self)
        
        for favouritePlaylistResult in favouritePlaylistsResults! {
            
            playlists.append(favouritePlaylistResult.playlist!)
     
            print(favouritePlaylistResult.playlist!.artwork[0].original)
            
        }
        
        //get only first 25
        //playlists =  Array (playlists.prefix(25))
        self.cvPlaylists.reloadData()
        
        if(playlists.count == 0){
            cvPlaylists.setEmptyMessage("Your favourited playlists will appear here")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let vc = segue.destination as! PlaylistViewController
        if let cell = sender as? UICollectionViewCell,
            let indexPath = self.cvPlaylists.indexPath(for: cell) {
            vc.playlistID = self.playlists[indexPath.row].id
        }
        }
    }
    

extension FavouritePlaylistsViewController :UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    //MARL: - Delegate datasource methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playlists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //handle playlists cellForItemAt
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "playlistCell", for: indexPath) as! PlaylistCell
        
        let url : String
        
        if ( playlists[indexPath.row].artwork.count > 0){
            
            url =  playlists[indexPath.row].artwork[0].medium
        }
        else
        {
            //might be a blank array
            url = "no_cover"
        }
        
        //set the genre image using sd web image library asycnhronously
        
        cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = playlists[indexPath.row].name
        
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
        
        return cell
        
        
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size : CGSize? = nil
        
        if collectionView == self.cvPlaylists {
            
            let padding: CGFloat =  20
            let collectionViewWidth = collectionView.frame.size.width - padding
            
            size = CGSize(width: collectionViewWidth/2, height: 192)
            
        }
        return size!
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
  }
}
