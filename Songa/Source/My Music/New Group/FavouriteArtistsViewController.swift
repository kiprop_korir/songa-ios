//
//  ArtistViewController.swift
//  Songa
//
//  Created by Collins Korir on 8/30/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SDWebImage
import RealmSwift

class FavouriteArtistsViewController: BaseViewController ,IndicatorInfoProvider {
    
    
    @IBOutlet weak var cvArtists: UICollectionView!
    var artists : [Artist] = []
    var artistID : String = ""
    let realm = try! Realm()
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "ARTISTS")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cvArtists.delegate =  self
        cvArtists.dataSource =  self
        
        getArtistsFromRealm()
        
    }
    
    func getArtistsFromRealm(){
        
        artists.removeAll()
        
        let favouriteArtistsResults: Results<FavouriteArtist>? = realm.objects(FavouriteArtist.self)
        
        for favouriteArtistResult in favouriteArtistsResults! {
            
            artists.append(favouriteArtistResult.artist!)
            
             
        }
        
        //get only first 25
        artists =  Array (artists.prefix(25))
        //self.cvArtists.reloadData()
        
        if(artists.count == 0){
            cvArtists.setEmptyMessage("Your favourited artists will appear here")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! ArtistPageViewController
        vc.artistID = artistID
    }
}


extension FavouriteArtistsViewController :UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    //MARL: - Delegate datasource methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return artists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //handle artists cellForItemAt
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "artistCell", for: indexPath) as! ArtistCell
        
        let url : String
        
        if ( artists[indexPath.row].artwork.count > 0){
            
            url =  artists[indexPath.row].artwork[0].medium
        }
        else
        {
            //might be a blank array
            url = "no_cover"
        }
        
        //set the genre image using sd web image library asycnhronously
        
        cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblName.text = artists[indexPath.row].name
        
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
        
        return cell
        
        
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size : CGSize? = nil
        
        if collectionView == self.cvArtists {
            
            let padding: CGFloat =  20
            let collectionViewWidth = collectionView.frame.size.width - padding
            
            size = CGSize(width: collectionViewWidth/2, height: 192)
            
        }
        return size!
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }
}
