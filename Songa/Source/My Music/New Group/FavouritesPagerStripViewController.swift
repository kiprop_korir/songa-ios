//
//  FavouritesViewPagerController.swift
//  Songa
//
//  Created by Collins Korir on 8/30/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import XLPagerTabStrip
import RealmSwift

class FavouritesPagerStripViewController: ButtonBarPagerTabStripViewController {
 
    var favourites: [Favourites] = []
    var playlistID:String = ""
    let realm = try! Realm()

    override func viewDidLoad() {
   
        // change selected bar color
        //settings.style.buttonBarBackgroundColor = UIColor(named: "colorBackground" )
        settings.style.buttonBarItemBackgroundColor = ThemeManager.currentTheme().primaryColor
        settings.style.selectedBarBackgroundColor = UIColor.colorAccent
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.buttonBarBackgroundColor = ThemeManager.currentTheme().primaryColor
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = ThemeManager.currentTheme().textColor
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        changeCurrentIndexProgressive = {  (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = ThemeManager.currentTheme().textColor
            newCell?.label.textColor = UIColor.colorAccent
        }
        super.viewDidLoad()

    }
    
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = UIStoryboard(name: "MyMusic", bundle: nil).instantiateViewController(withIdentifier: "tracks")
        let child_2 = UIStoryboard(name: "MyMusic", bundle: nil).instantiateViewController(withIdentifier: "artists")
        let child_3 = UIStoryboard(name: "MyMusic", bundle: nil).instantiateViewController(withIdentifier: "albums")
        let child_4 = UIStoryboard(name: "MyMusic", bundle: nil).instantiateViewController(withIdentifier: "playlists")
        
        return [child_1, child_2, child_3 ,child_4]
    }
    
    ///IB Actions
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

