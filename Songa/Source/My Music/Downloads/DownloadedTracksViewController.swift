//
//  DownloadedTracksViewController.swift
//  Songa
//
//  Created by Collins Korir on 2/6/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SDWebImage
import RealmSwift


class DownloadedTracksViewController: BaseViewController ,IndicatorInfoProvider {
    
    
    @IBOutlet weak var tvTracks: UITableView!
    
    let headers = APIurls.Headers
    
    let playlistTracksAPI = APIurls.Playlist

    var tracks : [Track] = []
    let realm = try! Realm()
    
    var source : String = ""
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "TRACKS")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get instance of PlaylistViewController
        
        tvTracks.delegate = self
        tvTracks.dataSource = self
        
        //remove separator form tableview
        tvTracks.separatorStyle = UITableViewCell.SeparatorStyle.none
        tvTracks.allowsSelection = false
        
        getTracksFromRealm()
        
        
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var tracks = Array<Track>()
        var currrentPlayingIndex = Int()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
         NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": sender.tracks , "currentPlayingIndex": sender.currrentPlayingIndex])
        
    }
    
    class optionsTapGesture: UITapGestureRecognizer {
        var track = Track()
    }
    
    @objc func optionsTapped(_ sender: optionsTapGesture) {
        showOptionsController(track: sender.track)
    }
    
    
    func getTracksFromRealm(){
        
        tracks.removeAll()
        
        
        let downloadedTracksResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self)
        
        
        for downloadedTracksResult in downloadedTracksResults! {
            
            tracks.append(downloadedTracksResult.track!)
            
            
        }
        
        //get only first 25
        //tracks =  Array (tracks.prefix(25))
        self.tvTracks.reloadData()
      
        if(tracks.count == 0){
            tvTracks.setEmptyMessage("Your downloaded tracks will appear here")
        }
        

    }
    
    
    
    
}

extension DownloadedTracksViewController :UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //handle albums cellForItemAt
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as! TrackTableCell
        
        let url : String
        
        if ( tracks[indexPath.row].artwork.count > 0){
            
            url = tracks[indexPath.row].artwork[0].small
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        //set the genre image using sd web image library asycnhronously
        
        
        cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = tracks[indexPath.row].title
        cell.lblArtist.text = tracks[indexPath.row].artistName
        cell.lblTrackIndex.text = String(indexPath.row + 1)
        
        cell.lblTrackIndex.textColor = ThemeManager.currentTheme().textColorSecondary
        cell.lblArtist.textColor = ThemeManager.currentTheme().textColorSecondary
        cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        
        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        
        tappy.tracks =  tracks
        tappy.currrentPlayingIndex =  indexPath.row
        
        //cell.btnPlay.addGestureRecognizer(tappy)
        cell.addGestureRecognizer(tappy)
        
        
        let tapOptions = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
        tapOptions.track = tracks[indexPath.row]
        
        cell.btnOptions.addGestureRecognizer(tapOptions)
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        

        // Add a separator below each cell
        let horizontalGap = 15.0 as CGFloat
        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
        cell.addSubview(seperatorView)
        
        return cell
    }
    
    
    
    
}

