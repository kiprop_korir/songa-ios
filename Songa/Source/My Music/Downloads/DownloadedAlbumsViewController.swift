//
//  DownloadedAlbumsViewController.swift
//  Songa
//
//  Created by Collins Korir on 2/6/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SDWebImage
import RealmSwift

class DownloadedAlbumsViewController: BaseViewController ,IndicatorInfoProvider {
    
    @IBOutlet weak var cvAlbums: UICollectionView!
    var albums : [Album] = []
    var albumID : String = ""
    let realm = try! Realm()
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "ALBUMS")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cvAlbums.delegate =  self
        cvAlbums.dataSource =  self
        
        getAlbumsFromRealm()
        
    }
    
    func getAlbumsFromRealm(){
        
        albums.removeAll()
        
        let downloadedAlbumsResults: Results<DownloadedAlbum>? = realm.objects(DownloadedAlbum.self)
        
        for downloadedAlbumResult in downloadedAlbumsResults! {
            
            albums.append(downloadedAlbumResult.album!)
            
        }
        
        //get only first 25
        //albums =  Array (albums.prefix(25))
        self.cvAlbums.reloadData()
        
        if(albums.count == 0){
            cvAlbums.setEmptyMessage("Your downloaded albums will appear here")
        }
        
    }
    
    
    class cellTapGesture: UITapGestureRecognizer {
        var album = Album()
        var currrentPlayingIndex = Int()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
        //go to album page
    }
}

extension DownloadedAlbumsViewController :UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    //MARL: - Delegate datasource methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //handle albums cellForItemAt
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "albumCell", for: indexPath) as! AlbumCell
        
        let url : String
        
        if ( albums[indexPath.row].artwork.count > 0){
            
            url =  albums[indexPath.row].artwork[0].medium
        }
        else
        {
            //might be a blank array
            url = "no_cover"
        }
        
        //set the genre image using sd web image library asycnhronously
        
        cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = albums[indexPath.row].title
        
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
        
        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        
        tappy.album =  albums[indexPath.row]
        tappy.currrentPlayingIndex = indexPath.row
        
        cell.addGestureRecognizer(tappy)
        
        return cell
        
        
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size : CGSize? = nil
        
        if collectionView == self.cvAlbums {
            
            let padding: CGFloat =  20
            let collectionViewWidth = collectionView.frame.size.width - padding
            
            size = CGSize(width: collectionViewWidth/2, height: 180)
            
        }
        return size!
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }
}
