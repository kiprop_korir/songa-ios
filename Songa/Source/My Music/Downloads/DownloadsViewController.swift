//
//  DownloadsViewController.swift
//  Songa
//
//  Created by Collins Korir on 8/30/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import Foundation
import  Alamofire
import AlamofireObjectMapper
import SDWebImage
import RealmSwift

class DownloadsViewController: BaseViewController {
   
    override func viewDidLoad() {
               self.navigationItem.title = "Downloads"
    }
    ///IB Actions
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

