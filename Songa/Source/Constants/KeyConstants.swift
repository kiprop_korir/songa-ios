//
//  KeyConstants.swift
//  Songa
//
//  Created by Collins Korir on 7/24/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//


struct Key {

    static let DeviceType = "iOS"
    struct Beacon{
        static let ONEXUUID = "xxxx-xxxx-xxxx-xxxx"
    }

    struct UserDefaults {
        static let k_App_Running_FirstTime = "userRunningAppFirstTime"
    }

    struct Headers {
        static let Authorization = "Authorization"
        static let ContentType = "Content-Type"
    }
    struct Google{
        static let placesKey = "some key here"//for photos
        static let serverKey = "some key here"
    }

    struct ErrorMessage{
        static let listNotFound = "ERROR_LIST_NOT_FOUND"
        static let validationError = "ERROR_VALIDATION"
    }
}
