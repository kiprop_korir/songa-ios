//
//  Constants.swift
//  Songa
//
//  Created by Collins Korir on 7/24/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift

struct APIurls {

    private struct Domains {
        static let Live = "https://content.songamusic.com"
        static let Auth =  "https://auth.songamusic.com"
    
    }

    private  struct Routes {
        static let ApiV3     = "/api/v3"
        static let ApiV4     = "/api/v4"
    }

    private  static let Domain = Domains.Live
    private  static let BaseURL = Domain + Routes.ApiV3
    private  static let BaseURLAuth = Domains.Auth
    private  static let BaseURLV4 = Domain + Routes.ApiV4

    static var FacebookLogin: String {
        return BaseURL  + "/auth/facebook"
    }
    
    static var msisdnLogin: String {
            return "https://auth.songamusic.com/api/v1/msisdn/login"
     }
   
    static var Discover: String {
        return BaseURLV4  + "/discover"
    }
    
    static var Search: String {
            return BaseURLV4 + "/search"
    }
    
    static var authToken: String {
        return BaseURLAuth + "/oauth/token"
    }
    static var Genres: String {
        return BaseURL  + "/genres/"
    }

    static var Tracks: String {
        return BaseURL  + "/tracks/"
    }
    static var Playlist: String {
        return BaseURL  + "/playlists/"
    }
    static var Chart: String {
        return BaseURL  + "/charts/"
    }
    static var album: String {
        return BaseURL  + "/albums/"
    }
    static var Artists: String {
        return BaseURL  + "/artists/"
    }

    static var FMRadio: String {
        return BaseURL  + "/stations/country/ke"
    }

    static var showMoreAllTime: String {
        return BaseURL  + "/trending/tracks?per_page=25"
    }
    static var showMoreRecommended: String {
        return BaseURL  + "/recommendations/tracks?per_page=25"
    }
    static var showMoreTrending: String {
        return BaseURL  + "/popular/tracks?per_page=25"
    }
    static var showMoreArtists: String {
        return BaseURL  + "/artists?page=1"
    }
    static var showMoreCharts: String {
        return BaseURL  + "/charts?per_page=25"
    }
    static var showMoreCelebrityPlaylists: String {
        return BaseURL  + "/charts?per_page=25"
    }
    static var showMoreDevotions: String {
        return BaseURL  + "/charts?per_page=25"
    }
    static var showMorePlaylists: String {
        return BaseURL  + "/playlists?per_page=25"
    }
    static var showMoreReleases: String {
        return BaseURL  + "/releases/tracks?per_page=25"
    }
    static var EZDRM: String {
        return "https://fps.ezdrm.com/api/licenses/"
    }

    static var AuthRefresh: String {
        return "https://fps2.ezdrm.com/api/licenses/"
    }

    static var StreamingURLBase: String {
        return "http://iosstreams.songamusic.com/auth/media/"
    }

    static var StreamingURLSuffix: String {
        return ".ism/.m3u8"
    }
    
    static var feedbackURL: String {
        return ".ism/.m3u8"
    }
    
    //billing
    
    static var PaymentPlans: String {
        return "https://dev-billing.songamusic.com/api/v1/paymentplans"
    }
    
    static var PaymentResponse: String {
        return "https://dev-billing.songamusic.com/api/v1/subscriptions"
    }
    
    static var PaymentStatus: String {
        return "https://dev-billing.songamusic.com/api/v1/subscriptions/status"
    }
    
    static var SubscriptionDetails: String {
        return "https://dev-billing.songamusic.com/api/v1/subscriptions/status"
    }
    
    static var getSubscription: String {
        return "https://billing.songamusic.com/subscriptions/productId/56112786-21c1-4593-a9c7-e40723e22964/externalKey/"
    }
    static var createSubscription: String {
        return "https://billing.songamusic.com/subscriptions/productId/56112786-21c1-4593-a9c7-e40723e22964/"
    }
    
    static var getPlans: String {
        return "https://billing.songamusic.com/products/56112786-21c1-4593-a9c7-e40723e22964/plans"
    }
    
    static var refreshAccount: String {
        return "https://billing.songamusic.com/subscriptions/refresh/productId/56112786-21c1-4593-a9c7-e40723e22964/externalKey/"
    }
    
    
    static var getKillbillPaymentPluginName: String {
        //return "safaricom-sdp-payment-plugin"
        return "safaricom-cbs-payment-plugin"
    }
    
    static var Headers: Dictionary<String, String> {
       
        let accessToken = Defaults.getString(key: Defaults.accessTokenKey)
//     let accessToken = "eyJhbGciOiJIUzUxMiIsImtpZCI6ImRlNDFiOTdiNzRhY2M2OTY1OWQ0MDJhODVlMzkwZmQxMTU1YzBiZjViNjVhNzM1MjYzZTRkMmMxMzI4MDBiMDQifQ.eyJ0b2tlbiI6eyJyZXNvdXJjZV9vd25lcl9pZCI6bnVsbCwic2NvcGVzIjoiIiwiYXBwbGljYXRpb24iOiIjPERvb3JrZWVwZXI6OkFwcGxpY2F0aW9uOjB4MDA1NjBlNjgyOGVhNzA-IiwiZXhwaXJlc19pbiI6ODY0MDAsImNyZWF0ZWRfYXQiOiIyMDE5LTAyLTIwIDA5OjMwOjI5IFVUQyJ9LCJqdGkiOiI0OWFiY2EzYi1hZTc0LTRhMTQtOGU3ZS00Mjg0NzkzNDM4NjEifQ.k7USlrWovdEEAfA2cOGluLp1iUv2WmUyxU7ztqqGCzR9pn5BcW-6m-ER58c0Ig328pFEOsjXLmq3sQPxjDmGGA"
//
        var headers = [String: String]()

        headers["Content-Type"] = "application/json"

        //get Token from Realm
        headers["Authorization"] = "Bearer " + accessToken!

        return headers
        }
    }
    struct SongaInfo {
        static var privacyPolicy: String {
            return "https://open.songamusic.com/privacy"
        }
        static var termsAndConditions: String {
            return "https://open.songamusic.com/terms"
        }
}

struct Analytics {
    static var songaFeedback: String {
        return "https://analytics.songamusic.com/feedback"
    }
}


struct Auth {
    static var updateUser: String {
        return "https://auth.songamusic.com/api/v1/users/"
    }
}

struct ProfilePicture {
    static var facebook: String {
        return "http://graph.facebook.com/"
    }
    
}
        struct GoogleLogin {
            static var clientID: String {
                return "634584214811-q3mei98iaispdmh47i3h9s4st0i6sl7p.apps.googleusercontent.com"
            }
        
}
