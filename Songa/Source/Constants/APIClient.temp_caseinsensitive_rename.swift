//
//  ApiClient.swift
//  Songa
//
//  Created by Collins Korir on 2/15/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//


import RxSwift
import Alamofire
import ObjectMapper


class APIClient {
    
 
    static func getPlaylist(playlistId: String, completion:@escaping (Result<PlaylistResponse>)->Void) {
        Alamofire.request(APIRouter.getPlaylist(playlistId: playlistId)).responseObject() { (response: DataResponse<PlaylistResponse>) in
                completion(response.result)
        }
    }
}
