//
//  UserDefaults.swift
//  Songa
//
//  Created by Kiprop Korir on 04/01/2019.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation

struct Defaults {
    
    //user details/app settings
    static let (idKey, nameKey, emailKey,phoneKey,cityKey,countryKey,dobKey,genderKey , createdAtKey,loginStatusKey, nightModeKey,entitlementKey,plansKey,planSelectedKey,hasLoggedInKey , hasSelectedPlansKey) = ("id","name","email","phone_number","city","country","date_of_birth","gender","created_at","login_status","night_mode","entitlement","plans","plan_selected","has_logged_in","has_selected_plans")
 
    //session
       static let ( accessTokenKey, tokenTypeKey,refreshTokenKey) = ("access_token","token_type","refresh_token")
    
    //subscriptions
        static let (accountIdKey, billingFrequencyKey, externalKeyKey,planIdKey,planNameKey,productIdKey,productPlanTypeKey,productPriceKey,stateKey, paymentStatusKey,messageKey ,nextBillingDateKey) = ("accountId","billingFrequency","externalKey","planId","planName","productId","productPlanType","planPrice","state","paymentStatus","message","nextBillingDate")
    
    //player
    static let ( repeatKey, shuffleKey) = ("repeat","shuffle")

    static let userSessionKey = "com.save.usersession"
    static let userSubscriptionSessionKey = "com.save.subscription.usersession"
    
    static let selectedThemeKey = "selectedTheme"
    
    static let ACTIVE = "ACTIVE"
    static let BLOCKED = "BLOCKED"
    static let CANCELLED = "CANCELLED"
     static let PENDING = "PENDING"
    static let TRIAL = "TRIAL"
    static let STANDARD = "STANDARD"
    static let PREMIUM = "PREMIUM"
    static let AUTHORIZATION_REJECTED = "AUTHORIZATION_REJECTED"
    
    
    struct User {
         var id : String?
        var name: String?
         var email : String?
         var phone : String?
         var city : String?
         var country : String?
         var dob : String?
         var gender : String?
        var createdAt : String?
        
        init(_ json: [String: String]) {
            self.id = json[idKey]
             self.name = json[nameKey]
             self.email = json[emailKey]
             self.phone = json[phoneKey]
             self.city = json[cityKey]
             self.country = json[countryKey]
             self.dob = json[dobKey]
             self.gender = json[genderKey]
            self.createdAt = json[createdAtKey]

        }
        
        
    
}
    
    struct SongaUser: Codable {
        var id : String?
        var name: String?
        var email : String?
        var phone : String?
        var city : String?
        var country : String?
        var dob : String?
        var gender : String?
        var createdAt : String?
    }
    
    struct Subscription: Codable {
        
        var accountId : String?
        var billingFrequency: String?
        var externalKey : String?
        var planId : String?
        var planName : String?
        var productId : String?
        var productPlanType : String?
        var productPrice : String?
        var state : String?
        var paymentStatus : String?
        var message : String?
        var nextBillingDate : Int?
    }
    
    
    static func saveUser(id: String, name: String, email:String , phone:String, city:String,country:String, dob:String, gender:String,createdAt:String){
        
        let user = SongaUser(id: id, name: name, email: email, phone: phone, city: city, country: country, dob: dob, gender: gender, createdAt: createdAt)
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(user) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: userSessionKey)
        }
    }
    
    static func getUser () -> SongaUser?{
        let defaults = UserDefaults.standard
        var person: SongaUser? = nil
        if let user = defaults.object(forKey: userSessionKey) as? Data {
            let decoder = JSONDecoder()
            if let loadedPerson = try? decoder.decode(SongaUser.self, from: user) {
                person = loadedPerson
            }
        }
        return person
    }

    static func saveUserSubscription(accountId: String, billingFrequency: String, externalKey:String , planId:String, productId:String,productPlanType:String,productPrice:String, state:String, planName:String , paymentStatus:String,message:String ,nextBillingDate:Int){
        
        let subscription = Subscription(accountId: accountId, billingFrequency: billingFrequency, externalKey: externalKey, planId: planId,  planName: planName, productId: productId, productPlanType: productPlanType,productPrice:productPrice, state: state, paymentStatus:paymentStatus, message:message ,nextBillingDate:nextBillingDate)
        
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(subscription) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: userSubscriptionSessionKey)
        }
    }
    
    static func getUserSubscription () -> Subscription{
        let defaults = UserDefaults.standard
        var subscription: Subscription? = nil
        if let sub = defaults.object(forKey: userSubscriptionSessionKey) as? Data {
            let decoder = JSONDecoder()
            if let loadedSubscription = try? decoder.decode(Subscription.self, from: sub) {
                subscription = loadedSubscription
            }
        }
        return subscription!
    }

     static func isEligible() -> Bool {
        
    let subscription = getUserSubscription()
     
        if(subscriptionActive() || subscription.state == BLOCKED && subscription.paymentStatus == "INSUFFICIENT_FUNDS"){
       return true
        }
        else{
            return false
        }
    }
    
    static func getPaymentPlan () -> String{
        switch getUserSubscription().state {
        case CANCELLED:
            return "You do not have an active subscription. Choose a plan and listen to local, African and international music, on-demand and offline and so much more."
        case BLOCKED:
            if(isPaymentPending()){
            return "You are currently subscribed to %s at KES %s but your subscription is being processed…"
            }
            else {
                return "You are currently subscribed to %s at KES %s but your subscription is has not been paid."
            }
        default:
            if(getUserSubscription().paymentStatus == TRIAL){
                 return "Trial"
            }
            else {
                return "You are currently subscribed to %s at KES %s."
            }
        }
    }
    
    static func isPaymentPending () -> Bool{
        let subscription = getUserSubscription
        if (subscription().paymentStatus == PENDING){
            return true
        }
    else {
            return false
        }
    }
   
    static func isAuthorizationRejected () -> Bool{
        let subscription = getUserSubscription
        if (subscription().state == BLOCKED && subscription().paymentStatus == AUTHORIZATION_REJECTED){
            return true
        }
        else {
            return false
        }
    }
    
    
    static func getSubscriptionExpiryDate() -> String {

        switch getUserSubscription().state {
        case ACTIVE:
            if(getUserSubscription().nextBillingDate != nil){
                let billingDate = getBillingDate()
               
            return "Your subscription will be renewed on \(billingDate)"
            }
                else {
                   return  "Your subscription expired and has not been renewed."
                }
             case CANCELLED:
                return  "Your subscription expired and has not been renewed."
                
                case BLOCKED:
                return  "Your subscription expired and has not been renewed."
                
        default:
            return "Your subscription expired and has not been renewed."
        }
    }
    static func getBillingDate() -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MMM d, h:mm a"
        
        let date =  Date(timeIntervalSince1970: TimeInterval(Defaults.getUserSubscription().nextBillingDate!))
       
        return  dateFormatter.string(from:  date)
     
    }

    static func subscriptionActive () -> Bool{
        let subscription = getUserSubscription
        if (subscription().state == ACTIVE){
            return true
        }
    else {
            return false
        }
    }
    
    static func getBoolean(key:String) -> Bool? {
        return Bool?(UserDefaults.standard.bool(forKey: key))
    }
    static func putBoolean(key:String, value:Bool) {
      UserDefaults.standard.set(value, forKey: key)
    }
   
    static func getString(key:String) -> String? {
        let defaults = UserDefaults.standard
        return String?(defaults.string(forKey: key) ?? "")
    }
    
    static func putString(key:String, value:String) {
      let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
    
    }
    
    static var removeString = { (key: String) in
        UserDefaults.standard.removeObject(forKey: key)
    }
    static func removeObject(key:String) {
        UserDefaults.standard.removeObject(forKey: key)
    }
    
    static func synchronize() {
        UserDefaults.standard.synchronize()
    }
    static func clearUserData(){
        UserDefaults.standard.removeObject(forKey: userSessionKey)
}
    static func getSelectedTheme() -> Int? {
        return Int?(UserDefaults.standard.integer(forKey: selectedThemeKey))
    }
    
    static func setTheme(theme:Int)  {
        let defaults = UserDefaults.standard
        defaults.set(theme, forKey: selectedThemeKey)
    }
    static func clearAllData(){
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }

}
