//
//  FontConstants.swift
//  Songa
//
//  Created by Collins Korir on 7/24/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

//FontsConstants.swift
struct FontNames {

    static let LatoName = "Lato"
    struct Lato {
        static let LatoBold = "Lato-Bold"
        static let LatoMedium = "Lato-Medium"
        static let LatoRegular = "Lato-Regular"
        static let LatoExtraBold = "Lato-ExtraBold"
    }
}
