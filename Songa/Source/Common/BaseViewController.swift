//
//  BaseViewController.swift
//  Songa
//
//  Created by Kiprop Korir on 27/10/2019.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
     var tfSearchText: SearchTextField? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavBar()
}
    
    @objc func setUpNavBar(){
        
        let searchButton = UIBarButtonItem(image: UIImage(#imageLiteral(resourceName: "ic_search")).withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(self.searchClicked))
        
        self.navigationItem.rightBarButtonItem = searchButton
        self.navigationItem.rightBarButtonItem?.tintColor = ThemeManager.currentTheme().barTint
        
        
    }
    
    @objc func searchClicked(){
           let storyboard = UIStoryboard(name: "Search", bundle: nil)
            let searchVC = storyboard.instantiateInitialViewController()
        //let navController = UINavigationController(rootViewController: searchVC!)

        //self.navigationController?.present(navController, animated: true, completion: nil)
             self.present(searchVC!, animated: true)
        
//        let tf = SearchTextField(frame: CGRect(x: 40, y: 7, width: self.view.frame.width - 70, height: 30))
//        
//        tf.placeHolderColor = UIColor.white
//        tf.attributedPlaceholder =
//            NSAttributedString(string: "Search", attributes: [NSAttributedString.Key.foregroundColor: ThemeManager.currentTheme().textColor!])
//        tf.textColor = ThemeManager.currentTheme().textColor
//        let leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 16.0, height: 2.0))
//        tf.tintColor = UIColor.colorAccent
//        tf.leftView = leftView
//        tf.leftViewMode = .always
//        tf.backgroundColor = UIColor.colorSecondary
//        
//        tfSearchText = tf
//        
//        let barButtonItem = UIBarButtonItem(customView: tf)
//        self.navigationItem.rightBarButtonItem = barButtonItem
      }
}
