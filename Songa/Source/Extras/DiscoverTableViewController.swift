//
//  DiscoverTableViewController.swift
//  Songa
//
//  Created by Collins Korir on 6/12/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit

import RealmSwift

final class TaskList: Object {
    @objc dynamic var text = ""
    @objc dynamic var id = ""
    let items = List<Task>()

    override static func primaryKey() -> String? {
        return "id"
    }
}

final class Task: Object {
    @objc dynamic var text = ""
    @objc dynamic var completed = false
}


class DiscoverTableViewController: UITableViewController {


    var items = List<Task>()
let realm = try! Realm()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()


        items.append(Task(value: ["text":"Realm Object one"]))



        items.append(Task(value: ["text": "My First Task"]))
          items.append(Task(value: ["text": "My Second Realm"]))


    }

    func setupUI() {

        title = "My Tasks"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(add))

}


    override func tableView(_ tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let item = items[indexPath.row]
        cell.textLabel?.text = item.text
        cell.textLabel?.alpha = item.completed ? 0.5 : 1
        return cell
    }

    @objc func add() {

        let alertController = UIAlertController(title: "New Task", message: "Enter Task Name", preferredStyle: .alert)
        var alertTextField: UITextField!

        alertController.addTextField {
            textField in
            alertTextField = textField
            textField.placeholder = "Task Name"
        }

        alertController.addAction(UIAlertAction(title: "Add", style: .default) { _ in
            guard let text = alertTextField.text , !text.isEmpty else { return }
            self.items.append(Task(value: ["text": text]))
            self.tableView.reloadData()
        })
        present(alertController, animated: true, completion: nil)

    }
}
