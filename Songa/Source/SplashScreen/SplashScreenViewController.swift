//
//  SplashScreenViewController.swift
//  Songa
//
//  Created by Kiprop Korir on 05/01/2019.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

class SplashScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad() 
        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.peformNeccessaryAction(sender:)), userInfo: nil, repeats: false)
    }
    
    @objc func peformNeccessaryAction(sender : Timer){
        //depending on log in details,present relevant VCs to the user
        
        if(!isUserAvailable()){
               print("user not avaialble  ")
            instantiateOnboarding()
        }
        else {
 
        let hasLoggedIn = Defaults.getBoolean(key: Defaults.hasLoggedInKey) ?? false
        let hasSelectedPlans = Defaults.getBoolean(key: Defaults.hasSelectedPlansKey) ?? false
            
            if(hasLoggedIn  && hasSelectedPlans){
                //go to main home VC
                instantiateHome()
            }
            else {
    
            //take user to select plans
                instantiateOnboarding()
            }
        }
    }
    
    func isUserAvailable() -> Bool{

        if(Defaults.getUser()?.id != nil) {
         return true
        }
        else {
               return false
        }
    }
    
    func instantiateLogin(){
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "HomeView") as UIViewController
            self.view.window?.rootViewController = initialViewController
            self.view.window?.makeKeyAndVisible()
    }
    
    func instantiateOnboarding(){
        let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "onboarding") as UIViewController
        self.view.window?.rootViewController = initialViewController
        self.view.window?.makeKeyAndVisible()
    }
    
    func instantiateHome(){
        
        //set up app theme
                     
                     let theme = Defaults.getSelectedTheme()
                    
                     (theme == 1) ? ThemeManager.applyTheme(theme: .lightTheme) : ThemeManager.applyTheme(theme: .darkTheme)
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        self.view.window?.rootViewController = storyboard.instantiateInitialViewController()
        self.view.window?.makeKeyAndVisible()
        
       
              
    }

    }
