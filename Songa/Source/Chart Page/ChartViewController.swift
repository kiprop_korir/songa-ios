//
//  ChartViewController.swift
//  Songa
//
//  Created by Collins Korir on 8/16/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import Alamofire
import UIKit
import XLPagerTabStrip
import RealmSwift
import MarqueeLabel

class ChartViewController: BaseViewController {

    @IBOutlet weak var lblChartTitle: UILabel!
    @IBOutlet weak var lblUpdatedAt: MarqueeLabel!
    @IBOutlet weak var viewDownload: UIView!
    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var ivCoverBg: UIImageView!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var ivDownload: UIImageView!
    @IBOutlet weak var tvTracks: UITableView!
    @IBOutlet weak var lblDownload: UILabel!
    @IBOutlet weak var lblFollowersCount: UILabel!
    @IBOutlet weak var lblFollowerCount: UILabel!
    @IBOutlet weak var ivFollowers: UIImageView!
    
    let headers = APIurls.Headers
    var chart: ChartResponse?  = nil
    var chartID : String = ""
    var tracks : [Track] = []
    let chartTracksAPI = APIurls.Chart
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ivDownload.tintColor = .white
               lblFollowerCount.textColor = .white
               ivFollowers.tintColor = .white
               lblDownload.textColor = .white
        
        tvTracks.delegate = self
        tvTracks.dataSource =  self
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.downloadAll))
        viewDownload.addGestureRecognizer(tap)
        
        self.navigationItem.title = "Chart"
        loadChart()
    
        
        if(isChartFavourited(trackID: chartID)){
            btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite_filled"), for: .normal)
        }
        
        //remove separator form tableview
        tvTracks.separatorStyle = UITableViewCell.SeparatorStyle.none
        tvTracks.allowsSelection = false
    }
    
    func isChartFavourited(trackID:String) -> Bool {
        let queryResults: Results<FavouriteChart>? = realm.objects(FavouriteChart.self).filter("id == \"\(chartID)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
        else {
            return false
        }
    }
    
    func isTrackDownloaded (trackID:String) -> Bool {
        let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(trackID)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
        else {
            return false
        }
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var tracks = Array<Track>()
        var currrentPlayingIndex = Int()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
        
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
        
        
    }
    class optionsTapGesture: UITapGestureRecognizer {
        var track = Track()
    }
    @objc func optionsTapped(_ sender: optionsTapGesture) {
        showOptionsController(track: sender.track)
    }
    
    
    @objc func reloadCollectionViewData(_ notification: Notification)  {
        
        tracks = notification.userInfo?["tracks"] as! Array <Track>
        tvTracks.reloadData()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
            
            if let vc = segue.destination as? ArtistPageViewController
            {
                vc.artistID = sender as! String
            }
            else if let destinationNavigationController = segue.destination as? UINavigationController {
                       let targetController = destinationNavigationController.topViewController as! AddToPlaylistViewController
                targetController.track = sender as? Track
                   }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        FirebaseUtil.shared.getPlaylistFollowCount(playlistID: chartID)
        
        //add retry observer
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(retry),
                                               name: Notification.Name.retryClicked,
                                               object: nil)
         NotificationCenter.default.addObserver(self,
                                                    selector: #selector(showFollowCount),
                                                    name: Notification.Name.playlistFollowCount
                ,
                                                    object: nil)
            NotificationCenter.default.addObserver(self,
                                                           selector: #selector(showFollowCount),
                                                           name: Notification.Name.playlistFollowCountModified
                       ,
                                                           object: nil)
        }
    
    
    @objc func showFollowCount(_ notification: Notification){
     let followers = notification.userInfo?["count"] as! Int?
        var desc = ""
        if followers! > 0 {
            desc = "Followers"
        }
        else {
            desc =
            "Follower"
        }
        lblFollowersCount.text =  "\(String(describing: followers!) ) \(desc)"
    }
    @objc func retry(){
        self.dismissErrorView()
        loadChart()
    }
    
    
    func loadChart()  {
        
        self.showLoadingView()
        
        APIClient.getChart(chartId: chartID){ result in
      self.dismissLoadingView()
            switch result.result {
            case .success(let value):
                
            self.chart = value
                self.tracks =  value.tracks!
            
            self.lblChartTitle.textColor =  .red
            self.lblUpdatedAt.textColor = .white
            self.btnShare.imageView?.tintColor = .white
            self.ivDownload.tintColor = .white
            self.lblDownload.textColor = .white
                
                if let coverImageURL =  value.artwork?[safe: 0]?.original {
                    
                    self.ivCover.sd_setImage(with: URL(string: coverImageURL), placeholderImage: #imageLiteral(resourceName: "empty_album"))
                    self.ivCoverBg.sd_setImage(with: URL(string: coverImageURL), placeholderImage: #imageLiteral(resourceName: "empty_album"))
                    
                }
                
                self.setDate(dateUpdated: value.updatedAt!)
                self.lblChartTitle.text =  value.name!
                self.ivCoverBg.blurImage()
                
                self.tvTracks.reloadData()
                
                
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.loadChart()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
    }
    
    private func setDate(dateUpdated:String) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateStyle = DateFormatter.Style.long
        //dateFormatterGet.timeStyle = .medium
        let dateString = dateFormatterGet.string(from: dateFormatter.date(from: dateUpdated)!)
        
        
        self.lblUpdatedAt.text = "Updated \(dateString)"
    }
    
    @objc func downloadAll(){
        
        
    }
    
   
    
    ///IB Actions
    @IBAction func playAll(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": self.chart?.tracks as Any ,"currentPlayingIndex": 0])
    }
    @IBAction func mix(_ sender: Any) {
        //to do shuffle the indexes
        
        let randomIndex = Int.random(in: 0 ..< tracks.count - 1)
        
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": self.chart?.tracks as Any ,"currentPlayingIndex": randomIndex])
    }
    @IBAction func favourite(_ sender: Any) {
        
        let chartObject = Chart()
                     
                          
                      for artwork in chart!.artwork! {
                         chartObject.artwork.append(artwork)
                      }
                      
                      for track in chart!.tracks! {
                          chartObject.tracks.append(track)
                      }
                      chartObject.name = (chart?.name)!
                      chartObject.updatedAt = (chart?.updatedAt)!
                      chartObject.id = chart!.id!
               
               let favouritedChart: FavouriteChart = FavouriteChart()
                favouritedChart.id = chart!.id!
                favouritedChart.chart = chartObject
                
               
        if(!isChartFavourited(trackID: chartID)){
              
              do {
                   let realm = try Realm()
                   try realm.write {
                       realm.add(favouritedChart,update: .modified)
                       btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite_filled"), for: .normal)
                   }
               
               } catch let error as NSError {
                   print(error)
               }
                   
                   //subsribe user to topic on firebase
                   FirebaseUtil.shared.subscribeToTopic(topic: chart!.name!)
            FirebaseUtil.shared.followPlaylist(playlistName: chart!.name!, playlistID: chartID, toUnfollow: false)
               }else {
                   //unfavourite chart
                             
                             let queryResults: Results<FavouriteChart>? = realm.objects(FavouriteChart.self).filter("id == \"\(chartID)\"")
                           
                             do {
                                 let realm = try Realm()
                                 try realm.write {
                                     realm.delete((queryResults?[0])!)
                                     btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite"), for: .normal)
                                 }
                                 
                                 
                             } catch let error as NSError {
                                 
                                 print(error)
                                 
                             }
                   
                   //unsubsribe user to topic on firebase
                   FirebaseUtil.shared.unsubscribeToTopic(topic: chart!.name!)
                  FirebaseUtil.shared.followPlaylist(playlistName: chart!.name!, playlistID: chartID, toUnfollow: true)
    }
    }
    
    @IBAction func share(_ sender: Any) {
        self.share(artist:chart!.name! , link: "https//open.songamusic.com/chart/\(chart!.id  ?? "")")
    }
    
   
}

extension ChartViewController: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as! TrackTableCell
        let url : String
        
        if ( tracks[indexPath.row].artwork.count > 0){
            
            url =  tracks[indexPath.row].artwork[0].small
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = tracks[indexPath.row].title
        cell.lblArtist.text = tracks[indexPath.row].artistName
        cell.lblTrackIndex.text = String(indexPath.row + 1)
        
        cell.lblTrackIndex.textColor = ThemeManager.currentTheme().textColorSecondary
        cell.lblArtist.textColor = ThemeManager.currentTheme().textColorSecondary
        cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        
        // Add a separator below each cell
        let horizontalGap = 15.0 as CGFloat
        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
        cell.addSubview(seperatorView)
        
        
        
        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        
        let tapOptions = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
        tapOptions.track = tracks[indexPath.row]
        
        cell.btnOptions.addGestureRecognizer(tapOptions)
         cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
        if(isTrackDownloaded(trackID: tracks[indexPath.row].id)){
            cell.ivDownloaded.isHidden = false
        }
        
        
        
        tappy.tracks = tracks
        tappy.currrentPlayingIndex = indexPath.row
        cell.addGestureRecognizer(tappy)
        
        return cell
    }
}
