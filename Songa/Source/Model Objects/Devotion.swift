//
//  Devotion.swift
//  Songa
//
//  Created by Collins Korir on 9/12/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//


import Foundation
import RealmSwift
import ObjectMapper

class Devotion: Object, Mappable {
    
    @objc dynamic var id = ""
    @objc dynamic var title = ""
    @objc dynamic var album_sequence = 0
    @objc dynamic var track_duration = ""
    @objc dynamic var stream_url = ""
    @objc dynamic var upc_ean = ""
    @objc dynamic var isrc = ""
    @objc dynamic var release_date = ""
    @objc dynamic var status = 0
    @objc dynamic var album_id = ""
    @objc dynamic var created_at = ""
    @objc dynamic var updated_at = ""
    @objc dynamic var label_id = ""
    @objc dynamic var publisher_id = ""
    @objc dynamic var provider_id = ""
    @objc dynamic var commercial_model_type = ""
    var usage = List<String> ()
    @objc dynamic var take_down_date = ""
    @objc dynamic var street_title = ""
    @objc dynamic var artist_name = ""
    @objc dynamic var artist_id = ""
    var featuring = List<String> ()
    var artwork = List<Artwork> ()
    @objc dynamic var provider_name = ""
    @objc dynamic var premium = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        title <- map["title"]
        album_sequence <- map["album_sequence"]
        track_duration <- map["track_duration"]
        stream_url <- map["stream_url"]
        upc_ean <- map["upc_ean"]
        isrc <- map["isrc"]
        release_date <- map["release_date"]
        status <- map["status"]
        album_id <- map["album_id"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        label_id <- map["label_id"]
        publisher_id <- map["publisher_id"]
        provider_id <- map["provider_id"]
        commercial_model_type <- map["commercial_model_type"]
        usage <- map["usage"]
        take_down_date <- map["take_down_date"]
        street_title <- map["street_title"]
        artist_name <- map["artist_name"]
        artist_id <- map["artist_id"]
        artwork <- (map["images"],RealmListTransform<Artwork>())
        provider_name <- map["provider_name"]
        premium <- map["premium"]
    }
    
}




    



