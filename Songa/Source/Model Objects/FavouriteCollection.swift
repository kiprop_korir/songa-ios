//
//  Favourite.swift
//  Songa
//
//  Created by Collins Korir on 8/22/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//


import Foundation
import ObjectMapper

class FavouriteCollection:  Mappable {
    
    var userID : String?
    var tracks = Array<String>()
    var artists = Array<String>()
    var playlists = Array<String>()
    var stations = Array<String>()
    var albums = Array<String>()
    var creator : String?
    
    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        userID    <- map["user_id"]
        stations   <- map["station_ids"]
        artists   <- map["artist_ids"]
        tracks    <- map["track_ids"]
        albums    <- map["album_ids"]
        playlists    <- map["playlist_ids"]
        
    }
    
}
