//
//  FavouriteTrack.swift
//  Songa
//
//  Created by Collins Korir on 2/5/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift


class FavouriteTrack: Object{
    
    @objc dynamic var id = ""
    @objc dynamic var track : Track? = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
