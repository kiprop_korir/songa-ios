//
//  FavouritePodcastEpisode.swift
//  Songa
//
//  Created by Collins Korir on 9/30/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift


class FavouritePodcastEpisode: Object{
    
    @objc dynamic var id = ""
    @objc dynamic var episode : PodcastEpisode? = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
