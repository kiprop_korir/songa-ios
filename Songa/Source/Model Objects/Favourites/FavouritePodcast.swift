//
//  FavouritePodcast.swift
//  Songa
//
//  Created by Collins Korir on 9/24/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift


class FavouritePodcast: Object{
    
    @objc dynamic var id = ""
    @objc dynamic var podcast : Podcast? = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
