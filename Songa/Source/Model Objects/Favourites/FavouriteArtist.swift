//
//  FavouriteArtists.swift
//  Songa
//
//  Created by Collins Korir on 2/5/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift


class FavouriteArtist: Object{
    
    @objc dynamic var id = ""
    @objc dynamic var artist : Artist? = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
