//
//  FavouriteChart.swift
//  Songa
//
//  Created by Collins Korir on 4/2/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift


class FavouriteChart: Object{
    
    @objc dynamic var id = ""
    @objc dynamic var chart : Chart? = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
