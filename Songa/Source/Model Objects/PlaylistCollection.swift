//
//  Collection.swift
//  Songa
//
//  Created by Collins Korir on 8/21/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class PlaylistCollection:  Mappable {
    
    var name : String?
    var playlistDescription : String?
    var shared : Bool?
    var tracks = Array<String>()
    var creator : String?
    
  
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
         creator    <- map["track_ids"]
         playlistDescription   <- map["description"]
         shared   <- map["shared"]
         tracks    <- map["track_ids"]
         creator    <- map["creator"]
        
    }
    
}



