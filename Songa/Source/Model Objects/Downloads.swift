//
//  Download.swift
//  Songa
//
//  Created by Collins Korir on 8/27/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift

class Downloads: Object {
    
    @objc dynamic var userId = ""
    var tracks = List<Track>()
    var albums = List<Album>()
    var playlists = List<Station>()
  
    override static func primaryKey() -> String? {
        return "userId"
    }
}
