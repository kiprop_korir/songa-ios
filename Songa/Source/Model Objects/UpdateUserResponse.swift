//
//  UserUpdateResponse.swift
//
//
//  Created by Collins Korir on 9/26/18.
//

import Foundation
import RealmSwift
import ObjectMapper

class UpdateUserResponse: Object, Mappable {
 
   // @objc dynamic var user : User? = nil
    @objc dynamic var success = false
    @objc dynamic var message = ""

    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
     //   user <- map["user"]
        success <- map["success"]
        message <- map["message"]
        
    }
}
