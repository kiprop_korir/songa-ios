//
//  Featured.swift
//  Songa
//
//  Created by Collins Korir on 7/16/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

  class Heroe: Object, Mappable {

    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var type = ""
    @objc dynamic var tag = ""
    @objc dynamic var premium = 0
    @objc dynamic var status = 0
    var tracks = List<Track>()
    var artwork = List<Artwork> ()

    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }

        func mapping(map: Map) {
            id    <- map["id"]
            name <- map["name"]
            type <- map["featureable_type"]
            tag <- map["tag"]
            premium <- map["premium"]
            status <- map["status"]
            artwork <- (map["images"],RealmListTransform<Artwork>())
            tracks <- (map["tracks"],RealmListTransform<Track>())
        }

    override static func primaryKey() -> String? {
        return "id"
    }


}



