//
//  Featured.swift
//  Songa
//
//  Created by Collins Korir on 7/16/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import AVFoundation


class Track: Object, Mappable {


    @objc dynamic var albumId = ""
    @objc dynamic var albumSequence =  0
    @objc dynamic var artistId = ""
    @objc dynamic var artistName = ""
    @objc dynamic var createdAt = ""
    @objc dynamic var id = ""
    var artwork = List<Artwork>()
    @objc dynamic var premium =  0
    @objc dynamic var providerId = ""
    @objc dynamic var providerName = ""
    @objc dynamic var status =  0
    @objc dynamic var streamUrl = ""
    @objc dynamic var streetTitle = ""
    @objc dynamic var title = ""
    @objc dynamic var updatedAt = ""
    @objc dynamic var downloadTask: AVAssetDownloadTask?


    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }


    func mapping(map: Map)
    {
        albumId <- map["album_id"]
        albumSequence <- map["album_sequence"]
        artistId <- map["artist_id"]
        artistName <- map["artist_name"]
       createdAt <- map["created_at"]
        id <- map["id"]
        premium <- map["premium"]
        providerId <- map["provider_id"]
        providerName <- map["provider_name"]
       status <- map["status"]
        streamUrl <- map["stream_url"]
        streetTitle <- map["street_title"]
        title <- map["title"]
      updatedAt <- map["updated_at"]
   albumSequence <- map["album_sequence"]
        providerId <- map["provider_id"]
        artwork <- (map["images"],RealmListTransform<Artwork>())


    }

    override static func primaryKey() -> String? {
        return "id"
    }
}




