//
//  User.swift
//  Songa
//
//  Created by Collins Korir on 9/25/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class User:  Mappable , Codable {
    
    var phoneNumber : String?
    var id : String?
    var email : String?
    var createdAt : String?
    var updatedat : String?
    var name : String?
    var firstName : String?
    var surname : String?
    var username : String?
    var gender : String?
    var dob : String?
    var registrationAt : String?
    var active : String?
    var otherName : String?
    var phoneSerialNumber : String?
    var city : String?
    var country : String?
    var billingApproval : Int?
    var billingApprovalDate : String?
    var roles : [String]?
    var facebookUserId : String?
  

    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        phoneNumber <- map["phone_number"]
        id <- map["id"]
        email <- map["email"]
        createdAt <- map["created_at"]
        updatedat <- map["updated_at"]
        name <- map["name"]
        firstName <- map["first_name"]
        surname <- map["surname"]
        username <- map["username"]
        gender <- map["gender"]
        dob <- map["date_of_birth"]
        registrationAt <- map["registration_at"]
        active <- map["active"]
        otherName <- map["other_name"]
        phoneSerialNumber <- map["phone_serial_number"]
        city <- map["city"]
        country <- map["country"]
        billingApproval <- map["billing_approval"]
        billingApprovalDate <- map["billing_approval_date"]
        roles <- map["roles"]
        facebookUserId <- map["facebook_user_id"]

     
    }
    
}



