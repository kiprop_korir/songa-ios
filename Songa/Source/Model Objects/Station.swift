//
//  Featured.swift
//  Songa
//
//  Created by Collins Korir on 7/16/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

  class Station: Object, Mappable {

        @objc dynamic var id = ""
        @objc dynamic var affiliate = true
        @objc dynamic var name = ""
        @objc dynamic var  stationDescription = ""
        @objc dynamic var location = ""
         @objc dynamic var url = ""
        @objc dynamic var title = ""
        @objc dynamic var stream = ""
        var artwork = List<Artwork>()


    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }

        func mapping(map: Map) {
            id    <- map["id"]
            affiliate <- map["affiliate"]
            name <- map["name"]
            location <- map["location"]
            stationDescription <- map["description"]
            title <- map["name"]
            stream <- map["url"]
            artwork <- (map["images"] ,RealmListTransform<Artwork>())

        }

    override static func primaryKey() -> String? {
        return "id"
    }
}



