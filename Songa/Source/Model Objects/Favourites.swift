//
//  FavouritePlaylists.swift
//  Songa
//
//  Created by Collins Korir on 8/30/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift

class Favourites: Object {

    @objc dynamic var userId = ""
    var tracks = List<Track>()
    var artists = List<Album>()
    var albums = List<Album>()
    var playlists = List<Station>()
  
    override static func primaryKey() -> String? {
        return "userId"
    }
}
