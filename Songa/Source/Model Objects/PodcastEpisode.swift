//
//  PodcastEpisode.swift
//  Songa
//
//  Created by Collins Korir on 9/20/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//


import Foundation
import RealmSwift
import ObjectMapper

class PodcastEpisode:  Object, Mappable  {
    
    @objc dynamic  var audioLengthSec = 0
    @objc dynamic  var id = ""
    @objc dynamic  var audio = ""
   @objc dynamic   var title = ""
   @objc dynamic   var podcastDescription = ""
    @objc dynamic  var explicitContent = false
    @objc dynamic  var image = ""

    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        podcastDescription <- map["description"]
        audioLengthSec <- map["audio_length_sec"]
        title <- map["title"]
        audio <- map["audio"]
        explicitContent <- map["explicit_content"]
        image <- map["image"]
    
        
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
