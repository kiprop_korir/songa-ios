//
//  Subcription.swift
//  Songa
//
//  Created by Collins Korir on 1/23/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class Subscription : Mappable{
    var accountId : String?
    var billingFrequency: String?
    var externalKey : String?
    var planId : String?
    var planName : String?
    var productId : String?
    var productPlanType : String?
    var state : String?
    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        accountId <- map["accountIdKey"]
        billingFrequency <- map["billingFrequency"]
        externalKey <- map["externalKey"]
        planId <- map["planId"]
        productId <- map["productId"]
        productPlanType <- map["productPlanType"]
        state <- map["state"]
        planName <- map["planName"]
    }
}
