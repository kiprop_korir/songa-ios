//
//  PodcastList.swift
//  Songa
//
//  Created by Collins Korir on 9/20/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class PodcastList:  Object, Mappable  {
  
    var podcasts = List<Podcast>()
    var id : String?
    var podcastDescription : String?
    var title : String?
    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        podcastDescription <- map["description"]
        podcasts <- (map["podcasts"],RealmListTransform<Podcast>())
        title <- map["title"]
        
    }
    override static func primaryKey() -> String? {
        return "id"
    }
}
