//
//  Featured.swift
//  Songa
//
//  Created by Collins Korir on 7/16/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

  class Chart: Object, Mappable {

        @objc dynamic var id = ""
        @objc dynamic var name = ""
        @objc dynamic var chartDescription = ""
        @objc dynamic var shared = ""
        @objc dynamic var status = ""
        @objc dynamic var tracksCount = ""
        @objc dynamic var streamableCount = ""
        @objc dynamic var updatedAt = ""
       @objc dynamic var createdAt = ""
       @objc dynamic var creator = ""
        var artwork = List<Artwork>()
       var tracks = List<Track>()





    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }

        func mapping(map: Map) {

             id    <- map["id"]
             name <- map["name"]
             status <- map["status"]
             chartDescription <- map["description"]
             creator <- map["creator"]
            tracksCount <- map["tracks_count"]
            streamableCount <- map["streamable_count"]
             updatedAt <- map["updated_at"]
            createdAt <- map["created_at"]
             tracks <- map["tracks"]
             artwork <- (map["images"],RealmListTransform<Artwork>())


        }

    override static func primaryKey() -> String? {
        return "id"
    }

}



