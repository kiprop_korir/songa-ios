//
//  Featured.swift
//  Songa
//
//  Created by Collins Korir on 7/16/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

  class Video: Object, Mappable {

        @objc dynamic var id = ""
        @objc dynamic var title = ""
        @objc dynamic var stream = ""
        @objc dynamic var type = ""
      @objc dynamic var videoableId = ""
      @objc dynamic var artist = ""
      @objc dynamic var artistId = ""


    var artwork = List<Artwork>()





    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }

        func mapping(map: Map) {
            id    <- map["id"]
            title <- map["title"]
            stream <- map["stream_url"]
            type <- map["videoable_type"]
            videoableId <- map["videoable_id"]
            artist <- map["artist_name"]
            artistId <- map["artist_id"]
            videoableId <- map["videoable_id"]
             artwork <- (map["images"] ,RealmListTransform<Artwork>())


        }

    override static func primaryKey() -> String? {
        return "id"
    }

}



