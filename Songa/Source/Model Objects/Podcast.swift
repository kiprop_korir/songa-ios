//
//  Podcast.swift
//  Songa
//
//  Created by Collins Korir on 9/20/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Podcast:  Object, Mappable  {
    
    
    
    var episodes = List<PodcastEpisode>()
    @objc dynamic var id = ""
    @objc dynamic var publisher = ""
    @objc dynamic var totalEpisodes = 0
    @objc dynamic var title = ""
    @objc dynamic var podcastDescription = ""
    @objc dynamic var explicitContent = false
    @objc dynamic var image = ""
    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        podcastDescription <- map["description"]
        episodes <- (map["episodes"],RealmListTransform<PodcastEpisode>())
        title <- map["title"]
        publisher <- map["publisher"]
        totalEpisodes <- map["total_episodes"]
        explicitContent <- map["explicit_content"]
        image <- map["image"]
        
    }
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
