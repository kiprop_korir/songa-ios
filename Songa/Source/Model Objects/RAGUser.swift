//
//  RAGUser.swift
//  Songa
//
//  Created by Collins Korir on 9/25/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class RAGUser: Object, Mappable {
 
//    @objc dynamic var user : User? = nil
    @objc dynamic var accessToken = ""
    @objc dynamic var tokenType = ""
    @objc dynamic var refreshToken = ""
    @objc dynamic var expiryPeriod = 0

    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
       // user <- map["user"]
        accessToken <- map["access_token"]
        tokenType <- map["token_type"]
        refreshToken <- map["refresh_token"]
        expiryPeriod <- map["expires_in"]
    }
    
}
