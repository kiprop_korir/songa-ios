//
//  Featured.swift
//  Songa
//
//  Created by Collins Korir on 7/16/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

  class Playlist: Object, Mappable {

        @objc dynamic var id = ""
        @objc dynamic var name = ""
        @objc dynamic var creator = ""
        @objc dynamic var playlistDescription = ""
        @objc dynamic var shared = false
        @objc dynamic var curated = ""
        @objc dynamic var updatedAt = ""
        var artwork = List<Artwork>()
        var tracks = List<Track>()
        var stations = List<Station>()


    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
        func mapping(map: Map) {

             id    <- map["id"]
             name <- map["name"]
             creator <- map["creator"]
             playlistDescription <- map["description"]
             shared <- map["shared"]
             curated <- map["curated"]
             updatedAt <- map["updated_at"]
             artwork <- (map["images"],RealmListTransform<Artwork>())
             tracks <- (map["images"],RealmListTransform<Track>())
             stations <- (map["stations"],RealmListTransform<Station>())

        }

    override static func primaryKey() -> String? {
        return "id"
    }
}



