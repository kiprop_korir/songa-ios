//
//  Featured.swift
//  Songa
//
//  Created by Collins Korir on 7/16/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

  class VideoSeries: Object, Mappable {

        @objc dynamic var id = ""
        @objc dynamic var title = ""
        @objc dynamic var videoCount = ""
        @objc dynamic var artist = ""

    var artwork = List<Artwork>()
    var videos = List<Video>()



    //Impl. of Mappable protocol
    required convenience init?(map: Map) {

        self.init()

    }

        func mapping(map: Map) {
            id    <- map["id"]
            title <- map["title"]
            videoCount <- map["video_count"]
            artist <- map["artist_name"]
            artwork <- (map["images"],RealmListTransform<Artwork>())
            videos <- (map["videos"],RealmListTransform<Video>())
        }

    override static func primaryKey() -> String? {
        return "id"
    }
}



