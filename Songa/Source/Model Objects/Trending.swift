//
//  Featured.swift
//  Songa
//
//  Created by Collins Korir on 7/16/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

  class Trending: Object, Mappable {

    @objc dynamic var id = ""
    var artists = List<Artist>()
    var tracks = List<Track>()



    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }

        func mapping(map: Map) {
            id    <- map["id"]
            artists <- (map["artists"]    ,RealmListTransform<Artist>())
            tracks <- (map["tracks"],RealmListTransform<Track>())

        }

    override static func primaryKey() -> String? {
        return "id"
    }

}



