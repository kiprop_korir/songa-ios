//
//  DownloadingTrack.swift
//  Songa
//
//  Created by Collins Korir on 11/6/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift


class DownloadingTrackItem: Object{
    
    @objc dynamic var id = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
