//
//  DownloadedTrack.swift
//  Songa
//
//  Created by Collins Korir on 2/6/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift


class DownloadedTrack: Object{
    
    @objc dynamic var id = ""
    @objc dynamic var localStorageLocation : Data? = nil
    @objc dynamic var track : Track? = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
