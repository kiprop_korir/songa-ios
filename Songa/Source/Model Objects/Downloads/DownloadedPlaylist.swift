//
//  DownloadedPlaylist.swift
//  Songa
//
//  Created by Collins Korir on 2/6/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift


class DownloadedPlaylist: Object{
    
    @objc dynamic var id = ""
    @objc dynamic var playlist : Playlist? = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
