//
//  DownloadedAlbum.swift
//  Songa
//
//  Created by Collins Korir on 2/6/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift


class DownloadedAlbum: Object{
    
    @objc dynamic var id = ""
    @objc dynamic var album : Album? = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
