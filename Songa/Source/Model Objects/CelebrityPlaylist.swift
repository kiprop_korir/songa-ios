//
//  CelebrityPlaylist.swift
//  Songa
//
//  Created by Collins Korir on 9/12/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//


import Foundation
import RealmSwift
import ObjectMapper

class CelebrityPlaylist: Object, Mappable {
    
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var playlistDescription = ""
    @objc dynamic var shared = ""
    @objc dynamic var status = false
    @objc dynamic var createdAt = ""
    @objc dynamic var updatedAt = ""
    @objc dynamic var creator = ""
    @objc dynamic var curated = false
    @objc dynamic var streamableCount = 0
    @objc dynamic var tracksCount = 0
    var artwork = List<Artwork> ()
    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        name <- map["name"]
        playlistDescription <- map["description"]
        shared <- map["shared"]
        status <- map["status"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
        creator <- map["creator"]
        curated <- map["curated"]
        streamableCount <- map["streamable_count"]
        tracksCount <- map["tracks_count"]
        artwork <- (map["images"],RealmListTransform<Artwork>())

    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
}



