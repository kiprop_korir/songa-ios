//
//  Plan.swift
//  Songa
//
//  Created by Collins Korir on 1/28/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class Plan: Mappable{
    
    var billingFrequency : String?
    var createdAt : Int?
    var id : String?
    var planName : String?
    var planPrice : Int?
    var productId : String?
    var productPlanType : String?
    var sdpProductId : String?
    var sdpServiceId : String?
    var updatedAt : Int?
    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        billingFrequency <- map["billingFrequency"]
        createdAt <- map["createdAt"]
        id <- map["id"]
        planName <- map["planName"]
        planPrice <- map["planPrice"]
        productId <- map["productId"]
        productPlanType <- map["productPlanType"]
        sdpProductId <- map["sdpProductId"]
        sdpServiceId <- map["sdpServiceId"]
        updatedAt <- map["updatedAt"]
    }
}
