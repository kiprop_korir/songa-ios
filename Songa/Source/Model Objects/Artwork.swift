//
//  Featured.swift
//  Songa
//
//  Created by Collins Korir on 7/16/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper


class Artwork: Object, Mappable

{

    @objc dynamic var copyright = ""
    @objc dynamic var createdAt = ""
    @objc dynamic var fileContentType = ""
    @objc dynamic var fileFileName = ""
    @objc dynamic var fileFileSize =  0
    @objc dynamic var fileUpdatedAt = ""
    @objc dynamic var id = ""
    @objc dynamic var imageableId = ""
    @objc dynamic var imageableType = ""
    @objc dynamic var large = ""
    @objc dynamic var medium = ""
    @objc dynamic var original = ""
    @objc dynamic var small = ""
    @objc dynamic var status =  0
    @objc dynamic var updatedAt = ""


    required convenience init?(map: Map) {
        self.init()
    }


    func mapping(map: Map)
    {
        copyright <- map["copyright"]
        createdAt <- map["created_at"]
        fileContentType <- map["file_content_type"]
        fileFileName <- map["file_file_name"]
        fileFileSize <- map["file_file_size"]
        fileUpdatedAt <- map["file_updated_at"]
        id <- map["id"]
        imageableId <- map["imageable_id"]
        imageableType <- map["imageable_type"]
        large <- map["large"]
        medium <- map["medium"]
        original <- map["original"]
        small <- map["small"]
        status <- map["status"]
        updatedAt <- map["updated_at"]

    }


    override static func primaryKey() -> String? {
        return "id"
    }

}



