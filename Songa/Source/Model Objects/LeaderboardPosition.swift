//
//  LeaderboardPosition.swift
//  Songa
//
//  Created by Collins Korir on 10/4/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import ObjectMapper
class LeaderboardPosition: Mappable {
    
    var position: Int?
    var name: String?
    var plays: Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        plays <- map["Plays"]
        position <- map["position"]

    }
}
