//
//  PodcastArtist.swift
//  Songa
//
//  Created by Collins Korir on 9/21/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class PodcastArtist: Object, Mappable {
    
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var bio = ""
    var tracks = List<Track>()
    var artwork = List<Artwork>()
    var albums = List<Album>()
    
    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        name <- map["name"]
        bio <- map["bio"]
        artwork <- (map["images"],RealmListTransform<Artwork>())
        tracks <- (map["tracks"],RealmListTransform<Track>())
        albums <- (map["albums"],RealmListTransform<Album>())
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}



