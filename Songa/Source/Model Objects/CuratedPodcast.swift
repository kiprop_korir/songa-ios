//
//  CuratedPodcast.swift
//  Songa
//
//  Created by Collins Korir on 9/20/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class CuratedPodcast:  Object, Mappable  {
    
    var hasNext: Bool?
    var id : String?
    var curatedLists = List<PodcastList>()
    var pageNumber : Int?
 
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        pageNumber <- map["page_number"]
        curatedLists <- (map["curated_lists"],RealmListTransform<PodcastList>())
        hasNext <- map["has_next"]
        
    }
    override static func primaryKey() -> String? {
        return "id"
    }
}
