//
//  Voucher.swift
//  Songa
//
//  Created by Collins Korir on 9/25/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

//
//  PodcastArtist.swift
//  Songa
//
//  Created by Collins Korir on 9/21/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Voucher: Object, Mappable {
    

    @objc dynamic var code = ""
    @objc dynamic var updatedAt = ""
    @objc dynamic var codeShort = ""
    @objc dynamic var userID = ""
    @objc dynamic var limit = 0
    @objc dynamic var createdAt = ""
    @objc dynamic var id = ""
    @objc dynamic var campaignId = ""
    @objc dynamic var status = false
    
    
    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        code <- map["code"]
        updatedAt <- map["updated_at"]
        codeShort <- map["code_short"]
        userID <- map["user_id"]
        limit <- map["limit"]
        createdAt <- map["created_at"]
        campaignId <- map["campaign_id"]
        status <- map["status"]
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}



