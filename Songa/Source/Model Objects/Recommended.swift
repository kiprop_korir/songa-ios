//
//  Featured.swift
//  Songa
//
//  Created by Collins Korir on 7/16/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

  class Recommended: Object, Mappable {

        @objc dynamic var id = ""
       var tracks = List<Track>()
       var albums = List<Album>()




    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }

        func mapping(map: Map) {
            id    <- map["id"]

            albums <- (map["albums"],RealmListTransform<Album>())
            tracks <- (map["tracks"],RealmListTransform<Track>())

        }

    override static func primaryKey() -> String? {
        return "id"
    }

}



