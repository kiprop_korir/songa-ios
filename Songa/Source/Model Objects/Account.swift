//
//  Account.swift
//  Songa
//
//  Created by Collins Korir on 1/30/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class Account: Mappable{

    
    var name : String?
    var externalKey : String?
    var email : String?
    var phone : String?
    var notes : String?

    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        externalKey <- map["externalKey"]
        email <- map["email"]
        phone <- map["phone"]
        notes <- map["notes"]
    }
}
