////
////  Featured.swift
////  Songa
////
////  Created by Collins Korir on 7/16/18.
////  Copyright © 2018 Collins Korir. All rights reserved.
////
//
//import Foundation
//import RealmSwift
//import ObjectMapper
//
//  class Genre: Object, Mappable {
//
//        @objc dynamic var id = ""
//        @objc dynamic var name = ""
//        @objc dynamic var type = ""
//        @objc dynamic var tag = ""
//        @objc dynamic var premium = ""
//        var artists = List<Artist>()
//        var albums = List<Album>()
//       var playlists = List<Playlist>()
//    var tracks = List<Track>()
//    var artwork = List<Artwork>()
//    var series = List<VideoSeries>()
//      var subGenres = List<Genre>()
//
//
//
//
//    //Impl. of Mappable protocol
//    required convenience init?(map: Map) {
//        self.init()
//    }
//
//        func mapping(map: Map) {
//            id    <- map["id"]
//            name <- map["name"]
//            artists <- (map["albums"],RealmListTransform<Artist>())
//            playlists <- (map["playlists"],RealmListTransform<Playlist>())
//            tracks <- (map["tracks"],RealmListTransform<Track>())
//            artwork <- (map["images"],RealmListTransform<Artwork>())
//             series <- (map["series"],RealmListTransform<VideoSeries>())
//            subGenres <- (map["sub_genres"],RealmListTransform<Genre>())
//
//        }
//
//    override static func primaryKey() -> String? {
//        return "id"
//    }
//
//}
//
//
//
