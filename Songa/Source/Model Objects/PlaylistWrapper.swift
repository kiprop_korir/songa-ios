//
//  PlaylistWrapper.swift
//  Songa
//
//  Created by Collins Korir on 8/21/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class PlaylistWrapper:  Mappable {
    
    var playlist : PlaylistCollection?
  
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        playlist   <- map["playlist"]
    }
    
}
