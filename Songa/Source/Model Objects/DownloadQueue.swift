//
//  DownloadQueue.swift
//  Songa
//
//  Created by Collins Korir on 11/29/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation

import Foundation
import RealmSwift

class DownloadQueue: Object {
    
    @objc dynamic var processId = 0
    @objc dynamic var id = ""
    var track = Track()
   
    override static func primaryKey() -> String? {
        return "id"
    }
}
