//
//  UserPlaylists.swift
//  Songa
//
//  Created by Collins Korir on 8/22/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift


class UserPlaylist: Object {
  
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var creator = ""
    @objc dynamic var playlistDescription = ""
    @objc dynamic var shared = false
    @objc dynamic var curated = false
    @objc dynamic var updatedAt = ""
    @objc dynamic var createdAt = ""
    var tracks = List<Track>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}


