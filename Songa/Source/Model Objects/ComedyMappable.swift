//
//  Devotion.swift
//  Songa
//
//  Created by Collins Korir on 9/12/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//


import Foundation
import ObjectMapper
import RealmSwift

class ComedyMappable:  Mappable {

    var artists: [Comedy]?
   
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {

        artists <- map["artists"]
    }
    
}

