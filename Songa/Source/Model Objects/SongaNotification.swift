//
//  Notification.swift
//  Songa
//
//  Created by Collins Korir on 3/19/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift

class SongaNotification: Object {
    
    @objc dynamic var messageId = ""
    @objc dynamic var title = ""
    @objc dynamic var message = ""
    @objc dynamic var itemType = ""
    @objc dynamic var imageUrl = ""
    @objc dynamic var itemId = ""
    @objc dynamic var timeStamp = ""
    @objc dynamic var isRead = false
    
    
    override static func primaryKey() -> String? {
        return "itemId"
    }
}
