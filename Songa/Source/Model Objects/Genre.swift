//
//  Featured.swift
//  Songa
//
//  Created by Collins Korir on 7/16/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

  class Genre: Object, Mappable {

        @objc dynamic var id = ""
        @objc dynamic var name = ""
        @objc dynamic var status = ""
        @objc dynamic var tier = ""
        @objc dynamic var parentID = ""
        var artwork = List<Artwork>()


    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }

        func mapping(map: Map) {
            id    <- map["id"]
            name <- map["name"]
            status <- map["status"]
            tier <- map["tier"]
            parentID <- map["parent_id"]
          artwork <- (map["images"],RealmListTransform<Artwork>())


        }

    override static func primaryKey() -> String? {
        return "id"
    }

}



