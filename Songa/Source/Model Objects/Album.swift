//
//  Featured.swift
//  Songa
//
//  Created by Collins Korir on 7/16/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

  class Album: Object, Mappable {

    @objc dynamic var id = ""
    @objc dynamic var artistId = ""
    @objc dynamic var title = ""
    @objc dynamic var artist = ""
    @objc dynamic var artistBio = ""
    @objc dynamic var publisherName = ""
    @objc dynamic var labelName = ""

    var tracks = List<Track>()
    var artwork = List<Artwork>()

    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }

        func mapping(map: Map) {
            id    <- map["id"]
            artistId <- map["artist_id"]
            title <- map["title"]
            artist <- map["artist_name"]
            artwork <- (map["images"],RealmListTransform<Artwork>())
            tracks <- (map["tracks"],RealmListTransform<Track>())
            publisherName <- map["provider_name"]
            labelName <- map["label_name"]

        }

    override static func primaryKey() -> String? {
        return "id"
    }

}



