//
//  RecentlyPlayed.swift
//  Songa
//
//  Created by Collins Korir on 8/8/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import Foundation
import RealmSwift


class RecentlyPlayed: Object{

    @objc dynamic var id = ""
    @objc dynamic var track : Track? = nil

    override static func primaryKey() -> String? {
        return "id"
    }


}
