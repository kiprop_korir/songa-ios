//
//  Campaign.swift
//  Songa
//
//  Created by Collins Korir on 9/25/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

//
//  Voucher.swift
//  Songa
//
//  Created by Collins Korir on 9/25/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

//
//  PodcastArtist.swift
//  Songa
//
//  Created by Collins Korir on 9/21/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Campaign: Object, Mappable {
    
     @objc dynamic var id = ""
     @objc dynamic var name = ""
     @objc dynamic var starting = ""
     @objc dynamic var ending = ""
     @objc dynamic var limit = 0
     @objc dynamic var promoCode = ""
     @objc dynamic var gifterLimit = 0
     @objc dynamic var redeemerLimit = 0
     @objc dynamic var campaignDescription = ""
     @objc dynamic var status = false
     @objc dynamic var deactivate = false
     @objc dynamic var createdAt = ""
     @objc dynamic var updatedAt = ""
    
    
     func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        starting <- map["starting"]
        ending <- map["ending"]
        limit <- map["limit"]
        promoCode <- map["promo_code"]
        gifterLimit <- map["gifter_limit"]
        redeemerLimit <- map["redeemer_limit"]
        campaignDescription <- map["description"]
        status <- map["status"]
        deactivate <- map["deactivate"]
        createdAt <- map["created_at"]
        updatedAt <- map["updated_at"]
    }

    
    //Impl. of Mappable protocol
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}



