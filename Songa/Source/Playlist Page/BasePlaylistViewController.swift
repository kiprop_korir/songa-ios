//
//  BasePlaylistViewController.swift
//  Songa
//
//  Created by Collins Korir on 11/27/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import UIKit
import Foundation
import Alamofire
import SDWebImage
import RealmSwift
import SDWebImage
import MarqueeLabel
import AVFoundation

class BasePlaylistViewController: UIViewController,UITableViewDelegate , UITableViewDataSource {
    
        @IBOutlet weak var lblPlaylistTitle: UILabel!
        @IBOutlet weak var lblUpdatedAt: MarqueeLabel!
        @IBOutlet weak var viewDownload: UIView!
        @IBOutlet weak var btnFavourite: UIButton!
        @IBOutlet weak var ivCover: UIImageView!
        @IBOutlet weak var ivCoverBg: UIImageView!
        @IBOutlet weak var ivDownload: UIImageView!
        @IBOutlet weak var btnShare: UIButton!
        @IBOutlet weak var tvTracks: UITableView!
        @IBOutlet weak var lblFollowerCount: UILabel!
        @IBOutlet weak var ivFollowers: UIImageView!
        @IBOutlet weak var lblDownload: UILabel!
        
        let headers = APIurls.Headers
        let realm = try! Realm()
        
        let playlistTracksAPI = APIurls.Playlist
        
        var tracks : [Track] = []
        var playlistID : String = ""

       
        var playlist: PlaylistResponse?  = nil
        var userPlaylist: UserPlaylist?  = nil
    
     var tasks: [URLSessionTask] = []


        override func viewDidLoad() {
            super.viewDidLoad()
            
               ivDownload.tintColor = .white
                    lblFollowerCount.textColor = .white
                    ivFollowers.tintColor = .white
                    lblDownload.textColor = .white
            
                    self.btnShare.imageView?.tintColor = .white
            
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.downloadAll))
                    viewDownload.addGestureRecognizer(tap)
            
                      self.setDate(dateUpdated: userPlaylist?.updatedAt ?? "")
                        self.ivCoverBg.blurImage()
                    
                    
                    if(isPlaylistFavourited()){
                         btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite_filled"), for: .normal)
                    }
            
                    tvTracks.delegate =  self
                    tvTracks.dataSource =  self
            
            
                    //remove separator form tableview
                    tvTracks.separatorStyle = UITableViewCell.SeparatorStyle.none
                    tvTracks.allowsSelection = false
            
                    lblUpdatedAt.textColor = .white
                    lblPlaylistTitle.textColor = .red
                    btnFavourite.tintColor =  .gray
            
            
    }
    
    func getDownloadTasks()  {
          //show view resuming download if file was downloading
              //get download session task

              DownloadManager.shared.downloadSession.getAllTasks { tasksArray in
                         // For each task, restore the state in the app
                self.tasks = tasksArray
                
        }
    }
    
    func setTrackDownloadingView (track: Track) {
        
        getDownloadTasks()
        for task in self.tasks {
                                    guard let downloadTask = task as? AVAssetDownloadTask else { break }
                                 if(downloadTask.taskDescription == track.id){
                                    //track is currently donwloading
                                     self.downloadProgressIndicator!.isHidden = true
                                     self.spinnerView?.isHidden = false
                                     self.btnDownload!.isHidden = true
                                     self.btnDownloaded.isHidden = true
                                     DownloadManager.shared.downloadProgressIndicator = self.downloadProgressIndicator
                                     DownloadManager.shared.spinnerView = self.spinnerView
                                     
                                 }
                            }
    }
                        
       
    
        @objc func downloadAll(){
    
        }
    
        private func setDate(dateUpdated:String) {
    
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateStyle = DateFormatter.Style.long
            //dateFormatterGet.timeStyle = .medium
            let dateString = dateFormatterGet.string(from: dateFormatter.date(from: dateUpdated)!)
    
            self.lblUpdatedAt.text = "Updated \(dateString)"
        }
    
        class cellTapGesture: UITapGestureRecognizer {
            var tracks = Array<Track>()
            var currrentPlayingIndex = Int()
        }
    
        @objc func cellTapped(_ sender: cellTapGesture) {
    
    
            NotificationCenter.default.post(name: Notification.Name.startPlay,
                                            object: nil,
                                            userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
    
        }
        class optionsTapGesture: UITapGestureRecognizer {
            var track = Track()
        }
        @objc func optionsTapped(_ sender: optionsTapGesture) {
            showOptionsController(track: sender.track)
        }
    
        func isTrackDownloaded (trackID:String) -> Bool {
            let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(trackID)\"")
    
            if(queryResults?.count ?? 0 >= 1){
                return true
            }
            else {
                return false
            }
        }
    
    func isTrackDownloading (trackID:String) -> Bool {
             let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(trackID)\"")
     
             if(queryResults?.count ?? 0 >= 1){
                 return true
             }
             else {
                 return false
             }
         }
    
        func isPlaylistFavourited() -> Bool {
            let queryResults: Results<FavouritePlaylist>? = realm.objects(FavouritePlaylist.self).filter("id == \"\(playlistID)\"")
    
            if(queryResults?.count ?? 0 >= 1){
                return true
            }
            else {
                return false
            }
        }

        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return tracks.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as! TrackTableCell
            
            
            let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
            tappy.tracks = tracks
                      tappy.currrentPlayingIndex = indexPath.row
                      cell.addGestureRecognizer(tappy)
            
            let tapOptions = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
            tapOptions.track = tracks[indexPath.row]
            cell.btnOptions.addGestureRecognizer(tapOptions)
            
            if(isTrackDownloaded(trackID: tracks[indexPath.row].id)){
                cell.ivDownloaded.isHidden = false
            }
            
            cell.configure(track: tracks[indexPath.row], position: indexPath.row, downloaded: ist, download: <#T##Track?#>)
            
            return cell
        }
        
        
    }
