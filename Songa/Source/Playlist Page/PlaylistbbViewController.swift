////
////  PlaylistViewController.swift
////  Songa
////
////  Created by Collins Korir on 8/16/18.
////  Copyright © 2018 Collins Korir. All rights reserved.
////
//
//import UIKit
//import Foundation
//import Alamofire
//import SDWebImage
//import RealmSwift
//import SDWebImage
//import MarqueeLabel
//
//class PlaylistViewController: BaseViewController {
//    @IBOutlet weak var lblPlaylistTitle: UILabel!
//    @IBOutlet weak var lblUpdatedAt: MarqueeLabel!
//    @IBOutlet weak var viewDownload: UIView!
//    @IBOutlet weak var btnFavourite: UIButton!
//    @IBOutlet weak var ivCover: UIImageView!
//    @IBOutlet weak var ivCoverBg: UIImageView!
//    @IBOutlet weak var ivDownload: UIImageView!
//    @IBOutlet weak var btnShare: UIButton!
//    @IBOutlet weak var tvTracks: UITableView!
//    @IBOutlet weak var lblFollowerCount: UILabel!
//    @IBOutlet weak var ivFollowers: UIImageView!
//    @IBOutlet weak var lblDownload: UILabel!
//
//    let headers = APIurls.Headers
//    let realm = try! Realm()
//
//    let playlistTracksAPI = APIurls.Playlist
//
//    var tracks : [Track] = []
//    var playlistID : String = ""
//
//    @IBOutlet weak var navBar: UINavigationBar!
//
//    var playlist: PlaylistResponse?  = nil
//    var userPlaylist: UserPlaylist?  = nil
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        //setViewColors()
//        ivDownload.tintColor = .white
//        lblFollowerCount.textColor = .white
//        ivFollowers.tintColor = .white
//        lblDownload.textColor = .white
//
//        self.btnShare.imageView?.tintColor = .white
//
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.downloadAll))
//        viewDownload.addGestureRecognizer(tap)
//
//        if(userPlaylist == nil){
//            //not loading users created playlist
//              loadPlaylist()
//        }
//        else {
//            for track in userPlaylist!.tracks {
//                self.tracks.append(track)
//            }
//            tvTracks.reloadData()
//
//                self.ivCover.sd_setImage(with: URL(string: userPlaylist?.tracks[0].artwork[0].medium ?? ""), placeholderImage: #imageLiteral(resourceName: "empty_album"))
//            self.ivCoverBg.sd_setImage(with: URL(string: userPlaylist?.tracks[0].artwork[0].medium ?? ""), placeholderImage: #imageLiteral(resourceName: "empty_album"))
//
//
//            self.setDate(dateUpdated: userPlaylist?.updatedAt ?? "")
//            self.lblPlaylistTitle.text =  userPlaylist?.name
//            self.ivCoverBg.blurImage()
//        }
//         self.navigationItem.title = "Playlist"
//
//        if(isPlaylistFavourited()){
//             btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite_filled"), for: .normal)
//        }
//
//        tvTracks.delegate =  self
//        tvTracks.dataSource =  self
//
//
//        //remove separator form tableview
//        tvTracks.separatorStyle = UITableViewCell.SeparatorStyle.none
//        tvTracks.allowsSelection = false
//
//        lblUpdatedAt.textColor = .white
//        lblPlaylistTitle.textColor = .red
//        btnFavourite.tintColor =  .gray
//
//
//
//    }
//    func isTrackDownloaded (trackID:String) -> Bool {
//        let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(trackID)\"")
//
//        if(queryResults?.count ?? 0 >= 1){
//            return true
//        }
//        else {
//            return false
//        }
//    }
//
//    class cellTapGesture: UITapGestureRecognizer {
//        var tracks = Array<Track>()
//        var currrentPlayingIndex = Int()
//    }
//
//    @objc func cellTapped(_ sender: cellTapGesture) {
//
//
//        NotificationCenter.default.post(name: Notification.Name.startPlay,
//                                        object: nil,
//                                        userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
//
//    }
//    class optionsTapGesture: UITapGestureRecognizer {
//        var track = Track()
//    }
//    @objc func optionsTapped(_ sender: optionsTapGesture) {
//        showOptionsController(track: sender.track)
//    }
//    override func viewDidAppear(_ animated: Bool) {
//
//        FirebaseUtil.shared.getPlaylistFollowCount(playlistID: playlistID)
//
//
//        //add retry observer
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(retry),
//                                               name: Notification.Name.retryClicked,
//                                               object: nil)
//
//        NotificationCenter.default.addObserver(self,
//                                                selector: #selector(showFollowCount),
//                                                name: Notification.Name.playlistFollowCount
//            ,
//                                                object: nil)
//        NotificationCenter.default.addObserver(self,
//                                                       selector: #selector(showFollowCount),
//                                                       name: Notification.Name.playlistFollowCountModified
//                   ,
//                                                       object: nil)
//    }
//
//    @objc func showFollowCount(_ notification: Notification){
//     let followers = notification.userInfo?["count"] as! Int?
//        var desc = ""
//        if followers! > 0 {
//            desc = "Followers"
//        }
//        else {
//            desc =
//            "Follower"
//        }
//        lblFollowerCount.text =  "\(String(describing: followers!) ) \(desc)"
//    }
//    @objc func retry(){
//        self.dismissErrorView()
//        loadPlaylist()
//    }
//
//    func loadPlaylist()  {
//        self.showLoadingView()
//        APIClient.getPlaylist(playlistId: playlistID){ result in
//
//            self.dismissLoadingView()
//
//            switch result.result {
//            case .success(let value):
//
//                self.playlist = value
//
//
//                                for track in value.tracks!{
//
//                    self.tracks.append(track)
//
//                        }
//                                var description = ""
//                                if let des =  value.description as String?{
//
//                                    description =  des
//
//                                }
//
//                                if let coverImageURL =  value.artwork![safe: 0]?.original {
//
//                                    self.ivCover.sd_setImage(with: URL(string: coverImageURL), placeholderImage: #imageLiteral(resourceName: "empty_album"))
//                                    self.ivCoverBg.sd_setImage(with: URL(string: coverImageURL), placeholderImage: #imageLiteral(resourceName: "empty_album"))
//
//                                }
//
//                                self.setDate(dateUpdated: value.updatedAt!)
//                self.lblPlaylistTitle.text =  value.name
//                                self.ivCoverBg.blurImage()
//
//                                //send a notification that tracks have been fetched
//
//                                NotificationCenter.default.post(name: Notification.Name.PlaylistHasLoaded,
//                                                                object: nil,
//                                                                userInfo:["tracks": self.tracks,"description": description])
//
//                self.tvTracks.reloadData()
//
//            case .failure(let encodingError):
//                var statusCode:Int
//                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
//                    // no internet connection
//                    statusCode = 1
//                }
//                else{
//                    statusCode = result.response?.statusCode ?? 404
//                }
//                if(statusCode == 401 )
//                {
//                    APIClient.getAuthToken(){ result in
//
//                        switch result.result {
//
//                        case .success:
//
//                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
//                            self.loadPlaylist()
//
//                        case .failure:
//
//                            print("do something when token refresh fails")
//                        }
//                    }
//                }
//                else{
//                    self.showErrorView(errorCode: statusCode)
//                }
//            }
//        }
//    }
//
//    func isPlaylistFavourited() -> Bool {
//        let queryResults: Results<FavouritePlaylist>? = realm.objects(FavouritePlaylist.self).filter("id == \"\(playlistID)\"")
//
//        if(queryResults?.count ?? 0 >= 1){
//            return true
//        }
//        else {
//            return false
//        }
//    }
//
//    private func setDate(dateUpdated:String) {
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
//        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//
//        let dateFormatterGet = DateFormatter()
//        dateFormatterGet.dateStyle = DateFormatter.Style.long
//        //dateFormatterGet.timeStyle = .medium
//        let dateString = dateFormatterGet.string(from: dateFormatter.date(from: dateUpdated)!)
//
//        self.lblUpdatedAt.text = "Updated \(dateString)"
//    }
//
//
//    @objc func downloadAll(){
//
//    }
//
//    ///IB Actions
//
//
//    @IBAction func goBack(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
//    }
//    @IBAction func playAll(_ sender: Any) {
//        NotificationCenter.default.post(name: Notification.Name.startPlay,
//                                        object: nil,
//                                        userInfo:["tracks": self.tracks ,"currentPlayingIndex": 0])
//    }
//    @IBAction func mix(_ sender: Any) {
//        //to do shuffle the indexes
//
//         let randomIndex = Int.random(in: 0 ..< tracks.count - 1)
//
//        NotificationCenter.default.post(name: Notification.Name.startPlay,
//                                        object: nil,
//                                        userInfo:["tracks": self.tracks as Any ,"currentPlayingIndex": randomIndex])
//    }
//    @IBAction func favourite(_ sender: Any) {
//
//        let playlistObject = Playlist()
//
//
//               for artwork in playlist!.artwork! {
//                  playlistObject.artwork.append(artwork)
//               }
//
//               for track in playlist!.tracks! {
//                   playlistObject.tracks.append(track)
//               }
//               playlistObject.name = (playlist?.name)!
//               playlistObject.updatedAt = (playlist?.updatedAt)!
//               playlistObject.id = playlist!.id!
//
//        let favouritedPlaylist: FavouritePlaylist = FavouritePlaylist()
//         favouritedPlaylist.id = playlist!.id!
//         favouritedPlaylist.playlist = playlistObject
//
//
//        if(!isPlaylistFavourited()){
//
//       do {
//            let realm = try Realm()
//            try realm.write {
//                realm.add(favouritedPlaylist,update: .modified)
//                btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite_filled"), for: .normal)
//            }
//
//        } catch let error as NSError {
//            print(error)
//        }
//
//            //subsribe user to topic on firebase
//            FirebaseUtil.shared.subscribeToTopic(topic: playlist!.name!)
//           FirebaseUtil.shared.followPlaylist(playlistName: playlist!.name!, playlistID: playlistID, toUnfollow: false)
//        }else {
//            //unfavourite playlist
//
//                      let queryResults: Results<FavouritePlaylist>? = realm.objects(FavouritePlaylist.self).filter("id == \"\(playlistID)\"")
//
//                      do {
//                          let realm = try Realm()
//                          try realm.write {
//                              realm.delete((queryResults?[0])!)
//                              btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite"), for: .normal)
//                          }
//
//
//                      } catch let error as NSError {
//
//                          print(error)
//
//                      }
//
//            //unsubsribe user to topic on firebase
//            FirebaseUtil.shared.unsubscribeToTopic(topic: playlist!.name!)
//           FirebaseUtil.shared.followPlaylist(playlistName: playlist!.name!, playlistID: playlistID, toUnfollow: false)
//                      }
//    }
//
//    @IBAction func share(_ sender: Any) {
//        self.share(artist:playlist!.name! , link: "https//open.songamusic.com/playlist/\(playlist!.id  ?? "")")
//    }
//
//    @objc func reloadCollectionViewData(_ notification: Notification)  {
//
//        tracks = notification.userInfo?["tracks"] as! Array <Track>
//        tvTracks.reloadData()
//
//    }
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
//    {
//
//        if let vc = segue.destination as? ArtistPageViewController
//        {
//            vc.artistID = sender as! String
//        }
//        else if let destinationNavigationController = segue.destination as? UINavigationController {
//                   let targetController = destinationNavigationController.topViewController as! AddToPlaylistViewController
//            targetController.track = sender as? Track
//               }
//    }
//
//
//
//}
//
//extension PlaylistViewController: UITableViewDelegate , UITableViewDataSource {
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return tracks.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as! TrackTableCell
//        let url : String
//
//        if ( tracks[indexPath.row].artwork.count > 0){
//
//            url =  tracks[indexPath.row].artwork[0].small
//
//        }
//        else
//        {
//            //might be a blank array
//            url = "no_cover"
//
//        }
//
//        cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
//        cell.lblTitle.text = tracks[indexPath.row].title
//        cell.lblArtist.text = tracks[indexPath.row].artistName
//        cell.lblArtist.textColor = ThemeManager.currentTheme().textColorSecondary
//        cell.lblTrackIndex.textColor = ThemeManager.currentTheme().textColorSecondary
//        cell.lblTrackIndex.text = String(indexPath.row + 1)
//
//        // Add a separator below each cell
//        let horizontalGap = 15.0 as CGFloat
//        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
//        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
//        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
//        cell.addSubview(seperatorView)
//
//
//
//        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
//
//        let tapOptions = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
//        tapOptions.track = tracks[indexPath.row]
//
//        cell.btnOptions.addGestureRecognizer(tapOptions)
//         cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
//
//        //add rounded edge
//        cell.layer.cornerRadius =  5
//
//        if(isTrackDownloaded(trackID: tracks[indexPath.row].id)){
//            cell.ivDownloaded.isHidden = false
//        }
//
//
//
//        tappy.tracks = tracks
//        tappy.currrentPlayingIndex = indexPath.row
//        cell.addGestureRecognizer(tappy)
//
//        return cell
//    }
//
//
//}
