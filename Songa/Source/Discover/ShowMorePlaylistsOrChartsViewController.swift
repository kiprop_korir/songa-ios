//
//  ShowMoreArtistsOrTracksViewController.swift
//  Songa
//
//  Created by Collins Korir on 10/22/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import UIScrollView_InfiniteScroll

class ShowMorePlaylistsOrChartsViewController: BaseViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    var type : String = ""
    
    let playlistsAPI = APIurls.showMorePlaylists
    let chartsAPI = APIurls.showMoreCharts
    let devotionsAPI = APIurls.showMoreDevotions
    let celebrityPlaylistsAPI = APIurls.showMoreCelebrityPlaylists
    let headers = APIurls.Headers
    
    var playlists: [Playlist] = []
    var devotions: [Devotion] = []
    var celebrityPlaylists: [CelebrityPlaylist] = []
    var charts: [Chart] = []
    
     var tappedCellID : String = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.navigationItem.title = type
        activityIndicator.startAnimating()
        loadItems()
        
        collectionView.addInfiniteScroll { (collectionView) -> Void in
            collectionView.performBatchUpdates({ () -> Void in
                // update collection view
            }, completion: { (finished) -> Void in
                // finish infinite scroll animations
                collectionView.finishInfiniteScroll()
            });
        }
        
        let frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        
        collectionView.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: frame)
        
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var id = String()
    }
    
    @objc func showMoreTapped(_ sender: cellTapGesture) {
        
        tappedCellID = sender.id
        
         if type == "Devotions" {
       
        }
         else if type == "Charts" {
            performSegue(withIdentifier: "segueShowMoreChart", sender: self)
        }
        
         else {
            //both playlist types
             performSegue(withIdentifier: "segueShowMorePlaylist", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "segueShowMorePlaylist":
            
            let vc = segue.destination as! PlaylistViewController
            vc.playlistID = self.tappedCellID
            
        case "segueShowMoreChart":
            
            let vc = segue.destination as! ChartViewController
           vc.chartID = self.tappedCellID
            
        default:
        print("do nothing")
        }
    }

    //MARK: - Load Genres From API
    
    func loadItems()  {
        
        if type == "Playlists" {
  
        Alamofire.request(playlistsAPI, method: .get,  headers: headers).responseArray { (response: DataResponse<[Playlist]>) in
            
            print(self.playlistsAPI)
            
            let playlistResponse = response.result.value
            
            if let playlists = playlistResponse {
                
                
                for playlist in playlists
                    
                {
                    self.playlists.append(playlist)
            
                }
                self.collectionView.reloadData()
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
            }
        
          
            
        }
        
    }
            
    
        else  if type == "Charts" {
            
            
            Alamofire.request(chartsAPI, method: .get,  headers: headers).responseArray { (response: DataResponse<[Chart]>) in
                
                // print(self.genresAPI)
                //print(response.result.value)
                
                let chartResponse = response.result.value
                
                print(chartResponse)
                if let charts = chartResponse {
                    
                    
                    for chart in charts
                        
                    {
                        self.charts.append(chart)
                    }
                    
                }
                self.collectionView.reloadData()
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
             
            }
        }
        else {
            self.collectionView.reloadData()
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }
        
    }

    
}


extension ShowMorePlaylistsOrChartsViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
           if type == "Playlists" {
          return playlists.count
        }
        else if type == "Celebrity Playlists" {
            return celebrityPlaylists.count
        }
        if type == "Devotions" {
            return devotions.count
        }
           else {
            return charts.count
        }
     
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "playlistCell", for: indexPath) as! PlaylistCell

         var id = ""
          if type == "Playlists" {

            
        let url : String
        
        if ( playlists[indexPath.row].artwork.count > 0){
            
            url = playlists[indexPath.row].artwork[0].medium
              
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        //set the genre image using sd web image library asycnhronously
        
        cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = playlists[indexPath.row].name
            
            id = playlists[indexPath.row].id
            
        }
          else if type == "Charts"{
          
            let url : String
            
            if ( charts[indexPath.row].artwork.count > 0){
                
                url = charts[indexPath.row].artwork[0].medium
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = charts[indexPath.row].name
            
             id = charts[indexPath.row].id
            
        }
        
         else if type == "Celebrity Playlists"{
        
            let url : String
            
            if ( celebrityPlaylists[indexPath.row].artwork.count > 0){
                
                url = celebrityPlaylists[indexPath.row].artwork[0].medium
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = celebrityPlaylists[indexPath.row].name
            
            id = celebrityPlaylists[indexPath.row].id
            
        }
        
          else {
            
            let url : String
            
            if ( devotions[indexPath.row].artwork.count > 0){
                
                url = devotions[indexPath.row].artwork[0].medium
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = devotions[indexPath.row].title
            
            id = devotions[indexPath.row].id
            
        }
        
        //adding a corner radius to the cell
        cell.layer.cornerRadius = 5
        cell.layer.masksToBounds = true
        
        
        let tappy = cellTapGesture(target: self, action: #selector(showMoreTapped(_:)))
        
        tappy.id = id
        
        cell.addGestureRecognizer(tappy)
        
   return cell
        
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let padding: CGFloat =  20
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: 194)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }
}

