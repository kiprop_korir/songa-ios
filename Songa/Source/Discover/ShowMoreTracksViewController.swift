//
//  ShowMoreViewController.swift
//  Songa
//
//  Created by Collins Korir on 9/11/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import RealmSwift
import UIScrollView_InfiniteScroll


class ShowMoreTracksViewController: BaseViewController {
    
    @IBOutlet weak var tvTracks: UITableView!

    let releasesAPI = APIurls.showMoreReleases
    let recommendedAPI = APIurls.showMoreRecommended
    let allTimeAPI = APIurls.showMoreReleases
    let headers = APIurls.Headers
    var page = 0
    var type : String = ""
    var shouldFetch : Bool = true
    var apiURL: String = ""
    var tracks : [Track] = []
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.title = type
        tvTracks.delegate = self
        tvTracks.dataSource = self
        
        //remove separator form tableview
        tvTracks.separatorStyle = UITableViewCell.SeparatorStyle.none
        tvTracks.allowsSelection = false
        
        if(shouldFetch){
  
        loadTracks()
        }
        
        tvTracks.addInfiniteScroll { (tableView) -> Void in
            // update table view
            
            self.page = self.page + 1
            self.loadTracks()
            // finish infinite scroll animation
            tableView.finishInfiniteScroll()
        }
        
        let frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        
        tvTracks.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: frame)
    }
    
    func loadMore(_ pageNumber: Int, _ pageSize: Int, onSuccess: ((Bool) -> Void)?, onError: ((Error) -> Void)?) {
        // Call your api here
        // Send true in onSuccess in case new data exists, sending false will disable pagination
        self.page = pageNumber
        loadTracks()
        // If page number is first, reset the list
//        if pageNumber == 1 { self.tracks = [Track]() }
//
//        // else append the data to list
//        let startFrom = (self.tracks.last ?? Track) + 1
//        for number in startFrom..<(startFrom + pageSize) {
//            self.list.append(number)
//        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            onSuccess?(true)
        }
//    }
    
}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc = segue.destination as? ArtistPageViewController
        {
            vc.artistID = sender as! String
        }
    }
    
    func isTrackDownloaded (trackID:String) -> Bool {
        let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(trackID)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
        else {
            return false
        }
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var tracks = Array<Track>()
        var currrentPlayingIndex = Int()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
        
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
        
    }
    class optionsTapGesture: UITapGestureRecognizer {
        var track = Track()
    }
    
    @objc func optionsTapped(_ sender: optionsTapGesture) {
        showOptionsController(track: sender.track)
    }
    

    //MARK: - Load Genres From API
    
    func loadTracks()  {
        
        if(page == 0){
        self.showLoadingView()
        }
     
        switch type {
        case "New Releases":
            apiURL = releasesAPI
        case "Recommended":
        apiURL = recommendedAPI
        case "All Time Radio Hits":
            apiURL = allTimeAPI
        default:
          break
        }
        
        apiURL = apiURL  + "&page=\(String(self.page))"
        
        
        Alamofire.request(apiURL, method: .get,  headers: headers).responseArray { (response: DataResponse<[Track]>) in
    
    self.dismissLoadingView()
    print(self.apiURL)
    
    let tracksResponse = response.result.value
    
    if let tracks = tracksResponse {
        
        
        for track in tracks
            
        {
            self.tracks.append(track)
        }
        self.tvTracks.reloadData()
           }
        }
        
    }
}

extension ShowMoreTracksViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //handle albums cellForItemAt
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as! TrackTableCell
        
        let url : String
        
        if ( tracks[indexPath.row].artwork.count > 0){
            
            url = tracks[indexPath.row].artwork[0].small
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        //set the genre image using sd web image library asycnhronously
        
        
        cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = tracks[indexPath.row].title
        cell.lblArtist.text = tracks[indexPath.row].artistName
        cell.lblTrackIndex.text = String(indexPath.row + 1)
        cell.lblTrackIndex.textColor = ThemeManager.currentTheme().textColorSecondary
        cell.lblArtist.textColor = ThemeManager.currentTheme().textColorSecondary
        
        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        
        tappy.tracks =  tracks
        tappy.currrentPlayingIndex =  indexPath.row
        
        //cell.btnPlay.addGestureRecognizer(tappy)
        cell.addGestureRecognizer(tappy)
        
        
        let tapOptions = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
        tapOptions.track = tracks[indexPath.row]
        
        cell.btnOptions.addGestureRecognizer(tapOptions)
        cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
        if(isTrackDownloaded(trackID: tracks[indexPath.row].id)){
            cell.ivDownloaded.isHidden = false
        }
        
        // Add a separator below each cell
        let horizontalGap = 15.0 as CGFloat
        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
        cell.addSubview(seperatorView)
        
        return cell
    }
    
}
