 //
 //  DiscoverViewController.swift
 //  Songa
 //
 //  Created by Collins Korir on 7/31/18.
 //  Copyright © 2018 Collins Korir. All rights reserved.
 //
 
 import Foundation
 import UIKit
 import  Alamofire
 import AlamofireObjectMapper
 import SDWebImage
 import RealmSwift
 
 class DiscoverViewController : UIViewController  {
    
    let discoverAPI = APIurls.Discover
    let headers = APIurls.Headers
    let realm = try! Realm()

    @IBOutlet weak var stackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var viewCelebrityPlaylists: UIView!
    @IBOutlet weak var viewNewReleases: UIView!
    @IBOutlet weak var viewPlaylists: UIView!
    @IBOutlet weak var viewComedy: UIView!
    @IBOutlet weak var viewArtists: UIView!
    @IBOutlet weak var viewCharts: UIView!
    @IBOutlet weak var viewRecentlyPlayed: UIView!
    @IBOutlet weak var viewRecommended: UIView!
    @IBOutlet weak var viewDevotional: UIView!
    @IBOutlet weak var viewAllTimeHits: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var cvHeroes: UICollectionView!
    @IBOutlet weak var cvNewReleases: UICollectionView!
    @IBOutlet weak var cvPlaylists: UICollectionView!
    @IBOutlet weak var cvCharts: UICollectionView!
    @IBOutlet weak var cvArtists: UICollectionView!
    @IBOutlet weak var cvRecommended: UICollectionView!
    @IBOutlet weak var cvComedy: UICollectionView!
    @IBOutlet weak var cvAllTime: UICollectionView!
    @IBOutlet weak var cvDevotions: UICollectionView!
    @IBOutlet weak var cvCelebrityPlaylists: UICollectionView!
    @IBOutlet weak var cvRecentlyPlayed: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewShowMoreReleases: UIView!
    @IBOutlet weak var viewShowMorePlaylists: UIView!
    @IBOutlet weak var viewShowMoreCharts: UIView!
    @IBOutlet weak var viewShowMoreArtists: UIView!
    @IBOutlet weak var viewShowMoreRecommended: UIView!
    @IBOutlet weak var viewShowMoreAllTime: UIView!
    @IBOutlet weak var viewShowMoreCelebrityPlaylists: UIView!
    @IBOutlet weak var viewShowMoreComedy: UIView!
    @IBOutlet weak var viewShowMoreDevotions: UIView!
    @IBOutlet weak var viewShowMoreRecentlyPlayed: UIView!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var btnSearch: UIButton!
    
    var heroes: [Heroe] = []
    var loggedInUsers: [RAGUser] = []
    var recommended: [Track] = []
    var artists: [Artist] = []
    var trending: [Track] = []
    var recentlyPlayed: [Track] = []
    var charts: [Chart] = []
    var celebrityPlaylists: [CelebrityPlaylist] = []
    var devotions: [Devotion] = []
    var comedy: [Comedy] = []
    var playlists: [Playlist] = []
    var newReleases: [Track] = []
    var allTimeHits: [Track] = []
    
    var showMoreTypeSelected : String = ""
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    
        cvHeroes.dataSource = self
        cvHeroes.delegate = self
        
        cvNewReleases.dataSource = self
        cvNewReleases.delegate = self
        
        cvPlaylists.dataSource = self
        cvPlaylists.delegate = self
        
        cvCharts.dataSource = self
        cvCharts.delegate = self
  
        cvArtists.dataSource = self
        cvArtists.delegate = self
        
        
        cvRecommended.dataSource = self
        cvRecommended.delegate = self
        
        cvAllTime.dataSource = self
        cvAllTime.delegate = self
        
        cvRecentlyPlayed.dataSource = self
        cvRecentlyPlayed.delegate = self
        
        cvCelebrityPlaylists.dataSource = self
        cvCelebrityPlaylists.delegate = self
        
        cvDevotions.dataSource = self
        cvDevotions.delegate = self
        
        cvComedy.dataSource = self
        cvComedy.delegate = self
        
        fetchRealmObjects()
    
        scrollView.updateContentViewSize()
        
        //add tap gesture recognizers to show more views
        
        let showMoreReleasesTap = showMoreTapGesture(target: self, action: #selector(showMoreTapped(_:)))
        showMoreReleasesTap.showMoreTypeSelected =  "New Releases"
        viewShowMoreReleases.addGestureRecognizer(showMoreReleasesTap)
        
        let showMorePlaylistsTap = showMoreTapGesture(target: self, action: #selector(showMoreTapped(_:)))
        showMorePlaylistsTap.showMoreTypeSelected =  "Playlists"
        viewShowMorePlaylists.addGestureRecognizer(showMorePlaylistsTap)
        
        let showMoreChartsTap = showMoreTapGesture(target: self, action: #selector(showMoreTapped(_:)))
        showMoreChartsTap.showMoreTypeSelected =  "Charts"
        viewShowMoreCharts.addGestureRecognizer(showMoreChartsTap )
        
        let showMoreArtistsTap = showMoreTapGesture(target: self, action: #selector(showMoreTapped(_:)))
        showMoreArtistsTap.showMoreTypeSelected =  "Artists"
        viewShowMoreArtists.addGestureRecognizer(showMoreArtistsTap)
        
        let showMoreRecommendedTap = showMoreTapGesture(target: self, action: #selector(showMoreTapped(_:)))
        showMoreRecommendedTap.showMoreTypeSelected =  "Recommended"
        viewShowMoreRecommended.addGestureRecognizer(showMoreRecommendedTap)
        
        let showMoreComedy = showMoreTapGesture(target: self, action: #selector(showMoreTapped(_:)))
          showMoreComedy.showMoreTypeSelected =  "Comedy"
          viewShowMoreComedy.addGestureRecognizer(showMoreComedy)
        
        let showMoreAlltimeTap = showMoreTapGesture(target: self, action: #selector(showMoreTapped(_:)))
        showMoreAlltimeTap.showMoreTypeSelected =  "All Time Radio Hits"
        viewShowMoreAllTime.addGestureRecognizer(showMoreAlltimeTap)
        
        let showMoreCelebrityTap = showMoreTapGesture(target: self, action: #selector(showMoreTapped(_:)))
        showMoreCelebrityTap.showMoreTypeSelected =  "Celebrity Playlists"
        viewShowMoreCelebrityPlaylists.addGestureRecognizer(showMoreCelebrityTap)
        
        let showMoreDevotionalTap = showMoreTapGesture(target: self, action: #selector(showMoreTapped(_:)))
        showMoreDevotionalTap.showMoreTypeSelected =  "Devotions"
        viewShowMoreDevotions.addGestureRecognizer(showMoreDevotionalTap)
        
        let showMoreRecentsTap = showMoreTapGesture(target: self, action: #selector(showMoreTapped(_:)))
        showMoreRecentsTap.showMoreTypeSelected =  "Recents"
        viewShowMoreRecentlyPlayed.addGestureRecognizer(showMoreRecentsTap)
        
        
        //add tap gestires to top bar

        
        setUpNavBar()
      
        //loadDiscoverContent()
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        loadDiscoverContent()
        setUpNavBar()
        //add retry observer
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(retry),
                                               name: Notification.Name.retryClicked,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setUpNavBar),
                                               name: Notification.Name.notificationReceived,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                                 selector: #selector(respondToThemeChange),
                                                 name: Notification.Name.themeChanged,
                                                 object: nil)
    }
    
    @objc func respondToThemeChange(){
        self.view.layoutSubviews()
        self.viewDidAppear(true)
    }
    @objc func setUpNavBar(){
        
        let view = UIView(frame: CGRect(x: 0, y: 2, width: 40, height: 40))
        
        let logo = UIImageView(frame: CGRect(x: 0 , y: 10, width: 25, height: 25))
        logo.image = #imageLiteral(resourceName: "ic_profile")
        view.addSubview(logo)
        
        let count = RoundEdgesButton(frame: CGRect(x: 20, y: 0, width: 20, height: 20))
        count.backgroundColor = UIColor.red
        count.cornerRadius = 10
        
        let notificationsCount = realm.objects(SongaNotification.self).filter("isRead == false").count
        
        if(notificationsCount>0){
            count.setTitle(String(notificationsCount), for: .normal)
            count.titleLabel?.font = .systemFont(ofSize: 12)
            view.addSubview(count)
        }
        
        let barButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = barButtonItem
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.profileViewClicked))
        barButtonItem.customView?.addGestureRecognizer(tap)
        
        let searchButton = UIBarButtonItem(image: UIImage(imageLiteralResourceName: "ic_search").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(self.searchClicked))
 
        self.navigationItem.rightBarButtonItem = searchButton
        self.navigationItem.rightBarButtonItem?.tintColor = ThemeManager.currentTheme().barTint
        
        
    }
   

    @objc func profileViewClicked(){            
        performSegue(withIdentifier: "segueProfile", sender: self)
    }
    
    @objc func searchClicked(){
           performSegue(withIdentifier: "segueSearch", sender: self)
    }
    
    //MARK: - Prepare for Segue to Genre Detail Method
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        if let identifier = segue.identifier {
            switch identifier {
                
            case "showArtist":
                
                let vc = segue.destination as! ArtistPageViewController
                //get the current selected cell
                if let cell = sender as? UICollectionViewCell,
                    let indexPath = self.cvArtists.indexPath(for: cell) {
                    vc.artistID = self.artists[indexPath.row].id
                    
                }
                
                case "showComedy":
                           
                           let vc = segue.destination as! ArtistPageViewController
                           //get the current selected cell
                           if let cell = sender as? UICollectionViewCell,
                               let indexPath = self.cvComedy.indexPath(for: cell) {
                               vc.artistID = self.comedy[indexPath.row].id
                               
                           }
                
            case "showPlaylist":
                
                let vc = segue.destination as! PlaylistViewController
                //get the current selected cell
                if let cell = sender as? UICollectionViewCell,
                    let indexPath = self.cvPlaylists.indexPath(for: cell) {
                    print(self.playlists[indexPath.row].id)
                    vc.playlistID = self.playlists[indexPath.row].id
                }
                case "showCelebrityPlaylist":
                         print("heeeee")
                         let vc = segue.destination as! PlaylistViewController
                         //get the current selected cell
                         if let cell = sender as? UICollectionViewCell,
                             let indexPath = self.cvCelebrityPlaylists.indexPath(for: cell) {
                             print(self.celebrityPlaylists[indexPath.row].id)
                             vc.playlistID = self.celebrityPlaylists[indexPath.row].id
                            
                            print(self.celebrityPlaylists[indexPath.row].id)
                         }
                
            case "showChart":
                
                let vc = segue.destination as! ChartViewController
                //get the current selected cell
                if let cell = sender as? UICollectionViewCell,
                    let indexPath = self.cvCharts.indexPath(for: cell) {
                    vc.chartID = self.charts[indexPath.row].id
                }
                
            case "showMorePlaylistsOrCharts":
                
                let vc = segue.destination as! ShowMorePlaylistsOrChartsViewController
                
                print("type is" ,self.showMoreTypeSelected)
                vc.type = sender as! String
                vc.devotions = self.devotions
                vc.celebrityPlaylists = self.celebrityPlaylists
                  
            case "showMoreTracks":
                
                let vc = segue.destination as! ShowMoreTracksViewController
                 print("type is" ,self.showMoreTypeSelected)
                vc.type = sender as! String
                vc.tracks = self.recentlyPlayed
                
            default:
                break
            }
        }
        
    }
    
    //MARK: - Load Discover Content From API
    
    func loadDiscoverContent()  {
        
        //only show loading when there's no data on DB
        if(heroes.count == 0)
        {
            self.showLoadingView()
        }
        
        
        APIClient.getDiscoverContent(){ result in
            
            self.dismissLoadingView()
        
            switch result.result {
            case .success(let value):
                
                let group = DispatchGroup()
                group.enter()
                
                // we have to use main thread since Realm needs to be accessed from the main thread deadlocks by not using .main queue here
                
                DispatchQueue.main.async {
                    
                    self.writeHeroes(heroes: value.heroes!)
                    self.writeReleases(releases: value.releases!)
                    self.writePopular(popular: value.popular!)
                    self.writePlaylists(playlists:  value.playlists!)
                    self.writeRecommended(recommended: value.recommended!)
                    self.writeTrending(trending:value.trending!)
                    self.writeRecent(recent: value.recent!)
                    self.writeCharts(charts: value.charts!)
                    self.writeDevotions(devotions : value.devotions!)
                    self.writeCelebrityPlaylists(celebrityPlaylists : value.celebrityPlaylists!)
                    self.writeComedy(comedies : value.comedyMappable!.artists!)
                    
                    group.leave()
                    
                    
                }
                
                group.notify(queue: .main) {
                    
                    // ... fetch objects as soon as they are written to Realm
                    //as soon as group is notified
                    
                    self.fetchRealmObjects()
                    
                    self.cvPlaylists.reloadData()
                    self.cvHeroes.reloadData()
                    self.cvArtists.reloadData()
                    self.cvCharts.reloadData()
                    self.cvRecommended.reloadData()
                    self.cvNewReleases.reloadData()
                    self.cvAllTime.reloadData()
                    self.cvRecentlyPlayed.reloadData()
                    self.cvDevotions.reloadData()
                    self.cvCelebrityPlaylists.reloadData()
                    self.cvComedy.reloadData()
            
                    //the cards have different sizes so we add them up to get the total size of the scrollview
                    
                    var totalStackHeight = 300 //hero top view
                    
                    if(self.playlists.count == 0 ){
                        self.viewPlaylists.isHidden = true
                    }
                    else {
                        totalStackHeight = totalStackHeight + 270
                    }
                    if(self.artists.count == 0 ){
                        self.viewArtists.isHidden = true
                    }
                    else {
                        totalStackHeight = totalStackHeight + 270
                    }
                    if(self.charts.count == 0 ){
                        self.viewCharts.isHidden = true
                    }
                    else {
                        totalStackHeight = totalStackHeight + 270
                    }
                    if(self.recommended.count == 0 ){
                        self.viewRecommended.isHidden = true
                    }
                    else {
                        totalStackHeight = totalStackHeight + 290
                    }
                    if(self.newReleases.count == 0 ){
                        self.viewNewReleases.isHidden = true
                    }
                    else {
                        totalStackHeight = totalStackHeight + 290
                    }
                    if(self.allTimeHits.count == 0 ){
                        self.viewAllTimeHits.isHidden = true
                    }
                    else {
                        totalStackHeight = totalStackHeight + 290
                    }
                    if(self.recentlyPlayed.count == 0 ){
                        self.viewRecentlyPlayed.isHidden = true
                    }
                    else {
                        totalStackHeight = totalStackHeight + 290
                    }
                    if(self.devotions.count == 0 ){
                        self.viewDevotional.isHidden = true
                    }
                    else {
                        totalStackHeight = totalStackHeight + 270
                    }
                    
                    if(self.comedy.count == 0 ){
                                         self.viewComedy.isHidden = true
                                     }
                                     else {
                                         totalStackHeight = totalStackHeight + 270
                                     }
                    if(self.celebrityPlaylists.count == 0 ){
                        self.viewCelebrityPlaylists.isHidden = true
                    }
                    else {
                        totalStackHeight = totalStackHeight + 290
                    }
        
                   self.scrollView.contentSize = CGSize(width: self.view.frame.width, height: CGFloat(totalStackHeight))
                    
                }
                
                
                
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                     statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.loadDiscoverContent()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    //only show error view when there's no data on DB
                    if(self.heroes.count == 0)
                    {
                        self.showErrorView(errorCode: statusCode)
                    }
                }
            }
        }
        
        
    }
    
    
    //MARK: - Realm Delete Methods
    
    func deleteHeroes(){
        
        do {
            let realm = try Realm()
            
            let heroes = realm.objects(Heroe.self)
            try realm.write {
                realm.delete(heroes)
            }
        }
        catch let error as NSError {
            
            print(error)
            
        }
    }
    
    //MARK: - Realm Write Methods
    
    
    
    
    func writeHeroes( heroes : [Object]){
        
        for heroe in heroes {
            
            do {
                let realm = try Realm()
                try realm.write {
                    
                    realm.add(heroe,update: .modified)
                    
                    
                }
                
                
            } catch let error as NSError {
                
                print(error)
                
            }
            
        }
        
    }
    
    
    
    func writeReleases(  releases : Object){
        
        
        do {
            let realm = try Realm()
            try realm.write {
                
                realm.add(releases,update: .modified)
                
            }
        } catch let error as NSError {
            
            
            print(error)
            
        }
        
        
    }
    
    func writePopular(  popular : Object){
        
        
        do {
            let realm = try Realm()
            try realm.write {
                
                realm.add(popular,update: .modified)
                
            }
        } catch let error as NSError {
            
            
            print(error)
            
        }
        
        
    }
    
    func writePlaylists(  playlists : [Object]){
        
        
        do {
            let realm = try Realm()
            try realm.write {
                
                realm.add(playlists,update: .modified)
                
            }
        } catch let error as NSError {
            
            print(error)
            
        }
        
    }
    
    func writeRecommended(  recommended : Object){
        
        
        do {
            let realm = try Realm()
            try realm.write {
                
                realm.add(recommended,update: .modified)
                
            }
        } catch let error as NSError {
            
            
            print(error)
            
        }
        
        
    }
    
    func writeTrending(  trending : Object){
        
        
        do {
            let realm = try Realm()
            try realm.write {
                
                realm.add(trending,update: .modified)
                
            }
        } catch let error as NSError {
            
            
            print(error)
            
        }
        
        
    }
    
    func writeRecent(  recent : Object){
        
        
        do {
            let realm = try Realm()
            try realm.write {
                
                realm.add(recent,update: .modified)
                
            }
        } catch let error as NSError {
            
            
            print(error)
            
        }
        
        
    }
    
    func writeCharts(  charts : [Object]){
        
        
        do {
            let realm = try Realm()
            try realm.write {
                
                realm.add(charts,update: .modified)
                
            }
        } catch let error as NSError {
            
            
            print(error)
            
        }
        
    }
    func writeDevotions(  devotions : [Object]){
        
        
        do {
            let realm = try Realm()
            try realm.write {
                
                realm.add(devotions,update: .modified)
                
            }
        } catch let error as NSError {
            
            
            print(error)
            
        }
        
    }
    
    func writeComedy( comedies : [Object]){
           
           
           do {
               let realm = try Realm()
               try realm.write {
                   
                   realm.add(comedies,update: .modified)
                   
               }
           } catch let error as NSError {
               
               
               print(error)
               
           }
           
       }
    func writeCelebrityPlaylists(  celebrityPlaylists : [Object]){
        
        
        do {
            let realm = try Realm()
            try realm.write {
                
                realm.add(celebrityPlaylists,update: .modified)
                
            }
        } catch let error as NSError {
            
            
            print(error)
            
        }
        
    }
    
    //MARK: - Realm Fetch And Set Methods
    
    func fetchRealmObjects()  {
        
        
        
        //empty the array variables
        
        heroes.removeAll()
        recommended.removeAll()
        artists.removeAll()
        trending.removeAll()
        recentlyPlayed.removeAll()
        charts.removeAll()
        playlists.removeAll()
        newReleases.removeAll()
        allTimeHits.removeAll()
        celebrityPlaylists.removeAll()
        devotions.removeAll()
        recentlyPlayed.removeAll()
        comedy.removeAll()
        
        let heroesResults: Results<Heroe>? = realm.objects(Heroe.self)
        let releasesResults: Results<Releases>? = realm.objects(Releases.self)
        let popularResults: Results<Popular>? = realm.objects(Popular.self)
        //let artistsResults: Results<Artist>? = realm.objects(Artist.self)
        let playlistsResults: Results<Playlist>? = realm.objects(Playlist.self)
        let recommendedResults: Results<Recommended>? = realm.objects(Recommended.self)
        let trendingResults: Results<Trending>? = realm.objects(Trending.self)
        let recentResults: Results<Recent>? = realm.objects(Recent.self)
        let chartsResults: Results<Chart>? = realm.objects(Chart.self)
        let celebrityPlaylistsResults: Results<CelebrityPlaylist>? = realm.objects(CelebrityPlaylist.self)
        let devotionsResults: Results<Devotion>? = realm.objects(Devotion.self)
        let comedyResults: Results<Comedy>? = realm.objects(Comedy.self)
        let recentlyPlayedResults: Results<RecentlyPlayed>? = realm.objects(RecentlyPlayed.self)
        
        for heroesResult in heroesResults! {
            
            heroes.append(heroesResult)
            
        }
        
        //get latest 6 heroes
        heroes = Array(heroes.suffix(6))
        //inverse to get latest first :)
        //heroes.reverse()
        
        for recommendedResult in recommendedResults! {
            recommended.append(contentsOf: recommendedResult.tracks)
        }
        for releaseResult in releasesResults! {
            recommended.append(contentsOf: releaseResult.tracks)
        }
        for popularResult in popularResults! {
            artists.append(contentsOf: popularResult.artists)
        }
        for playlistResult in playlistsResults! {
            playlists.append(playlistResult)
        }
        for trendingResult in trendingResults! {
            trending.append(contentsOf: trendingResult.tracks)
        }
        //        for recentlyPlayedResult in recentlyPlayedResults! {
        //            recentlyPlayed.append(recentlyPlayedResult.tracks)
        //        }
        for chartsResult in chartsResults! {
            charts.append(chartsResult)
        }
        for release in releasesResults! {
            newReleases.append(contentsOf: release.tracks)
        }
        for recent in recentResults! {
            allTimeHits.append(contentsOf: recent.tracks)
        }
        for recentlyPlayed in recentlyPlayedResults! {
            self.recentlyPlayed.append(recentlyPlayed.track!)
        }
        for celebrityPlaylist in celebrityPlaylistsResults! {
            celebrityPlaylists.append(celebrityPlaylist)
        }
        for devotion in devotionsResults! {
            devotions.append(devotion)
        }
        
        for comedyObj in comedyResults! {
            self.comedy.append(comedyObj)
              }
        
        cvHeroes.reloadData()
        cvNewReleases.reloadData()
        cvRecommended.reloadData()
        cvArtists.reloadData()
        cvCharts.reloadData()
        cvPlaylists.reloadData()
        cvNewReleases.reloadData()
        cvAllTime.reloadData()
        cvRecentlyPlayed.reloadData()
        cvDevotions.reloadData()
        cvCelebrityPlaylists.reloadData()
        cvComedy.reloadData()
        
        //initialize the page control (getting data from realm at this point)
        
        pageControl.numberOfPages = heroes.count
        pageControl.currentPage = 0
        
    }
    
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
        
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
        
    }
    
    @objc func showMoreTapped(_ sender: showMoreTapGesture) {
        
        showMoreTypeSelected = sender.showMoreTypeSelected
        
        //get type of item selected
        
        switch showMoreTypeSelected {
        case "New Releases":
            performSegue(withIdentifier: "showMoreTracks", sender: showMoreTypeSelected)
        case "Playlists":
            performSegue(withIdentifier: "showMorePlaylistsOrCharts", sender: showMoreTypeSelected)
        case "Charts":
            performSegue(withIdentifier: "showMorePlaylistsOrCharts", sender: showMoreTypeSelected)
        case "Artists":
            performSegue(withIdentifier: "showMoreArtists", sender: showMoreTypeSelected)
        case "Recommended":
            performSegue(withIdentifier: "showMoreTracks", sender: showMoreTypeSelected)
        case "All Time Radio Hits":
            performSegue(withIdentifier: "showMoreTracks", sender: showMoreTypeSelected)
        case "Recents":
            performSegue(withIdentifier: "showMoreTracks", sender: showMoreTypeSelected)
        case "Devotions":
            performSegue(withIdentifier: "showMorePlaylistsOrCharts", sender: showMoreTypeSelected)
        case "Comedy":
                       performSegue(withIdentifier: "showMoreArtists", sender: showMoreTypeSelected)
        case "Celebrity Playlists":
            performSegue(withIdentifier: "showMorePlaylistsOrCharts", sender: showMoreTypeSelected)
        default:
            //do nothing
            break
        }
        
        
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var tracks = Array<Track>()
        var currrentPlayingIndex = Int()
    }
    class showMoreTapGesture: UITapGestureRecognizer {
        var showMoreTypeSelected = String()
    }
    
    @objc func retry(){
        self.dismissErrorView()
        loadDiscoverContent()
    }
    
    @objc func updateNotificationsCount(){
        self.dismissErrorView()
        loadDiscoverContent()
    }
    
 }
 
 
 extension DiscoverViewController :  UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    //TODO- Achieve infinite scrolling for heroes below code form
    
    //https://stackoverflow.com/questions/34396108/how-to-implement-horizontally-infinite-scrolling-uicollectionview
    
//    10
//
//    Apparently the closest to good solution was proposed by the Manikanta Adimulam. The cleanest solution would be to add the last element at the beginning of the data list, and the first one to the last data list position (ex: [4] [0] [1] [2] [3] [4] [0]), so we scroll to the first array item when we are triggering the last list item and vice versa. This will work for collection views with one visible item:
//
//    Subclass UICollectionView.
//    Override UICollectionViewDelegate and override the following methods:
//
//    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        let numberOfCells = items.count
//        let page = Int(scrollView.contentOffset.x) / Int(cellWidth)
//        if page == 0 { // we are within the fake last, so delegate real last
//            currentPage = numberOfCells - 1
//        } else if page == numberOfCells - 1 { // we are within the fake first, so delegate the real first
//            currentPage = 0
//        } else { // real page is always fake minus one
//           currentPage = page - 1
//        }
//        // if you need to know changed position, you can delegate it
//        customDelegate?.pageChanged(currentPage)
//    }
//
//
//    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let numberOfCells = items.count
//        if numberOfCells == 1 {
//            return
//        }
//        let regularContentOffset = cellWidth * CGFloat(numberOfCells - 2)
//        if (scrollView.contentOffset.x >= cellWidth * CGFloat(numberOfCells - 1)) {
//            scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x - regularContentOffset, y: 0.0)
//        } else if (scrollView.contentOffset.x < cellWidth) {
//            scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x + regularContentOffset, y: 0.0)
//        }
//    }
//    Override layoutSubviews() method inside your UICollectionView in order to always to make a correct offset for the first item:
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
//        let numberOfCells = items.count
//        if numberOfCells > 1 {
//            if contentOffset.x == 0.0 {
//                contentOffset = CGPoint(x: cellWidth, y: 0.0)
//            }
//        }
//    }
//    Override init method and calculate your cell dimensions:
//
//    let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
//    cellPadding = layout.minimumInteritemSpacing
//    cellWidth = layout.itemSize.width
    
    
    
    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        var size : CGSize? = nil
        
        
        switch collectionView {
        case self.cvHeroes:
            
            let collectionViewWidth = collectionView.bounds.width
            let collectionViewHeight = collectionView.bounds.height
            
            size = CGSize(width: collectionViewWidth, height: collectionViewHeight)
            
        case self.cvNewReleases:
            
            size = CGSize(width: 160, height: 217)
            
        case self.cvPlaylists:
            
            size = CGSize(width: 160, height: 194)
            
        case self.cvCharts:
            
            size = CGSize(width: 160, height: 194)
        case self.cvCelebrityPlaylists:
            
            size = CGSize(width: 160, height: 194)
        case self.cvDevotions:
            
            size = CGSize(width: 160, height: 194)
            
            case self.cvComedy:
                       
                       size = CGSize(width: 160, height: 194)
        case self.cvRecentlyPlayed:
            
            size = CGSize(width: 160, height: 217)
        case self.cvArtists:
            
            size = CGSize(width: 160, height: 194)
       
        case self.cvRecommended:
            
            size = CGSize(width: 160, height: 217)
        case self.cvAllTime:
            
            size = CGSize(width: 160, height: 217)
            
        default:
            break
        }
        
        return size!
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if(collectionView == self.cvHeroes){
            
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
            
        else {
            return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if(collectionView == self.cvHeroes){
            return 0
        }
        else {
            return 5
        }
    }
    
    
    
    //MARK: - Collectionview Delegate Methods
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case self.cvHeroes:
            
            return heroes.count
            
        case self.cvNewReleases:
            //get latest 10
            return   Array(newReleases.suffix(10)).count
            
        case self.cvPlaylists:
            return Array(playlists.suffix(10)).count
            
        case self.cvCharts:
            return Array(charts.suffix(10)).count
            
        case self.cvArtists:
            return Array(artists.suffix(10)).count
            
            
        case self.cvRecommended:
            return Array(recommended.suffix(10)).count
            
        case self.cvRecentlyPlayed:
            return Array(recentlyPlayed.suffix(10)).count
            
        case self.cvAllTime:
            return Array(allTimeHits.suffix(10)).count
            
        case self.cvDevotions:
            return Array(devotions.suffix(10)).count
            
            case self.cvComedy:
                       return Array(comedy.suffix(10)).count
            
        case self.cvCelebrityPlaylists:
            return Array(celebrityPlaylists.suffix(10)).count
            
            
        default:
            
            return  0
            
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView == self.cvHeroes {
            self.pageControl.currentPage = indexPath.row
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var returnedCell: UICollectionViewCell? = nil
      
        
        switch collectionView {
            
            
        case self.cvHeroes:
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "heroCell", for: indexPath) as! HeroCell
            
            let url : String
            
            if ( heroes[indexPath.row].artwork.count > 0){
                
                url = heroes[indexPath.row].artwork[0].original
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"), completed: {
                (image, error, cacheType, url) in
                // your code
                //if light theme set three gradient colors since it looks better that way
                (ThemeManager.currentTheme() == .darkTheme) ? cell.ivCover.addGradientLayer(frame: cell.ivCover.layer.frame, colors: [UIColor.clear , ThemeManager.currentTheme().secondaryColor]) : cell.ivCover.addGradientLayer(frame: cell.ivCover.layer.frame, colors: [UIColor.clear ,ThemeManager.currentTheme().secondaryColor])
            })
            //
            cell.lblTag.text = heroes[indexPath.row].tag
            cell.lblArtist.text = heroes[indexPath.row].name
            cell.lblTag.textColor = UIColor.colorAccent
            
            if(ThemeManager.currentTheme() == .darkTheme) { cell.lblArtist.textColor = .white }
            else { cell.lblArtist.textColor = ThemeManager.currentTheme().textColor}
            
            var tracks = Array<Track>()
            
            for track in heroes[indexPath.row].tracks{
                tracks.append(track)
            }
            
            
            let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
            
            tappy.tracks =  tracks
            tappy.currrentPlayingIndex =  0
            
            //cell.btnPlay.addGestureRecognizer(tappy)
            cell.addGestureRecognizer(tappy)
            
            returnedCell = cell
            
            
        case self.cvNewReleases:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newReleaseCell", for: indexPath) as! NewReleaseCell
            
            
            let url : String
            
            if ( newReleases[indexPath.row].artwork.count > 0){
                
                url = newReleases[indexPath.row].artwork[0].medium
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblArtist.text = newReleases[indexPath.row].artistName
            cell.lblArtist.textColor = ThemeManager.currentTheme().textColorSecondary
            cell.lblTitle.text = newReleases[indexPath.row].title
            
            //adding a corner radius to the cell
            cell.layer.cornerRadius = 5.0
            cell.layer.masksToBounds = true
            
            var tracks = Array<Track>()
            
            tracks.append(contentsOf:newReleases)
            
            let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
            
            tappy.tracks =  tracks
            tappy.currrentPlayingIndex =  indexPath.row
            
            cell.addGestureRecognizer(tappy)
            
            returnedCell = cell
            
            
        case self.cvPlaylists:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "playlistCell", for: indexPath) as! PlaylistCell
            
            let url : String
            
            if ( playlists[indexPath.row].artwork.count > 0){
                
                url = playlists[indexPath.row].artwork[0].medium
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = playlists[indexPath.row].name
            
            //adding a corner radius to the cell
            cell.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            
            
            
            returnedCell = cell
            
            
        case self.cvRecommended:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recommendedCell", for: indexPath) as! RecommendedCell
            
            let url : String
            
            print(indexPath.row)
            
            
            if ( recommended[indexPath.row].artwork.count > 0){
                
                url = recommended[indexPath.row].artwork[0].medium
                
            }
            else
            {
                //might be a blank arrayfpage
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblArtist.text = recommended[indexPath.row].artistName
            cell.lblArtist.textColor = ThemeManager.currentTheme().textColorSecondary
            cell.lblTitle.text = recommended[indexPath.row].title
            
            //adding a corner radius to the cell
            cell.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            
            var tracks = Array<Track>()
            tracks.append(contentsOf:recommended)
            
            let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
            
            tappy.tracks =  tracks
            tappy.currrentPlayingIndex =  indexPath.row
            
            cell.addGestureRecognizer(tappy)
            
            returnedCell = cell
            
            
            
        case self.cvCharts:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "chartCell", for: indexPath) as! ChartCell
            
            let url : String
            
            if ( charts[indexPath.row].artwork.count > 0){
                
                url = charts[indexPath.row].artwork[0].medium
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = charts[indexPath.row].name
            
            //adding a corner radius to the cell
            cell.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            
            returnedCell = cell
            
            
        case self.cvRecentlyPlayed:
            
            
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recentlyPlayedCell", for: indexPath) as! TrackCell
            
            
            let url : String
            
            if ( recentlyPlayed[indexPath.row].artwork.count > 0){
                
                url = recentlyPlayed[indexPath.row].artwork[0].medium
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblArtist.text = recentlyPlayed[indexPath.row].artistName
            cell.lblArtist.textColor = ThemeManager.currentTheme().textColorSecondary
            cell.lblTitle.text = recentlyPlayed[indexPath.row].title
            
            //adding a corner radius to the cell
            cell.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            
            var tracks = Array<Track>()
            
            tracks.append(contentsOf:recentlyPlayed)
            
            let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
            
            tappy.tracks =  tracks
            tappy.currrentPlayingIndex =  indexPath.row
            
            cell.addGestureRecognizer(tappy)
            
            returnedCell = cell
            
            
        case self.cvArtists:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "artistCell", for: indexPath) as! ArtistCell
            
            
            let url : String
            
            if ( artists[indexPath.row].artwork.count > 0){
                
                url = artists[indexPath.row].artwork[0].medium
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblName.text = artists[indexPath.row].name
            
            //adding a corner radius to the cell
            cell.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            
            
            returnedCell = cell
            
            
         
        case self.cvCelebrityPlaylists:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "celebrityPlaylistCell", for: indexPath) as! ArtistCell
            
            
            let url : String
            
            if ( celebrityPlaylists[indexPath.row].artwork.count > 0){
                
                url = celebrityPlaylists[indexPath.row].artwork[0].medium
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblName.text = celebrityPlaylists[indexPath.row].name
            
            //adding a corner radius to the cell
            cell.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            
            
            returnedCell = cell
            
        case self.cvDevotions:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "devotionCell", for: indexPath) as! ArtistCell
            
            let url : String
            
            if ( devotions[indexPath.row].artwork.count > 0){
                url = devotions[indexPath.row].artwork[0].original
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblName.text = devotions[indexPath.row].title
            
            //adding a corner radius to the cell
            cell.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            
            returnedCell = cell
                
            case self.cvComedy:
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "comedyCell", for: indexPath) as! ArtistCell
                
                let url : String
                
                if ( comedy[indexPath.row].artwork.count > 0){
                    url = comedy[indexPath.row].artwork[0].original
                }
                else
                {
                    //might be a blank array
                    url = "no_cover"
                    
                }
                
                //set the genre image using sd web image library asycnhronously
                
                cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
                cell.lblName.text = comedy[indexPath.row].name
                
                //adding a corner radius to the cell
                cell.layer.cornerRadius = 5
                cell.layer.masksToBounds = true
                
                returnedCell = cell
            
            
            
        case self.cvAllTime:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "allTimeCell", for: indexPath) as! AllTimeCell
            
            
            
            let url : String
            
            if ( allTimeHits[indexPath.row].artwork.count > 0){
                
                url = allTimeHits[indexPath.row].artwork[0].medium
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblArtist.text = allTimeHits[indexPath.row].artistName
            cell.lblArtist.textColor = ThemeManager.currentTheme().textColorSecondary
            cell.lblTitle.text = allTimeHits[indexPath.row].title
            
            //adding a corner radius to the cell
            cell.layer.cornerRadius = 5
            cell.layer.masksToBounds = true
            
            
            
            var tracks = Array<Track>()
            tracks.append(contentsOf: allTimeHits)
            
            let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
            
            tappy.tracks =  tracks
            tappy.currrentPlayingIndex =  indexPath.row
            cell.addGestureRecognizer(tappy)
            
            returnedCell = cell
            
            
        default:
            break
            
            
        }
        
        return returnedCell!
    }
    
    
    
 }
