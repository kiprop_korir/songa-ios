//
//  ShowMoreArtistsViewController.swift
//  Songa
//
//  Created by Collins Korir on 10/22/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class ShowMoreArtistsViewController: BaseViewController {

    @IBOutlet weak var cvArtists: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
    let artistsAPI = APIurls.showMoreArtists
    let headers = APIurls.Headers
    
    var artists: [Artist] = []
    var charts: [Chart] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        cvArtists.delegate = self
        cvArtists.dataSource = self

        activityIndicator.startAnimating()
        loadArtists()
         self.navigationItem.title = "More Artists"
    }


    //MARK: - Load Genres From API
    
    func loadArtists()  {
        
        
            Alamofire.request(artistsAPI, method: .get,  headers: headers).responseArray { (response: DataResponse<[Artist]>) in
                
                print(self.artistsAPI)
                
                let artistResponse = response.result.value
                
                if let artists = artistResponse {
                    
                    
                    for artist in artists
                        
                    {
                        self.artists.append(artist)
                        
                    }
                    self.cvArtists.reloadData()
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                }
      
            }
    
}
    
}

extension ShowMoreArtistsViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method
    
    
    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size : CGSize? = nil
 
            let padding: CGFloat =  20
            let collectionViewWidth = collectionView.frame.size.width - padding
            
            size = CGSize(width: collectionViewWidth, height: 75)
  
        return size!
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }
    

    
    //MARK: - Collectionview Delegate Methods
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return artists.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "trackCell", for: indexPath) as! ArtistCell
        
        let url : String
        
        if ( artists[indexPath.row].artwork.count > 0){
            
            url =  artists[indexPath.row].artwork[0].medium
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        //set the genre image using sd web image library asycnhronously
        
        cell.backgroundColor = ThemeManager.currentTheme().secondaryColor
        cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        //cell.ivTrackCover.sizeToFit()
        cell.lblName.text = artists[indexPath.row].name
        cell.lblIndex.text = String (indexPath.row + 1)
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
//        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
//
//        tappy.tracks =  tracks
//        tappy.currrentPlayingIndex =  0
//
//        cell.addGestureRecognizer(tappy)
        
        return cell
    }
    

}
