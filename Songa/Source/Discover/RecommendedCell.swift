//
//  GenreCell.swift
//  Songa
//
//  Created by Collins Korir on 7/23/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import SDWebImage

class RecommendedCell: UICollectionViewCell {


    @IBOutlet weak var lblTitle: UILabel!

    @IBOutlet weak var lblArtist: UILabel!

    @IBOutlet weak var ivCover: UIImageView!

}
