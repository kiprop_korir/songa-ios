//
//  MyProfileLoaderViewController.swift
//  Songa
//
//  Created by Collins Korir on 1/19/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

class MyProfileLoaderViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
            //load MyProfile Storyboard
            
            let storyBoard = UIStoryboard(name: "Profile", bundle: nil)
            let controller = storyBoard.instantiateInitialViewController()
            addChild(controller!)
            view.addSubview((controller?.view)!)
            controller?.didMove(toParent: self)
    
    }
}
