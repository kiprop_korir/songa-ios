//
//  MyMusicViewController.swift
//  Songa
//
//  Created by Collins Korir on 6/12/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit

class MyMusicLoaderViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //load MyMusic Storyboard

        let fmStoryBoard = UIStoryboard(name: "MyMusic", bundle: nil)
        let controller =  fmStoryBoard.instantiateInitialViewController()
        addChild(controller!)
        view.addSubview((controller?.view)!)
        controller?.didMove(toParent: self)

    }



}
