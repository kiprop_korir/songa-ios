//
//  DiscoverViewController.swift
//  Songa
//
//  Created by Collins Korir on 6/12/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import RealmSwift

class GenresLoaderViewController: UIViewController{

   override func viewDidLoad() {
        super.viewDidLoad()


        // Do any additional setup after loading the view.

        //load Discover Storyboard

        let fmStoryBoard = UIStoryboard(name: "Genres", bundle: nil)
        let controller =  fmStoryBoard.instantiateInitialViewController()
            addChild(controller!)
        view.addSubview((controller?.view)!)
            controller?.didMove(toParent: self)

    }
}
