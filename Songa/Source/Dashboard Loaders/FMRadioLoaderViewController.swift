//
//  FMRadioViewController.swift
//  Songa
//
//  Created by Collins Korir on 6/11/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit

class FMRadioLoaderViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //load Discover Storyboard

        let fmStoryBoard = UIStoryboard(name: "RadioAndPodcasts", bundle: nil)
        let controller =  fmStoryBoard.instantiateInitialViewController()
        addChild(controller!)
        view.addSubview((controller?.view)!)
        controller?.didMove(toParent: self)


    }

}
