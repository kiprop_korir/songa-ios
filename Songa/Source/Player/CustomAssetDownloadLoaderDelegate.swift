
//
//  CustomAssetDownloadLoaderDelegate.swift
//  SPLPlayer
//
//  Created by Sean Gray on 12/8/17.
//

import UIKit
import AVFoundation
import AVKit

class CustomAssetDownloadLoaderDelegate: CustomAssetLoaderDelegate {
    var assetID : String? = nil;

    func getKeySaveLocation(_ assetId:String) -> URL {
        let persistantPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        return URL(fileURLWithPath:persistantPathString!+"/"+assetId)
    }

    func returnLocalKey(request:AVAssetResourceLoadingRequest,context:Data) -> Bool {
        guard let contentInformationRequest = request.contentInformationRequest else {
            print("contentInformationError")
            return false

        }
        contentInformationRequest.contentType = AVStreamingKeyDeliveryPersistentContentKeyType
        request.dataRequest!.respond(with: context)
        request.finishLoading()
        return true;
        
    }

    override func removeKey(fileManager: FileManager){
            guard let assetID = self.assetID else { return }
            if fileManager.fileExists(atPath: getKeySaveLocation(assetID).path) {
        do {
                    try fileManager.removeItem(at: self.getKeySaveLocation(assetID))
                }
                catch {
                    print("Key removal Error \(error)")
        }
        }
    }
    override func resourceLoader(_ resourceLoader: AVAssetResourceLoader,
                                 shouldWaitForLoadingOfRequestedResource loadingRequest:AVAssetResourceLoadingRequest) -> Bool{
        let assetURI: NSURL = loadingRequest.request.url! as NSURL;
        
        //if let assetIdentifier = assetURI.parameterString {
       // self.assetID = assetIdentifier
     //   self.assetID =  assetURI.parameterString!
        
        if let assetID = assetURI.parameterString {
            self.assetID = assetID
        }
        else {
           return false
        }
        let scheme:String = assetURI.scheme!
        if (!(scheme == "skd")){
            return false;
        }
        do {
            let persistentContentKeyContext = try Data(contentsOf:getKeySaveLocation(assetID!))
            return returnLocalKey(request:loadingRequest,context:persistentContentKeyContext)
        }
        catch {
            if !Reachability.isConnectedToNetwork() {
                return false;
            }
            var requestBytes:Data? = nil;
            var certificate: Data? = nil;
            NSLog("assetId:  %@", assetID!);
            do{
                certificate = try getAppCertificate(assetId: assetID!)
            }
            catch {
                loadingRequest.finishLoading(with: NSError(domain:NSURLErrorDomain,code:NSURLErrorClientCertificateRejected, userInfo:nil))
            }
            do{
                requestBytes = try loadingRequest.streamingContentKeyRequestData(
                    forApp: certificate!,
                    contentIdentifier: assetID!.data(using: String.Encoding.utf8)!,
                    options: [AVAssetResourceLoadingRequestStreamingContentKeyRequestRequiresPersistentKey: true])
            }
            catch{
                loadingRequest.finishLoading(with:error)
                return true;
            }

            let passthruParams: String = "?userId=281807fc-a59f-44f2-b997-0858b2130fcb";
            var responseData: Data? = nil;
            let error: Error? = nil;

            responseData = getContentKeyAndLeaseExpiryfromKeyServerModuleWithRequest(requestBytes: requestBytes!,
                                                                                     assetId: assetID!,
                                                                                     customParams: passthruParams,
                                                                                     errorOut: error)
            if ( responseData != nil){
                let dataRequest: AVAssetResourceLoadingDataRequest = loadingRequest.dataRequest!;
                do {
                    let persistantContentKeyContext = try loadingRequest.persistentContentKey(fromKeyVendorResponse: responseData!, options: nil)
                    try persistantContentKeyContext.write(to: getKeySaveLocation(assetID!), options: .atomic)
                    guard let contentInformationRequest = loadingRequest.contentInformationRequest else {
                        print("contentInformationError")
                        return false

                    }
                    contentInformationRequest.contentType = AVStreamingKeyDeliveryPersistentContentKeyType
                    dataRequest.respond(with: persistantContentKeyContext)
                }
                catch {

                    print("Error info: \(error)")
                    return false;
                }
                loadingRequest.finishLoading()
            }
            else{
                loadingRequest.finishLoading(with:error);
            }
           // }
        }
//            else {
//                         NotificationCenter.default.post(name: Notification.Name.playbackError,
//                                                                                                   object: nil,
//                                                                                                   userInfo:["description": "Error downloading track"])
//                 }

            return true;
        
    }

    override func resourceLoader(_ resourceLoader: AVAssetResourceLoader, shouldWaitForRenewalOfRequestedResource renewalRequest: AVAssetResourceRenewalRequest) -> Bool {
        print("shouldWaitForRenewalOfRequestedResource")
        return self.resourceLoader(resourceLoader, shouldWaitForLoadingOfRequestedResource: renewalRequest)
    }

}
