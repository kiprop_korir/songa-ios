//
//  AudioManager.swift
//  Songa
//
//  Created by Collins Korir on 11/20/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import RealmSwift
import AVKit;
import MediaPlayer
import SDWebImage

class AudioManager:NSObject {

   static var shared = AudioManager()
    
    private let AVPlayerTestPlaybackViewControllerStatusObservationContext: UnsafeMutableRawPointer? = UnsafeMutableRawPointer.init(mutating:nil);
       
    //Apple presumes that all conections require authorization and a credential space. If we don't define this and register in viewDidLoad then we would get an error saying that the app
    //Can't find credentials for the connection. This error dosn't effect anything if using HTTP, but might need to be implemented based on authentication on your media server
    let protectionSpace = URLProtectionSpace.init(host: "licenses.digitalprimates.net",
                                                     port: 80,
                                                     protocol: "http",
                                                     realm: nil,
                                                     authenticationMethod: nil)
    let userCredential = URLCredential(user: "",
                                          password: "",
                                          persistence: .permanent)
       
      
    let ezrdmKeyServerURL = APIurls.EZDRM
    var mediaServerEndpoint : String = ""
    var currentPlayingTrackIndex : Int = 0
    var player:AVPlayer? = AVPlayer ()
    var playerItem:AVPlayerItem? = nil
    var playerQueue:AVQueuePlayer? = nil
    var loaderDelegate:CustomAssetLoaderDelegate? = nil
    var asset: AVURLAsset? = nil
    var session: AVAssetDownloadURLSession? = nil
    let PLAYABLE_KEY:String = "playable"
    let STATUS_KEY:String = "status"
    
    var isPodcast:Bool = false
    var isFmRadioStream:Bool = false
    var repeatOn:Bool = false
    var shuffleOn:Bool = false

    var repeatedTimes : Int = 0
    var tracks: [Track] = []
    var station: Station? = nil
    var episodes: [PodcastEpisode] = []
    let realm = try! Realm()
    
    
 override public init() {
    super.init()
    
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(playNextTrack),
                                           name: Notification.Name.playNextClicked,
                                           object: nil)
    
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(playPreviousTrack),
                                           name: Notification.Name.playPreviousClicked,
                                           object: nil)
    
    
    NotificationCenter.default.addObserver(self,
                                             selector: #selector(setSongDuration(_:)),
                                             name: Notification.Name.sliderValueChanged,
                                             object: nil)
    
    setupRemoteCommandCenter()
    
    
     }

    @objc func pausePlay(){
        if(player != nil){
        self.player?.pause()
        }
    }
    @objc func resumePlay(){
         if(player != nil){
        self.player?.play()
        }
    }
    
    func startRadioStationPlay(station:Station){
               
        self.station  = station
        unload()
        isFmRadioStream = true
        isPodcast = false
                //start radio stream
        self.playerItem = AVPlayerItem.init(url: URL(string:self.station?.stream ?? "")!)
        self.player = AVPlayer.init(playerItem: self.playerItem)
        self.player?.play()
        
        let url : String
                if ( station.artwork.count > 0){
                    url = station.artwork[0].small
                }
                else
                {    url = "no_cover"
                }
        
        setupNowPlaying(title: station.title, artist: station.name, imageUrl: url)
        
    }
            

   
    func startPlay(tracks: [Track],currentPlayingTrackIndex: Int ){
        
        
        isPodcast = false
           isFmRadioStream = false
           self.tracks = tracks
           self.currentPlayingTrackIndex = currentPlayingTrackIndex
        
       initPlay()
         
     
    }

    
 
    func startPodcastPlay(episodes: [PodcastEpisode],currentPlayingTrackIndex: Int){
        unload()
        isFmRadioStream = false
        isPodcast = true
        self.episodes = episodes
        self.currentPlayingTrackIndex = currentPlayingTrackIndex
        let  streamURL = URL.init(string: (episodes[currentPlayingTrackIndex].audio))
        self.playerItem = AVPlayerItem.init(url: streamURL!)
        self.player = AVPlayer.init(playerItem: self.playerItem)
        self.player?.play()
        
    }
    
    func initPlay(){
        
         let url : String
                if ( tracks[currentPlayingTrackIndex].artwork.count > 0){
                                url = tracks[currentPlayingTrackIndex].artwork[0].small
                            }
                            else
                            {    url = "no_cover"
                            }
                    
                setupNowPlaying(title: tracks[currentPlayingTrackIndex].title, artist: tracks[currentPlayingTrackIndex].artistName, imageUrl: url)
              
        if(currentPlayingTrackIndex < tracks.count && currentPlayingTrackIndex >= 0) {
            
            mediaServerEndpoint = constructStreamingURL(track: tracks[currentPlayingTrackIndex])
            
            if(self.player != nil){
                      print("does has asset")
                       unload()
            }
            
            print(mediaServerEndpoint)
            //check if file is downloaded

            if(!isTrackDownloaded(trackID: tracks[currentPlayingTrackIndex].id)){
                 initOnlineURLAsset(urlString: mediaServerEndpoint)
                 }
            else {
                 initOfflineURLAsset(track: getDownloadedTrack(trackID: tracks[currentPlayingTrackIndex].id))
            }
          
           readyMediaStream()
        }
    }
    
    /* -----------------------------------------
     ** playMediaStream
     **
     ** checks for if the asset is loaded and playable
     ** if so it starts playback if not it throws error
     ** ----------------------------------------*/
    //attempts to create a new AVURLAsset then checks for when the asset is loaded to start the playback initialization.
    
    func readyMediaStream(){
        let requestedKeys:Array<String> = [PLAYABLE_KEY];
        //load the value of playable and execute code block with result
        if let asset = self.asset {
            
            DispatchQueue.main.async {
                
            asset.loadValuesAsynchronously(forKeys: requestedKeys, completionHandler: ({
                () -> Void in
                var error: NSError? = nil
                switch self.asset!.statusOfValue(forKey: self.PLAYABLE_KEY, error: &error){
                case .loaded:
                    if(self.asset!.isPlayable){
                        
                                               print(requestedKeys)
                                               self.initPlay(asset:self.asset!,keys:requestedKeys)
                                            }
                                            else {
                                                print("Not Playable")
                        
                                               NotificationCenter.default.post(name: Notification.Name.playbackError,
                                                                               object: nil,
                                                                                userInfo:["description": "This track is not playable"])
                        
                                           }
                                        case .failed:
                                            NotificationCenter.default.post(name: Notification.Name.playbackError,
                                                                            object: nil,
                                                                            userInfo:["description": "Track file not found"])
                    
                    
                                        case .cancelled:
                                            NotificationCenter.default.post(name: Notification.Name.playbackError,
                                                                            object: nil,
                                                                            userInfo:["description": "Loading cancelled"])
                                        default:
                                            NotificationCenter.default.post(name: Notification.Name.playbackError,
                                                                            object: nil,
                                                                            userInfo:["description": "Loading error unknown"])
                                        }
            }))
        }
        }
    }
    
    //makes the asset preperation calls Async so other actions can occur at the same time
    func initPlay(asset: AVURLAsset, keys: Array<String>){
        DispatchQueue.main.async(execute: {() -> Void in
            /* IMPORTANT: Must dispatch to main queue in order to operate on the AVPlayer and AVPlayerItem. */
            self.prepareToPlayAsset(asset:asset, requestedKeys: keys)
        })
    }
    
    /* --------------------------------------------------------------
        **
        **  prepareToPlayAsset:withKeys
        **
        **  Invoked at the completion of the loading of the values for all
        **  keys on the asset that we require. Checks whether loading was
        **  successfull and whether the asset is playable. If so, sets up
        **  an AVPlayerItem and an AVPlayer to play the asset.
        **
        ** ----------------------------------------------------------- */
       
       func prepareToPlayAsset(asset: AVURLAsset,requestedKeys:Array<String>){
           
           // if we are already have an AVPlayer Item stop listening to it
           if(self.playerItem != nil){
               self.playerItem?.removeObserver(self, forKeyPath: STATUS_KEY)
           }
           // Create a new AVPlayerItem with the given AVURLAsset
           self.playerItem = AVPlayerItem(asset: asset)
           
           //If we don't have a AVPlayer already Create a new player with the new AVPlayerItem
           if (self.player == nil){
               self.player = AVPlayer(playerItem: self.playerItem);
              self.player!.usesExternalPlaybackWhileExternalScreenIsActive = true;
           }
           
           NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.playerItem)
           
           
           
           // Add an Observer to the new AVPlayerItem to listen to the Status Key for when it is ready to play
           self.playerItem?.addObserver(self, forKeyPath: STATUS_KEY, options: [.initial, .new], context: nil)
           
           //If we have a player not playing, replace the current item with the new playerItem
           if(self.player?.currentItem != self.playerItem){
               self.player?.replaceCurrentItem(with: self.playerItem)
           }
       }
    
    /* ------------------------------------------
     ** observeValue:
     **
     **    Called when the value at the specified key path relative
     **  to the given object has changed.
     **  Start movie playback when the AVPlayerItem is ready to
     **  play.
     **  Report and error if the AVPlayerItem cannot be played.
     **
     **  NOTE: this method is invoked on the main queue.
     ** ------------------------------------------------------- */
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        //make sure the
        if context == AVPlayerTestPlaybackViewControllerStatusObservationContext {
            let status = AVPlayerItem.Status(rawValue: change![.newKey] as! Int)
            
            switch status! {
            case .unknown:
                
                print("status unknown")
                break
            case .readyToPlay:
                
                addTrackToRecentlyPlayed(track: tracks[currentPlayingTrackIndex])
                play()
                //notify that play has started
                
                NotificationCenter.default.post(name: Notification.Name.playerStartedPlaying,
                                                object: nil,
                                                userInfo:["track": tracks[currentPlayingTrackIndex]])
                
                break
            case .failed:
                
                print("Failed")
                let playerItem = object as? AVPlayerItem
                assetFailedToPrepare(error:playerItem?.error)
                
            }
        }
        else{
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    func play(){
        
        if self.player != nil {
            
            print("file is ready to play, proceeding to play the file")
            
            
                 
            player?.play()
                 
            //initializing a time frame of 1 second for the player
            
            let interval = CMTime(value: 1, timescale: 1)
            
            player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in
                
                let progressSecondsCMTime = CMTimeGetSeconds(progressTime)
                //post current player time to all subscribing classes
                
                if let duration = self.player?.currentItem?.duration {
                    
                    let durationSeconds = CMTimeGetSeconds(duration)
                    
                    if (self.player?.timeControlStatus == .playing){
                    
                    NotificationCenter.default.post(name: Notification.Name.playerTimeElapsed,
                                                               object: nil,
                                                               userInfo:["time": Float(progressSecondsCMTime/durationSeconds) as Float])
                    }
                }
                
            })
        }
    }
    
    /*
     assetFailedToPrepare:
     
     if the AVPlayer state returns failed,
     assetFailedToPrepare will output the error in a alert box
     */
    func assetFailedToPrepare(error:Error?){
        /* Display the error. */
        if self.loaderDelegate is CustomAssetDownloadLoaderDelegate {
            if let errorWCode = error as NSError? {
                self.loaderDelegate!.removeKey(fileManager: FileManager.default)
                if Reachability.isConnectedToNetwork() == true && errorWCode.code == -11800{
                    print("Rerequesting Key")
                    self.initOfflineURLAsset(track: getDownloadedTrack(trackID: tracks[currentPlayingTrackIndex].id))
                    self.readyMediaStream()
                    return;
                }
            }
        }
      //post a notification about it
        NotificationCenter.default.post(name: Notification.Name.playbackError,
                                                                                  object: nil,
                                                                                  userInfo:["description": (error as NSError?)?.localizedFailureReason ?? ""])
        self.unload();
    }
    
    @objc func playerDidFinishPlaying(note:Any){
          
          print("done playing")
          
          //play next song on track
          playNextTrack()
          
      }
    
    @objc func playNextTrack(){
          
           if(currentPlayingTrackIndex < tracks.count) {
               if(repeatOn && repeatedTimes == 0){
                   player?.seek(to: .zero)
                   player?.play()
                   self.repeatedTimes = 1
                   
                   NotificationCenter.default.post(name: Notification.Name.playerFinishedPlaying,
                                                   object: nil,
                                                   userInfo:["currentPlayingIndex": currentPlayingTrackIndex, "tracks":self.tracks, "player":self.player!])
               }
               else{
                    self.repeatedTimes = 0
                   
                    if(self.player != nil) {
                       self.player?.pause()
                       self.player = nil;
                   }
               unload()
               currentPlayingTrackIndex =  currentPlayingTrackIndex + 1
                   
                   if(isPodcast){
                    startPodcastPlay(episodes: self.episodes, currentPlayingTrackIndex: currentPlayingTrackIndex)
                   }
                   else {
                       initPlay()
                   }
        
                   
                   //post a notification that the player is done playing
                   
                   NotificationCenter.default.post(name: Notification.Name.playerFinishedPlaying,
                                                   object: nil,
                                                   userInfo:["currentPlayingIndex": currentPlayingTrackIndex, "tracks":self.tracks])
                   
               }
           }
           
       }
    
    
    @objc func playPreviousTrack(){
        if(currentPlayingTrackIndex > 0 ) {
            unload()
            currentPlayingTrackIndex =  currentPlayingTrackIndex - 1
            self.repeatedTimes = 0
                       
                       if(isPodcast){
                        startPodcastPlay(episodes: self.episodes, currentPlayingTrackIndex: currentPlayingTrackIndex)
                       }
                       else {
                           initPlay()
                       }
                }
    }
    
  
    
    func unload(){
        self.player?.pause()
        self.player = nil;
        self.asset = nil;
    }
    
    /* -----------------------------------------
     ** initURLAsset
     **
     ** creates the AVURLAsset to use for playback if one has not been already created
     ** since we only have one stream we can presume the asset will always be the same
     ** so by checking we can save memory and prevent state issues due to configuration
     ** on the asset changing
     ** ----------------------------------------*/
    
    func initOnlineURLAsset(urlString:String){
        let urlStr: String = urlString
        var url:URL? = nil
        url = URL(string: urlStr)
        //set AssetLoaderDelegate to version that dosn't set keys
        self.loaderDelegate = CustomAssetLoaderDelegate()
        self.asset = AVURLAsset(url: url!)
        self.asset!.resourceLoader.setDelegate(self.loaderDelegate, queue: globalNotificationQueue())
        
    }

    func initOfflineURLAsset(track:DownloadedTrack){
        var url:URL? = nil
        //Check if there is a saved download if true use that insead of path
        let localURL = track.localStorageLocation

        var bookmarkDataIsStale = false
             
                   do {
                   // let fileBookmark = Data(localURL.utf8)
                    url = try URL(resolvingBookmarkData:localURL!, bookmarkDataIsStale: &bookmarkDataIsStale)
                       print("Using Local File")
                   }
                   catch {
                       print ("URL from Bookmark Error: \(error)")
                   }
     //set AssetLoaderDelegate to version with persistant keys
        self.loaderDelegate = CustomAssetDownloadLoaderDelegate()
        self.asset = AVURLAsset(url: url!);
        self.asset!.resourceLoader.setDelegate(self.loaderDelegate, queue: globalNotificationQueue());
    }
    
    func constructStreamingURL ( track : Track) -> String  {
          
          mediaServerEndpoint  = APIurls.StreamingURLBase + track.id + APIurls.StreamingURLSuffix
          print(mediaServerEndpoint)
          return mediaServerEndpoint
          
      }
    
    func addTrackToRecentlyPlayed( track: Track){
        
        let recentlyPlayedTrack: RecentlyPlayed = RecentlyPlayed()
        recentlyPlayedTrack.id = track.id
        recentlyPlayedTrack.track = track
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(recentlyPlayedTrack,update: .modified)
            }
            
        } catch let error as NSError {
            
            print(error)
            
        }
        
    }
    
    @objc func setSongDuration(_ notification: Notification){
           
           let value = notification.userInfo?["value"] as! Float
           let playerTimescale = self.player?.currentItem?.asset.duration.timescale ?? 1
           let time =  CMTime(seconds: Double(value), preferredTimescale: playerTimescale)
           self.player?.seek(to: time, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)

       }
    
    //remote control of the player
       
       func setupRemoteCommandCenter() {
           
           // Get the shared MPRemoteCommandCenter
           
           let commandCenter = MPRemoteCommandCenter.shared()
           
           // Add handler for Play Command
           
           commandCenter.playCommand.addTarget { event in
               
               self.player?.play()
               
               print("headset play")
               
               return .success
           }
           
           // Add handler for Pause Command
           
           commandCenter.pauseCommand.addTarget { event in
               
               self.player?.pause()
               
               print("headset pause")
               
               return .success
           }
           
           // Add handler for Next Command
           
           commandCenter.nextTrackCommand.addTarget { event in
            self.playNextTrack()
               
               return .success
           }
           
           // Add handler for Previous Command
           
           commandCenter.previousTrackCommand.addTarget { event in
            self.playPreviousTrack()
               
               return .success
           }
       }
       
       //set up data to be displayed on Lockscreen and control center
       func setupNowPlaying( title : String , artist:String, imageUrl:String) {
           
           // Define Now Playing Info
           var nowPlayingInfo = [String : Any]()
           nowPlayingInfo[MPMediaItemPropertyTitle] = title
           
           SDWebImageManager.shared().imageDownloader?.downloadImage(with:URL(string: imageUrl), options: SDWebImageDownloaderOptions.useNSURLCache, progress: nil, completed: { (image, error, cacheType, url) in
               if let _ = image {
                   // Use the image object
                   nowPlayingInfo[MPMediaItemPropertyArtwork] =  MPMediaItemArtwork(boundsSize: (image?.size)!) { size in
                       return image!
                   }
               } else {
                   // Put Default Cover Art
                   nowPlayingInfo[MPMediaItemPropertyArtwork] =  MPMediaItemArtwork(boundsSize: (#imageLiteral(resourceName: "no_cover").size)) { size in
                       return #imageLiteral(resourceName: "no_cover")
                   }
                   
               }
               //set again
               MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
               
           })
           nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = playerItem?.currentTime().seconds
           nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = playerItem?.asset.duration.seconds
           //nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = player?.rate
           
           // Set the metadata
           MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
       }

       //image downloading methods (asynchronous)
       
       func downloadImage(url:NSURL, completion: @escaping ((_ image: UIImage?) -> Void)){
           
           getDataFromUrl(url: url) { data in
               DispatchQueue.main.async() {
                   completion(UIImage(data: data! as Data))
               }
           }
       }
       
       func getDataFromUrl(url:NSURL, completion: @escaping (( _ data: NSData?) -> Void)) {
           URLSession.shared.dataTask(with: url as URL) { (data, response, error) in
               completion(data! as NSData)
               }.resume()
       }
    
    func isTrackDownloaded (trackID:String) -> Bool {
        let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(trackID)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
        else {
            return false
        }
    }
    
    func getDownloadedTrack (trackID:String) -> DownloadedTrack {
        let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(trackID)\"")
       return (queryResults?[0])!
    }
       
}
