//
//  Player.swift
//  Songa
//
//  Created by Collins Korir on 7/27/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Foundation
import MBCircularProgressBar
import RealmSwift



class PlayerViewController : UIViewController {

    //IB Outlets

    @IBOutlet weak var ivCoverBg: UIImageView!
    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblArtist: UILabel!
    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var btnDownload: UIButton!
    @IBOutlet weak var btnShuffle: UIButton!
    @IBOutlet weak var btnRepeat: UIButton!
    @IBOutlet weak var sliderProgress: UISlider!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var spinnerView: SpinnerView!
    @IBOutlet weak var btnDownloaded: UIButton!
    @IBOutlet weak var downloadProgressIndicator: MBCircularProgressBarView!


    var loaderDelegate:CustomAssetLoaderDelegate? = nil;
    var downloadSession: AVAssetDownloadURLSession? = nil;
    var player: AVPlayer? = nil
    var playerItem:AVPlayerItem? = nil
    var stri :String!
    var isFMRadio:Bool = false
    var isPodcast:Bool = false
    var fmStationName = ""
    var fmStationDes = ""
    var fmStationImageURL = ""
    var currentPlayerTime = 0
    
    let realm = try! Realm()

    //track(s) passed from previous VC

    var tracks: [Track] = []
    var episodes: [PodcastEpisode] = []

    var playerIsPlaying = false
    var repeatOn:Bool = false
    var shuffleOn:Bool = false
    var currentPlayingTrackIndex = 0
    
    /* ----------------------------
     ** globalNotificationQueue
     
     ** Returns a Dispatch Queue for the AVAssetLoader Delegate
     
     ---------------------------- */
    
    func globalNotificationQueue() -> DispatchQueue {
        
        return DispatchQueue(label: "Stream Queue")
        
    }
    
//    deinit {
//        self.downloadSession?.finishTasksAndInvalidate();
//    }
  
    override func viewDidLoad() {
            super.viewDidLoad()
       
        initController()

        loadInitialPage ()

        setUpNowPlaying()

        showDownloadingView()
    }
    
    func  initController()  {
            self.downloadSession = DownloadManager.shared.downloadSession
            self.tracks = AudioManager.shared.tracks
            self.episodes = AudioManager.shared.episodes
            self.isPodcast = AudioManager.shared.isPodcast
            self.isFMRadio = AudioManager.shared.isFmRadioStream
            self.currentPlayingTrackIndex = AudioManager.shared.currentPlayingTrackIndex
            self.isPodcast = AudioManager.shared.isPodcast
            self.player = AudioManager.shared.player
    }
    
    func showDownloadingView(){
        //show view resuming download if file was downloading
                  
            //get download session task
            DownloadManager.shared.downloadSession.getAllTasks { tasksArray in
                       // For each task, restore the state in the app
                
                       for task in tasksArray {
                           guard let downloadTask = task as? AVAssetDownloadTask else { break }
                        if(downloadTask.taskDescription == self.tracks[self.currentPlayingTrackIndex].id){
                           //track is currently donwloading
                            self.downloadProgressIndicator!.isHidden = true
                          
                            self.btnDownload!.isHidden = true
                            self.btnDownloaded.isHidden = true
//                            DownloadManager.shared.downloadProgressIndicator = nil
//                            DownloadManager.shared.downloadProgressIndicator = self.downloadProgressIndicator
//
//                            //set identifiers to the views
//                            DownloadManager.shared.downloadProgressIndicator?.accessibilityIdentifier = downloadTask.taskDescription
//                            DownloadManager.shared.spinnerView = nil
//                            DownloadManager.shared.spinnerView = self.spinnerView
//                            DownloadManager.shared.spinnerView?.accessibilityIdentifier = downloadTask.taskDescription
                            
                            NotificationCenter.default.addObserver(self,
                                                                   selector: #selector(self.updateDownloadViews),
                                                                     name: Notification.Name.trackDownloadProgress,
                                                                     object: nil)
                            
                            
                            NotificationCenter.default.addObserver(self,
                                                                   selector: #selector(self.showDownloadComplete(_:)),
                                                                     name: Notification.Name.trackDownloadComplete,
                                                                     object: nil)
                        
                        }
                    }
                }
    }
    
    @objc func updateDownloadViews(notification: Notification){
        
        let percentComplete = notification.userInfo?["percentage"] as! Double
        let trackID = notification.userInfo?["taskID"] as! String
        
        if(trackID == self.tracks[self.currentPlayingTrackIndex].id){
          self.spinnerView?.isHidden = true
            self.downloadProgressIndicator.isHidden = false
        self.downloadProgressIndicator?.value = CGFloat(percentComplete)
        }
    }
    
    @objc func showDownloadComplete(_ notification: Notification){
        
        let success = notification.userInfo?["isSuccessful"] as! Bool
        let trackID = notification.userInfo?["trackID"] as! String
        
        //check if notification is of the current track shown
        if(trackID == self.tracks[self.currentPlayingTrackIndex].id){
        
        if(success){
            self.downloadProgressIndicator!.isHidden = true
            spinnerView?.isHidden = true
            self.spinnerView!.isHidden = true
            self.downloadProgressIndicator!.isHidden = true
            self.btnDownload!.isHidden = true
            self.btnDownloaded.isHidden = false
        }
        else {
            self.downloadProgressIndicator!.isHidden = true
            spinnerView?.isHidden = true
            self.spinnerView!.isHidden = true
            self.downloadProgressIndicator!.isHidden = true
            self.btnDownload!.isHidden = false
            self.btnDownloaded!.isHidden = true
        
        let alert = UIAlertController(title: "Failed", message: "Download failed please try again", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            
        }))
                DispatchQueue.main.async {
        self.present(alert, animated: true, completion: nil);
            }
        }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadPlayingPage),
                                               name: Notification.Name.playerStartedPlaying,
                                               object: nil)

   
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(loadNextTrackView),
                                               name: Notification.Name.playerFinishedPlaying,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playBackError),
                                               name: Notification.Name.playbackError,
                                               object: nil)

        
        NotificationCenter.default.addObserver(self,
                                                 selector: #selector(downloadTrack(_:)),
                                                 name: Notification.Name.downloadRequested,
                                                 object: nil)
        
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if(segue.identifier == "showUpNext"){
        
            if let vc = segue.destination as? UpNextViewController {
              vc.tracks = self.tracks
                vc.currentPlayingTrackIndex = self.currentPlayingTrackIndex
         }
        } else if let vc = segue.destination as? ArtistPageViewController
        {
            vc.artistID = sender as! String
        }
        
        else if let destinationNavigationController = segue.destination as? UINavigationController {
            let targetController = destinationNavigationController.topViewController as! AddToPlaylistViewController
            targetController.track = self.tracks[self.currentPlayingTrackIndex]
        }
    }
    

    @IBAction func sliderValueChanged(_ sender: Any) {

        //post value of new slider value to player
        NotificationCenter.default.post(name: Notification.Name.sliderValueChanged,
                                                                                   object: nil,
                                                                                   userInfo:["value": sliderProgress.value])
        
        lblStartTime.text = sliderValue2Timestamp()
    }
    func loadInitialPage (){

        //hide spinner view
        spinnerView.isHidden =  true
        btnDownloaded.isHidden =  true
        downloadProgressIndicator.isHidden =  true
        
        //set custom thumb to UIslider
        //default is too large

        self.sliderProgress.setThumbImage(#imageLiteral(resourceName: "ic_slider_thumb"), for: .normal)
        self.sliderProgress.minimumTrackTintColor = .colorAccent

         //self.ivCoverBg.blurImage()

        lblStartTime.text = "--:--"
        //lblStartTime.text = "0:00"
        lblEndTime.text = "--:--"
        lblTitle.text = "Not Playing..."
        lblArtist.text = ""
        
        self.downloadProgressIndicator?.maxValue =  100
        
        lblArtist.textColor = .white
        lblEndTime.textColor = .white
        lblStartTime.textColor = .white
        lblTitle.textColor = .white
        btnPlayPause.imageView?.tintColor = .white
        btnShuffle.imageView?.tintColor = .white
        btnRepeat.imageView?.tintColor = .white
        btnFavourite.imageView?.tintColor  = .white
        btnDownload.imageView?.tintColor = .white
        

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.playPauseClicked))
        btnPlayPause.addGestureRecognizer(tap)


        print("index ", currentPlayingTrackIndex)


        if (tracks.count>0){
            lblArtist.text = tracks[currentPlayingTrackIndex].artistName
            lblTitle.text = tracks[currentPlayingTrackIndex].title
            ivCover.sd_setImage(with: URL(string: tracks[currentPlayingTrackIndex].artwork[0].medium), placeholderImage: #imageLiteral(resourceName: "no_cover"), completed: nil)
            ivCoverBg.sd_setImage(with: URL(string: tracks[currentPlayingTrackIndex].artwork[0].medium), placeholderImage: #imageLiteral(resourceName: "no_cover"), completed: nil)
            ivCoverBg.blurImage()
            
            if(repeatOn){
                 btnRepeat.tintColor = UIColor.textColorPrimary
            }
            else {
                btnRepeat.tintColor = UIColor.white
            }
            
            if(shuffleOn){
                btnShuffle.tintColor = UIColor.textColorPrimary
            }
            else {
                btnShuffle.tintColor = UIColor.white
            }
            
        }
        
        else {
           
            btnDownload.isEnabled = false
            btnFavourite.isEnabled = false
            btnShuffle.isEnabled = false
            btnNext.isEnabled = false
            btnPrevious.isEnabled = false
            btnRepeat.isEnabled = false
            sliderProgress.isEnabled = false
            lblStartTime.isEnabled = true
            lblEndTime.isEnabled = true
            
        }
        
        if(isFMRadio){
            btnDownload.isHidden = true
            btnShuffle.isHidden = true
            btnNext.isHidden = true
            btnPrevious.isHidden = true
            btnRepeat.isHidden = true
            sliderProgress.isHidden = true
            lblStartTime.isHidden = true
            lblEndTime.isHidden = true
            lblTitle.text = fmStationName
            lblArtist.text = fmStationDes
        }
        
        if(isPodcast){
                btnDownload.isHidden = true
                btnShuffle.isEnabled = true
                btnNext.isEnabled = true
                btnPrevious.isEnabled = true
                btnRepeat.isEnabled = true
                sliderProgress.isEnabled = true
                lblStartTime.isEnabled = true
                lblEndTime.isEnabled = true
                btnFavourite.isEnabled = true
                
              }


    }

    @objc func reloadPlayingPage (){
        initController()
       btnPlayPause.setImage(#imageLiteral(resourceName: "ic_pause"), for: .normal)
       setUpNowPlaying()
    }



    func setUpViews(){
        if(isFMRadio){
            
            ivCover.sd_setImage(with: URL(string: fmStationImageURL), placeholderImage: #imageLiteral(resourceName: "no_cover"), completed: nil)
            ivCoverBg.sd_setImage(with: URL(string: fmStationImageURL), placeholderImage: #imageLiteral(resourceName: "no_cover"), completed: nil)
            
          
        }
            else if(isPodcast){
            if (episodes.count>0 ){
                          
                          lblArtist.text = episodes[currentPlayingTrackIndex].podcastDescription
                          lblTitle.text = episodes[currentPlayingTrackIndex].title
                     
                              ivCover.sd_setImage(with: URL(string: episodes[currentPlayingTrackIndex].image), placeholderImage: #imageLiteral(resourceName: "no_cover"), completed: nil)
                              ivCoverBg.sd_setImage(with: URL(string: episodes[currentPlayingTrackIndex].image), placeholderImage: #imageLiteral(resourceName: "no_cover"), completed: nil)
                          
                      }
                      
                      //check if track is favourited or downloaded
                      
                      if(isEpisodeFavourited()){
                          btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite_filled"), for: .normal)
                      }
                      
                          btnDownload.isHidden = true
                      
        }
            
        else {
            if (tracks.count>0 ){
                
                lblArtist.text = tracks[currentPlayingTrackIndex].artistName
                lblTitle.text = tracks[currentPlayingTrackIndex].title
                
                if(tracks[currentPlayingTrackIndex].artwork.count > 0){
                    ivCover.sd_setImage(with: URL(string: tracks[currentPlayingTrackIndex].artwork[0].medium), placeholderImage: #imageLiteral(resourceName: "no_cover"), completed: nil)
                    ivCoverBg.sd_setImage(with: URL(string: tracks[currentPlayingTrackIndex].artwork[0].medium), placeholderImage: #imageLiteral(resourceName: "no_cover"), completed: nil)
                }
            }
            
            //check if track is favourited or downloaded
            
            if(isTrackFavourited()){
                btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite_filled"), for: .normal)
            }
            
            if(isTrackDownloaded()){
                btnDownloaded.setImage(#imageLiteral(resourceName: "ic_downloaded"), for: .normal)
                btnDownloaded.isHidden = false
                btnDownload.isHidden = true
            }
            
          
        }
    }
    
    func setUpNowPlaying (){

       setUpViews()
        if (player?.timeControlStatus == .paused){
              
                          btnPlayPause.setImage(#imageLiteral(resourceName: "ic_play"), for: .normal)
                      }
                       else {
                            btnPlayPause.setImage(#imageLiteral(resourceName: "ic_pause"), for: .normal)
              
                      }
              
              //set slider progress
              if  let currentPlayerTime = self.player?.currentItem?.currentTime(){
                  self.sliderProgress.value =  Float(CMTimeGetSeconds(currentPlayerTime))
              }
        
        if(!isFMRadio){
        //set start time text

        if(player != nil){
            
        let currentTime = CMTimeGetSeconds( (player?.currentItem?.currentTime())!)

        lblStartTime.text = seconds2Timestamp(intSeconds: Int(currentTime))

        //set end time text

        
        let trackLength = CMTimeGetSeconds( (player?.currentItem?.asset.duration)!)

        lblEndTime.text = seconds2Timestamp(intSeconds: Int(trackLength))
            
        }

        //initializing a time frame of 1 second for the player


        let interval = CMTime(value: 1, timescale: 1)

        player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in

            let progressSecondsCMTime = CMTimeGetSeconds(progressTime)

            //move progress view

            if let duration = self.player?.currentItem?.duration {


                let durationSeconds = CMTimeGetSeconds(duration)


                self.sliderProgress.maximumValue = Float(durationSeconds)
                self.sliderProgress.minimumValue = Float(0)

                self.sliderProgress.value = Float(progressSecondsCMTime)

                self.lblStartTime.text = self.seconds2Timestamp(intSeconds: Int(progressSecondsCMTime))

            }
        })
    }
}
    
    @objc func playBackError(_ notification: Notification){
        
        let errorDescription = notification.userInfo?["description"] as! String
        let alert = UIAlertController(title: "Failed", message: errorDescription, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            
        }))
            DispatchQueue.main.async {
        self.present(alert, animated: true, completion: nil);
        }
    }
    
    @objc func playPauseClicked(){

            //If the player is currently playing a track

            if (player?.timeControlStatus == .paused){

                NotificationCenter.default.post(name: Notification.Name.playClicked,
                                                object: nil)
                //resume play

                btnPlayPause.setImage(#imageLiteral(resourceName: "ic_pause"), for: .normal)

                if(self.player != nil) {
                    self.player?.play()
                }

            }

            else if (player?.timeControlStatus == .playing){

                NotificationCenter.default.post(name: Notification.Name.pauseClicked,
                                                object: nil)
                //pause the play
                btnPlayPause.setImage(#imageLiteral(resourceName: "ic_play"), for: .normal)

                if(self.player != nil) {
                    self.player?.pause()
                }
            }

    }
    
    func isTrackFavourited () -> Bool {
        let queryResults: Results<FavouriteTrack>? = realm.objects(FavouriteTrack.self).filter("id == \"\(tracks[currentPlayingTrackIndex].id)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
            else {
                return false
            }
    }
    
    
    func isEpisodeFavourited () -> Bool {
        let queryResults: Results<FavouritePodcastEpisode>? = realm.objects(FavouritePodcastEpisode.self).filter("id == \"\(episodes[currentPlayingTrackIndex].id)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
            else {
                return false
            }
    }
    
    func isTrackDownloaded () -> Bool {
        let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(tracks[currentPlayingTrackIndex].id)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
        else {
            return false
        }
    }
            


    //this function converts Int to string like 3:24

    func seconds2Timestamp(intSeconds:Int)->String {
        let mins:Int = intSeconds/60
        let secs:Int = intSeconds%60
        let strTimestamp:String = ((mins<10) ? "0" : "") + String(mins) + ":" + ((secs<10) ? "0" : "") + String(secs)
        return strTimestamp
    }
    
    func sliderValue2Timestamp()->String {
        
        let value = sliderProgress.value/sliderProgress.maximumValue
           
        let totalSeconds = CMTimeGetSeconds( player?.currentItem?.asset.duration ?? .zero)
        let newDuration = totalSeconds*Double(value)
        let newDurationInt = Int(newDuration)

        
        let mins:Int = newDurationInt/60
        let secs:Int = newDurationInt%60

        let strTimestamp:String = ((mins<10) ? "0" : "") + String(mins) + ":" + ((secs<10) ? "0" : "") + String(secs)
        return strTimestamp
        
    }
    
    func constructStreamingURL ( track : Track) -> String  {
        
      let   mediaServerEndpoint  = APIurls.StreamingURLBase + track.id + APIurls.StreamingURLSuffix
        
        //mediaServerEndpoint = "http://ec2-52-213-186-230.eu-west-1.compute.amazonaws.com:1080/auth/media/0000162f-9ccb-470c-a7fc-a6bcbe24c2ca.ism/.m3u8"
        
        print(mediaServerEndpoint)
        return mediaServerEndpoint
        
    }
    


    //MARK:- IB Actions

    @IBAction func playPause(_ sender: Any) {
    }
    @IBAction func back(_ sender: Any) {

      self.dismiss(animated: true, completion: nil)
         // self.navigationController?.popViewController(animated: true)

    }

    @IBAction func showTrackOptions(_ sender: Any) {
        if(tracks.count > 0){
        self.showOptionsController(track: tracks[currentPlayingTrackIndex])
    }
    }
    @IBAction func goToPreviousTrack(_ sender: Any) {
        
        if(tracks.count > 1){
            
         resetPlayerViews()
        if(currentPlayingTrackIndex > 0 ) {
        
               currentPlayingTrackIndex = currentPlayingTrackIndex + 1
       
        setUpNowPlaying()
                //post notification that previous button has been clicked
        NotificationCenter.default.post(name: Notification.Name.playPreviousClicked,
                                        object: nil)
        }
        }
        else {
            self.sliderProgress.value = 0.0
            lblStartTime.text = "--:--"
            lblEndTime.text = "--:--"
            NotificationCenter.default.post(name: Notification.Name.resetStream,
                                            object: nil)
        }
    }
    @IBAction func goToNextTrack(_ sender: Any) {
        
            if(tracks.count > 1){
        resetPlayerViews()
        currentPlayingTrackIndex = currentPlayingTrackIndex + 1
                
        if(currentPlayingTrackIndex < tracks.count) {
           
            setUpNowPlaying()
            NotificationCenter.default.post(name: Notification.Name.playNextClicked,
                                            object: nil)
        }
     }
            else {
                self.sliderProgress.value = 0.0
                lblStartTime.text = "--:--"
                lblEndTime.text = "--:--"
                NotificationCenter.default.post(name: Notification.Name.resetStream,
                                                object: nil)
        }
    }
    
    func resetPlayerViews(){

        self.sliderProgress.value = 0.0
        lblStartTime.text = "--:--"
        lblEndTime.text = "--:--"
        btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite"), for: .normal)
        btnDownload.isHidden = false
        btnDownloaded.isHidden = true
        spinnerView.isHidden = true
        downloadProgressIndicator.isHidden = true
        
        //setUpViews()
    }
    @objc func loadNextTrackView(_ notification: NSNotification) {
        
        currentPlayingTrackIndex = notification.userInfo?["currentPlayingIndex"] as! Int
        self.tracks = notification.userInfo?["tracks"] as! [Track]
        self.player = notification.userInfo?["player"] as? AVPlayer
 
            resetPlayerViews()
            setUpNowPlaying()
     
    }
    
    @objc func loadNewTrack(_ notification: NSNotification) {
        resetPlayerViews()
        let newIndex = notification.userInfo?["currentPlayingIndex"] as! Int
        currentPlayingTrackIndex = newIndex
        setUpNowPlaying()
    }

    @IBAction func downloadTrack(_ sender: Any) {

       DownloadManager.shared.setupAssetDownload(track: tracks[currentPlayingTrackIndex])
//       DownloadManager.shared.downloadProgressIndicator = self.downloadProgressIndicator
//
//        DownloadManager.shared.spinnerView = self.spinnerView
            showDownloadingView()

        
    }
    @IBAction func favouriteTrack(_ sender: Any) {
        
        if(!isTrackFavourited()){
        //add tracks to favourites in Realm
        let favouritedTrack: FavouriteTrack = FavouriteTrack()
        favouritedTrack.id = tracks[currentPlayingTrackIndex].id
        favouritedTrack.track = tracks[currentPlayingTrackIndex]
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(favouritedTrack,update: .modified)
                btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite_filled"), for: .normal)
            }
            
            
        } catch let error as NSError {
            
            print(error)
            
        }
    }
            
    else {
            
            //unfavourite track
            
            let queryResults: Results<FavouriteTrack>? = realm.objects(FavouriteTrack.self).filter("id == \"\(tracks[currentPlayingTrackIndex].id)\"")
          
            do {
                let realm = try Realm()
                try realm.write {
                    realm.delete((queryResults?[0])!)
                    btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite"), for: .normal)
                }
                
                
            } catch let error as NSError {
                
                print(error)
                
            }
            }
    }
//TO-DO shuffle
    @IBAction func shufflePlay(_ sender: Any) {
        if(shuffleOn){
             btnShuffle.tintColor = UIColor.white
            shuffleOn = false
              }
        else {
            btnShuffle.tintColor = UIColor.textColorPrimary
            shuffleOn = true
        }
        
        NotificationCenter.default.post(name: Notification.Name.togglePlayerShuffle,
                                        object: nil)
        
    }
    
    @IBAction func repeatCurrentPlayingTrack(_ sender: Any) {
        if(repeatOn){
            btnRepeat.tintColor = UIColor.white
            repeatOn = false
        }
        else {
            btnRepeat.tintColor = UIColor.textColorPrimary
            repeatOn = true
        }
        NotificationCenter.default.post(name: Notification.Name.togglePlayerRepeat,
                                        object: nil)
        
    }
}
