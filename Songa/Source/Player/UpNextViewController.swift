//
//  UpNextViewController.swift
//  Songa
//
//  Created by Collins Korir on 3/4/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import Foundation
import  Alamofire
import AlamofireObjectMapper
import SDWebImage
import RealmSwift
import SDWebImage

class UpNextViewController: UIViewController {
      var tracks : [Track] = []
      var currentPlayingTrackIndex = 0
    
    @IBOutlet weak var tvTracks: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title =  "Now Playing"
        tvTracks.delegate =  self
        tvTracks.dataSource =  self
     
        //remove separator form tableview
        tvTracks.separatorStyle = UITableViewCell.SeparatorStyle.none
        tvTracks.allowsSelection = false
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
         if let vc = segue.destination as? ArtistPageViewController
                {
                    vc.artistID = sender as! String
                }
        else if let destinationNavigationController = segue.destination as? UINavigationController {
                   let targetController = destinationNavigationController.topViewController as! AddToPlaylistViewController
                   targetController.track = self.tracks[self.currentPlayingTrackIndex]
               }
    }
    
        @objc func cellTapped(_ sender: cellTapGesture) {
            
            NotificationCenter.default.post(name: Notification.Name.startPlay,
                                            object: nil,
                                            userInfo:["tracks": sender.tracks , "currentPlayingIndex": sender.currrentPlayingIndex])
            
        }
        
        class cellTapGesture: UITapGestureRecognizer {
            var tracks = Array<Track>()
            var currrentPlayingIndex = Int()
        }
        
    class optionsTapGesture: UITapGestureRecognizer {
        var track = Track()
    }
    
    @objc func optionsTapped(_ sender: optionsTapGesture) {
        showOptionsController(track: sender.track)
    }

}

extension UpNextViewController :UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as! TrackTableCell
        let url : String
        
        if ( tracks[indexPath.row].artwork.count > 0){
            
            url =  tracks[indexPath.row].artwork[0].small
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = tracks[indexPath.row].title
        cell.lblTrackIndex.text = String(indexPath.row + 1)
        cell.lblTrackIndex.textColor = ThemeManager.currentTheme().textColorSecondary
        cell.lblArtist.textColor = ThemeManager.currentTheme().textColorSecondary
        
        let tapOptions = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
        tapOptions.track = tracks[indexPath.row]
        
        cell.btnOptions.addGestureRecognizer(tapOptions)
        cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        
        // Add a separator below each cell
        let horizontalGap = 15.0 as CGFloat
        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
        cell.addSubview(seperatorView)
        
        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        
        tappy.tracks = tracks
        tappy.currrentPlayingIndex = indexPath.row
        cell.addGestureRecognizer(tappy)
        
        return cell
    }
}
