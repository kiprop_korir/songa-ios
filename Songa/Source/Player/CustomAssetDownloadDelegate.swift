////
////  CustomAssetDownloadDelegate.swift
////  SPLPlayer
////
////  Created by Sean Gray on 12/7/17.
////
//      import UIKit
//      import AVKit;
//      import AVFoundation
//      import MBCircularProgressBar
//      import RealmSwift
//
//      class CustomAssetDownloadDelegate: NSObject, AVAssetDownloadDelegate {
//        var downloadPercent: String? = nil;
//        var assetDownloadTask:AVAssetDownloadTask? = nil;
//        var btnDownload: UIButton? = nil;
//        var btnDownloaded: UIButton? = nil;
//        var spinnerView: SpinnerView? = nil;
//        var track: Track? = nil;
//        var downloadProgressIndicator: MBCircularProgressBarView? = nil;
//
//        //get the deleteButton and downloadButton from View so we can update state from download Delegate
//        init(spinnerView:SpinnerView,btnDownload:UIButton , btnDownloaded:UIButton,  downloadProgressIndicator:MBCircularProgressBarView,  track:Track ) {
//            self.btnDownload = btnDownload
//            self.btnDownloaded = btnDownloaded
//            self.spinnerView = spinnerView
//            self.downloadProgressIndicator = downloadProgressIndicator
//            self.track = track
//            super.init()
//        }
//
//        func setupAssetDownload(target:URL,session:AVAssetDownloadURLSession) {
//            let hlsAsset = AVURLAsset(url: target)
//            // Download Track at 5 mbps
//            // TODO: Test different speeds
//            self.assetDownloadTask = session.makeAssetDownloadTask(asset: hlsAsset, assetTitle: (track?.title)!,
//                                                                   assetArtworkData: nil, options: [AVAssetDownloadTaskMinimumRequiredMediaBitrateKey: 5000000])!
//            self.assetDownloadTask!.resume()
//
//            //self.spinnerView!.isHidden = true
//        }
//
//
//        }
//
//        //Updates the state of UI based on if and asset is downloaded or not. Envoked on download, deletion and canceling
//        func updateViews(){
//            //checks to see if we have a bookmark at key savedPath if so presume we have a downloaded asset
//            if UserDefaults.standard.data(forKey:"savedPath") != nil {
//
//                print("update views called")
//                print("download location " ,Defaults.getString(key: "savedPath2"))
//                print("download location " ,Defaults.getString(key: "savedPath3"))
//
//                self.downloadProgressIndicator!.isHidden = true
//                spinnerView?.isHidden = true
//
//            }
//                // if there is no saved bookmark presume there is no downloaded asset
//            else{
//                print("no asset")
//                self.spinnerView!.isHidden = true
//                self.downloadProgressIndicator!.isHidden = true
//                self.btnDownload!.isHidden = false
//            }
//        }
//
//        //This function is envoked whenever the AVAssetDownloadTask is completed Note this includes if the user cancelled
//        func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask, didFinishDownloadingTo location: URL) {
//
//            //If download percent didnt hit 100% persume user cancelled and we need to clear memory
//
//            print("download completed")
//            print("download completed" ,downloadPercent!)
//
//            if(self.downloadPercent != "100"){
//
//                print("download deleted")
//                delete()
//            }
//            else {
//                //post a notification that the track is done downloading
//
//                let userDefaults = UserDefaults.standard
//                do {
//                    //wrike bookmark of location to key savedPath this allows us to acess it later and persistantly
//                    userDefaults.set(try location.bookmarkData(), forKey:"savedPath")
//                    Defaults.putString(key: "savedPath2", value: String(describing: location.absoluteURL))
//                    Defaults.putString(key: "savedPath3", value: String(describing: location.absoluteString))
//
//                }
//                catch {
//                    print("bookmark Error \(error)")
//                }
//
//                //save the download to realm
//
//                let currentDownload: DownloadedTrack = DownloadedTrack()
//                currentDownload.id = track!.id
//                currentDownload.track = track
//                currentDownload.localStorageLocation = String(describing: location.absoluteURL)
//
//                do {
//                    let realm = try Realm()
//                    try realm.write {
//
//                        realm.add(currentDownload,update: .modified)
//
//                    }
//                } catch let error as NSError {
//                    print(error)
//
//                }
//                //update UI
//                self.btnDownloaded!.isHidden = false
//
//                updateViews()
//            }
//        }
//
//        //this function is envoked whenever a segment has finished downloading, can be used to monitor status of download
//        func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask,
//                        didLoad timeRange: CMTimeRange, totalTimeRangesLoaded loadedTimeRanges: [NSValue],
//                        timeRangeExpectedToLoad: CMTimeRange) {
//
//            // Convert loadedTimeRanges to CMTimeRanges
//            var percentComplete = 0.0
//            for value in loadedTimeRanges {
//                let loadedTimeRange: CMTimeRange = value.timeRangeValue
//                percentComplete += CMTimeGetSeconds(loadedTimeRange.duration) /
//                    CMTimeGetSeconds(timeRangeExpectedToLoad.duration)
//            }
//            percentComplete *= 100
//            let stringPercent:String = String(Int(percentComplete))
//            self.downloadPercent = stringPercent
//            //        update download button to show percentages
//            //        self.downloadButton!.setTitle(self.downloadPercent!+"%",for: .normal)
//
//            spinnerView?.isHidden = true
//            downloadProgressIndicator?.isHidden = false
//            print("percentComplete ", percentComplete)
//            downloadProgressIndicator?.maxValue =  100
//            downloadProgressIndicator?.value = CGFloat(percentComplete)
//        }
//      }
