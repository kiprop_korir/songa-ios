//
//  DownloadManager.swift
//  Songa
//
//  Created by Collins Korir on 9/18/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import AVFoundation
import RealmSwift
import MBCircularProgressBar


/* ----------------------------
 ** globalNotificationQueue
 
 ** Returns a Dispatch Queue for the AVAssetLoader Delegate
 
 ---------------------------- */

func globalNotificationQueue() -> DispatchQueue {
    
    return DispatchQueue(label: "Stream Queue")
    
}

class DownloadManager:NSObject {
    
    static var shared = DownloadManager()
    var config: URLSessionConfiguration!
    var downloadSession: AVAssetDownloadURLSession!
    var track: Track? = nil
    var loaderDelegate:CustomAssetLoaderDelegate? = nil
    var asset: AVURLAsset? = nil
    var downloadPercent: String? = ""
    let realm = try! Realm()
    var assetDownloadTask:AVAssetDownloadTask? = nil
    var downloadProgressIndicator: MBCircularProgressBarView? = nil
    var spinnerView: SpinnerView? = nil
    
    override public init() {
        super.init()
        config = URLSessionConfiguration.background(withIdentifier: "\(Bundle.main.bundleIdentifier!).background")
        downloadSession = AVAssetDownloadURLSession(configuration: config, assetDownloadDelegate: self, delegateQueue: OperationQueue.main)
    }
    
    func setupAssetDownload(track : Track) {
        
        self.track = track
        
        let url = URL(string: self.constructStreamingURL())
        initURLAssetForDownload(url: url!)
        let options = [AVURLAssetAllowsCellularAccessKey: false]
        
        let asset = AVURLAsset(url: url!, options: options)
        asset.resourceLoader.preloadsEligibleContentKeys = true
        
        // Create new AVAssetDownloadTask for the desired asset
        assetDownloadTask = downloadSession.makeAssetDownloadTask(asset: asset,
                                                                 assetTitle: track.title,
                                                                 assetArtworkData: nil,
                                                                 options: nil)
        
        
//        makeAssetDownloadTask(asset: hlsAsset, assetTitle: (track?.title)!,
//        assetArtworkData: nil, options: [AVAssetDownloadTaskMinimumRequiredMediaBitrateKey: 5000000])!
        // Start task and begin download
        
        assetDownloadTask?.taskDescription = track.id
        assetDownloadTask?.resume()
        
        //save download to pending downloads
        
        do {
            let downloadQueue: DownloadQueue = DownloadQueue()
            downloadQueue.processId = assetDownloadTask?.taskIdentifier ?? 0
                           downloadQueue.track = track
                            
                             let realm = try Realm()
                             try realm.write {
                                 
                                 realm.add(downloadQueue,update: .modified)
                                 
                             }
                         } catch let error as NSError {
                             print(error)
                             
                         }
        
        //store the download track process to realm
                     
//        let currentDownload: DownloadingTrackItem = DownloadingTrackItem()
//        currentDownload.id = track.id
//
//                     do {
//                         try realm.write {
//                            realm.add(currentDownload,update: .modified)}
//                     } catch let error as NSError {
//                         print(error)
//                     }
    }
    
    func restorePendingDownloads() {
        // Grab all the pending tasks associated with the downloadSession
        downloadSession.getAllTasks { tasksArray in
            // For each task, restore the state in the app
            for task in tasksArray {
                guard let downloadTask = task as? AVAssetDownloadTask else { break }
                downloadTask.resume()
            }
        }
    }
    
    
    func playOfflineAsset() -> AVURLAsset? {
        guard let assetPath = UserDefaults.standard.value(forKey: "assetPath") as? String else {
            // Present Error: No offline version of this asset available
            return nil
        }
        let baseURL = URL(fileURLWithPath: NSHomeDirectory())
        let assetURL = baseURL.appendingPathComponent(assetPath)
        let asset = AVURLAsset(url: assetURL)
        if let cache = asset.assetCache, cache.isPlayableOffline {
            return asset
            // Set up player item and player and begin playback
        } else {
            return  nil
            // Present Error: No playable version of this asset exists offline
        }
    }
    
    func getPath() -> String {
        return UserDefaults.standard.value(forKey: "assetPath") as? String ?? ""
    }
    
    func deleteOfflineAsset() {
        do {
            let userDefaults = UserDefaults.standard
            if let assetPath = userDefaults.value(forKey: "assetPath") as? String {
                let baseURL = URL(fileURLWithPath: NSHomeDirectory())
                let assetURL = baseURL.appendingPathComponent(assetPath)
                try FileManager.default.removeItem(at: assetURL)
                userDefaults.removeObject(forKey: "assetPath")
            }
        } catch {
            print("An error occured deleting offline asset: \(error)")
        }
    }
    
    /* -----------------------------------------
     ** initURLAssetForDownload
     **
     ** creates the AVURLAseet to use for playback that is set up to store persistant keys
     ** this allows for the preloading for offline HLS
     ** ----------------------------------------*/
    
    func initURLAssetForDownload(url:URL){
        self.asset = AVURLAsset(url: url);
        //set AssetLoaderDelegate to version that can save keys
        self.loaderDelegate = CustomAssetDownloadLoaderDelegate()
        //Imediatly request keys instead of waiting for playback, this allows for saving keys
        self.asset!.resourceLoader.preloadsEligibleContentKeys = true
        self.asset!.resourceLoader.setDelegate(self.loaderDelegate, queue: globalNotificationQueue());
    }
    
    func constructStreamingURL () -> String  {
        
        let   mediaServerEndpoint  = APIurls.StreamingURLBase + self.track!.id + APIurls.StreamingURLSuffix
        return mediaServerEndpoint
        
    }
}

extension DownloadManager: AVAssetDownloadDelegate {
    
   
    
    //This function is envoked whenever the AVAssetDownloadTask is completed Note this includes if the user cancelled
          func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask, didFinishDownloadingTo location: URL) {
              
              //If download percent didnt hit 100% persume user cancelled and we need to clear memory
              
              print("download completed")
              print("download completed" ,downloadPercent!)
            
              
              if(self.downloadPercent != "100"){
                  //TODO: Delete from realm as well
                //TODO: Implement cancel download option
                  print("download deleted")
                  delete()
              }
              else {
                 
//
//                  let userDefaults = UserDefaults.standard
//                  do {
//                      //wrike bookmark of location to key savedPath this allows us to acess it later and persistantly
//                    //write path to realm
//                      userDefaults.set(try location.bookmarkData(), forKey:"savedPath")
//                      Defaults.putString(key: "savedPath2", value: String(describing: location.absoluteURL))
//                      Defaults.putString(key: "savedPath3", value: String(describing: location.absoluteString))
//
//                  }
//                  catch {
//                      print("bookmark Error \(error)")
//                  }
                  
                  
                  do {
                    //save the download to realm
                    
                    let currentDownload: DownloadedTrack = DownloadedTrack()
                    currentDownload.id = track!.id
                    currentDownload.track = track
                    currentDownload.localStorageLocation =  try location.bookmarkData()
                    
                      let realm = try Realm()
                      try realm.write {
                          
                          realm.add(currentDownload,update: .modified)
                          
                      }
                  } catch let error as NSError {
                      print(error)
                      
                  }
                  //post a notification that the track is done downloading
                                     
                                  NotificationCenter.default.post(name: Notification.Name.trackDownloadComplete,
                                                                  object: nil,
                                                                  userInfo:["trackID": track!.id ,"isSuccessful": true])
              }
          }
          
          //this function is envoked whenever a segment has finished downloading, can be used to monitor status of download
          func urlSession(_ session: URLSession, assetDownloadTask: AVAssetDownloadTask,
                          didLoad timeRange: CMTimeRange, totalTimeRangesLoaded loadedTimeRanges: [NSValue],
                          timeRangeExpectedToLoad: CMTimeRange) {
              
              // Convert loadedTimeRanges to CMTimeRanges
              var percentComplete = 0.0
              for value in loadedTimeRanges {
                  let loadedTimeRange: CMTimeRange = value.timeRangeValue
                  percentComplete += CMTimeGetSeconds(loadedTimeRange.duration) /
                      CMTimeGetSeconds(timeRangeExpectedToLoad.duration)
              }
              percentComplete *= 100
              let stringPercent:String = String(Int(percentComplete))
              self.downloadPercent = stringPercent
              //        update download button to show percentages
              //        self.downloadButton!.setTitle(self.downloadPercent!+"%",for: .normal)
              
              downloadProgressIndicator?.isHidden = false
              spinnerView?.isHidden = true
            
              print("percentComplete ", percentComplete)
              downloadProgressIndicator?.maxValue =  100
              downloadProgressIndicator?.value = CGFloat(percentComplete)
            
            //post a notification of track download progress
                                         
                                      NotificationCenter.default.post(name: Notification.Name.trackDownloadProgress,
                                                                      object: nil,
                                                                      userInfo:["percentage": percentComplete,"taskID": assetDownloadTask.description])
            
          }
    
    //this method is also called after force-quit and the app is restarted
    //so TODO/// DELETE INCOMPLETE DOWNLOADED FILE
        func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
            debugPrint("Task completed: \(task), error: \(String(describing: error))")
            
             var isSuccessful = true
            if(error != nil ){
                isSuccessful = false
            }
             //post a notification that the track finished downloading
            if let trackID = track?.id {
                                           NotificationCenter.default.post(name: Notification.Name.trackDownloadComplete,
                                                                           object: nil,
                                                                           userInfo:["trackID": trackID,"isSuccessful": isSuccessful])
                
                 print("DOWNLOAD: FINISHED")
            }
           
    }
    
    //Cancels the download, note this will call the urlSession DidFinishDownloading method and needs to be handled in there
      func cancel(){
          if let downloadTask = self.assetDownloadTask{
              downloadTask.cancel()
          }
      }
      
      func delete(){
          //get the persistent data library
          let userDefaults = UserDefaults.standard
          //check if there is something saved under key savedPath. Should be a bookmark
          if let fileBookmark = userDefaults.data(forKey: "savedPath") {
              let fileManager=FileManager.default
              do {
                  var bookmark: Bool = false;
                  //attempt to delete the resource found at the path corelating to the found bookmark
                  let mediaURL = try URL(resolvingBookmarkData:fileBookmark, bookmarkDataIsStale: &bookmark)
                  try fileManager.removeItem(at: mediaURL)
                  //remove the bookmark from the userDefaults library
                  userDefaults.removeObject(forKey: "savedPath")
                  //update UI
                  //post a notification that the track failed downloading
                                                               
                                                            NotificationCenter.default.post(name: Notification.Name.trackDownloadComplete,
                                                                                            object: nil,
                                                                                            userInfo:["trackID": track!.id ,"isSuccessful": false])
              }
              catch {
                  print("Failed to Delete File")
              }
          }
    }
}
//do something when all download events finish

extension SearchViewController: URLSessionDelegate {
  func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
    DispatchQueue.main.async {
      if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
        let completionHandler = appDelegate.backgroundSessionCompletionHandler {
        appDelegate.backgroundSessionCompletionHandler = nil
        
        completionHandler()
      }
    }
  }
}

