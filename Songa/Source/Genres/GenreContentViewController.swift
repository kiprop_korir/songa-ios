//
//  GenreContentViewController.swift
//  Songa
//
//  Created by Collins Korir on 7/25/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import  Alamofire
import AlamofireObjectMapper
import SDWebImage
import RealmSwift

class GenreContentViewController: BaseViewController {

    //@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var ivCoverBg: UIImageView!
    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblArtist: UILabel!
    @IBOutlet weak var tblTracks: UITableView!
    @IBOutlet weak var viewShowMore: UIView!
    @IBOutlet weak var collectionViewAlbums: UICollectionView!

    @IBOutlet weak var viewShowMoreAlbums: UIView!
    @IBOutlet weak var viewShowMoreArtist: UIView!
    @IBOutlet weak var tvTracks: UICollectionView!
    @IBOutlet weak var navBar: UINavigationItem!

    let realm = try! Realm()
    let headers = APIurls.Headers

     var genreID:String = ""
    var trackTitle:String = ""
        var trackID:String = ""
    var artistID:String = ""

    let genresAPI = APIurls.Genres
    let tracksAPI = APIurls.Tracks



    var albums : [Album] = []
     var tracks : [Track] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = trackTitle

   // activityIndicator.startAnimating()


        collectionViewAlbums.dataSource = self
        collectionViewAlbums.delegate = self

        tvTracks.dataSource = self
        tvTracks.delegate = self


        //we also load cover and bg on this method
        loadTrack()
        loadArtistAlbums()

        let showMoreArtistTap = showMoreTapGesture(target: self, action: #selector(showMoreTapped(_:)))
        showMoreArtistTap.showMoreTypeSelected =  "Artist"
        viewShowMoreArtist.addGestureRecognizer(showMoreArtistTap)
        
        let showMoreAlbumsTap = showMoreTapGesture(target: self, action: #selector(showMoreTapped(_:)))
        showMoreAlbumsTap.showMoreTypeSelected =  "Albums"
        viewShowMoreAlbums.addGestureRecognizer(showMoreAlbumsTap)

      
    }

    
    func isTrackDownloaded (trackID:String) -> Bool {
        let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(trackID)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
        else {
            return false
        }
    }
    
    
    class cellTapGesture: UITapGestureRecognizer {
        var tracks = Array<Track>()
        var currrentPlayingIndex = Int()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
        
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
        
    }
    class optionsTapGesture: UITapGestureRecognizer {
        var track = Track()
    }
    @objc func optionsTapped(_ sender: optionsTapGesture) {
        showOptionsController(track: sender.track)
    }
    

    //MARK: - dismiss method
    @objc func backToPreviousVC(){
            self.navigationController?.popViewController(animated: true)
    }
    class showMoreTapGesture: UITapGestureRecognizer {
        var showMoreTypeSelected = String()
    }

    @objc func showMoreTapped(_ sender: showMoreTapGesture) {
        
        switch sender.showMoreTypeSelected {
        case "Artist":
            performSegue(withIdentifier: "segueArtist", sender: nil)
        case "Albums":
            performSegue(withIdentifier: "segueArtistAlbums", sender: nil)
         default:
            //do nothing
            break
        }
        
    }

    override func viewDidAppear(_ animated: Bool) {
        //add retry observer
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(retry),
                                               name: Notification.Name.retryClicked,
                                               object: nil)
    }
    @objc func retry(){
        self.dismissErrorView()
        loadArtistAlbums()
        loadTrack()
    }

    //MARK: - Load Current Artist's Albums From API

    func loadArtistAlbums()  {
self.showLoadingView()
        APIClient.getArtist(artistId: artistID){ result in
            self.dismissLoadingView()
            switch result.result {
            case .success(let value):
           
                    let  albums = value.albums
                    
                    for album in albums ?? []
                        
                    {
                        self.albums.append(album)
                        
                    }
                
                self.collectionViewAlbums.reloadData()
                
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.loadArtistAlbums()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }


}


func loadTrack()  {

    self.showLoadingView()
    APIClient.getTrack(trackID: trackID){ result in
        
        self.dismissLoadingView()
        switch result.result {
        case .success(let value):
            
            self.tracks.append(value)
            self.loadHeader(track: value)
            self.tvTracks.reloadData()
        
            
        case .failure(let encodingError):
            var statusCode:Int
            if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                // no internet connection
                statusCode = 1
            }
            else{
                statusCode = result.response?.statusCode ?? 404
            }
            if(statusCode == 401 )
            {
                APIClient.getAuthToken(){ result in
                    
                    switch result.result {
                        
                    case .success:
                        
                        Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                        self.loadTrack()
                        
                    case .failure:
                        
                        print("do something when token refresh fails")
                    }
                }
            }
            else{
                self.showErrorView(errorCode: statusCode)
            }
        }
    }
    }


    func  loadHeader ( track: Track){
        
        if(track.artwork.count > 0){

        ivCover.sd_setImage(with: URL(string: track.artwork[0].medium), completed: nil)

        ivCoverBg.sd_setImage(with: URL(string: track.artwork[0].large), completed: nil)
        }

        //blur the bg
        ivCoverBg.blurImage()

        lblTitle.text = track.title

        lblArtist.text = track.artistName

    }


    //MARK: - Prepare for Segue to Player Method

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {


        switch segue.identifier {

        case "seguePlayer":
            if let vc = segue.destination as? PlayerViewController
            {

                if (self.tracks.count > 0)
                {
                    vc.tracks = self.tracks
                }
            }

        case "segueAlbumPage":
            if let vc = segue.destination as? AlbumPageViewController {
                //get the current selected cell
                if let cell = sender as? UICollectionViewCell,
                    let indexPath = self.collectionViewAlbums.indexPath(for: cell) {
                    vc.albumID = self.albums[indexPath.row].id
                }
            }
            
        case "segueArtistAlbums":
    
             if let vc = segue.destination as? ArtistAlbumsViewController
            {
                    vc.albums = albums
            }
            
        case "segueArtist":
            
            if let vc = segue.destination as? ArtistPageViewController
            {
                vc.artistID = artistID
            }
            case "segueAddToPlaylist":
                     
                  if let destinationNavigationController = segue.destination as? UINavigationController {
                            let targetController = destinationNavigationController.topViewController as! AddToPlaylistViewController
                     targetController.track = sender as? Track
                        }

        default:
            print("Fallback option")
        }

}

    
    //IB Outlets
    
    @IBAction func goBack(_ sender: Any) {
        
         self.navigationController?.popViewController(animated: true) 
    }
    
}




extension GenreContentViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {


    //MARL: - Delegate datasource methods

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if collectionView == self.collectionViewAlbums{
            return albums.count
        }

        else {
           return tracks.count
        }

    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {



              //handle albums cellForItemAt

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "albumCell", for: indexPath) as! AlbumCell

        let url : String

        if ( albums[indexPath.row].artwork.count > 0){

            url =  albums[indexPath.row].artwork[0].medium

        }
        else
        {
            //might be a blank array
            url = "no_cover"

        }

        //set the genre image using sd web image library asycnhronously

        cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = albums[indexPath.row].title



            //add rounded edge
            cell.layer.cornerRadius =  5

        return cell

        }





        //MARK: - UICollectionViewDelegateFlowLayout Delegate Method


        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

  return CGSize(width: 160, height: 194)

    }


        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

            return 5
        }
}



extension GenreContentViewController: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as! TrackTableCell
        let url : String
        
        if ( tracks[indexPath.row].artwork.count > 0){
            
            url =  tracks[indexPath.row].artwork[0].small
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        print(tracks[indexPath.row].artistName)
        print(tracks[indexPath.row].title)
        cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = tracks[indexPath.row].title
        cell.lblArtist.text = tracks[indexPath.row].artistName
        cell.lblTrackIndex.text = String(indexPath.row + 1)
        
        // Add a separator below each cell
        let horizontalGap = 15.0 as CGFloat
        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
        cell.addSubview(seperatorView)
        
        
        
        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        
        let tapOptions = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
        tapOptions.track = tracks[indexPath.row]
        
        cell.btnOptions.addGestureRecognizer(tapOptions)
        cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
        if(!isTrackDownloaded(trackID: tracks[indexPath.row].id)){
            cell.ivDownloaded.isHidden = true
        }
        
        
        
        tappy.tracks = tracks
        tappy.currrentPlayingIndex = indexPath.row
        cell.addGestureRecognizer(tappy)
        
        return cell
}


}
