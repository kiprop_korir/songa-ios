//
//  SubGenresViewController.swift
//  Songa
//
//  Created by Collins Korir on 9/19/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper
import SDWebImage
import RealmSwift

class SubGenresViewController : BaseViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout  {
    
   
    @IBOutlet var collectionView: UICollectionView!
    let realm = try! Realm()
    var loggedInUsers: [RAGUser] = []
    
    
    var genres: [Genre] = []
    var parentID  = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        loadSubGenres()
     
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //add retry observer
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(retry),
                                               name: Notification.Name.retryClicked,
                                               object: nil)
        
    }
    
  
    @objc func retry(){
        self.dismissErrorView()
        loadSubGenres()
    }
    
    
   
    //MARK: - Prepare for Segue to Genre Detail Method
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc = segue.destination as? GenreTracksViewController
        {
            
            //get the current selected cell
            if let cell = sender as? UICollectionViewCell,
                let indexPath = self.collectionView.indexPath(for: cell) {
                
                vc.genreTitle = genres[indexPath.row].name
                vc.genreID = genres[indexPath.row].id
                
            }
        }
    }
    
    
    
    //MARK: - Load Genres From API
    
    func loadSubGenres()  {
        
        self.showLoadingView()
        APIClient.getSubGenres(parentId: self.parentID) { result in
            
            
           
            self.dismissLoadingView()
            switch result.result {
            case .success(let value):
                
                
                for genre in value.subGenres ?? []
                    
                {
                    self.genres.append(genre)
                    
                }
                
                self.collectionView.reloadData()
                
                
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.loadSubGenres()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
        
        
    }
    
    
    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  20
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }
    
    //MARK: - Collectionview Delegate Methods
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return genres.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "genreCell", for: indexPath) as! GenreCell
        
        let url : String
        
        if ( genres[indexPath.row].artwork.count > 0){
            
            url = temporaryChangeURL(oringinalURL: genres[indexPath.row].artwork[0].medium)
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        
        //set the genre image using sd web image library asycnhronously
        
          cell.ivGenre.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"), completed: {
                 (image, error, cacheType, url) in
                 
                 // check subviews and remove layers to avoid multiplelayering of layers after cv cell appear
                 
                 if(cell.ivGenre.layer.sublayers != nil){
                 for layer in cell.ivGenre.layer.sublayers! {
                         layer.removeFromSuperlayer()
                              }
                 }
                let coverLayer = CALayer()
                 coverLayer.frame = CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height)
                 coverLayer.backgroundColor = UIColor.black.cgColor
                 coverLayer.opacity = 0.5
                 cell.ivGenre.layer.addSublayer(coverLayer)
             })
        cell.lblGenre.text = genres[indexPath.row].name
        cell.lblGenre.textColor = UIColor.white
        
        //add rounded edge
        
        cell.layer.cornerRadius =  5
        
        //         cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(genreTapped(_:))))
        
        return cell
        
        
    }
    
    
    
    
    func temporaryChangeURL ( oringinalURL : String) -> String  {
        
        let finalURL = oringinalURL.replacingOccurrences(of: "http://d1hvwy4f60owrp.cloudfront.net/", with: "https://assets.songamusic.com/")
        
        return finalURL
        
        
    }
  
}
