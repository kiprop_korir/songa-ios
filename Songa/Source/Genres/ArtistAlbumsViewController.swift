//
//  ArtistAlbumsViewController.swift
//  Songa
//
//  Created by Collins Korir on 2/22/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import UIKit
import  Alamofire
import SDWebImage
import  RealmSwift


class ArtistAlbumsViewController: BaseViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout  {
    
    //var mainVC:MainViewController = MainViewController()
    
    var albums: [Album] = []
    @IBOutlet var collectionView: UICollectionView!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        
    }
    
    @IBAction func goBack(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    //MARK: - Prepare for Segue to Genre Detail Method
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc = segue.destination as? AlbumPageViewController
        {
            
            //get the current selected cell
            if let cell = sender as? UICollectionViewCell,
                let indexPath = self.collectionView.indexPath(for: cell) {
                
                vc.title = albums[indexPath.row].title
                vc.albumID = albums[indexPath.row].id
                
            }
        }
    }
    
    
    
  
    
    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  20
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: 194)
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }
    
    //MARK: - Collectionview Delegate Methods
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "albumCell", for: indexPath) as! AlbumCell
        
        let url : String
        
        if ( albums[indexPath.row].artwork.count > 0){
            
            url =  albums[indexPath.row].artwork[0].medium
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        
        //set the genre image using sd web image library asycnhronously
        
        
        
        cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = albums[indexPath.row].title
        
        //add rounded edge
        
        cell.layer.cornerRadius =  5
        
        //         cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(genreTapped(_:))))
        
        return cell
        
        
    }

}
