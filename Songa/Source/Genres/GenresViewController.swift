//
//  GenresController.swift
//  Songa
//
//  Created by Collins Korir on 7/23/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AlamofireObjectMapper
import SDWebImage
import RealmSwift

class GenresViewController : UIViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout  {
    
    let genresAPI = APIurls.Genres
    let headers = APIurls.Headers

    @IBOutlet var collectionView: UICollectionView!
     let realm = try! Realm()
        var loggedInUsers: [RAGUser] = []


     var genres: [Genre] = []

    override func viewDidLoad() {

        super.viewDidLoad()

        collectionView.dataSource = self
        collectionView.delegate = self
        loadGenres()
        setUpNavBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //add retry observer
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(retry),
                                               name: Notification.Name.retryClicked,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setUpNavBar),
                                               name: Notification.Name.notificationReceived,
                                               object: nil)
        
        setUpNavBar()
    }
    
    @objc func setUpNavBar(){
            let view = UIView(frame: CGRect(x: 0, y: 2, width: 40, height: 40))
               
               let logo = UIImageView(frame: CGRect(x: 0 , y: 10, width: 25, height: 25))
               logo.image = #imageLiteral(resourceName: "ic_profile")
               view.addSubview(logo)
               
               let count = RoundEdgesButton(frame: CGRect(x: 20, y: 0, width: 20, height: 20))
               count.backgroundColor = UIColor.red
               count.cornerRadius = 10
               
               let notificationsCount = realm.objects(SongaNotification.self).filter("isRead == false").count
               
               if(notificationsCount>0){
                   count.setTitle(String(notificationsCount), for: .normal)
                   count.titleLabel?.font = .systemFont(ofSize: 12)
                   view.addSubview(count)
               }
               
               let barButtonItem = UIBarButtonItem(customView: view)
               self.navigationItem.leftBarButtonItem = barButtonItem
               
               let tap = UITapGestureRecognizer(target: self, action: #selector(self.profileViewClicked))
               barButtonItem.customView?.addGestureRecognizer(tap)
               
               let searchButton = UIBarButtonItem(image: UIImage(imageLiteralResourceName: "ic_search").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(self.searchClicked))
        
               self.navigationItem.rightBarButtonItem = searchButton
               self.navigationItem.rightBarButtonItem?.tintColor = ThemeManager.currentTheme().barTint
               
    }
    
    @objc func retry(){
        self.dismissErrorView()
        loadGenres()
    }
    
   
    @objc func profileViewClicked(){
        performSegue(withIdentifier: "segueProfile", sender: self)
    }
    
    @objc func searchClicked(){
        //performSegue(withIdentifier: "segueSearch", sender: self)
        let storyboard = UIStoryboard(name: "Search", bundle: nil)
        let searchVC = storyboard.instantiateInitialViewController()
        self.navigationController?.present(searchVC!, animated: true)
    }
      //MARK: - Prepare for Segue to Genre Detail Method

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
      if let vc = segue.destination as? SubGenresViewController
      {

        //get the current selected cell
        if let cell = sender as? UICollectionViewCell,
            let indexPath = self.collectionView.indexPath(for: cell) {

            vc.parentID = genres[indexPath.row].id
            vc.title = genres[indexPath.row].name

        }
        }
    }



    //MARK: - Load Genres From API

    func loadGenres()  {

        self.showLoadingView()
        APIClient.getGenres() { result in

            self.dismissLoadingView()
            switch result.result {
            case .success(let value):
                
                
                for genre in value
                    
                {
                    
                    self.genres.append(genre)
                    
                    
                }
            
            self.collectionView.reloadData()
    
                
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.loadGenres()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
    

    }


    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  20
        let collectionViewSize = collectionView.frame.size.width - padding

        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)


    }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

        return 5
    }

    //MARK: - Collectionview Delegate Methods




    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return genres.count
    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {


        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "genreCell", for: indexPath) as! GenreCell

        let url : String

        if ( genres[indexPath.row].artwork.count > 0){

        url =  genres[indexPath.row].artwork[0].medium

        }
        else
        {
            //might be a blank array
          url = "no_cover"

       }


        //set the genre image using sd web image library asycnhronously



        cell.ivGenre.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"), completed: {
            (image, error, cacheType, url) in
            
            // check subviews and remove layers to avoid multiplelayering of layers after cv cell appear
            
            if(cell.ivGenre.layer.sublayers != nil){
            for layer in cell.ivGenre.layer.sublayers! {
                    layer.removeFromSuperlayer()
                         }
            }
           let coverLayer = CALayer()
            coverLayer.frame = CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height)
            coverLayer.backgroundColor = UIColor.black.cgColor
            coverLayer.opacity = 0.5
            cell.ivGenre.layer.addSublayer(coverLayer)
        })
        cell.lblGenre.text = genres[indexPath.row].name
        cell.lblGenre.textColor = UIColor.white

        //add rounded edge

        cell.layer.cornerRadius =  5

//         cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(genreTapped(_:))))
        
        

        // when tapped
        //coverLayer.opacity = 0.1

        return cell


    }

}
