//
//  GenreDetailsViewController.swift
//  Songa
//
//  Created by Collins Korir on 7/24/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import UIKit
import  Alamofire
import AlamofireObjectMapper
import SDWebImage

class GenreTracksViewController: BaseViewController , UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    var genreTitle:String = ""
    var genreCover:String = ""
    var genreID:String = ""
    var tracks: [Track] = []
    let headers = APIurls.Headers
    
    @IBOutlet weak var btnBack: UIBarButtonItem!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var navItemGenre: UINavigationItem!
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.navigationItem.title = genreTitle
       
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        loadtracks()
        
        
        
    }
    
    
    //MARK: - Prepare for Segue to Genre Content View Method
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc = segue.destination as? GenreContentViewController
        {
            
            //get the current selected cell
            if let cell = sender as? UICollectionViewCell,
                let indexPath = self.collectionView.indexPath(for: cell) {
                
                vc.trackTitle = tracks[indexPath.row].title
                vc.genreID = genreID
                vc.trackID = tracks[indexPath.row].id
                vc.artistID = tracks[indexPath.row].artistId
                
            }
        }
    }
    
    
    //MARK: - Load tracks From API
    
    func loadtracks()  {
        
        self.showLoadingView()
        APIClient.getTracks(type: "genres", typeID: genreID){ result in

            self.dismissLoadingView()
            switch result.result {
            case .success(let value):
                
                
                for track in value
                {
                    self.tracks.append(track)
                    
                }
                
                self.collectionView.reloadData()
                
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.loadtracks()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
    }
    
    
    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  20
        let collectionViewWidth = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewWidth/2, height: 217)
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }
    
    //MARK: - Collectionview Delegate Methods
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tracks.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "trackCell", for: indexPath) as! TrackCell
        
        let url : String
        
        if ( tracks[indexPath.row].artwork.count > 0){
            
            url = temporaryChangeURL(oringinalURL: tracks[indexPath.row].artwork[0].medium)
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        //set the genre image using sd web image library asycnhronously
        
        cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = tracks[indexPath.row].title
        cell.lblArtist.text = tracks[indexPath.row].artistName
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        return cell
        
    }
    
    
    func temporaryChangeURL ( oringinalURL : String) -> String  {
        let finalURL = oringinalURL.replacingOccurrences(of: "http://d1hvwy4f60owrp.cloudfront.net/", with: "https://assets.songamusic.com/")
        return finalURL
    }
    
}
