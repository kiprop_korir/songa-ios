//
//  NewPlaylistViewController.swift
//  Songa
//
//  Created by Kiprop Korir on 26/08/2019.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import RealmSwift

class NewPlaylistViewController: UIViewController {

    var track:Track? = nil
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblArtist: UILabel!
    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var lblPlaylistName: SearchTextField!
    @IBOutlet weak var lblPlaylistDescription: SearchTextField!
    
    override func viewDidLoad() {
        lblTitle.text = track?.title
        lblArtist.text = track?.artistName
        
        lblPlaylistName.placeHolderColor = .lightGray
        lblPlaylistDescription.placeHolderColor = .lightGray
  
        let url : String
        if ( track?.artwork.count ?? 0 > 0){
            url = track?.artwork[0].medium ?? ""
        }
        else
        {    url = "no_cover"
        }
        ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
    }

    @IBAction func savePlaylist(_ sender: Any) {
      //create playlist
        
        if(lblPlaylistName.text?.count == 0){
            Toast.show(message: "Title can't be empty", controller: self)
        }
        else {
            
         
            
            Toast.show(message: "Track added to playlist", controller: self.parent!)
            
            syncPlaylists()
            
            self.dismissVC(self)
            
        }
    }
        
        func syncPlaylists(){
            let playlistCollection = PlaylistCollection()
            playlistCollection.name =  lblPlaylistName.text ?? ""
            playlistCollection.playlistDescription = lblPlaylistDescription.text ?? ""
            playlistCollection.creator = Defaults.getUser()?.id ?? ""
            playlistCollection.shared = false
            
            var tracksIds = Array<String>()
            tracksIds.append(self.track?.id ?? "")
            
            playlistCollection.tracks = tracksIds
            
            let playlistWrapper = PlaylistWrapper()
            playlistWrapper.playlist = playlistCollection
            
            APIClient.createPlaylist(playlistWrapper: playlistWrapper) { result in
                
               print(result.request)
                print(result.result)
              
                switch result.result {
                case .success:
                    
                    //save to realm
                    let newPlaylist =  UserPlaylist()
                    newPlaylist.name = self.lblPlaylistName.text ?? ""
                    newPlaylist.playlistDescription = self.lblPlaylistDescription.text ?? ""
                    newPlaylist.creator = Defaults.getUser()?.id ?? ""
                    newPlaylist.shared = false
                    newPlaylist.tracks.append(self.track!)
                    newPlaylist.updatedAt = result.value?.playlist?.updatedAt ?? ""
                    newPlaylist.id = result.value?.playlist?.id ?? ""
                    
                    do {
                        let realm = try Realm()
                        
                        try realm.write {
                            realm.add(newPlaylist, update: true)
                        }
                    } catch let error as NSError {
                        print(error)
                    }
                    print("success")
                    
                case .failure:
                    
                    print("fail")
                    
                }
            }
            
    }
    @IBAction func dismissVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
