//
//  NotificationsViewController.swift
//  Songa
//
//  Created by Collins Korir on 9/5/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import RealmSwift

class NotificationsViewController: UIViewController {

    
    // TODO: Add track play 
    @IBOutlet weak var tvNotifications: UITableView!
    var itemID = ""
        var notifications : [SongaNotification] = []
        let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tvNotifications.delegate = self
        tvNotifications.dataSource = self
        getNotificationsFromRealm()
        tvNotifications.separatorStyle = .none
         tvNotifications.allowsSelection = false
        self.navigationItem.title = "Notifications"
    }
    
    func getNotificationsFromRealm(){
        
            //empty the array variables
            notifications.removeAll()
           
            let notificationsResults: Results<SongaNotification>? = realm.objects(SongaNotification.self)
        
            for notificationsResult in notificationsResults! {
                notifications.append(notificationsResult)
            }
        
        if(notificationsResults?.count == 0){
            tvNotifications.setEmptyMessage("You have no new notifications")
        }
      
          tvNotifications.reloadData()

    }
    @IBAction func back(_ sender: Any) {
           //self.navigationController?.popViewController(animated: true)
           self.dismiss(animated: true, completion: nil)
       }
    
    class deleteTapGesture: UITapGestureRecognizer {
        var messageId = String()
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var type = String()
        var id = String()
    }
    
    @objc func deleteTapped(_ sender: deleteTapGesture) {
        
        let id  = sender.messageId
        deleteNotification(id: id)
    
    }
    
    func deleteNotification(id: String){
        do {
                   let realm = try Realm()
                   try realm.write {
                       
                       let objectsToDelete = realm.objects(SongaNotification.self).filter("itemId = '\(id)'")
                       realm.delete(objectsToDelete)
                       getNotificationsFromRealm()
                       tvNotifications.reloadData()
                       
                   }
               } catch let error as NSError {

                   print(error)
               }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let value = sender as! String
         switch value{
               case "artist":
                let vc = segue.destination as! ArtistPageViewController
                vc.artistID = sender as! String
               case "album":
                     let vc = segue.destination as! AlbumPageViewController
                                   vc.albumID = itemID
               case "chart":
                    let vc = segue.destination as! ChartViewController
                                   vc.chartID = sender as! String
               case "playlist":
                let vc = segue.destination as! PlaylistViewController
                                   vc.playlistID = sender as! String
               case "track":
                   //playTrack(track: <#T##Track#>)
                   print("")
         
               default:
                   print("unknown")
               }
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
        let type  = sender.type
        itemID = sender.id
    
        deleteNotification(id: itemID)
        switch type {
        case "artist":
            performSegue(withIdentifier: "segueArtist", sender: "artist")
        case "album":
             performSegue(withIdentifier: "segueAlbum", sender: "album")
        case "chart":
             performSegue(withIdentifier: "segueChart", sender: "chart")
        case "playlist":
             performSegue(withIdentifier: "seguePlaylist", sender: "playlist")
        case "track":
            //playTrack(track: <#T##Track#>)
            print("")
  
        default:
            print("unknown")
        }
        
    }
    func playTrack (track : Track){
        
        var tracks :[Track] = []
        tracks.append(track)
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                            object: nil,
                                            userInfo:["tracks": tracks ,"currentPlayingIndex": 0])
    }
    
    
    //MARK:- IB Outlets
    
 
    @IBAction func goBack(_ sender: Any) {
        
        let nav = self.navigationController //grab an instance of the current navigationController
        DispatchQueue.main.async { //make sure all UI updates are on the main thread.
           // nav?.view.layer.add(CATransition().popFromRight(), forKey: nil)
            nav?.popViewController(animated: true)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateNotificationsList),
                                               name: Notification.Name.notificationReceived,
                                               object: nil)
    }
    
    @objc func updateNotificationsList(){
        self.tvNotifications.reloadData()
    }
}

extension NotificationsViewController :UITableViewDelegate , UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   
         return notifications.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationCell
        
        let url : String
     
        url = notifications[indexPath.row].imageUrl
   
        //set the genre image using sd web image library asycnhronously
        
        
        cell.imgNotification.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = notifications[indexPath.row].title
        cell.lblDescription.text = notifications[indexPath.row].message
        
//        cell.lblTime.text = self.timeInterval(timeAgo: notifications[indexPath.row].timeStamp)
        
                cell.lblTime.text = notifications[indexPath.row].timeStamp
        cell.btnCancel.tintColor = .colorAccent
    
        let deleteTap = deleteTapGesture(target: self, action: #selector(deleteTapped(_:)))
        deleteTap.messageId = notifications[indexPath.row].itemId
        cell.btnCancel.addGestureRecognizer(deleteTap)
        
        let cellTap = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        cellTap.type = notifications[indexPath.row].itemType
        cellTap.id = notifications[indexPath.row].itemId
        cell.addGestureRecognizer(cellTap)
        
        // Add a separator below each cell
               let horizontalGap = 15.0 as CGFloat
               // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
               let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
               seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
               cell.addSubview(seperatorView)
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    

}

