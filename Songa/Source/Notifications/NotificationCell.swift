//
//  NotificationCell.swift
//  Songa
//
//  Created by Collins Korir on 9/5/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import UIKit

class NotificationCell: UITableViewCell {
    //sample cell
    
    @IBOutlet weak var lblTitle: SongaAccentTextLabel!
    @IBOutlet weak var imgNotification: UIImageView!
    @IBOutlet weak var lblTime: SongaSecondaryTextLabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnCancel: RoundEdgesButton!
    
}
