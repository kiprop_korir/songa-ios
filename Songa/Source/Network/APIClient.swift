//
//  ApiClient.swift
//  Songa
//
//  Created by Collins Korir on 2/15/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//



import Alamofire
import ObjectMapper


class APIClient {
    
    static func getPlaylist(playlistId: String, completion:@escaping (DataResponse<PlaylistResponse>)->Void) {
        Alamofire.request(APIRouter.Content.getPlaylist(playlistId: playlistId)).responseObject() { (response: DataResponse<PlaylistResponse>) in
            completion(response)
        }
    }
    static func getAlbum(albumId: String, completion:@escaping (DataResponse<AlbumResponse>)->Void) {
        Alamofire.request(APIRouter.Content.getAlbum(albumId: albumId)).responseObject() { (response: DataResponse<AlbumResponse>) in
            completion(response)
        }
    }
    static func getChart(chartId: String, completion:@escaping (DataResponse<ChartResponse>)->Void) {
        Alamofire.request(APIRouter.Content.getChart(chartId: chartId)).responseObject() { (response: DataResponse<ChartResponse>) in
            completion(response)
        }
    }
    static func getArtist(artistId: String, completion:@escaping (DataResponse<ArtistResponse>)->Void) {
        Alamofire.request(APIRouter.Content.getArtist(artistId: artistId)).responseObject() { (response: DataResponse<ArtistResponse>) in
            completion(response)
        }
    }
    static func getGenres (completion:@escaping (DataResponse<[Genre]>)->Void) {
        Alamofire.request(APIRouter.Content.getGenres).responseArray { (response: DataResponse<[Genre]>) in
            completion(response)
        }
    }
    static func getPodcasts (completion:@escaping (DataResponse<[PodcastArtist]>)->Void) {
        Alamofire.request(APIRouter.Content.getPodcasts).responseArray { (response: DataResponse<[PodcastArtist]>) in
            completion(response)
        }
    }
    static func getCuratedPodcasts (completion:@escaping (DataResponse<CuratedPodcastsResponse>)->Void) {
        
        let headers = [
            "X-ListenAPI-Key": "5496d82c282044adaf11d1f3ccbb1158"
        ]
        
        Alamofire.request( "https://listen-api.listennotes.com/api/v2/curated_podcasts?page=1", method: .get, headers: headers)
            .responseObject()  { (response: DataResponse<CuratedPodcastsResponse>) in
                completion(response)
        }
    }
    
    static func getCuratedPodcastDetails (podcastId: String, completion:@escaping (DataResponse<Podcast>)->Void) {
        
        let headers = [
            "X-ListenAPI-Key": "5496d82c282044adaf11d1f3ccbb1158"
        ]
        
        Alamofire.request( "https://listen-api.listennotes.com/api/v2/podcasts/\(podcastId)?sort=recent_first", method: .get, headers: headers)
            .responseObject()  { (response: DataResponse<Podcast>) in
                completion(response)
        }
    }
    static func getLeaderboard ( completion:@escaping (DataResponse<Any>)->Void) {
        
        let headers = [
            
            "CF-Access-Client-Id" : "f2eb71a71a2d489121273492c7271ce7.access.songamusic.com",
            "CF-Access-Client-Secret" : "75c93016e4b304986e9c29316aa2317d54ee81023efa29f3e1ed2998ef12ef7e"
        ]
        
        Alamofire.request( "https://metabase.songamusic.com/api/public/card/c82a1337-c0e9-4541-bbfb-0154097e7350/query", method: .get, headers: headers)
            .responseJSON()  { (response: DataResponse<Any>) in
                completion(response)
        }
    }
    
    static func getUserLeaderboardPosition ( completion:@escaping (DataResponse<[LeaderboardPosition]>)->Void) {
           
           let headers = [
               
               "CF-Access-Client-Id" : "f2eb71a71a2d489121273492c7271ce7.access.songamusic.com",
               "CF-Access-Client-Secret" : "75c93016e4b304986e9c29316aa2317d54ee81023efa29f3e1ed2998ef12ef7e"
           ]
        
        let userID  = Defaults.getUser()?.id
        Alamofire.request( "https://metabase.songamusic.com/api/public/card/5ce632d2-b93a-44ad-af2e-590591a1a0ae/query/json?parameters=[{\"type\":\"category\",\"target\":[\"variable\",[\"template-tag\",\"user_id\"]],\"value\":\(userID ?? "")\"}]", method: .get, headers: headers)
               .responseArray() { (response: DataResponse<[LeaderboardPosition]>) in
                   completion(response)
           }
       }
    
    static func getCampaigns (completion:@escaping (DataResponse<[Campaign]>)->Void) {
        Alamofire.request(APIRouter.Campaigns.getCampaigns).responseArray { (response: DataResponse<[Campaign]>) in
            completion(response)
        }
    }
    
    static func getVoucher (campaignID: String, completion:@escaping (DataResponse<Voucher>)->Void) {
        Alamofire.request(APIRouter.Campaigns.getVoucher(campaignID: campaignID)).responseObject() { (response: DataResponse<Voucher>) in
            completion(response)
        }
    }
    
    static func redeemVoucher (campaignID: String,codeShort: String,  completion:@escaping (DataResponse<VoucherRedemptionResponse>)->Void) {
        Alamofire.request(APIRouter.Campaigns.redeemVoucher(campaignID: campaignID,codeShort: codeShort )).responseObject() { (response: DataResponse<VoucherRedemptionResponse>) in
            completion(response)
        }
    }
    static func applyVoucher (campaignID: String,promoCode: String,gifterID: String,redemptionID: String,  completion:@escaping (DataResponse<SubscriptionResponse>)->Void) {
        Alamofire.request(APIRouter.Campaigns.applyVoucher( campaignID: campaignID, promoCode: promoCode, gifterID: gifterID, redemptionID: redemptionID)).responseObject() { (response: DataResponse<SubscriptionResponse>) in
            completion(response)
        }
    }
    
    
    
    static func getSubGenres (parentId: String, completion:@escaping (DataResponse<SubGenresResponse>)->Void) {
        Alamofire.request(APIRouter.Content.getSubGenres(parentId: parentId)).responseObject() { (response: DataResponse<SubGenresResponse>) in
            completion(response)
        }
    }
    static func getGenre(genreId: String, completion:@escaping (DataResponse<GenresResponse>)->Void) {
       
        Alamofire.request(APIRouter.Content.getGenre(genreId: genreId)).responseObject() { (response: DataResponse<GenresResponse>) in
            completion(response)
        }
    }
    static func getTracks(type: String ,typeID: String, completion:@escaping (DataResponse<[Track]>)->Void) {
        Alamofire.request(APIRouter.Content.getTracks(type: type ,typeID: typeID)).responseArray { (response: DataResponse<[Track]>) in
            completion(response)
        }
    }
    static func getAlbums(type: String ,typeID: String, completion:@escaping (DataResponse<[Album]>)->Void) {
        Alamofire.request(APIRouter.Content.getAlbums(type: type ,typeID: typeID)).responseArray { (response: DataResponse<[Album]>) in
            completion(response)
        }
    }
    static func getSearch(searchTerm: String, completion:@escaping (DataResponse<SearchResponse>)->Void) {
        Alamofire.request(APIRouter.Content.getSearch(searchTerm: searchTerm)).responseObject() { (response: DataResponse<SearchResponse>) in
            completion(response)
        }
    }
    static func getFMStations( completion:@escaping (DataResponse<[Station]>)->Void) {
        Alamofire.request(APIRouter.Content.getFMStations).responseArray { (response: DataResponse<[Station]>) in
            completion(response)
        }
    }
    
    static func getComedy( completion:@escaping (DataResponse<[Comedy]>)->Void) {
          Alamofire.request(APIRouter.Content.getComedy).responseArray { (response: DataResponse<[Comedy]>) in
              completion(response)
          }
      }
    static func getDiscoverContent( completion:@escaping (DataResponse<DiscoverResponse>)->Void) {
        Alamofire.request(APIRouter.Content.getDiscoverContent).responseObject() { (response: DataResponse<DiscoverResponse>) in
            completion(response)
        }
    }
    
    static func createPlaylist( playlistWrapper: PlaylistWrapper,completion:@escaping (DataResponse<FavouritesAndPlaylistResponse>)->Void) {
        Alamofire.request(APIRouter.Content.createPlaylist(playlist: playlistWrapper)).responseObject() { (response: DataResponse<FavouritesAndPlaylistResponse>) in
            completion(response)
        }
    }
    
    static func createFavourite( favouriteWrapper: FavouriteWrapper,completion:@escaping (DataResponse<FavouritesAndPlaylistResponse>)->Void) {
        Alamofire.request(APIRouter.Content.createFavourite(favourite: favouriteWrapper)).responseObject() { (response: DataResponse<FavouritesAndPlaylistResponse>) in
            completion(response)
        }
    }
    
    static func getUserProfile( userID: String,completion:@escaping (DataResponse<UserProfileResponse>)->Void) {
        Alamofire.request(APIRouter.Content.getUserProfile(userID: userID)).responseObject() { (response: DataResponse<UserProfileResponse>) in
            completion(response)
        }
    }
    
    static func getTrack(trackID: String,completion:@escaping (DataResponse<Track>)->Void) {
         Alamofire.request(APIRouter.Content.getTrack(trackID: trackID)).responseObject() { (response: DataResponse<Track>) in
            completion(response)
        }
    }
    static func getAuthToken( completion:@escaping (DataResponse<AuthResponse>)->Void) {
        Alamofire.request(APIRouter.Auth.getAuthToken).responseObject() { (response: DataResponse<AuthResponse>) in
            completion(response)
        }
    }
    static func msisdnLogin( msisdn: String,completion:@escaping (DataResponse<LoginResponse>)->Void) {
        Alamofire.request(APIRouter.Auth.msisdnLogin(msisdn:msisdn)).responseObject() { (response: DataResponse<LoginResponse>) in
            completion(response)
        }
    }
    
    //billing
    static func getUserSubscription(externalKey: String, completion:@escaping (DataResponse<SubscriptionResponse>)->Void) {
        Alamofire.request(APIRouter.Billing.getUserSubscription(externalKey: externalKey)).responseObject() { (response: DataResponse<SubscriptionResponse>) in
            completion(response)
        }
    }
    
    static func createUserSubscription(planName: String, planID: String, completion:@escaping (DataResponse<SubscriptionResponse>)->Void) {
        Alamofire.request(APIRouter.Billing.createSubscription(planName: planName, planID: planID)).responseObject() { (response: DataResponse<SubscriptionResponse>) in
            completion(response)
        }
    }
     
    static func cancelUserSubscription(completion:@escaping (DataResponse<SubscriptionResponse>)->Void) {
          Alamofire.request(APIRouter.Billing.cancelSubscription).responseObject() { (response: DataResponse<SubscriptionResponse>) in
              completion(response)
          }
      }
    static func getPlans( completion:@escaping (DataResponse<[Plan]>)->Void) {
        Alamofire.request(APIRouter.Billing.getPlans).responseArray() { (response: DataResponse<[Plan]>) in
            completion(response)
        }
    }
    
    static func payNow(externalKey: String,completion:@escaping (DataResponse<Any>)->Void) {
        Alamofire.request(APIRouter.Billing.payNow(externalKey: externalKey)).responseJSON { (response: DataResponse) in
            completion(response)
        }
    }
}
