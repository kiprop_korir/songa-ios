//
//  ChartResponse.swift
//  Songa
//
//  Created by Collins Korir on 9/7/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class ChartResponse: Mappable {
    
  
    var id: String?
    var name: String?
    var description: String?
    var updatedAt: String?
    var artwork: [Artwork]?
    var tracks: [Track]?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        updatedAt <- map["updated_at"]
        artwork <- map["images"]
        tracks <- map["tracks"]
        
    }
    
    
}
