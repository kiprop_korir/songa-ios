//
//  ArtistResponse.swift
//  Songa
//
//  Created by Collins Korir on 9/10/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class ArtistResponse: Mappable {
   
    var id: String?
    var name: String?
    var bio: String?
    var artwork: [Artwork]?
    var albums: [Album]?
    var tracks: [Track]?
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        bio <- map["bio"]
        albums <- map["albums"]
        artwork <- map["images"]
        tracks <- map["tracks"]
        
    }
    
    
}
