//
//  LoginResponse.swift
//  Songa
//
//  Created by Kiprop Korir on 08/01/2019.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginResponse: Mappable {
    
    var accessToken: String?
    var tokenType: String?
    var expiresIn: Int?
    var user: User?
    
    required init?(map: Map){
        
    }
    
    
    func mapping(map: Map) {
        accessToken <- map["access_token"]
        tokenType <- map["token_type"]
        expiresIn <- map["expires_in"]
        user <- map["user"]
        
        
    }
    
    
}
