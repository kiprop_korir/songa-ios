//
//  SearchResponse.swift
//  Songa
//
//  Created by Collins Korir on 9/19/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import ObjectMapper

class SearchResponse: Mappable {
    
    


    var albums: [Album]?
    var artists: [Artist]?
    var tracks: [Track]?
    var stations: [Station]?
    var playlists: [Playlist]?
    

    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
    
        albums <- map["albums"]
        artists <- map["artists"]
        tracks <- map["tracks"]
        stations <- map["stations"]
        playlists <- map["playlists"]
     
    }
    
    
}
