//
//  CuratedPodcastsResponse.swift
//  Songa
//
//  Created by Collins Korir on 9/21/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class CuratedPodcastsResponse: Mappable {
    
    var curatedLists: [PodcastList]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
       
        curatedLists <- map["curated_lists"]
        
    }
    
    
}
