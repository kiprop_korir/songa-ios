//
//  LeaderboardResponse.swift
//  Songa
//
//  Created by Collins Korir on 9/25/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//


import ObjectMapper

class LeaderboardResponse: Mappable {
    
    
    var leaderboardData: LeaderboardStats?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        leaderboardData <- map["data"]
        
    }
    
    
}

class LeaderboardStats: Mappable {
    
    
    var rows: [(String, Int)]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        rows <- map["rows"]
        
    }
    
    
}


