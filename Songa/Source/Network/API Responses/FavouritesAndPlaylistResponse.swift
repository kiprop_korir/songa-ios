//
//  FavouritesAndPlaylistResponse.swift
//  Songa
//
//  Created by Collins Korir on 8/21/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class FavouritesAndPlaylistResponse: Mappable {
  
    var success : Bool?
    var playlist : Playlist?
    var favourite : Favourites?
    var message : String?
    var updated : String?
  
    required init?(map: Map){
    }
   
    func mapping(map: Map) {
        success <- map["success"]
        playlist <- map["playlist"]
        favourite <- map["favourite"]
        message <- map["message"]
        updated <- map["updated"]
    }
    
}
