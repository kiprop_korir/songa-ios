//
//  AlbumResponse.swift
//  Songa
//
//  Created by Collins Korir on 9/11/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation

import ObjectMapper

class AlbumResponse: Mappable {
    
    
    var id: String?
    var title: String?
    var artistName: String?
    var trackCount: Int?
    var artwork: [Artwork]?
    var tracks: [Track]?
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        artistName <- map["artist_name"]
         trackCount <- map["track_count"]
        artwork <- map["images"]
        tracks <- map["tracks"]
        
    }
    
}

