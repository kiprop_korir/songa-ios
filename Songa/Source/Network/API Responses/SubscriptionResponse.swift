//
//  SubscriptionResponse.swift
//  Songa
//
//  Created by Collins Korir on 1/23/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class SubscriptionResponse: Mappable {
    
    var accountId : String?
    var action : String?
    var billingFrequency : String?
    var createdAt : Int?
    var externalKey : String?
    var id : String?
    var message : String?
    var nextBillingDate : Int? = nil
    var paymentStatus : String?
    var phoneNumber : String?
    var planId : String?
    var planName : String?
    var productId : String?
    var productPlanType : String?
    var reason : String?
    var state : String?
    var subscriptionId : String?
    var type : String?
    var updatedAt : Int?

    
    required init?(map: Map){
        
    }
    
    
    func mapping(map: Map) {
        accountId <- map["accountId"]
        action <- map["action"]
        billingFrequency <- map["billingFrequency"]
        createdAt <- map["createdAt"]
        externalKey <- map["externalKey"]
        id <- map["id"]
        message <- map["message"]
        nextBillingDate <- map["nextBillingDate"]
        paymentStatus <- map["paymentStatus"]
        phoneNumber <- map["phoneNumber"]
        planId <- map["planId"]
        planName <- map["planName"]
        productId <- map["productId"]
        productPlanType <- map["productPlanType"]
        reason <- map["reason"]
        state <- map["state"]
        subscriptionId <- map["subscriptionId"]
        type <- map["type"]
        updatedAt <- map["updatedAt"]
        
        
    }
    
}
