//
//  DiscoverResponse.swift
//  Songa
//
//  Created by Collins Korir on 7/19/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import ObjectMapper

class AuthResponse: Mappable {

    var accessToken: String?
     var tokenType: String?
     var expiresIn: Int?
    var createdAt: Int?


    required init?(map: Map){

    }


    func mapping(map: Map) {
        accessToken <- map["access_token"]
        tokenType <- map["token_type"]
         expiresIn <- map["expires_in"]
        createdAt <- map["created_at"]


    }


}


