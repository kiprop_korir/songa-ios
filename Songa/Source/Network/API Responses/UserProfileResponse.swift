//
//  UserProfileResponse.swift
//  Songa
//
//  Created by Collins Korir on 8/30/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class UserProfileResponse: Mappable {
    
    var playlist : [Playlist]?
    var favourite : [Any]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        playlist <- map["playlist"]
        favourite <- map["favourite"]
    }
    
}
