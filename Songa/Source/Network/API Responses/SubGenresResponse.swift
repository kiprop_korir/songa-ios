//
//  SubGenresResponse.swift
//  Songa
//
//  Created by Collins Korir on 9/19/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class SubGenresResponse: Mappable {
    
    var id: String?
    var name: String?
    var subGenres: [Genre]?
   
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        subGenres <- map["sub_genres"]
        
    }
    
    
}
