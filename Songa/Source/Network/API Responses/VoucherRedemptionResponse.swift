//
//  VoucherRedemptionResponse.swift
//  Songa
//
//  Created by Collins Korir on 9/25/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class VoucherRedemptionResponse: Mappable {
    
    var gifterID: String?
    var promoCode: String?
    var redeemerID: String?
    var redemptionID: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        gifterID <- map["gifter_id"]
        promoCode <- map["promo_code"]
        redemptionID <- map["redemption_id"]
        redeemerID <- map["redeemer_id"]
        
    }
    
    
}
