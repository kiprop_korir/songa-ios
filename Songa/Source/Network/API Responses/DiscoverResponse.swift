//
//  DiscoverResponse.swift
//  Songa
//
//  Created by Collins Korir on 7/19/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import ObjectMapper

class DiscoverResponse: Mappable {

    var id: String?
    var releases: Releases?
    var popular: Popular?
    var trending: Trending?
    var charts: [Chart]?
    var playlists: [Playlist]?
    var recent: Recent?
    var heroes: [Heroe]?
    var recommended: Recommended?
    var devotions: [Devotion]?
    var comedyMappable: ComedyMappable?
    var celebrityPlaylists: [CelebrityPlaylist]?

    required init?(map: Map){

    }

    func mapping(map: Map) {
         id <- map["id"]
         releases <- map["releases"]
         popular <- map["popular"]
         trending <- map["trending"]
         charts <- map["charts"]
         playlists <- map["playlists"]
         recent <- map["recent"]
         recommended <- map["recommended"]
         heroes <- map["heroes"]
         devotions <- map["devotional"]
         celebrityPlaylists <- map["celebrity_playlists"]
        comedyMappable <- map["comedy"]

    }


}


