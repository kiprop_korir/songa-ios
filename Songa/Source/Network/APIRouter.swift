//
//  APIRouter.swift
//  Songa
//
//  Created by Collins Korir on 2/13/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import Foundation
import Alamofire

enum APIRouter {
    
enum Content: URLRequestConvertible {
    
    //endpoints

    case getPlaylist(playlistId: String)
    case getAlbum(albumId: String)
    case getChart(chartId: String)
    case getArtist(artistId: String)
    case getArtists
    case getGenres
    case getSubGenres(parentId: String)
    case getGenre(genreId: String)
    case getSearch(searchTerm: String)
    case getFMStations
    case getComedy
    case getDiscoverContent
    case getAlbums(type:String, typeID:String)
    case getTracks(type:String, typeID:String)
    case getTrack(trackID:String)
    case getUserProfile(userID:String)
    case createPlaylist(playlist:PlaylistWrapper)
       case createFavourite(favourite:FavouriteWrapper)
    case getPodcasts
     
    //MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        
        let url = try (Constants.contentBaseURL + route + path ).asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(endpoint))
        
        //Http method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.acceptType.rawValue)
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.contentType.rawValue)
        urlRequest.setValue("Bearer " + Defaults.getString(key: Defaults.accessTokenKey)!, forHTTPHeaderField: Constants.HttpHeaderField.authentication.rawValue)
        
        
        //Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
    }
    
    //MARK: - HttpMethod
    //This returns the HttpMethod type. It's used to determine the type if several endpoints are peresent
    private var method: HTTPMethod {
        switch self {
        case .getSearch:
            return .post
        case .createPlaylist:
            return .post
        case .createFavourite:
            return .post
        default:
            return .get
        }
    }
    
    //MARK: - Path
    //The path is the part following the base url
    private var path: String {
        switch self {
        case .getAlbum:
            return "/albums/"
        case .getPlaylist:
            return "/playlists/"
        case .getChart:
            return "/charts/"
        case .getArtist, .getArtists:
            return "/artists/"
        case .getSubGenres, .getGenre:
            return "/genres/"
        case .getGenres:
            return "/genres/main"
        case .getSearch:
            return "/search"
        case .getFMStations:
            return "/stations/country/ke"
        case .getDiscoverContent:
            return "/discover"
        case .getAlbums(let type, let typeID):
            return "/\(type)/\(typeID)"
        case .getTracks(let type, let typeID):
            return "/\(type)/\(typeID)"
        case .getTrack:
            return "/tracks/"
        case .createPlaylist:
            return "/playlists/"
        case .createFavourite:
            return "/favorites/"
        case .getUserProfile:
            return "/profiles/"
        case .getPodcasts:
            return "/podcasts/artists/"
        case .getComedy:
            return "/comedy/artists/"
            
       
            
        }
    }
    
    private var route: String {
        switch self {
        case .getDiscoverContent:
            return Constants.Routes.ApiV4
        default:
             return Constants.Routes.ApiV3
        }
    }

    private var endpoint: String {
        switch self {
        case .getAlbum(let albumId):
            return albumId
        case .getPlaylist(let playlistId):
            return playlistId
        case .getChart(let chartId):
            return chartId
        case .getArtist(let artistId):
            return artistId
        case .getGenre(let genreId):
            return genreId
        case .getSubGenres(let parentId):
            return parentId
        case .getSearch(let searchTerm):
            return searchTerm
        case .getAlbums:
            return "albums"
        case .getTracks:
            return "tracks"
        case .getTrack(let trackID):
            return trackID
        case .getUserProfile(let userID):
            return userID
    
        default:
            return ""
        }
    }
    
    private var parameters: Parameters? {
        switch self {
        case .getSearch(let  searchTerm):
            return [Constants.Parameters.query : searchTerm]
        case .createPlaylist(let  playlistWrapper):
            return [Constants.Parameters.playlist : playlistWrapper.playlist?.toJSON() as Any]
        case .createFavourite(let  favouriteWrapper):
            return [Constants.Parameters.favourite : favouriteWrapper]
        default:
            return nil
        }
    }
    
    
   
}

enum Auth: URLRequestConvertible {
    
    case getAuthToken
    case msisdnLogin(msisdn:String)
    
    //MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        
        let url = try Constants.authBaseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        //Http method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.acceptType.rawValue)
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.contentType.rawValue)
        urlRequest.setValue("Bearer " + Defaults.getString(key: Defaults.accessTokenKey)!, forHTTPHeaderField: Constants.HttpHeaderField.authentication.rawValue)
        
        //Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
            
    }
    
    //MARK: - HttpMethod
    //This returns the HttpMethod type. It's used to determine the type if several endpoints are peresent
    private var method: HTTPMethod {
            return .post
    }
    
    //MARK: - Path
    //The path is the part following the base url
    private var path: String {
        switch self {
        case .getAuthToken:
            return "/oauth/token"
        case .msisdnLogin:
            return "/api/v1/msisdn/login"
        }
    }
    
    //MARK: - Parameters
    //This is the queries part, it's optional because an endpoint can be without parameters
    private var parameters: Parameters? {
        switch self {
        case .getAuthToken:
            return [Constants.Parameters.clientID : Constants.CLIENT_ID,Constants.Parameters.clientSecret : Constants.CLIENT_SECRET,Constants.Parameters.grantType : Constants.GRANT_TYPE]
        case .msisdnLogin(let msisdn):
            return [Constants.Parameters.phoneNumber : msisdn]
        }
    }
}

enum Billing: URLRequestConvertible {
    
    case getPlans
    case getUserSubscription(externalKey: String)
    case createSubscription(planName: String,planID: String)
    case cancelSubscription
    case refreshAccount
    case payNow(externalKey: String)
    
    //MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        
        let url = try (Constants.billingBaseURL) .asURL()
        
         var urlRequest = URLRequest(url: url.appendingPathComponent(path + endpoint))
        
        //Http method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.acceptType.rawValue)
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.contentType.rawValue)
        urlRequest.setValue("Bearer " + Defaults.getString(key: Defaults.accessTokenKey)!, forHTTPHeaderField: Constants.HttpHeaderField.authentication.rawValue)
        
        
        //Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
        
    }
    
    //MARK: - HttpMethod
    //This returns the HttpMethod type. It's used to determine the type if several endpoints are peresent
    private var method: HTTPMethod {
        switch self {
        case .getPlans:
            return .get
        case .getUserSubscription:
            return .get
        case .refreshAccount:
            return .get
        case .createSubscription:
            return .post
        case .cancelSubscription:
            return .delete
        case .payNow:
            return .post
        }
    }
 
    private var endpoint: String {
        switch self {
        case .getUserSubscription(let externalKey):
            return externalKey
        case .payNow(let externalKey):
            return externalKey
        case .cancelSubscription:
             return "/externalKey/\(Defaults.getUser()?.id ?? "")"
        default:
            return ""
        }
    }
    
  private var path: String {
        switch self {
        case .getPlans:
            return "/products/\(Constants.PRODUCT_ID)/plans"
        case .getUserSubscription:
            return "/subscriptions/productId/\(Constants.PRODUCT_ID)/externalKey/"
        case .refreshAccount:
            return "/subscriptions/refresh/productId/\(Constants.PRODUCT_ID)/externalKey/"
        case .createSubscription:
            return "/subscriptions/productId/\(Constants.PRODUCT_ID)/"
        case .cancelSubscription:
                     return "/subscriptions/productId/\(Constants.PRODUCT_ID)"
        case .payNow:
            return "/subscriptions/pay/productId/\(Constants.PRODUCT_ID)/externalKey/"
        }
    }
    
    
    //MARK: - Parameters
    //This is the queries part, it's optional because an endpoint can be without parameters
    private var parameters: Parameters? {
        switch self {
        case .createSubscription(let planName,let planID):
            
            let parameters: Parameters = [
               Constants.Parameters.paymentMethod : Constants.PAYMENT_METHOD,
                Constants.Parameters.subscription: [
                    Constants.Parameters.planName: planName,
                    Constants.Parameters.planID: planID
                ],
                Constants.Parameters.account: [
                    Constants.Parameters.country: "Kenya",
                    Constants.Parameters.email: Defaults.getUser()?.email,
                    Constants.Parameters.externalKey: Defaults.getUser()?.id,
                    Constants.Parameters.name: Defaults.getUser()?.name,
                    Constants.Parameters.notes: "Make payment",
                    Constants.Parameters.phone: Defaults.getUser()?.phone?.replacingOccurrences(of: "+", with: "")
                ]
            ]
            
            return parameters
        default: return nil
        }
    }
}




//CAMPAIGNS

enum Campaigns: URLRequestConvertible {
    
    case getCampaigns
    case getVoucher(campaignID: String)
    case redeemVoucher(campaignID: String, codeShort:String)
    case applyVoucher(campaignID:String,promoCode:String,gifterID:String, redemptionID:String)
  
    
    //MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        
        var url:URL
        switch  self {
        case .applyVoucher:
            url = try (Constants.billingBaseURL) .asURL()
        default:
             url = try (Constants.campaignsBaseURL) .asURL()
        }
      
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path + endpoint))
        
        //Http method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.acceptType.rawValue)
        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HttpHeaderField.contentType.rawValue)
        urlRequest.setValue("Bearer " + Defaults.getString(key: Defaults.accessTokenKey)!, forHTTPHeaderField: Constants.HttpHeaderField.authentication.rawValue)
        
        
        //Encoding
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding.default
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
        
    }
    
    //MARK: - HttpMethod
    //This returns the HttpMethod type. It's used to determine the type if several endpoints are peresent
    private var method: HTTPMethod {
        switch self {
        case .redeemVoucher:
            return .post
        case .applyVoucher:
            return .post
        case .getVoucher:
            return .post
        default:
            return .get
       
        }
    }
    
    
    
    private var endpoint: String {
        switch self {
        case .getVoucher:
            return "/vouchers"
        case .redeemVoucher:
            return "/redemptions"
        case .applyVoucher:
            return "/externalKey/\(Defaults.getUser()?.id ?? "")"
        default:
            return ""
        }
    }
    
    private var path: String {
        switch self {
        case .redeemVoucher(let campaignID, _  ):
            return campaignID
        case .getVoucher(let campaignID ):
            return campaignID
        case .applyVoucher:
            return "/subscriptions/promote/productId/\(Constants.PRODUCT_ID)"
        default:
            return ""
        }
    }
    
    private var parameters: Parameters? {
        switch self {
        case .redeemVoucher(let codeShort ,_):
            
            let parameters: Parameters = [
                "user_id" : Defaults.getUser()?.id ?? "", Constants.Parameters.codeShort : codeShort]
            
            print(parameters)
            return parameters
            
        case .applyVoucher(let campaignID, let promoCode,let gifterID, let redemptionID ):
            
            let parameters: Parameters = [
                Constants.Parameters.campaignID: campaignID , Constants.Parameters.promoCode: promoCode , Constants.Parameters.gifterID: gifterID, Constants.Parameters.redemptionID: redemptionID]
            
            print(parameters)
            return parameters
        case .getVoucher:
            
            let parameters: Parameters = [
                "user_id" : Defaults.getUser()?.id ?? ""]
  
            return parameters
        default: return nil
        }
    }
    
}
}

