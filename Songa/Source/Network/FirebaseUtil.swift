//
//  FollowUtil.swift
//  Songa
//
//  Created by Collins Korir on 11/7/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import Foundation
import FirebaseMessaging
import FirebaseCore
import FirebaseFirestore

class FirebaseUtil:NSObject {

    static var shared = FirebaseUtil()
    private let PLAYLIST_FOLLOWS = "playlist_follows"
    private let ARTIST_FOLLOWS = "artist_follows"
    
    
    private let firestore = Firestore.firestore()
  
//    override public init() {
//        super.init()
//        firestore = Firestore.firestore()
//    }
    
//    override public init() {
//        super.init()
//        config = URLSessionConfiguration.background(withIdentifier: "\(Bundle.main.bundleIdentifier!).background")
//        downloadSession = AVAssetDownloadURLSession(configuration: config, assetDownloadDelegate: self, delegateQueue: OperationQueue.main)
//    }
    func followArtist(artist: Artist,toUnfollow: Bool = false){
        
        //first check if the artist is alreqady on firestore db
        firestore.collection(ARTIST_FOLLOWS)
        .document(artist.id).getDocument { (document, error) in
            if let document = document, document.exists {
                
                //artist exists
              var followers = 0
                          if let count = document.data()!["follow_count"] as? String? {
                              followers = Int(count!)!
                          }
                          else {
                              followers = document.data()!["follow_count"] as! Int
                          }
                
                self.addToArtistFollowList(artist:artist, count: followers)
                
            } else {
                //artist does not exist
                print("Document does not exist")
                self.addToArtistFollowList(artist: artist, toUnfollow:  false)
            }
        }
    }
    
    func addToArtistFollowList(artist: Artist, count: Int = 0 ,toUnfollow: Bool = false){
        
        firestore.collection(ARTIST_FOLLOWS).document(artist.id).setData([
            "id": artist.id,
            "name": artist.name,
            "follow_count": String(describing: (toUnfollow && count != 0) ? count - 1 : count + 1)
        ]) { err in
            if err != nil {
                print("Failed updating follow count")
            } else {
                print("follow count modified")
                //post notification that count is gotten
                               NotificationCenter.default.post(name: Notification.Name.artistFollowCount,
                               object: nil,
                               userInfo:["count": (toUnfollow && count != 0) ? count - 1 : count + 1])
            }
        }
    }
    
    func getArtistFollowCount(artistID: String){
         
         //first check if the artist is alreqady on firestore db
         firestore.collection(ARTIST_FOLLOWS)
         .document(artistID).getDocument { (document, error) in
             if let document = document, document.exists {
                 
                var followers = 0
                               if let count = document.data()!["follow_count"] as? String? {
                                   followers = Int(count!)!
                               }
                               else {
                                   followers = document.data()!["follow_count"] as! Int
                               }
                             
              
                //post notification that count is gotten
                NotificationCenter.default.post(name: Notification.Name.artistFollowCount,
                object: nil,
                userInfo:["count": followers])
             } else {
                 //artist does not exist
                 print("Failed")
             }
         }
     }
    
    func followPlaylist(playlistName: String ,playlistID:String,toUnfollow: Bool = false){
        
        //first check if the artist is alreqady on firestore db
        firestore.collection(PLAYLIST_FOLLOWS)
        .document(playlistID).getDocument { (document, error) in
            if let document = document, document.exists {
                
             var followers = 0
                if let count = document.data()!["follow_count"] as? String? {
                    followers = Int(count!)!
                }
                else {
                    followers = document.data()!["follow_count"] as! Int
                }
                     self.addToPlaylistFollowList(playlistName: playlistName, playlistID: playlistID, count:followers, toUnfollow:  toUnfollow)
                

            } else {
                //artist does not exist
                print("Document does not exist")
                self.addToPlaylistFollowList(playlistName: playlistName, playlistID: playlistID, toUnfollow:  false)
            }
        }
    }
    
    func addToPlaylistFollowList(playlistName: String ,playlistID:String, count: Int = 0 ,toUnfollow: Bool = false){
        
               print(String(describing: (toUnfollow && count != 0) ? count - 1 : count + 1))
        firestore.collection(PLAYLIST_FOLLOWS).document(playlistID).setData([
            "id": playlistID,
            "name": playlistName,
            "follow_count": String(describing: (toUnfollow && count != 0) ? count - 1 : count + 1)
        ]) { err in
            if err != nil {
                print("Failed updating follow count")
            } else {
                print("follow count modified")
                //post notification that count is gotten
                NotificationCenter.default.post(name: Notification.Name.playlistFollowCount,
                object: nil,
                userInfo:["count": (toUnfollow && count != 0) ? count - 1 : count + 1])
            }
        }
       
    }
    
    func getPlaylistFollowCount(playlistID: String){
         
         //first check if the artist is alreqady on firestore db
         firestore.collection(PLAYLIST_FOLLOWS)
         .document(playlistID).getDocument { (document, error) in
             if let document = document, document.exists {
                 
               var followers = 0
                if let count = document.data()!["follow_count"] as? String? {
                    followers = Int(count!)!
                }
                else {
                    followers = document.data()!["follow_count"] as! Int
                }
              
                //post notification that count is gotten
                NotificationCenter.default.post(name: Notification.Name.playlistFollowCount,
                object: nil,
                userInfo:["count": followers])
             } else {
                 //artist does not exist
                 print("Failed")
             }
         }
     }
    
    func subscribeToTopic(topic: String){
           let normalizedTopic = normalizeString(topic: topic)
               if Messaging.messaging().fcmToken != nil {
                       Messaging.messaging().subscribe(toTopic: normalizedTopic)
                   }
       }
       
       func unsubscribeToTopic(topic: String){
            let normalizedTopic = normalizeString(topic: topic)
                if Messaging.messaging().fcmToken != nil {
                   Messaging.messaging().unsubscribe(fromTopic: normalizedTopic)
                    }
        }
       
       func normalizeString(topic:String) -> String{
           //replace spaces with underscores
           //replaces ampersands with _n_
           //remove apostrophes
        return topic.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "&", with: "_n_").replacingOccurrences(of: "'", with: "").replacingOccurrences(of: "#", with: "")
       }
}
