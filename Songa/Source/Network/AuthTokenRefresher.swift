//
//  AuthTokenRefresher.swift
//  Songa
//
//  Created by Collins Korir on 7/26/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import  Alamofire
import AlamofireObjectMapper

class AuthTokenRefresher {

     let defaults =  UserDefaults.standard
     let authAPI = APIurls.AuthRefresh
     let headers = APIurls.Headers


    func refreshToken()  ->  Bool {

        var refreshSuccess: Bool = false

        //instantiate a new user defaults instance

        let defaults =  UserDefaults.standard

        Alamofire.request(authAPI, method: .get,  headers: headers).responseObject() { (response: DataResponse<AuthResponse>) in

            if let responseCode = response.response?.statusCode {

                if responseCode == 200 {

                    let token = response.result.value

                    defaults.set("\(String(describing: token?.tokenType)) \(String(describing: token?.accessToken))" , forKey: "authToken")
                    refreshSuccess = true
                }

                else {

                    //token refresh failed
                       refreshSuccess = true
                }
            }

            else
            {
                //could not get any data from auth server
                       refreshSuccess = true
            }
    }

      return refreshSuccess
    }


}
