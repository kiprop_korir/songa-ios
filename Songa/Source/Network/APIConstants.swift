//
//  Constants.swift
//  Songa
//
//  Created by Collins Korir on 2/13/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
struct Constants {
    
    
    //The API's base URL
    static let contentBaseURL = "https://content.songamusic.com"
    static let authBaseURL =  "https://auth.songamusic.com"
    static let billingBaseURL =  "https://billing.songamusic.com"
    static let campaignsBaseURL =  "https://campaigns.songamusic.com/api/v1/campaigns"
    
    static let CLIENT_ID : String = "de41b97b74acc69659d402a85e390fd1155c0bf5b65a735263e4d2c132800b04"
    
    static let DEV_PRODUCT_ID : String = "97a533a6-0e0f-427e-a850-9eec96089154"
    static let PRODUCT_ID : String = "56112786-21c1-4593-a9c7-e40723e22964"
    
    static let CLIENT_SECRET : String = "138436f51d19ad4f82e20f0f3b745f3a9f1928594609095158aaa70e849895bd"
    static let FB_CLIENT_ID : String = "0e69fe5e9f346e1530da205afa85c2c6142bb9e51d4558a133d38d9be2a750ca"
    static let FB_CLIENT_SECRET : String = "e95abe78e0c90b6dcd76ee224d74ce7cc5d9cfaa20bfe3bb2945813d19be72ab"
    static let GRANT_TYPE : String = "client_credentials"
    
    static let PAYMENT_METHOD : String = "safaricom-sdp-payment-plugin"
    
    
    struct Parameters {
        static let userId = "userId"
        static let playlistId = "playlistID"
        static let phoneNumber = "phone_number"
        static let query = "query"
        
        static let playlist = "playlist"
        static let favourite = "favorite"
        
        //auth
        static let clientID = "client_id"
        static let clientSecret = "client_secret"
        static let grantType = "grant_type"
        
        //billing
        
        static let account = "account"
        static let country = "country"
        static let email = "email"
        static let externalKey = "externalKey"
        static let name = "name"
        static let notes = "notes"
        static let phone = "phone"
        static let paymentMethod = "paymentMethod"
        static let subscription = "subscription"
        static let planName = "planName"
        static let planID = "planId"
        
      
        
        //campaigns
        static let codeShort = "code_short"
        static let campaignID = "campaignId"
        static let promoCode = "promoCode"
        static let gifterID = "gifterId"
        static let redemptionID = "redemptionId"
       
        
        
    }
    
    struct Routes {
        static let ApiV1     = "/api/v1"
        static let ApiV3     = "/api/v3"
        static let ApiV4     = "/api/v4"
    }
    
    //The header fields
    enum HttpHeaderField: String {
        case authentication = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
    }
    
    enum ContentType: String {
        case json = "application/json"
    }
    
    enum ErrorStatusCodes: Int {
        case forbidden = 403
        case notFound = 404
        case conflict = 409
        case internalServerError = 500
    }

}
