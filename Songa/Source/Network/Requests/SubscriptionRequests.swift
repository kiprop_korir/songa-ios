//
//  SubscriptionRequests.swift
//  Songa
//
//  Created by Collins Korir on 1/30/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import ObjectMapper

class SubscriptionRequest: Mappable {
    
    var account : Account?
    var subscription : Subscription?
    var paymentMethod : String?
   
    
    
    required init?(map: Map){
        
    }
    
    
    func mapping(map: Map) {
        account <- map["account"]
        subscription <- map["subscription"]
        paymentMethod <- map["paymentMethod"]
       
    }
    
}
