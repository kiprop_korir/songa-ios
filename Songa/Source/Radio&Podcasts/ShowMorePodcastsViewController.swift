//
//  ShowMorePodcastsViewController.swift
//  Songa
//
//  Created by Collins Korir on 10/4/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//
//
//  ShowMoreArtistsOrTracksViewController.swift
//  Songa
//
//  Created by Collins Korir on 10/22/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import UIScrollView_InfiniteScroll

class ShowMorePodcastsViewController: UIViewController {

     @IBOutlet weak var collectionView: UICollectionView!
     var artists: [Artist] = []
     
     var tappedCellID : String = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var id = String()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ArtistPageViewController {
            vc.artistID = sender as! String
            vc.isPodcast = true
        }
    }
    
}

  


extension ShowMorePodcastsViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           
            return artists.count
  
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "artistCell", for: indexPath) as! ArtistCell
            cell.ivCover.sd_setImage(with: URL(string: artists[indexPath.row].artwork[0].medium), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblName.text = artists[indexPath.row].name
        cell.layer.cornerRadius = 8
        cell.layer.masksToBounds = true
                       

        return cell
        
        }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let padding: CGFloat =  20
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: 194)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         let artist = artists[indexPath.row]
                       performSegue(withIdentifier: "segueArtistPage", sender: artist.id)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }
}

