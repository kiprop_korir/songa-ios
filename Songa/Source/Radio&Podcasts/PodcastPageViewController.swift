//
//  PodcastPageViewController.swift
//  Songa
//
//  Created by Collins Korir on 9/24/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//
//


import UIKit
import Alamofire
import UIKit
import XLPagerTabStrip
import RealmSwift

class PodcastPageViewController: BaseViewController {
    
    
    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var ivCoverBg: UIImageView!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblAlbumTitle: UILabel!
    @IBOutlet weak var lblTrackCount: UILabel!
    @IBOutlet weak var tvEpisodes: UITableView!
    @IBOutlet weak var btnShare: UIButton!
    
    let headers = APIurls.Headers
    var podcast: Podcast?  = nil
    
    var podcastID : String = ""
    var episodes : [PodcastEpisode] = []
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title =  "Album"
        tvEpisodes.delegate = self
        tvEpisodes.dataSource =  self
        
        //remove separator form tableview
        tvEpisodes.separatorStyle = UITableViewCell.SeparatorStyle.none
        //tvEpisodes.allowsSelection = false
        
        
        loadPodcast()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        //add retry observer
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(retry),
                                               name: Notification.Name.retryClicked,
                                               object: nil)
    }
    @objc func retry(){
        self.dismissErrorView()
        loadPodcast()
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var tracks = Array<PodcastEpisode>()
        var currrentPlayingIndex = Int()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
        
//        NotificationCenter.default.post(name: Notification.Name.startPlay,
//                                        object: nil,
//                                        userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
        
         NotificationCenter.default.post(name: Notification.Name.startPodcastPlay,
                                                 object: nil,
                                                userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
        //
        
    }
    class optionsTapGesture: UITapGestureRecognizer {
        var episode = PodcastEpisode()
    }
    @objc func optionsTapped(_ sender: optionsTapGesture) {
        showShareController(id : sender.episode.id, title: sender.episode.title, artist: "", artwork: sender.episode.image)
    }
    
    
    
    
    func loadPodcast()  {
        
        self.showLoadingView()
        
        APIClient.getCuratedPodcastDetails(podcastId: podcastID){ result in
        
            
            self.dismissLoadingView()
            switch result.result {
            case .success(let value):
                
                self.podcast = value
                
                for episode in value.episodes {
                    self.episodes.append(episode)
                
                }
               
               self.navigationItem.title =  value.title
                let coverImageURL = value.image
                
                  self.ivCover.sd_setImage(with: URL(string: coverImageURL), placeholderImage: #imageLiteral(resourceName: "no_cover"))
                    self.ivCoverBg.sd_setImage(with: URL(string: coverImageURL), placeholderImage: #imageLiteral(resourceName: "no_cover"))


                 let trackCount = self.episodes.count

                    if(trackCount == 1){
                        self.lblTrackCount.text = "\(trackCount) Episode"
                    }
                    else {
                        self.lblTrackCount.text = "\(trackCount) Episodes"

                    }

                self.lblAlbumTitle.text =  value.title
                self.lblArtistName.text =  value.publisher

                self.lblAlbumTitle.textColor =  .red
                self.lblArtistName.textColor = .white
                self.btnShare.imageView?.tintColor = .white
                self.btnFavourite.imageView?.tintColor = ThemeManager.currentTheme().textColor
                self.lblTrackCount.textColor = .white
                self.ivCoverBg.blurImage()

               self.tvEpisodes.reloadData()
                
                print("all good")
                
                //print(value)
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.loadPodcast()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
    }
    
    ///IB Actions
    
    
//    @IBAction func playAll(_ sender: Any) {
//        NotificationCenter.default.post(name: Notification.Name.startPlay,
//                                        object: nil,
//                                        userInfo:["tracks": self.podcast!. as Any ,"currentPlayingIndex": 0])
//    }
//    @IBAction func mix(_ sender: Any) {
//        //to do shuffle the indexes
//        let randomIndex = Int.random(in: 0 ..< tracks.count - 1)
//        NotificationCenter.default.post(name: Notification.Name.startPlay,
//                                        object: nil,
//                                        userInfo:["tracks": self.podcast!.tracks as Any ,"currentPlayingIndex": randomIndex])
//
//    }
    
    @IBAction func favourite(_ sender: Any) {
        
        let favouritedPodcast: FavouritePodcast = FavouritePodcast()
        favouritedPodcast.id = podcast!.id
        
        favouritedPodcast.podcast = self.podcast
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(favouritedPodcast,update: .modified)
                btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite_filled"), for: .normal)
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    // TODO:- Share
    @IBAction func share(_ sender: Any) {
        self.share(artist: (self.podcast?.title)! , link: "https//open.songamusic.com/podcast/\(podcast!.id  )")
    }
    
    
}

extension PodcastPageViewController: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return episodes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "episodeCell", for: indexPath) as! TrackTableCell
        
        let url =  episodes[indexPath.row].image 
       
        
        cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = episodes[indexPath.row].title
        cell.lblArtist.text = episodes[indexPath.row].podcastDescription
        cell.lblTrackIndex.text = String(indexPath.row + 1)
        cell.lblTrackIndex.textColor = ThemeManager.currentTheme().textColorSecondary
        cell.lblArtist.textColor = ThemeManager.currentTheme().textColorSecondary
        cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        cell.btnOptions.isHidden = true
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        // Add a separator below each cell
        let horizontalGap = 15.0 as CGFloat
        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
        cell.addSubview(seperatorView)
        
        
        let tapOptions = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
        tapOptions.episode = episodes[indexPath.row]
        
        cell.btnOptions.addGestureRecognizer(tapOptions)
        cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         NotificationCenter.default.post(name: Notification.Name.startPodcastPlay,
                                                        object: nil,
                                                        userInfo:["tracks": episodes ,"currentPlayingIndex": indexPath.row])
    }
    
    
}

