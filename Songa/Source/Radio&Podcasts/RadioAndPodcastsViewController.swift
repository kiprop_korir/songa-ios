//
//  RadioAndPodcastsViewController.swift
//  Songa
//
//  Created by Collins Korir on 9/21/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SDWebImage
import RealmSwift



class RadioAndPodcastsViewController: UIViewController{

     var stations: [Station] = []
    var comedy: [Comedy] = []
     var podcasts: [PodcastArtist] = []
     var curatedPodcasts: [PodcastList] = []
    var storedOffsets = [Int: CGFloat]()
    let realm = try! Realm()
    @IBOutlet weak var cvComedy: UICollectionView!
    
    @IBOutlet weak var cvStations: UICollectionView!
    
    @IBOutlet weak var viewShowMoreComedy: UIView!
    @IBOutlet weak var viewShowMorePodcasts: UIView!
    @IBOutlet weak var cvPodcasts: UICollectionView!
    @IBOutlet weak var tvPodcasts: UITableView!
    override func viewDidLoad() {
        
   
        
        tvPodcasts.delegate = self
        tvPodcasts.dataSource = self
        
        cvComedy.delegate = self
        cvComedy.dataSource = self
        cvComedy.tag = 98
        
        cvStations.delegate = self
        cvStations.dataSource = self
        cvStations.tag = 99
        
        cvPodcasts.delegate = self
        cvPodcasts.dataSource = self
        cvPodcasts.tag = 100
        
        loadStations()
        loadCuratedPodcasts()
        loadPodcasts()
        loadComedy()
        
        tvPodcasts.separatorStyle = .none
        
        
        let showMore =  UITapGestureRecognizer(target: self, action: #selector(self.showMorePodcasts))
        viewShowMorePodcasts.addGestureRecognizer(showMore)
        
        let showMoreComedy =  UITapGestureRecognizer(target: self, action: #selector(self.showMoreComedy))
            viewShowMoreComedy.addGestureRecognizer(showMoreComedy)
        
        setUpNavBar()
        
    }
    
    func setUpNavBar(){
           
           let view = UIView(frame: CGRect(x: 0, y: 2, width: 40, height: 40))
           
           let logo = UIImageView(frame: CGRect(x: 0 , y: 10, width: 25, height: 25))
           logo.image = #imageLiteral(resourceName: "ic_profile")
           view.addSubview(logo)
           
           let count = RoundEdgesButton(frame: CGRect(x: 20, y: 0, width: 20, height: 20))
           count.backgroundColor = UIColor.red
           count.cornerRadius = 10
           
           let notificationsCount = realm.objects(SongaNotification.self).filter("isRead == false").count
           
           if(notificationsCount>0){
               count.setTitle(String(notificationsCount), for: .normal)
               count.titleLabel?.font = .systemFont(ofSize: 12)
               view.addSubview(count)
           }
           
           let barButtonItem = UIBarButtonItem(customView: view)
           self.navigationItem.leftBarButtonItem = barButtonItem
           
           let tap = UITapGestureRecognizer(target: self, action: #selector(self.profileViewClicked))
           barButtonItem.customView?.addGestureRecognizer(tap)
           
           let searchButton = UIBarButtonItem(image: UIImage(imageLiteralResourceName: "ic_search").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(self.searchClicked))
    
           self.navigationItem.rightBarButtonItem = searchButton
           self.navigationItem.rightBarButtonItem?.tintColor = ThemeManager.currentTheme().barTint
           
           
       }
    @objc func profileViewClicked(){
            let storyboard = UIStoryboard(name: "Profile", bundle: nil)
            let vc = storyboard.instantiateInitialViewController()
            self.present(vc!, animated: true)
      }
      
      @objc func searchClicked(){
             performSegue(withIdentifier: "segueSearch", sender: self)
      }
    
    @objc func showMorePodcasts() {
        
        var artists:[Artist] = []
        //convert to artit array
        for podcast in self.podcasts{
            let artist = Artist()
            artist.name = podcast.name
            artist.artwork = podcast.artwork
            artist.id = podcast.id
            artists.append(artist)
        }
        performSegue(withIdentifier: "showMore", sender: artists)
       }
    
    @objc func showMoreComedy() {
        var artists:[Artist] = []
        //convert to artit array
        for comedy in self.comedy{
            let artist = Artist()
            artist.name = comedy.name
            artist.artwork = comedy.artwork
            artist.id = comedy.id
            artists.append(artist)
        }
          performSegue(withIdentifier: "showMore", sender: artists)
         }
    
    func loadStations()  {
        
        self.showLoadingView()
        
        APIClient.getFMStations(){ result in
            
            self.dismissLoadingView()
            switch result.result {
            case .success(let value):
                
                for station in value
                    
                {
                  self.stations.append(station)
                }
                //sorting in alphabetical order
                self.stations = self.stations.sorted { $0.name < $1.name }
               
                self.cvStations.reloadData()
                //self.cvPartnerStations.reloadData()
                
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.loadStations()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
    }
    
    func loadComedy()  {
        
        self.showLoadingView()
        
        APIClient.getComedy(){ result in
            
            self.dismissLoadingView()
            switch result.result {
            case .success(let value):
                
                for comedy in value
                    
                {
                    
                    self.comedy.append(comedy)
                    
                }
                //sorting in alphabetical order
               // self.comedy = self.comedy.sorted { $0.name < $1.name }
               
                self.cvComedy.reloadData()
                //self.cvPartnerStations.reloadData()
                
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.loadStations()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
    }
    
    func loadPodcasts()  {
        
        self.showLoadingView()
        
        APIClient.getPodcasts(){ result in
            
            self.dismissLoadingView()
            switch result.result {
            case .success(let value):
                
                for podcast in value
                    
                {
                    
                    self.podcasts.append(podcast)
                    
                }
               
                self.cvPodcasts.reloadData()
                //self.cvPartnerStations.reloadData()
                
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.loadPodcasts()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    
                    print("errrrooo  \(statusCode)")
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
    }
    
    func loadCuratedPodcasts()  {
        
        self.showLoadingView()
        
        APIClient.getCuratedPodcasts(){ result in
            
            self.dismissLoadingView()
            
            switch result.result {
            case .success(let value):
                
                for podcast in value.curatedLists ?? []
                    
                {
                    self.curatedPodcasts.append(podcast)
                }
                
                self.tvPodcasts.reloadData()
                //self.cvPartnerStations.reloadData()
                
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.loadCuratedPodcasts()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ArtistPageViewController {
            vc.artistID = sender as! String
            vc.isPodcast = true
        }
        else  if let vc = segue.destination as? PodcastPageViewController {
            
            print("mna a")
            vc.podcastID = sender as! String
        }
        else if let vc = segue.destination as? ShowMorePodcastsViewController {
                   vc.artists = sender as! [Artist]
               }
    }
}
extension RadioAndPodcastsViewController:  UITableViewDelegate, UITableViewDataSource {
    
    
       // let model = generateRandomData()
    
        
         func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            //add two because of the first two
            return curatedPodcasts.count 
        }
        
         func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "radioPodcastCell", for: indexPath) as! RadioPodcastCell
            
           
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            
            guard let cell = cell as? RadioPodcastCell else { return }
            
            cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            cell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
            cell.lblPodcastTitle.text = curatedPodcasts[indexPath.row].title
            cell.lblPodcastDescription.text = curatedPodcasts[indexPath.row].podcastDescription
            cell.lblPodcastDescription.textColor = ThemeManager.currentTheme().textColorSecondary
            
        }
        
         func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            
            guard let tableViewCell = cell as? RadioPodcastCell else { return }
            
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 290
    }

    }
    
    extension RadioAndPodcastsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            switch collectionView.tag {
            case 98:
                return comedy.count
           case 99:
               return stations.count
            case 100:
                      return podcasts.count
            default:
                return curatedPodcasts[collectionView.tag].podcasts.count
            }
           
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            var returnedCell: UICollectionViewCell? = nil
            
            switch collectionView.tag {
                 case 98:
                      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "comedyCell", for: indexPath) as! ArtistCell
                                              cell.ivCover.sd_setImage(with: URL(string: comedy[indexPath.row].artwork[0].medium), placeholderImage: #imageLiteral(resourceName: "no_cover"))
                                              cell.lblName.text = comedy[indexPath.row].name
                                              
                                             returnedCell = cell
                case 99:
                     //fm stations
                                   let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "stationCell", for: indexPath) as! StationItemCell
                                   cell.ivCover.sd_setImage(with: URL(string: stations[indexPath.row].artwork[0].medium), placeholderImage: #imageLiteral(resourceName: "no_cover"))
                                   cell.lblName.text = stations[indexPath.row].name
                                   
                                  returnedCell = cell
                 case 100:
                          //podcasts
                           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "podcastCell", for: indexPath) as! StationItemCell
                           cell.ivCover.sd_setImage(with: URL(string: podcasts[indexPath.row].artwork[0].medium), placeholderImage: #imageLiteral(resourceName: "no_cover"))
                           cell.lblName.text = podcasts[indexPath.row].name
                           
                           returnedCell = cell
                 default:
      
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "podcastCell", for: indexPath) as! StationItemCell
            cell.ivCover.sd_setImage(with: URL(string: curatedPodcasts[collectionView.tag].podcasts[indexPath.row].image), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblName.text = curatedPodcasts[collectionView.tag].podcasts[indexPath.row].title
            
            returnedCell = cell
            
            }
        
            
            //adding a corner radius to the cell
            returnedCell!.layer.cornerRadius = 8
            returnedCell!.layer.masksToBounds = true
            return returnedCell!
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
             
            switch collectionView.tag {
                       case 98:
                            //podcasts
                                         performSegue(withIdentifier: "segueArtistPage", sender: comedy[indexPath.row].id)
                      case 99:
                           
                                         let station = stations[indexPath.row]
                                         
                                         print(station)
                                         //fm stations
                                         NotificationCenter.default.post(name: Notification.Name.startRadioPlay,
                                                                                                                 object: nil,
                                                                                                                 userInfo:["station": station])
                       case 100:
                                 //podcasts
                                 let podcast = podcasts[indexPath.row]
                                 performSegue(withIdentifier: "segueArtistPage", sender: podcast.id)
                       default:
                          performSegue(withIdentifier: "seguePodcastPage", sender: curatedPodcasts[collectionView.tag].podcasts[indexPath.row].id)
                       }
        }
}
