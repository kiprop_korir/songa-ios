//
//  AllStationsItemCell.swift
//  Songa
//
//  Created by Collins Korir on 8/6/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import  UIKit


class StationItemCell: UICollectionViewCell {

    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var lblName: UILabel!

}
