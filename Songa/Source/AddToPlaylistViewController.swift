//
//  AddToPlaylistViewController.swift
//  Songa
//
//  Created by Collins Korir on 8/22/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class AddToPlaylistViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var tvPlaylists: UITableView!
   var track:Track? = nil
    var playlists : [UserPlaylist] = []
    let realm = try! Realm()
    var playlistResults: Results<UserPlaylist>? =  nil
    
    override func viewDidLoad() {
        //get saved playlists from realm
        playlistResults = realm.objects(UserPlaylist.self)
        
        //add a first item to show on top of the list
        let createPlaylist = UserPlaylist()
        createPlaylist.name = "Create Playlist"
        playlists.append(createPlaylist)
    
        if playlistResults?.count ?? 0 > 0 {
            for playlist in playlistResults! {
            playlists.append(playlist)
            }
        }
        tvPlaylists.dataSource = self
        tvPlaylists.delegate = self
        //remove separator form tableview
        tvPlaylists.separatorStyle = UITableViewCell.SeparatorStyle.none
        tvPlaylists.allowsSelection = false
        
       

    }
    
    @IBAction func dismissVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    class cellTapGesture: UITapGestureRecognizer {
        var index = Int()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
    
        if(sender.index == 0){
           //show new playlist dialog
            performSegue(withIdentifier: "segueAddToNewPlaylist", sender: track)
        }
        else {
            //let selectedPlaylist = UserPlaylist()
            let selectedPlaylist: UserPlaylist? = realm.objects(UserPlaylist.self).filter("id == \"\(playlists[sender.index].id)\"").first
    
            
            if(selectedPlaylist?.tracks.contains(self.track!) ?? false){
                Toast.show(message: "Track already exists in \(String(describing: selectedPlaylist?.name))", controller: self)
            }
            else {
                do {
                    
                    let realm = try Realm()
                    
                    try realm.write {
                        selectedPlaylist?.tracks.append(self.track!)
                        realm.add(selectedPlaylist!, update: .modified)
                    }
                } catch let error as NSError {
                    print(error)
                }
                
                  Toast.show(message: "Track added to playlist", controller: self)
                
                syncPlaylists(playlist: selectedPlaylist!)
                
                self.dismissVC(self)
            }
        }
    }
    
    func syncPlaylists(playlist: UserPlaylist){
        
        let playlistCollection =  PlaylistCollection ()
        playlistCollection.creator = playlist.creator
        playlistCollection.name = playlist.name
        playlistCollection.playlistDescription = playlist.description
        playlistCollection.creator = playlist.creator
        playlistCollection.shared = playlist.shared
        
        var ids = Array<String>()
        
        for track in playlist.tracks {
            ids.append(track.id)
        }
        
        playlistCollection.tracks = ids
        
        let playlistWrapper = PlaylistWrapper()
        playlistWrapper.playlist = playlistCollection
        
        APIClient.createPlaylist(playlistWrapper: playlistWrapper) { result in
            
           
            switch result.result {
            case .success:
                
                print("success")
             
            case .failure:
                
               print("fail")
               
            }
    }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationNavigationController = segue.destination as? UINavigationController {
            let targetController = destinationNavigationController.topViewController as! NewPlaylistViewController
            targetController.track = self.track
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlists.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "playlistCell", for: indexPath) as! PlaylistTableViewCell
        let url : String
        
        if ( playlists.count > 0 && indexPath.row != 0){
            
            url =  playlists[indexPath.row].tracks[0].artwork[0].medium
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = playlists[indexPath.row].name
         // Add a separator below each cell
        let horizontalGap = 15.0 as CGFloat
        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
        cell.addSubview(seperatorView)
        
        
        
        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        
      
        cell.addGestureRecognizer(tappy)
        
        //add rounded edge
        //cell.layer.cornerRadius =  5
        
       
        tappy.index = indexPath.row
        cell.addGestureRecognizer(tappy)
        
        return cell
    }
    
    
}
