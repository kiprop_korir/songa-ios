//
//  GenreCell.swift
//  Songa
//
//  Created by Collins Korir on 7/23/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit

class TrackCell: UICollectionViewCell {

    @IBOutlet weak var ivTrackCover: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTrackIndex: UILabel!
    @IBOutlet weak var lblArtist: UILabel!
    @IBOutlet weak var btnOptions: UIButton!
    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var ivDownloaded: UIImageView!
    
}
