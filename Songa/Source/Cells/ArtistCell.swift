//
//  GenreCell.swift
//  Songa
//
//  Created by Collins Korir on 7/23/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import SDWebImage

class ArtistCell: UICollectionViewCell {

    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var ivCover: UIImageView!

}
