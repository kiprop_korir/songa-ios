//
//  TrackTableCell.swift
//  Songa
//
//  Created by Collins Korir on 2/27/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import UIKit

protocol TrackCellDelegate {
  func cancelTapped(_ cell: TrackTableCell)
  func downloadTapped(_ cell: TrackTableCell)
  func pauseTapped(_ cell: TrackTableCell)
  func resumeTapped(_ cell: TrackTableCell)
}

class TrackTableCell:UITableViewCell {
    
    /// Delegate identifies track for this cell, then
    /// passes this to a download service method.
    var delegate: TrackCellDelegate?
    
    @IBOutlet weak var ivTrackCover: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTrackIndex: SongaAccentTextLabel!
    @IBOutlet weak var lblArtist: SongaSecondaryTextLabel!
    @IBOutlet weak var btnOptions: UIButton!
    @IBOutlet weak var ivDownloaded: UIImageView!
    
     //
      // MARK: - IBActions
      //
      @IBAction func cancelTapped(_ sender: AnyObject) {
        delegate?.cancelTapped(self)
      }
      
      @IBAction func downloadTapped(_ sender: AnyObject) {
        delegate?.downloadTapped(self)
      }
      
      @IBAction func pauseOrResumeTapped(_ sender: AnyObject) {
//        if(pauseButton.titleLabel?.text == "Pause") {
//          delegate?.pauseTapped(self)
//        } else {
//          delegate?.resumeTapped(self)
//        }
      }
      //used to set stuff that's going to be there for the whole lifecycle of the cell
    override func awakeFromNib() {
        super.awakeFromNib()
        lblArtist.textColor = ThemeManager.currentTheme().textColorSecondary
               lblTrackIndex.textColor = ThemeManager.currentTheme().textColorSecondary
              
        
    }
      //
      // MARK: - Internal Methods
      //
      func configure(track: Track, position: Int, downloaded: Bool, download: Track?) {
           let url : String
        
        if ( track.artwork.count > 0){
            
            url =  track.artwork[0].small
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        lblTitle.text = track.title
        lblArtist.text = track.artistName
        lblTrackIndex.text = String(position + 1)
        
        // Add a separator below each cell
        let horizontalGap = 15.0 as CGFloat
        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: frame.size.height - 1, width: frame.size.width - horizontalGap * 2.0, height: 1))
        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
        addSubview(seperatorView)
        
       
                  btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
                 
                 //add rounded edge
                 layer.cornerRadius =  5
              
        
        // Show/hide download controls Pause/Resume, Cancel buttons, progress info.
        var showDownloadControls = false
        
        // Non-nil Download object means a download is in progress.
        if let download = download {
          showDownloadControls = true
          
//          let title = download.isDownloading ? "Pause" : "Resume"
//          pauseButton.setTitle(title, for: .normal)
//
//          progressLabel.text = download.isDownloading ? "Downloading..." : "Paused"
//        }
        
        pauseButton.isHidden = !showDownloadControls
        cancelButton.isHidden = !showDownloadControls
        
        progressView.isHidden = !showDownloadControls
        progressLabel.isHidden = !showDownloadControls
        
        // If the track is already downloaded, enable cell selection and hide the Download button.
        selectionStyle = downloaded ? UITableViewCell.SelectionStyle.gray : UITableViewCell.SelectionStyle.none
        downloadButton.isHidden = downloaded || showDownloadControls
      }
      
      func updateDisplay(progress: Float, totalSize : String) {
        progressView.progress = progress
        progressLabel.text = String(format: "%.1f%% of %@", progress * 100, totalSize)
      }
    }

    

    
}
