//
//  PlaylistTableViewCell.swift
//  Songa
//
//  Created by Collins Korir on 8/22/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation

import UIKit
import SDWebImage

class PlaylistTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivCover: UIImageView!
    
}
