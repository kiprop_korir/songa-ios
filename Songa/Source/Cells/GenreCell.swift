//
//  GenreCell.swift
//  Songa
//
//  Created by Collins Korir on 7/23/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import SDWebImage

class GenreCell: UICollectionViewCell {

   @IBOutlet weak var ivGenre: UIImageView!
    @IBOutlet weak var lblGenre: UILabel!

}
