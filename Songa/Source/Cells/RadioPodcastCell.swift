//
//  RadioPodcast.swift
//  Songa
//
//  Created by Collins Korir on 9/21/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import Foundation
import UIKit

//inspo from https://ashfurrow.com/blog/putting-a-uicollectionview-in-a-uitableviewcell-in-swift/
class RadioPodcastCell: UITableViewCell {
    
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    @IBOutlet weak var lblPodcastTitle: UILabel!
    @IBOutlet weak var lblPodcastDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
}

extension RadioPodcastCell {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}
