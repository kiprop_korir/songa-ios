//import UIKit
//import ARNTransitionAnimator
//import AVKit;
//import AVFoundation;
//import Foundation;
//import RealmSwift
//import MediaPlayer
//import SDWebImage
//
//class  MainViewController: UIViewController, UITabBarDelegate {
//    
//    var storyboardIDs:[String] = ["Discover","My Music","Genres","FM Radio","My Profile"]
//    var viewControllers:[UIViewController] = []
//    var drawerMenus:[UIViewController] = []
//    var activeController:UIViewController? = nil
//    var isFmRadioStream :Bool = false
//    var fmStationName = ""
//    var fmStationDes = ""
//    var fmStationImageURL = ""
//    
//    @IBOutlet weak var miniPlayer: UIView!
//    @IBOutlet weak var childView: UIView!
//    @IBOutlet weak var tabbar: UITabBar!
//    @IBOutlet weak var lblTrackTitle: UILabel!
//    @IBOutlet weak var lblArtist: UILabel!
//    @IBOutlet weak var btnPlayPause: UIButton!
//    @IBOutlet weak var trackProgressIndicator: UIProgressView!
//    @IBOutlet weak var childViewBottomConstraint: NSLayoutConstraint!
//    
//    fileprivate var playerVC : PlayerViewController!
//    
//    //player stuff
//    private let AVPlayerTestPlaybackViewControllerStatusObservationContext: UnsafeMutableRawPointer? = UnsafeMutableRawPointer.init(mutating:nil);
//    
//    //Apple presumes that all conections require authorization and a credential space. If we don't define this and register in viewDidLoad then we would get an error saying that the app
//    //Can't find credentials for the connection. This error dosn't effect anything if using HTTP, but might need to be implemented based on authentication on your media server
//    let protectionSpace = URLProtectionSpace.init(host: "licenses.digitalprimates.net",
//                                                  port: 80,
//                                                  protocol: "http",
//                                                  realm: nil,
//                                                  authenticationMethod: nil)
//    let userCredential = URLCredential(user: "",
//                                       password: "",
//                                       persistence: .permanent)
//    
//    let ezrdmKeyServerURL = APIurls.EZDRM
//    var mediaServerEndpoint : String = ""
//    var selectedTab : Int = 0
//    var currentPlayingTrackIndex : Int = 0
//    
//    var player:AVPlayer? = AVPlayer ()
//    var playerItem:AVPlayerItem? = nil
//    var playerLayer:AVPlayerLayer? = nil
//    var loaderDelegate:CustomAssetLoaderDelegate? = nil
//    var downloadDelegate:CustomAssetDownloadDelegate? = nil
//    var asset: AVURLAsset? = nil
//    var session: AVAssetDownloadURLSession? = nil
//    let PLAYABLE_KEY:String = "playable"
//    let STATUS_KEY:String = "status"
//    
//    //track(s) passed from previous VC
//    
//    var tracks: [Track] = []
//    
//    
//    /* ----------------------------
//     ** globalNotificationQueue
//     
//     ** Returns a Dispatch Que for the AVAssetLoader Delegate
//     
//     ---------------------------- */
//    
//    func globalNotificationQueue() -> DispatchQueue {
//        
//        return DispatchQueue(label: "Stream Queue")
//        
//    }
//    
//    @IBAction func playPauseClicked(_ sender: Any) {
//        
//        
//        //        let newFrame = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 100, height: 100))
//        //
//        //
//        //        miniPlayer.frame = newFrame;
//        
//        // miniPlayer.isHidden =  true
//        //check if  is a playlist loaded for the player
//        
//        //If the player is currently playing a track
//        
//        if (player?.timeControlStatus == .paused){
//            
//            resumePlay()
//            
//        }
//            
//        else if (player?.timeControlStatus == .playing){
//            
//            pausePlay()
//        }
//        
//        
//    }
//    
//    @objc func pausePlay(){
//        if(self.player != nil) {
//            btnPlayPause.setImage(#imageLiteral(resourceName: "ic_play"), for: .normal)
//            self.player?.pause()
//        }
//    }
//    
//    @objc func resumePlay(){
//        if(self.player != nil) {
//            btnPlayPause.setImage(#imageLiteral(resourceName: "ic_pause"), for: .normal)
//            self.player?.play()
//        }
//    }
//    
//    
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        loadPageDefaults()
//        
//        setupRemoteCommandCenter()
//        
//        URLCredentialStorage.shared.setDefaultCredential(userCredential, for: protectionSpace)
//        
//        
//        // Do any additional setup after loading the view.
//        
//        //We have created child to just use its from on different screen(s) i-e iPhone4/5/6/6+ or iPad without changing any code, So its hidden now
//        //self.childView.isHidden = true
//        //hide nav bar
//        //self.navigationController?.navigationBar.isHidden = true
//        
//        //set profile picture
//        
//        // Default value of layer's zPosition is 0 so setting it to -1 will place it behind its siblings.
//        //navigationController?.navigationBar.layer.zPosition = -1
//        //addConstraints()
//        
//        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.showPlayer(sender:)))
//        self.miniPlayer.addGestureRecognizer(gesture)
//        
//        
//        for storyboardID in self.storyboardIDs {
//            let controller = self.storyboard?.instantiateViewController(withIdentifier: storyboardID)
//            viewControllers.append(controller!)
//        }
//        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(searchClicked),
//                                               name: Notification.Name.searchButtonClicked,
//                                               object: nil)
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(notificationsViewClicked),
//                                               name: Notification.Name.notificationsViewClicked,
//                                               object: nil)
//        
//    }
//    
//    @objc func notificationsViewClicked(){
//        
//        self.performSegue(withIdentifier: "segueNotifications", sender: nil)
//        
//    }
//    
//    @objc func searchClicked(){
//        self.performSegue(withIdentifier: "segueSearch", sender: nil)
//        
//    }
//    
//    
//    
//    func displayContentController(contentController:UIViewController) {
//        self.addChild(contentController)
//        contentController.view.frame = self.childView.frame
//        self.view.addSubview(contentController.view)
//        contentController .didMove(toParent: self)
//        self.activeController = contentController
//    }
//    
//    
//    func hideContentController(contentController:UIViewController) {
//        contentController.willMove(toParent: nil)
//        contentController.view .removeFromSuperview()
//        contentController.removeFromParent()
//    }
//    
//    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        if let tempActiveController = activeController {
//            self.hideContentController(contentController: tempActiveController)
//        }
//        switch item.tag {
//            
//        case 0: //Discover
//            
//            self.displayContentController(contentController: viewControllers[0])
//            self.selectedTab = 0
//            
//            
//            
//        case 1: //My Music
//            self.displayContentController(contentController: viewControllers[1])
//            self.selectedTab = 1
//            
//            
//            
//        case 2: //Genres
//            self.displayContentController(contentController: viewControllers[2])
//            self.selectedTab = 2
//            
//            
//            
//        case 3: //FM Radio
//            self.displayContentController(contentController: viewControllers[3])
//            self.selectedTab = 3
//            
//            
//            
//        case 4: //Me
//            self.displayContentController(contentController: viewControllers[4])
//            self.selectedTab = 4
//            
//            
//        default:
//            break;
//        }
//    }
//    
//    
//    
//    
//    
//    
//    @objc func showPlayer(sender : UITapGestureRecognizer) {
//        
//        
//        self.performSegue(withIdentifier: "showPlayer", sender:sender)
//        
//        //        let storyboard = UIStoryboard(name: "Player", bundle: nil)
//        //        self.playerVC = storyboard.instantiateViewController(withIdentifier: "playerViewController") as? PlayerViewController
//        //        self.playerVC.modalPresentationStyle = .overCurrentContext
//        //        self.playerVC.tracks = tracks
//        //        self.playerVC.currentPlayingTrackIndex = currentPlayingTrackIndex
//        //
//        //         self.present(self.playerVC, animated: true, completion: nil)
//        
//    }
//    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "showPlayer" {
//            if let destinationNavigationController = segue.destination as? UINavigationController {
//                let targetController = destinationNavigationController.topViewController as! PlayerViewController
//                targetController.player = self.player
//                targetController.tracks = self.tracks
//                targetController.currentPlayingTrackIndex = self.currentPlayingTrackIndex
//                targetController.isFMRadio = isFmRadioStream
//                targetController.fmStationName = self.fmStationName
//                targetController.fmStationDes = self.fmStationDes
//                targetController.fmStationImageURL = self.fmStationImageURL
//            }
//        }
//    }
//    
//    
//    override func viewDidAppear(_ animated: Bool) {
//        
//        //placing the tab bar delegate stuff here because we want to show the various view controllers after the view has appeared to avoid constraint issues
//        
//        self.tabbar.delegate = self
//        let selectedItem = self.tabbar.items?[selectedTab] as UITabBarItem?
//        self.tabbar.selectedItem = selectedItem
//        self.tabBar(tabbar, didSelect: selectedItem!)
//        
//    }
//    override func viewWillAppear(_ animated: Bool) {
//        
//        
//        //MARK:- Register class observers
//        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(startPlay),
//                                               name: Notification.Name.startPlay,
//                                               object: nil)
//        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(pausePlay),
//                                               name: Notification.Name.pauseClicked,
//                                               object: nil)
//        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(resumePlay),
//                                               name: Notification.Name.playClicked,
//                                               object: nil)
//        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(playNextTrack),
//                                               name: Notification.Name.playNextClicked,
//                                               object: nil)
//        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(playPreviousTrack),
//                                               name: Notification.Name.playPreviousClicked,
//                                               object: nil)
//        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(playRadioStream),
//                                               name: Notification.Name.startRadioPlay,
//                                               object: nil)
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(resetStream),
//                                               name: Notification.Name.resetStream,
//                                               object: nil)
//        
//        
//    }
//    
//    
//    
//    func loadPageDefaults(){
//        
//        lblTrackTitle.text = "Not Playing"
//        lblArtist.text = ""
//    }
//    
//    
//    
//}
//
//
//
////MARK:- Player Functions
//
//extension MainViewController {
//    
//    /* ------------------------------------------
//     ** observeValue:
//     **
//     **    Called when the value at the specified key path relative
//     **  to the given object has changed.
//     **  Start movie playback when the AVPlayerItem is ready to
//     **  play.
//     **  Report and error if the AVPlayerItem cannot be played.
//     **
//     **  NOTE: this method is invoked on the main queue.
//     ** ------------------------------------------------------- */
//    
//    
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        //make sure the
//        if context == AVPlayerTestPlaybackViewControllerStatusObservationContext {
//            let status = AVPlayerItem.Status(rawValue: change![.newKey] as! Int)
//            
//            switch status! {
//            case .unknown:
//                
//                print("status unknown")
//                break
//            case .readyToPlay:
//                
//                addTrackToRecentlyPlayed(track: tracks[currentPlayingTrackIndex])
//                play()
//                
//                
//                break
//            case .failed:
//                
//                print("Failed")
//                let playerItem = object as? AVPlayerItem
//                assetFailedToPrepare(error:playerItem?.error)
//                
//            }
//        }
//        else{
//            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
//        }
//    }
//    
//    /*
//     assetFailedToPrepare:
//     
//     if the AVPlayer state returns failed,
//     assetFailedToPrepare will output the error in a alert box
//     */
//    func assetFailedToPrepare(error:Error?){
//        /* Display the error. */
//        if self.loaderDelegate is CustomAssetDownloadLoaderDelegate {
//            if let errorWCode = error as NSError? {
//                self.loaderDelegate!.removeKey(fileManager: FileManager.default)
//                if Reachability.isConnectedToNetwork() == true && errorWCode.code == -11800{
//                    print("Rerequesting Key")
//                    self.initOfflineURLAsset(urlString: mediaServerEndpoint)
//                    self.readyMediaStream()
//                    return;
//                }
//            }
//        }
//        let alert = UIAlertController(title: error?.localizedDescription, message: (error as NSError?)?.localizedFailureReason, preferredStyle: .alert)
//        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
//        })
//        alert.addAction(defaultAction)
//        present(alert, animated: true, completion: nil)
//        self.unload();
//    }
//    
//    /* --------------------------------------------------------------
//     **
//     **  prepareToPlayAsset:withKeys
//     **
//     **  Invoked at the completion of the loading of the values for all
//     **  keys on the asset that we require. Checks whether loading was
//     **  successfull and whether the asset is playable. If so, sets up
//     **  an AVPlayerItem and an AVPlayer to play the asset.
//     **
//     ** ----------------------------------------------------------- */
//    
//    func prepareToPlayAsset(asset: AVURLAsset,requestedKeys:Array<String>){
//        
//        // if we are already have an AVPlayer Item stop listening to it
//        if(self.playerItem != nil){
//            self.playerItem?.removeObserver(self, forKeyPath: STATUS_KEY)
//        }
//        // Create a new AVPlayerItem with the given AVURLAsset
//        self.playerItem = AVPlayerItem(asset: asset)
//        
//        //If we don't have a AVPlayer already Create a new player with the new AVPlayerItem
//        if (self.player == nil){
//            self.player = AVPlayer(playerItem: self.playerItem);
//            self.playerLayer = AVPlayerLayer(player:self.player);
//            self.playerLayer!.frame = self.view.bounds;
//            self.view.layer.addSublayer(self.playerLayer!)
//            self.player!.usesExternalPlaybackWhileExternalScreenIsActive = true;
//        }
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.playerItem)
//        
//        
//        
//        // Add an Observer to the new AVPlayerItem to listen to the Status Key for when it is ready to play
//        self.playerItem?.addObserver(self, forKeyPath: STATUS_KEY, options: [.initial, .new], context: nil)
//        
//        //If we have a player not playing, replace the current item with the new playerItem
//        if(self.player?.currentItem != self.playerItem){
//            self.player?.replaceCurrentItem(with: self.playerItem)
//        }
//    }
//    
//    
//    @objc func playerDidFinishPlaying(note:Any){
//        print("done playing")
//        if(self.player != nil) {
//            self.player?.pause()
//            self.playerLayer?.removeFromSuperlayer()
//            self.player = nil;
//            self.playerLayer = nil;
//        }
//        
//        //post a notification that track is done playing
//        
//        NotificationCenter.default.post(name: Notification.Name.playerFinishedPlaying,
//                                        object: nil,
//                                        userInfo:["currentPlayingIndex": currentPlayingTrackIndex])
//        
//        //play next song on track
//        playNextTrack()
//        
//    }
//    
//    /* -----------------------------------------
//     ** initURLAsset
//     **
//     ** creates the AVURLAsset to use for playback if one has not been already created
//     ** since we only have one stream we can presume the asset will always be the same
//     ** so by checking we can save memory and prevent state issues due to configuration
//     ** on the asset changing
//     ** ----------------------------------------*/
//    
//    func initOnlineURLAsset(urlString:String){
//        let urlStr: String = urlString;
//        var url:URL? = nil;
//        url = URL(string: urlStr)
//        //set AssetLoaderDelegate to version that dosn't set keys
//        self.loaderDelegate = CustomAssetLoaderDelegate();
//        self.asset = AVURLAsset(url: url!);
//        self.asset!.resourceLoader.setDelegate(self.loaderDelegate, queue: globalNotificationQueue());
//        
//        //set the duration of the track
//        
//        
//        //        print("duration of asset " )
//        //        print("duration of asset " , asset?.duration)
//        //        //asset.d
//        //        if let duration = self.player?.currentItem?.duration.seconds {
//        //
//        //            lblEndTime.text =  String(duration)
//        //
//        //        }
//        
//    }
//    
//    /* -----------------------------------------
//     ** initURLAsset
//     **
//     ** creates the AVURLAseet to use for playback if one has not been already created
//     ** since we only have one stream we can presume the asset will always be the same
//     ** so by checking we can save memory and prevent state issues due to configuration
//     ** on the asset changing
//     ** ----------------------------------------*/
//    
//    func initOfflineURLAsset(urlString:String){
//        var url:URL? = nil
//        let userDefaults = UserDefaults.standard
//        var bookmarkDataIsStale = false;
//        //Check if there is a saved download if true use that insead of path
//        if let fileBookmark = userDefaults.data(forKey:"savedPath") {
//            print("Using Local File")
//            do {
//                url = try URL(resolvingBookmarkData:fileBookmark, bookmarkDataIsStale: &bookmarkDataIsStale)
//            }
//            catch {
//                print ("URL from Bookmark Error: \(error)")
//            }
//            //set AssetLoaderDelegate to version with persistant keys
//            self.loaderDelegate = CustomAssetDownloadLoaderDelegate()
//            self.asset = AVURLAsset(url: url!);
//            self.asset!.resourceLoader.setDelegate(self.loaderDelegate, queue: globalNotificationQueue());
//        }
//        else {
//            print("no local file")
//        }
//    }
//    
//    
//    /* -----------------------------------------
//     ** playMediaStream
//     **
//     ** checks for if the asset is loaded and playable
//     ** if so it starts playback if not it throws error
//     ** ----------------------------------------*/
//    //attempts to create a new AVURLAsset then checks for when the asset is loaded to start the playback initialization.
//    
//    func readyMediaStream(){
//        let requestedKeys:Array<String> = [PLAYABLE_KEY];
//        //load the value of playable and execute code block with result
//        if let asset = self.asset {
//            asset.loadValuesAsynchronously(forKeys: requestedKeys, completionHandler: ({
//                () -> Void in
//                var error: NSError? = nil
//                switch self.asset!.statusOfValue(forKey: self.PLAYABLE_KEY, error: &error){
//                case .loaded:
//                    if(self.asset!.isPlayable){
//                        
//                        print(requestedKeys)
//                        self.initPlay(asset:self.asset!,keys:requestedKeys)
//                    }
//                    else {
//                        print("Not Playable")
//                        
//                        let logoutAlert = UIAlertController(title: "Failed", message: "This song is not playable", preferredStyle: UIAlertController.Style.alert)
//                        logoutAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
//                            
//                        }))
//                        self.present(logoutAlert, animated: true, completion: nil);
//                        
//                    }
//                case .failed:
//                    print("No Playable Status")
//                    let logoutAlert = UIAlertController(title: "Failed", message: "File not found", preferredStyle: UIAlertController.Style.alert)
//                    logoutAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
//                        
//                    }))
//                    self.present(logoutAlert, animated: true, completion: nil);
//                    
//                case .cancelled:
//                    print("Loading Cancelled")
//                default:
//                    print("loading error Unknown")
//                }
//            }))
//        }
//    }
//    
//    //makes the asset preperation calls Async so other actions can occur at the same time
//    func initPlay(asset: AVURLAsset, keys: Array<String>){
//        DispatchQueue.main.async(execute: {() -> Void in
//            /* IMPORTANT: Must dispatch to main queue in order to operate on the AVPlayer and AVPlayerItem. */
//            self.prepareToPlayAsset(asset:asset, requestedKeys: keys)
//        })
//    }
//    
//    
//    func playerPause (){
//        self.player?.pause()
//    }
//    
//    func changePlaylist(){
//        
//        self.player?.pause()
//        self.playerLayer?.removeFromSuperlayer()
//        self.player = nil;
//        self.asset = nil;
//        self.playerLayer = nil;
//        
//        
//    }
//    
//    
//    //MARK:- player methods
//    @objc func startPlay(_ notification: Notification){
//        showMiniPlayer()
//        isFmRadioStream = false
//        trackProgressIndicator.isHidden = false
//        tracks = notification.userInfo?["tracks"] as! Array <Track>
//        currentPlayingTrackIndex = notification.userInfo?["currentPlayingIndex"] as! Int
//        mediaServerEndpoint = constructStreamingURL(track: tracks[currentPlayingTrackIndex])
//        
//        let player = AudioManager.sharedInstance
//        player.tracks = self.tracks
//        initPlay()
//        
//    }
//    
//    
//    @objc func playNextTrack(){
//        
//        if(currentPlayingTrackIndex < tracks.count) {
//            unload()
//            currentPlayingTrackIndex =  currentPlayingTrackIndex + 1
//            
//            initPlay()
//        }
//        
//    }
//    
//    @objc func playPreviousTrack(){
//        if(currentPlayingTrackIndex > 0 ) {
//            unload()
//            currentPlayingTrackIndex =  currentPlayingTrackIndex - 1
//            
//            initPlay()
//        }
//        
//    }
//    
//    @objc func resetStream(){
//        
//        player?.seek(to: .zero)
//        player?.pause()
//        
//    }
//    
//    @objc func playRadioStream(_ notification: Notification){
//        showMiniPlayer()
//        isFmRadioStream = true
//        trackProgressIndicator.isHidden = true
//        unload()
//        
//        
//        let stationName = notification.userInfo?["name"] as! String
//        
//        //description might be optional that's why we have a guard
//        guard let description = notification.userInfo?["description"] as! String?
//            else {
//                return
//        }
//        
//        guard let imageURL = notification.userInfo?["image_url"] as! String?
//            else {
//                return
//        }
//        
//        //some streams might have a problem or missing that's why we try catch
//        
//        guard let streamURL = URL.init(string: (notification.userInfo?["streaming_url"] as! String?)!)
//            else {
//                
//                lblTrackTitle.text = "There was an error loading this radio stream"
//                lblArtist.text = ""
//                return
//        }
//        
//        lblTrackTitle.text = stationName
//        lblArtist.text = description
//        
//        self.fmStationName = stationName
//        self.fmStationDes = description
//        self.fmStationImageURL = imageURL
//        
//        //start radio stream
//        
//        self.playerItem = AVPlayerItem.init(url: streamURL)
//        
//        
//        self.player = AVPlayer.init(playerItem: self.playerItem)
//        self.player?.play()
//        btnPlayPause.setImage(#imageLiteral(resourceName: "ic_pause"), for: .normal)
//        
//    }
//    
//    
//    @objc func showSearchVC(_ notification: Notification){
//        let fmStoryBoard = UIStoryboard(name: "Search", bundle: nil)
//        let controller =  fmStoryBoard.instantiateInitialViewController()
//        self.displayContentController(contentController: controller!)
//    }
//    
//    func showMiniPlayer(){
//        childViewBottomConstraint.constant = 0
//        miniPlayer.isHidden = false
//        self.view.layoutSubviews()
//        self.viewDidAppear(true)
//        
//    }
//    
//    func initPlay(){
//        
//        if(currentPlayingTrackIndex < tracks.count && currentPlayingTrackIndex >= 0) {
//            
//            mediaServerEndpoint = constructStreamingURL(track: tracks[currentPlayingTrackIndex])
//            
//            lblArtist.text = "Loading..."
//            lblTrackTitle.text = tracks[currentPlayingTrackIndex].title
//            
//            if(self.player == nil){
//                
//                
//                if !tracks.isEmpty
//                {
//                    
//                    initOnlineURLAsset(urlString: mediaServerEndpoint)
//                    readyMediaStream()
//                    
//                }
//            }
//            else {
//                print("does has asset")
//                //unload asset and try again
//                
//                unload()
//                
//                initOnlineURLAsset(urlString: mediaServerEndpoint)
//                readyMediaStream()
//                
//            }
//        }
//    }
//    
//    //remote control of the player
//    
//    func setupRemoteCommandCenter() {
//        
//        // Get the shared MPRemoteCommandCenter
//        
//        let commandCenter = MPRemoteCommandCenter.shared()
//        
//        // Add handler for Play Command
//        
//        commandCenter.playCommand.addTarget { event in
//            
//            self.player?.play()
//            
//            print("headset play")
//            
//            return .success
//        }
//        
//        // Add handler for Pause Command
//        
//        commandCenter.pauseCommand.addTarget { event in
//            
//            self.player?.pause()
//            
//            print("headset pause")
//            
//            return .success
//        }
//        
//        // Add handler for Next Command
//        
//        commandCenter.nextTrackCommand.addTarget { event in
//            
//            return .success
//        }
//        
//        // Add handler for Previous Command
//        
//        commandCenter.previousTrackCommand.addTarget { event in
//            
//            return .success
//        }
//    }
//    
//    //set up data to be displayed on Lockscreen and control center
//    func setupNowPlaying( title : String , artist:String, url:String) {
//        
//        // Define Now Playing Info
//        var nowPlayingInfo = [String : Any]()
//        nowPlayingInfo[MPMediaItemPropertyTitle] = title
//        
//        SDWebImageManager.shared().imageDownloader?.downloadImage(with:URL(string: url), options: SDWebImageDownloaderOptions.useNSURLCache, progress: nil, completed: { (image, error, cacheType, url) in
//            if let _ = image {
//                // Use the image object
//                nowPlayingInfo[MPMediaItemPropertyArtwork] =  MPMediaItemArtwork(boundsSize: (image?.size)!) { size in
//                    return image!
//                }
//            } else {
//                // Put Default Cover Art
//                nowPlayingInfo[MPMediaItemPropertyArtwork] =  MPMediaItemArtwork(boundsSize: (#imageLiteral(resourceName: "no_cover").size)) { size in
//                    return #imageLiteral(resourceName: "no_cover")
//                }
//                
//            }
//            //set again
//            MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
//            
//        })
//        nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = playerItem?.currentTime().seconds
//        nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = playerItem?.asset.duration.seconds
//        //nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = player?.rate
//        
//        // Set the metadata
//        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
//    }
//    
//    //    private func updateNowPlayingInfoCenter(artwork: UIImage? = nil) {
//    //        guard let file = currentItem else {
//    //            MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo = [String: AnyObject]()
//    //            return
//    //        }
//    //        if let imageURL = file.album?.imageUrl where artwork == nil {
//    //            Haneke.Shared.imageCache.fetch(URL: imageURL, success: {image in
//    //                self.updateNowPlayingInfoCenter(image)
//    //            })
//    //            return
//    //        }
//    //        MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo = [
//    //            MPMediaItemPropertyTitle: file.title,
//    //            MPMediaItemPropertyAlbumTitle: file.album?.title ?? "",
//    //            MPMediaItemPropertyArtist: file.album?.artist?.name ?? "",
//    //            MPMediaItemPropertyPlaybackDuration: audioPlayer.duration,
//    //            MPNowPlayingInfoPropertyElapsedPlaybackTime: audioPlayer.progress
//    //        ]
//    //        if let artwork = artwork {
//    //            MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo?[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(image: artwork)
//    //        }
//    //}
//    
//    //image downloading methods (asynchronous)
//    
//    func downloadImage(url:NSURL, completion: @escaping ((_ image: UIImage?) -> Void)){
//        
//        getDataFromUrl(url: url) { data in
//            DispatchQueue.main.async() {
//                completion(UIImage(data: data! as Data))
//            }
//        }
//    }
//    
//    func getDataFromUrl(url:NSURL, completion: @escaping (( _ data: NSData?) -> Void)) {
//        URLSession.shared.dataTask(with: url as URL) { (data, response, error) in
//            completion(data! as NSData)
//            }.resume()
//    }
//    
//    
//    //this function converts CMTime to string like 3:24
//    
//    func seconds2Timestamp(intSeconds:Int)->String {
//        let mins:Int = intSeconds/60
//        let secs:Int = intSeconds%60
//        
//        let strTimestamp:String = ((mins<10) ? "0" : "") + String(mins) + ":" + ((secs<10) ? "0" : "") + String(secs)
//        return strTimestamp
//        
//    }
//    
//    func unload(){
//        self.player?.pause()
//        self.playerLayer?.removeFromSuperlayer()
//        self.player = nil;
//        self.asset = nil;
//        self.playerLayer = nil;
//        self.trackProgressIndicator.progress = 0.0
//    }
//    
//    func play(){
//        
//        if self.player != nil {
//            
//            print("file is ready to play, proceeding to play the file")
//            
//            setupNowPlaying(title: tracks[currentPlayingTrackIndex].title, artist: tracks[currentPlayingTrackIndex].artistName, url: tracks[currentPlayingTrackIndex].artwork[0].small)
//            
//            //notify that play has started
//            
//            NotificationCenter.default.post(name: Notification.Name.playerStartedPlaying,
//                                            object: nil,
//                                            userInfo:["player": player as Any])
//            
//            
//            lblArtist.text = tracks[currentPlayingTrackIndex].artistName
//            
//            player?.play()
//            btnPlayPause.setImage(#imageLiteral(resourceName: "ic_pause"), for: .normal)
//            
//            
//            
//            //initializing a time frame of 1 second for the player
//            
//            let interval = CMTime(value: 1, timescale: 1)
//            
//            player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { (progressTime) in
//                
//                let progressSecondsCMTime = CMTimeGetSeconds(progressTime)
//                //move progress view
//                
//                if let duration = self.player?.currentItem?.duration {
//                    
//                    let durationSeconds = CMTimeGetSeconds(duration)
//                    
//                    self.trackProgressIndicator.progress = Float(progressSecondsCMTime/durationSeconds)
//                    
//                }
//                
//            })
//        }
//    }
//    
//    
//    func constructStreamingURL ( track : Track) -> String  {
//        
//        mediaServerEndpoint  = APIurls.StreamingURLBase + track.id + APIurls.StreamingURLSuffix
//        print(mediaServerEndpoint)
//        return mediaServerEndpoint
//        
//    }
//    
//    func addTrackToRecentlyPlayed( track: Track){
//        
//        let recentlyPlayedTrack: RecentlyPlayed = RecentlyPlayed()
//        recentlyPlayedTrack.id = track.id
//        recentlyPlayedTrack.track = track
//        
//        do {
//            let realm = try Realm()
//            try realm.write {
//                realm.add(recentlyPlayedTrack,update: true)
//            }
//            
//        } catch let error as NSError {
//            
//            print(error)
//            
//        }
//        
//    }
//    
//}
//
//
