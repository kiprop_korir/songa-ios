//
//  CustomSliderView.swift
//  customSildermenu
//
//  Created by Kip on 11/04/18.
//  Copyright © 2018 Kip. All rights reserved.
//

import UIKit
import RealmSwift
import SDWebImage

class CustomSliderView: UIView,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate{
    
    var tableMenus : UITableView! = nil
    var currentUser: RAGUser? = nil

    var viewCTR : UIViewController = UIViewController()
    var viewToHideSlideMenu : UIView = UIView()
    var tapGesture : UITapGestureRecognizer = UITapGestureRecognizer()
    var rightSideIcon:Bool = false
    var rigthsideSlider:Bool = false
    var shownavigation: Bool = false
    var menuName:UILabel! = nil
    var imgMehu:UIImageView! =  nil
    
    var lblBadgeCount:UILabel! =  nil
    var imglist: [UIImage] = []
    var titlelist: [String] = []
    var badgelist: [String] = []

    
    //MARK:- Gesture Handle
    
    static func getCustomSliderMenus() -> CustomSliderView{
        var viewCustom : CustomSliderView
        viewCustom = CustomSliderView.init(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height))
        
        let color = UIColor(hexString: "#111111")
        viewCustom.backgroundColor = color
        
        
    
        return viewCustom;
    }
  
    
    //MARK:- ShowSliderMenu Function
    func showSliderMenu(_ viewCTRTemp : UIViewController){
        viewCTR = viewCTRTemp
        self.backgroundColor = UIColor.white
        let window : UIWindow = UIApplication.shared.windows[0]
        window.backgroundColor = UIColor.white
        if rigthsideSlider == true{
            tableMenus = UITableView(frame: CGRect(x: 0 , y: 0, width: 256, height: window.frame.size.height), style: .grouped)
        }else{
            tableMenus = UITableView(frame: CGRect(x: 0, y: 0, width: 256, height: window.frame.size.height ), style: .grouped)
        }
        
        tableMenus.bounces = false;
        tableMenus.autoresizingMask = [.flexibleHeight, .flexibleWidth, .flexibleRightMargin, .flexibleLeftMargin, .flexibleBottomMargin, .flexibleTopMargin]
        tableMenus.translatesAutoresizingMaskIntoConstraints = true
        tableMenus.delegate = self
        tableMenus.dataSource = self
        
        //table view background color
        
        tableMenus.backgroundColor = UIColor.white
        tableMenus.tableFooterView = UIView.init(frame: CGRect.zero)
        tableMenus.tableHeaderView = UIView.init(frame: CGRect.zero)
        tableMenus.sectionHeaderHeight = 110
        
        //table footer view
        
        
        let viewFooter: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 256, height: 80));
        
        viewFooter.backgroundColor = UIColor (hexString: "#1C1E32")
        // Dark theme
        
        let lblName2 = UILabel(frame: CGRect(x:15, y: viewFooter.frame.size.height - 60, width: viewFooter.frame.size.width - 120 , height: 30));
        lblName2.text = "Enable Dark Theme"
        lblName2.textColor = UIColor ( hexString: "ffffff")
        lblName2.font = UIFont.systemFont(ofSize: 15.0)
        viewFooter.addSubview(lblName2);
        
        
        let switchtheme =  UISwitch (frame: CGRect(x:lblName2.frame.size.width + 40, y: viewFooter.frame.size.height - 60, width: viewFooter.frame.size.width - 40 , height: 30));
        switchtheme.isOn = true
        viewFooter.addSubview(switchtheme);
      
        //table header view
        
        // contains profile pic , name and username
        let viewHeader: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 256, height: 190));
        
        
        
        //MARK:- Title OF nemu If you don't want to put logo image
        //        let lbltitle: UILabel = UILabel.init(frame: CGRect(x: 62, y: 15, width: 200, height: 50))
        //        lbltitle.text = Title;
        //        lbltitle.font = lbltitle.font.withSize(23)
        //        lbltitle.font = UIFont(name: "Helvetica-Condensed" as String, size: 23);
        //        lbltitle.textColor = UIColor.init(red: 34/255, green: 34/255, blue: 34/255 , alpha: 1.0)
        //        viewHeader.addSubview(lbltitle);
        
        
        //MARK:- Logo Image
        
        let ivPpic:UIImageView = UIImageView(frame: CGRect(x: 15, y: 40, width: 70, height: 70));
        ivPpic.contentMode = .scaleAspectFit
        
        ///get logged in users from realm
        let realm = try! Realm()
        var loggedInUsers: [RAGUser] = []
        let loggedInUserResults: Results<RAGUser>? = realm.objects(RAGUser.self)
        
        for loggedInUserResults in loggedInUserResults! {
            
            loggedInUsers.append(loggedInUserResults)
            
        }
        //inverse to get latest first
        
        loggedInUsers.reverse()
        
        currentUser = loggedInUsers[0]
        
        //get ppic url from realm
        
        let ppicURL = ProfilePicture.facebook + (currentUser?.user?.facebookUserID)! + "/picture?type=normal"
        
        ivPpic.sd_setImage(with: URL(string: ppicURL), placeholderImage: #imageLiteral(resourceName: "user_avatar"))
     
   
        
        //make it rounded
        
        ivPpic.layer.cornerRadius = (ivPpic.frame.height)/2
        ivPpic.clipsToBounds = true
        
        //add border
        
        ivPpic.layer.borderColor = UIColor.black.cgColor
        ivPpic.layer.borderWidth = 0.5
        
        
        let color = UIColor(hexString: "#1C1E32")
        
        viewHeader.backgroundColor = color
        ivPpic.layoutIfNeeded()
        viewHeader.addSubview(ivPpic);
       

        
        
        //        let imgMehu2:UIImageView = UIImageView(frame: CGRect(x: 30, y: 10, width: 100, height: 200));
        //        imgMehu2.contentMode = .scaleAspectFit
        //        imgMehu2.image = UIImage(named: "OnOneCall")
        //        viewHeader.backgroundColor = UIColor.init(red: 52/255, green: 138/255, blue: 61/255, alpha: 1.0)
        //        imgMehu2.layoutIfNeeded()
        //        viewHeader.addSubview(imgMehu2)
        
        
        //        //MARK:- Username
        //        let btnUser = UIButton(frame: CGRect(x:10, y: viewHeader.frame.size.height - 40, width: viewHeader.frame.size.width - 40 , height: 30));
        //        btnUser.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
        //        btnUser.setTitle("Songa User", for: .normal)
        //        //btnUser.setImage(UIImage(named: "UserIconWhite"), for: .normal);
        //        btnUser.imageView?.contentMode = .scaleAspectFit;
        //        btnUser.addTarget(self, action: #selector(self.btnUserProfileClicked(_:)), for: .touchUpInside);
        //        btnUser.contentHorizontalAlignment = .left;
        //        btnUser.setTitleColor(UIColor.white,for: UIControlState.normal)
        //        btnUser.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0)  // Making Gap Between Image and Title
        //
        
        
        // user's name
        
        let lblName = UILabel(frame: CGRect(x:15, y: viewHeader.frame.size.height - 60, width: viewHeader.frame.size.width - 40 , height: 30));
        lblName.text = currentUser?.user?.name
        lblName.textColor = UIColor ( hexString: "ffffff")
        lblName.font = UIFont.boldSystemFont(ofSize: 17.0)
        viewHeader.addSubview(lblName);
        
        
        // user's username
        
        let lblUsername = UILabel(frame: CGRect(x:15, y: viewHeader.frame.size.height - 42.5, width: viewHeader.frame.size.width - 40 , height: 30));
        lblUsername.text = "@Kip254"
        lblUsername.textColor = UIColor ( hexString: "D3D3D3")
        lblUsername.font = UIFont.systemFont(ofSize: 15.0)
        viewHeader.addSubview(lblUsername);
        
        //        //MARK:- set image on edit button
        //        let imgIcon:UIImageView = UIImageView(frame: CGRect(x: viewHeader.frame.size.width - 35, y: viewHeader.frame.size.height - 35, width: 20 , height: 20));
        //        imgIcon.contentMode = .scaleAspectFit
        //        imgIcon.image = UIImage(named: "EditProfile")
        //        imgIcon.layoutIfNeeded()
        //        viewHeader.addSubview(imgIcon);
        
        if shownavigation == true{
            tableMenus.tableHeaderView = viewHeader;
            tableMenus.tableFooterView =  viewFooter;
            tableMenus.backgroundColor = UIColor(hexString: "#1C1E32")
            
        }
        
        tableMenus.separatorInset = .zero;
        self.addSubview(tableMenus);
        tableMenus.showsVerticalScrollIndicator = false;
        tableMenus.reloadData();
        
        if rigthsideSlider == true{
            viewToHideSlideMenu = UIView.init(frame: CGRect(x: 256, y: 0, width: self.frame.size.width, height: self.frame.size.height));
        }else{
            viewToHideSlideMenu = UIView.init(frame: CGRect(x: -256, y: 0, width: self.frame.size.width, height: self.frame.size.height));
        }
        
        viewToHideSlideMenu.backgroundColor = UIColor.clear;
        window.addSubview(viewToHideSlideMenu);
        viewToHideSlideMenu.isHidden = false;
        
        //Set TAP Gesture
        let tapGestureTemp = UITapGestureRecognizer.init(target: self, action: #selector(CustomSliderView.tapGestureHappen));
        tapGestureTemp.numberOfTapsRequired = 1;
        tapGestureTemp.numberOfTouchesRequired = 1;
        viewToHideSlideMenu.addGestureRecognizer(tapGestureTemp);
        
        //Set Swipe Gesture
        let swipeGesture = UISwipeGestureRecognizer.init(target: self, action: #selector(CustomSliderView.swipeGestureHappen));
        viewToHideSlideMenu.addGestureRecognizer(swipeGesture);
        
        self.frame = CGRect(x: 0, y: 0, width: window.frame.size.width, height: self.frame.size.height);
        window.addSubview(self);
        
        UIView.transition(with: self, duration: 0.25, options: UIView.AnimationOptions( ), animations: { () -> Void in
           
            self.viewCTR.navigationController?.view.frame = CGRect(x: 256, y: self.viewCTR.navigationController!.view.frame.origin.y, width: self.viewCTR.navigationController!.view.frame.size.width, height: self.viewCTR.navigationController!.view.frame.size.height);
         
            window.bringSubviewToFront(self.viewCTR.navigationController!.view);

          }, completion: { (finished) -> Void in
            self.viewToHideSlideMenu.isHidden = false;
            window.bringSubviewToFront(self.viewToHideSlideMenu);
        });
        self.layoutIfNeeded()

    }
    
    @objc func tapGestureHappen() {
        self.dissmissPopView();
    }
    
    @objc func swipeGestureHappen() {
        self.dissmissPopView();
    }
    
    func dissmissPopView() {
        
        viewCTR.view.isUserInteractionEnabled = true
        
        let _ : UIWindow = UIApplication.shared.windows[0]
        
        UIView.transition(with: self, duration: 0.25, options: UIView.AnimationOptions(), animations: { () -> Void in
            
            self.viewCTR.navigationController!.view.frame = CGRect(x: 0, y: 0, width: self.viewCTR.navigationController!.view.frame.size.width, height: self.viewCTR.navigationController!.view.frame.size.height)
            
        }) { (finished) -> Void in
            self.tableMenus.removeFromSuperview()
            self.viewToHideSlideMenu.isHidden = true
        };
        self.layoutIfNeeded()
        
    }
    
    func gestureRecognizer (_ gestureRecognizer : UIGestureRecognizer, touch:UITouch) -> Bool{
        
        if (touch.view == tableMenus) {
            return false;
        }
        return true;
    }
    
    
    //MARK: - UITableView delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlelist.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//        return 56
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier : String = "Cell"
        var cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if (cell == nil) {
            cell = UITableViewCell.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: cellIdentifier)
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        //cell back ground color
        
        
        
        cell.backgroundColor = UIColor(hexString: "#1C1E32")
        
        
        menuName = UILabel(frame: CGRect(x: 60, y: 10, width: 200, height: 20))
        
        
        
        menuName.font = UIFont.systemFont(ofSize: 15.0)
        //  menuName.font = menuName.font.withSize(16)
        menuName.text = titlelist[indexPath.row]
        menuName.tag = 101;
        menuName.textColor = UIColor.white
        
        cell.addSubview(menuName)
        
        imgMehu = UIImageView(frame: CGRect(x: 20, y: 10, width: 24, height: 24))
        
        
        imgMehu.image = imglist[indexPath.row]
        imgMehu.contentMode = .scaleAspectFit
        imgMehu.tintColor = UIColor(hexString: "D3D3D3")
        
        cell.addSubview(imgMehu)
        
        
        lblBadgeCount = NotificationBadge(frame: CGRect(x: 216, y: 10, width: 20, height: 20))
        lblBadgeCount.text = badgelist[indexPath.row]
        
      if(lblBadgeCount.text != ""){
            
            cell.addSubview(lblBadgeCount)
        }
        
        
        let imgNav:UIImageView = UIImageView(frame: CGRect(x: tableMenus.frame.size.width - 20, y: 10, width: 10, height: 24))
        imgNav.image = UIImage(named: "next-green")
        imgNav.contentMode = .scaleAspectFit
        
        
        if indexPath.row != titlelist.count - 1 {
            cell.addSubview(imgNav)
        }
        self.tableMenus.separatorColor = UIColor.black
        UITableView.appearance().separatorColor = UIColor.black
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            menuItemClicked(index: 0)
        case 1:
            menuItemClicked(index: 1)
        case 2:
          let languageAlert = UIAlertController(title: "Language", message: nil, preferredStyle: .alert)
          languageAlert.addAction(UIAlertAction(title: "English", style: .default, handler: { (action) in
            //execute some code when this option is selected
           //english selected
          }))
          languageAlert.addAction(UIAlertAction(title: "Swahili", style: .default, handler: { (action) in
            //execute some code when this option is selected
           //swa
          }))
            self.window?.rootViewController?.present(languageAlert, animated: true, completion: nil)
            
        case 3:
            menuItemClicked(index: 3)
        case 4:
            menuItemClicked(index: 4)
        case 5:
            menuItemClicked(index: 5)
        case 6:
            let logoutAlert = UIAlertController(title: "Logout", message: "Are you sure, you want to log out?", preferredStyle: UIAlertController.Style.alert)
            logoutAlert.addAction(UIAlertAction(title: "NO", style: .default, handler: { (action: UIAlertAction!) in
                print("Handle NO logic here")
            }))
            logoutAlert.addAction(UIAlertAction(title: "YES", style: .cancel, handler: { (action: UIAlertAction!) in
                
                print("yes clicked")
                self.dissmissPopView()
                
            }))
            self.window?.rootViewController?.present(logoutAlert, animated: true, completion: nil)
     
        default:
          
            print("Nothing clicked")
        }
    
        
        sliderhandle()
    }
    
    func sliderhandle(){
        //eneble the main view to respond to touch
        viewCTR.view.isUserInteractionEnabled = true
        
        let window : UIWindow = UIApplication.shared.windows[0]
        UIView.transition(with: self, duration: 0.25, options: UIView.AnimationOptions(), animations: { () -> Void in
            
              self.viewCTR.navigationController!.view.frame = CGRect(x: 0, y: 0, width: self.viewCTR.navigationController!.view.frame.size.width, height: self.viewCTR.navigationController!.view.frame.size.height)
            
            window.bringSubviewToFront(self.viewCTR.navigationController!.view)
            
        })
        {
            (finished) -> Void in
            
            self.tableMenus.removeFromSuperview()
            self.viewToHideSlideMenu.isHidden = false
        }
        self.layoutIfNeeded()
    }
    
    
    func menuItemClicked( index: Int){
        NotificationCenter.default.post(name: Notification.Name.menuItemClicked,
                                        object: nil,
  userInfo:["index": index])
    }
    
    @objc func btnUserProfileClicked(_ sender: UIButton) {
        // Username edit Action
    }
    
  
}
