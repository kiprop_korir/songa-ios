//
//  SearchSectionHeader.swift
//  Songa
//
//  Created by Collins Korir on 9/19/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import Foundation
import UIKit

class SearchSectionHeader : UICollectionReusableView {
    
    @IBOutlet weak var categoryLabel : UILabel!
    
    var categoryTitle:String! {
        
        didSet {
            categoryLabel.text = categoryTitle
        }
    }
    
}
