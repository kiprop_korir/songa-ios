//
//  ShowMoreArtistsOrTracksViewController.swift
//  Songa
//
//  Created by Collins Korir on 10/22/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class ShowMoreDefaultViewController: UIViewController {
    

    @IBOutlet weak var collectionView: UICollectionView!
    var type : String = ""
    

    
    var playlists: [Playlist] = []
    var stations: [Station] = []
    var albums: [Album] = []
     var artists: [Artist] = []
    
    var tappedCellID : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.navigationItem.title = type
       
        
    }
   
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "seguePlaylistPage":
            let vc = segue.destination as! PlaylistViewController
                vc.playlistID = sender as! String
            
        case "segueAlbumPage":
            let vc = segue.destination as! AlbumPageViewController
            vc.albumID = sender as! String
            
        case "segueArtistPage":
            let vc = segue.destination as! ArtistPageViewController
            vc.artistID = sender as! String
            
        default:
            print("")
        }
    }
    

    @objc func stationCellTapped(_ sender: stationCellTapGesture) {
        
        
        NotificationCenter.default.post(name: Notification.Name.startRadioPlay,
                                        object: nil,
                                        userInfo:["streaming_url": sender.radioStreamingURL , "name": sender.radioName, "image_url": sender.radioImageURL ,"description": sender.radioDescription])
        
    }
    
    class stationCellTapGesture: UITapGestureRecognizer {
        var radioStreamingURL = String()
        var radioName = String()
        var radioDescription = String()
        var radioImageURL = String()
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var type = String()
        var id = String()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
        switch sender.type {
        case "Artist":
            performSegue(withIdentifier: "segueArtistPage", sender: sender.id)
        case "Playlist":
            performSegue(withIdentifier: "seguePlaylistPage", sender: sender.id)
        case "Album":
            performSegue(withIdentifier: "segueAlbumPage", sender: sender.id)
        default:
            print("")
        }
        
    }
    
    
    
}


extension ShowMoreDefaultViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var count =  Int()
        
        switch type {
        case "Playlists":
           count =  playlists.count
        case "Albums":
            count =  albums.count
        case "Stations":
            count =  stations.count
        case "Artists":
            count =  artists.count
        default:
            print("")
        }
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "playlistCell", for: indexPath) as! PlaylistCell
        
        
        
        switch type {
        case "Playlists":
            let url : String
            
            if ( playlists[indexPath.row].artwork.count > 0){
                url = playlists[indexPath.row].artwork[0].medium
            }
            else
            {
         url = "no_cover"
            }
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = playlists[indexPath.row].name
            let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
            tappy.type = "Playlist"
            tappy.id = playlists[indexPath.row].id
            cell.addGestureRecognizer(tappy)
            
            
        case "Albums":
            let url : String
            
            if ( albums[indexPath.row].artwork.count > 0){
                url = albums[indexPath.row].artwork[0].medium
            }
            else
            {
                url = "no_cover"
            }
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = albums[indexPath.row].title
            let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
            tappy.type = "Album"
                  tappy.id = albums[indexPath.row].id
            cell.addGestureRecognizer(tappy)
          
        case "Stations":
            let url : String
            
            if ( stations[indexPath.row].artwork.count > 0){
                url = stations[indexPath.row].artwork[0].medium
            }
            else
            {
                url = "no_cover"
            }
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = stations[indexPath.row].name
            
            let tappy = stationCellTapGesture(target: self, action: #selector(stationCellTapped(_:)))
            
            tappy.radioStreamingURL =  stations[indexPath.row].stream
            tappy.radioName =  stations[indexPath.row].name
            tappy.radioDescription =  stations[indexPath.row].stationDescription
            tappy.radioImageURL =  stations[indexPath.row].artwork[0].large
            cell.addGestureRecognizer(tappy)
            
        case "Artists":
            let url : String
            
            if ( artists[indexPath.row].artwork.count > 0){
                url = artists[indexPath.row].artwork[0].medium
            }
            else
            {
                url = "no_cover"
            }
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = artists[indexPath.row].name
            let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
            tappy.type = "Artist"
            tappy.id = artists[indexPath.row].id
            cell.addGestureRecognizer(tappy)
         
        default:
            print("")
        }
        
        
        //adding a corner radius to the cell
        cell.layer.cornerRadius = 10
        cell.layer.masksToBounds = true
        
   
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  20
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: 194)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }
}

