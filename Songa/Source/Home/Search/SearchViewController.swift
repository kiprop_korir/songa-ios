//
//  SearchViewController.swift
//  Songa
//
//  Created by Collins Korir on 9/18/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import SDWebImage


class SearchViewController: UIViewController, UITextFieldDelegate {
    
    let searchAPI = APIurls.Search
    let headers = APIurls.Headers
    
    var albums: [Album] = []
    var tracks: [Track] = []
    var artists: [Artist] = []
    var stations: [Station] = []
    var playlists: [Playlist] = []
    var numberOfSections :Int =  0
    
    @IBOutlet weak var miniPlayer: BackgroundView!
    @IBOutlet weak var navItem: UINavigationItem!
    var tfSearchText: SearchTextField? = nil
    @IBOutlet weak var btnCancelSearch: UIButton!
    @IBOutlet weak var cvSearch: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
            setUpNavBar()
        
        cvSearch.delegate =  self
        cvSearch.dataSource = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
              
            self.tfSearchText!.becomeFirstResponder()
        }
    }
    
   
    //below method called when return key in keybourd is clicked
    
    func textFieldShouldReturn(_ tf: UITextField) -> Bool {
        self.view.endEditing(true)
        tf.resignFirstResponder()
        
        albums.removeAll()
        tracks.removeAll()
        artists.removeAll()
        stations.removeAll()
        playlists.removeAll()
        
        cvSearch.reloadData()
        
        fetchSearchResults(searchTerm: tfSearchText!.text!)
        return true
    }
    
    func setUpNavBar(){
    
        tfSearchText = SearchTextField(frame: CGRect(x: 40, y: 7, width: self.view.frame.width - 70, height: 30))
        
        tfSearchText!.delegate = self
        //tfSearchText!.addDoneCancelToolbar(onDone: (target: self, action: #selector(textFieldShouldReturn(_:))))
        UITextField.connectFields(fields: [tfSearchText!])
        
        tfSearchText?.attributedPlaceholder =
            NSAttributedString(string: "Songs, Artists, Playlists and More", attributes: [NSAttributedString.Key.foregroundColor: ThemeManager.currentTheme().textColor!])
         
        tfSearchText?.backgroundColor = ThemeManager.currentTheme().secondaryColor
        tfSearchText?.layer.cornerRadius = 5.0
        tfSearchText?.clearButtonMode = UITextField.ViewMode.whileEditing
        tfSearchText?.placeHolderColor = ThemeManager.currentTheme().textColorSecondary
        tfSearchText?.setLeftImage(imageName: "ic_search")
        tfSearchText?.textColor = ThemeManager.currentTheme().textColor
        
        let barButtonItem = UIBarButtonItem(customView: tfSearchText!)
        self.navigationItem.rightBarButtonItem = barButtonItem


    }
    func showLoadingIndicator(){
    
            let customView = UIView(frame: CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.height))
        customView.backgroundColor = ThemeManager.currentTheme().secondaryColor
            customView.tag = -888755
            // customView.addConstraint(MainViewController.botto)
            let activityIndicator = UIActivityIndicatorView(style: .white)
        activityIndicator.tintColor = ThemeManager.currentTheme().loadingIndicatorColor
            activityIndicator.center = self.view.center
            activityIndicator.startAnimating()
            activityIndicator.center = CGPoint(x: (navigationController!.view.bounds.width)/2, y: (navigationController!.view.bounds.height)/2 - 44)
            customView.addSubview(activityIndicator)
            self.cvSearch.addSubview(customView)
        
    }
    //IB Actions
    
    @IBAction func goBack(_ sender: Any) {
        
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
        //self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    //MARK: - Load Search Content From API
    
    func fetchSearchResults(searchTerm: String){
        numberOfSections = 0
 self.showLoadingIndicator()
        
        Alamofire.request(APIurls.Search, method: .post, parameters: ["query": searchTerm], encoding: JSONEncoding.default ,  headers: headers ).responseObject() { (response: DataResponse<SearchResponse>) in
            
        self.cvSearch.viewWithTag(-888755)?.removeFromSuperview()
   
           
            let searchResponse = response.result.value
            if let value = searchResponse {
                

                //do sth with the data
                self.albums = value.albums!
                self.tracks = value.tracks!
                self.artists = value.artists!
                self.stations = value.stations!
                self.playlists = value.playlists!
                
           
                
                if (self.albums.count > 0) {
                    
                    print(" albumsnumber is ",self.albums.count)
                    self.numberOfSections =  self.numberOfSections + 1
                }
                if (self.tracks.count > 0) {
                    print(" track number is ",self.tracks.count)
                    self.numberOfSections =  self.numberOfSections + 1
                }
                if (self.artists.count > 0) {
                    print("artis number is ",self.artists.count)
                    self.numberOfSections =  self.numberOfSections + 1
                }
                if (self.stations.count > 0) {
                    print(" statio number is ",self.stations.count)
                    self.numberOfSections =  self.numberOfSections + 1
                }
                if (self.playlists.count > 0) {
                    print(" playl number is ",self.playlists.count)
                    self.numberOfSections =  self.numberOfSections + 1
                }
                self.cvSearch.reloadData()
                
                print(" number of sections is ",self.numberOfSections)
                
            }
            
            else {
                if(self.playlists.count == 0){
                    self.cvSearch.setEmptyMessage("Could not load search results")
                }
                
            }
        }
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var tracks = Array<Track>()
        var currrentPlayingIndex = Int()
        var cellType = String()
         var id = String()
    }
    
    class showMoreTapGesture: UITapGestureRecognizer {
        var section = Int()
    }
    class optionsTapGesture: UITapGestureRecognizer {
        var track = Track()
    }
    
    @objc func optionsTapped(_ sender: optionsTapGesture) {
        showOptionsController(track: sender.track)
    }
    @objc func trackCellTapped(_ sender: cellTapGesture) {
        
        
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
        
    }
    @objc func showMoreTapped(_ sender: showMoreTapGesture) {
        
        switch sender.section {
            
        case 0 :
            performSegue(withIdentifier: "showMoreTracks", sender: "Tracks")
        case 1 :
            performSegue(withIdentifier: "showMoreDefault", sender: "Artists")
        case 2 :
            performSegue(withIdentifier: "showMoreDefault", sender: "Albums")
        case 3 :
            performSegue(withIdentifier: "showMoreDefault", sender: "Playlists")
        case 4 :
            performSegue(withIdentifier: "showMoreDefault", sender:"Stations")
        default:
            print("")
        
    }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "showMoreTracks":
            let vc = segue.destination as! ShowMoreTracksViewController
                vc.tracks = self.tracks
            vc.shouldFetch = false
            vc.type = sender as! String
            
        case "showMoreDefault":
            let vc = segue.destination as! ShowMoreDefaultViewController
            vc.type = sender as! String
            vc.albums = self.albums
            vc.playlists = self.playlists
            vc.artists = self.artists
            vc.stations = self.stations
            
        case "segueArtistPage":
            let vc = segue.destination as! ArtistPageViewController
            vc.artistID = sender as! String
            
        case "seguePlaylistPage":
            let vc = segue.destination as! PlaylistViewController
            vc.playlistID = sender as! String
            
        case "segueAlbumPage":
            let vc = segue.destination as! AlbumPageViewController
            vc.albumID = sender as! String
         
        

        default:
            print("")
        }
    }
    
    @objc func otherCellTapped(_ sender: cellTapGesture) {
        
        //peform segue with identifier -- > sender.segue_id
        
        switch sender.cellType {
        case "artist":
            performSegue(withIdentifier: "segueArtistPage", sender: sender.id)
        case "playlist":
            performSegue(withIdentifier: "seguePlaylistPage", sender: sender.id)
        case "album":
            performSegue(withIdentifier: "segueAlbumPage", sender: sender.id)
        default:
            print("")
        }
        
    }
}

extension SearchViewController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    
    //MARL: - Delegate dat/Users/kip/Documents/XCode Projects/songa-ios/Songa/Source/Discoverasource methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        var number : Int = 0
        
        switch section {
            
        case 0:
            number = tracks.prefix(5).count
        case 1:
            number = artists.prefix(5).count
        case 2:
            number = albums.prefix(5).count
        case 3:
            number = playlists.prefix(5).count
        case 4:
            number = stations.prefix(5).count
        default :
            break
            
        }
        
        return number
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return  numberOfSections
    }
    
    //header/footer view
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        var  cell: UICollectionReusableView? = nil
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
    
            
            
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "searchSectionHeader", for: indexPath) as! SearchSectionHeader
           
            switch indexPath.section {
            case 0 :
                if(tracks.count) > 0 {
                headerView.categoryTitle = "Tracks"
                }
                else {
                   headerView.categoryTitle = ""
                }
            case 1 :
                if(artists.count) > 0 {
                    headerView.categoryTitle = "Artists" }
                
                else {
                    headerView.categoryTitle = ""
                }
            case 2 :
                    if(albums.count) > 0 {
                        headerView.categoryTitle = "Albums" }
                        else {
                            headerView.categoryTitle = ""
                        }
                    
            case 3 :
                    if(playlists.count) > 0 {
                    headerView.categoryTitle = "Playlists"}
                else {
                    headerView.categoryTitle = ""
                }
            case 4 :
                    if(stations.count) > 0 {
                        headerView.categoryTitle = "Stations"}
                    else {
                        headerView.categoryTitle = ""
                    }
                    
                
            default:
                break
            }
            
            
            cell =  headerView
            
        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "searchSectionFooter", for: indexPath) as! SearchSectionFooter
            
            switch indexPath.section {
            case 0 :
                if(tracks.count<5){
                    footerView.viewShowMore.isHidden = true
                }
            case 1 :
                if(artists.count<5){
                    footerView.viewShowMore.isHidden = true
                    footerView.sizeToFit()
                }
            case 2 :
                if(albums.count<5){
                    footerView.viewShowMore.isHidden = true
                }
            case 3 :
                if(playlists.count<5){
                    footerView.viewShowMore.isHidden = true
                }
            case 4 :
                if(stations.count<5){
                    footerView.viewShowMore.isHidden = true
                }
                
            default:
                break
            }
            
            cell = footerView
            
            let tappy = showMoreTapGesture(target: self, action: #selector(showMoreTapped(_:)))
            
            tappy.section =  indexPath.section
          
            cell!.addGestureRecognizer(tappy)
            
        default:
            
            assert(false, "Unexpected element kind")
            break
        }
        
        return cell!
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var searchCell : UICollectionViewCell? = nil
    
        
        
         switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchCell", for: indexPath) as! SearchCell
            
            let url : String
            
            if ( tracks[indexPath.row].artwork.count > 0){
                
                url = tracks[indexPath.row].artwork[0].small
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTrackTitle.text = tracks[indexPath.row].title
            cell.lblArtist.text = tracks[indexPath.row].artistName
            cell.lblTrackIndex.text = String(indexPath.row + 1)
            
            //unhide for tracks
            
            cell.ivDownloaded.isHidden = true
            cell.lblTitle.isHidden = true
            cell.lblTrackTitle.isHidden = false
            cell.lblArtist.isHidden = false
            cell.btnOptions.isHidden =  false
            cell.lblTrackIndex.isHidden =  false
            
            
            
            //add rounded edge
            cell.layer.cornerRadius =  5
            
            let tappy = cellTapGesture(target: self, action: #selector(trackCellTapped(_:)))
            
            tappy.tracks =  tracks
            tappy.currrentPlayingIndex =  0
            
            cell.addGestureRecognizer(tappy)
            
            let optionsTap = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
            optionsTap.track = tracks[indexPath.row]
            
             cell.btnOptions.addGestureRecognizer(optionsTap)
             cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
            
            cell.addGestureRecognizer(tappy)
            
            // Add a separator below each cell
            let horizontalGap = 15.0 as CGFloat
            // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
            let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
            seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
            cell.addSubview(seperatorView)
            
            searchCell = cell
            
        case 1:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchCell", for: indexPath) as! SearchCell
            let url : String
            
            if ( artists[indexPath.row].artwork.count > 0){
                
                url = artists[indexPath.row].artwork[0].small
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = artists[indexPath.row].name
             cell.lblTrackIndex.text = String(indexPath.row + 1)
            cell.btnOptions.isHidden =  true
            
            //add rounded edge
            cell.layer.cornerRadius =  5
            
            let tappy = cellTapGesture(target: self, action: #selector(otherCellTapped(_:)))
            tappy.cellType =  "artist"
            tappy.id =  artists[indexPath.row].id
            
            cell.addGestureRecognizer(tappy)
            
            // Add a separator below each cell
            let horizontalGap = 15.0 as CGFloat
            // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
            let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
            seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
            cell.addSubview(seperatorView)
            
             searchCell = cell
            
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchCell", for: indexPath) as! SearchCell
            
            let url : String
            
            if ( albums[indexPath.row].artwork.count > 0){
                
                url = albums[indexPath.row].artwork[0].small
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTrackTitle.text = albums[indexPath.row].title
            cell.lblArtist.text = albums[indexPath.row].artist
            cell.lblTrackIndex.text = String(indexPath.row + 1)
            
            cell.lblTitle.isHidden = true
            cell.lblTrackTitle.isHidden = false
            cell.lblArtist.isHidden = false
             cell.btnOptions.isHidden =  true
            
            
            //add rounded edge
            cell.layer.cornerRadius =  5
            
            let tappy = cellTapGesture(target: self, action: #selector(otherCellTapped(_:)))
            tappy.cellType =  "album"
            tappy.id =  albums[indexPath.row].id
            
            cell.addGestureRecognizer(tappy)
            
            // Add a separator below each cell
            let horizontalGap = 15.0 as CGFloat
            // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
            let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
           seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
            cell.addSubview(seperatorView)
            
             searchCell = cell
            
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchCell", for: indexPath) as! SearchCell
            
            let url : String
            
            if ( playlists[indexPath.row].artwork.count > 0){
                
                url = playlists[indexPath.row].artwork[0].small
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = playlists[indexPath.row].name
            cell.lblTrackIndex.text = String(indexPath.row + 1)
            cell.btnOptions.isHidden =  true
            
            //add rounded edge
            cell.layer.cornerRadius =  5
            
            let tappy = cellTapGesture(target: self, action: #selector(otherCellTapped(_:)))
            tappy.cellType =  "playlist"
            tappy.id =  playlists[indexPath.row].id
            
            cell.addGestureRecognizer(tappy)
            
            // Add a separator below each cell
            let horizontalGap = 15.0 as CGFloat
            // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
            let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
           seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
            cell.addSubview(seperatorView)
            
             searchCell = cell
            
        case 4:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchCell", for: indexPath) as! SearchCell
            
            let url : String
            
            if ( stations[indexPath.row].artwork.count > 0){
                
                url = stations[indexPath.row].artwork[0].small
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = stations[indexPath.row].name
            cell.lblTrackIndex.text = String(indexPath.row + 1)
             cell.btnOptions.isHidden =  true
            
            //add rounded edge
            cell.layer.cornerRadius =  5
            
            let tappy = cellTapGesture(target: self, action: #selector(otherCellTapped(_:)))
            tappy.cellType =  "station"
            
            cell.addGestureRecognizer(tappy)
            // Add a separator below each cell
            let horizontalGap = 15.0 as CGFloat
            // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
            let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
            seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
            cell.addSubview(seperatorView)
            
             searchCell = cell
            
            
        default:
            print("do nothing")
            
        }
        
        searchCell?.backgroundColor = ThemeManager.currentTheme().secondaryColor
        return  searchCell!
        
    }
    
    
    
    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  20
        let collectionViewWidth = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewWidth, height: 75)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }
    
}
