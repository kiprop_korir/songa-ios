//
//  SearchCell.swift
//  Songa
//
//  Created by Collins Korir on 9/19/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit

class SearchCell: UICollectionViewCell {
    
    
    @IBOutlet weak var btnOptions: UIButton!
    @IBOutlet weak var ivDownloaded: UIImageView!
    
    @IBOutlet weak var lblTrackTitle: UILabel!
    @IBOutlet weak var lblArtist: UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var lblTrackIndex: UILabel!
    
    override func prepareForReuse() {
        // Do whatever you want here, but don't forget this :
        lblTrackIndex.text = ""
        super.prepareForReuse()
        // You don't need to do `self.viewSelection.alpha = 0` here
        // because `super.prepareForReuse()` will update the property `isSelected`
        
    }
}
