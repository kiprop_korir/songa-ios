//
//  SendFeedbackViewController.swift
//  Songa
//
//  Created by Collins Korir on 9/14/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
class SendFeedbackViewController: UIViewController,UIPickerViewDataSource, UIPickerViewDelegate  {

    
 var feedbackTypes:[String] = ["Feature improvement","Broken feature","Design/ease of use", "Software/hardware compatibility" , "Data loss" ,"Other"]
    
    var songaAccessMethods:[String] = ["With mobile data mostly","With WI-FI mostly","With data and WI-FI equally"]
    var featureTypes:[String] = ["Discover","My Music","Genres", "Radio" ,"Other"]
    
    let feedbackAPI = Analytics.songaFeedback
    let headers = APIurls.Headers

    @IBOutlet weak var lblEmail: UITextField!
    @IBOutlet weak var lblName: UITextField!
    @IBOutlet weak var pickerFeedbackType: UIPickerView!
    @IBOutlet weak var lblFeedbackComments: UITextField!
    @IBOutlet weak var pickerFeature: UIPickerView!
    @IBOutlet weak var lblFeatureComments: UITextField!
    @IBOutlet weak var pickerSongaAccess: UIPickerView!
    @IBOutlet weak var btnProceed: RoundEdgesButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerFeature.dataSource = self
        pickerFeature.delegate = self
        
        pickerSongaAccess.dataSource = self
        pickerSongaAccess.delegate = self
        
        pickerFeedbackType.dataSource = self
        pickerFeedbackType.delegate = self

      lblFeatureComments.addDoneCancelToolbar()
        lblFeedbackComments.addDoneCancelToolbar()
        
        self.title = "Send Feedback"
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
      return 1
      
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if ( pickerView == pickerFeedbackType) {
            return feedbackTypes.count
        }
        else if ( pickerView == pickerFeature) {
            return featureTypes.count
        }
            
        else {
            return songaAccessMethods.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if ( pickerView == pickerFeedbackType) {
            return feedbackTypes[row]
        }
        else if ( pickerView == pickerFeature) {
            return featureTypes[row]
        }
            
        else {
            return songaAccessMethods[row]
        }
    }

    @IBAction func goBack(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendFeedback(_ sender: Any) {
      //check if fields are empty
      
       
        let feedbackType = pickerFeedbackType.selectedRow(inComponent: 0) as Int
        guard let feedbackComments = lblFeedbackComments.text, !feedbackComments.isEmpty else {
            lblFeedbackComments.isError(baseColor: UIColor.red.cgColor, numberOfShakes: 5, revert: true)
            showErrorToast(message: "feedback comments")
            return
        }
        
       let featureType = pickerFeature.selectedRow(inComponent: 0) as Int
        guard let featureComments = lblFeatureComments.text, !featureComments.isEmpty else {
            lblFeatureComments.isError(baseColor: UIColor.red.cgColor, numberOfShakes: 5, revert: true)
            showErrorToast(message: "feature comments")
            return
        }
        let songaAccessType = pickerSongaAccess.selectedRow(inComponent: 0) as Int
     
        sendFeedback( feedbackType: feedbackTypes[feedbackType], feedbackComments: feedbackComments , featureType: featureTypes[featureType] , featureComments: featureComments , songaAccessType: songaAccessMethods[songaAccessType])

    }
    
    func showErrorToast(message: String){
       Toast.show(message: "Please enter \(message)", controller: self)
    }
    
    
    func sendFeedback( feedbackType: String, feedbackComments: String, featureType: String, featureComments: String ,songaAccessType: String ){
        
        let hud = JGProgressHUD(style: .light)
        hud.textLabel.text = "Loading..."
        hud.show(in: self.view)
        hud.dismiss(afterDelay: 5.0)

        let params : Parameters = ["feature":featureType,"featureComments":featureComments,"type":feedbackType ,"feedbackComments":feedbackComments,"accessMode":songaAccessType ,"device":"iOS"]

        
        Alamofire.request(APIurls.Search, method: .post, parameters: params, encoding: JSONEncoding.default ,  headers: headers ).responseJSON { response in
             hud.dismiss()
            if (response.result.isSuccess){
                Toast.show(message: "Feedback sent successfully", controller: self.parent!)
                self.willMove(toParent: nil)
                self.view.removeFromSuperview()
                self.removeFromParent()
    }
            else {
                 Toast.show(message: "There was an error submitting the feedback", controller: self)
            }
    }
  }
}
