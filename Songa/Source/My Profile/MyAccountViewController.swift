//
//  MyAccountViewController.swift
//  Songa
//
//  Created by Collins Korir on 6/17/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

class MyAccountViewController: UIViewController {
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblCurrentPlan: UILabel!
    @IBOutlet weak var btnUpgrade: RoundEdgesButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let subName = Defaults.getUserSubscription().planName
        lblCurrentPlan.text = subName
        
        lblStatus.text = Defaults.getUserSubscription().state
            if(Defaults.getUserSubscription().state == Defaults.CANCELLED){
                
                btnUpgrade.isHidden = true
                lblCurrentPlan.text = Defaults.getPaymentPlan()
                
            }
            else if (Defaults.getUserSubscription().state == Defaults.BLOCKED){
                
                lblCurrentPlan.text = Defaults.getUserSubscription().message
                btnUpgrade.isHidden = true
                
            }
    }
}
