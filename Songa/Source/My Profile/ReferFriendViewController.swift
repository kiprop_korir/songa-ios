//
//  ReferFriendViewController.swift
//  Songa
//
//  Created by Collins Korir on 9/25/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import RealmSwift

class ReferFriendViewController: UIViewController {

    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var ivQrCode: UIImageView!
    @IBOutlet weak var btnCode: RoundEdgesButton!
    var code = ""
    let realm = try! Realm()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnCode.backgroundColor = ThemeManager.currentTheme().primaryColor
        btnCode.isEnabled = false
        let vouchersResults: Results<Voucher>? = realm.objects(Voucher.self)
        
        if(vouchersResults?.count ?? 0 > 0){
            //displayVoucher(voucher: vouchersResults![0].toJSONString())
        }
        
      fetchVoucher()
    }
   
    func displayVoucher(voucher: String?){
        
       // ivQrCode.backgroundColor = UIColor.black
        
        
        let myString = voucher
        // Get data from the string
        let data = myString!.data(using: String.Encoding.ascii)
        // Get a QR CIFilter
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        // Input the data
        qrFilter.setValue(data, forKey: "inputMessage")
        // Get the output image
        guard let qrImage = qrFilter.outputImage else { return }
        // Scale the image
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        // Do some processing to get the UIImage
        let context = CIContext()
        guard let cgImage = context.createCGImage(scaledQrImage, from: scaledQrImage.extent) else { return }
        let processedImage = UIImage(cgImage: cgImage)
        ivQrCode.image = processedImage
    
    }
    
    func fetchVoucher(){
      
            
            //self.showLoadingView()
            APIClient.getCampaigns { result in
                
                self.dismissLoadingView()
                
                switch result.result {
                case .success(let value):
                    
                    
                    APIClient.getVoucher(campaignID: value[0].id) { result in
                      
                    switch result.result {
                    case .success(let voucher):
                        
                        if(voucher.status){
                            //delete all existing vouchers before storing new one
                            //there's aonly one voucher at a time
                            self.displayVoucher(voucher: voucher.toJSONString())
                            self.loadingIndicator.stopAnimating()
                            self.btnCode.setTitle(voucher.codeShort, for: UIControl.State.normal)
                            self.btnCode.isHidden = false
                            self.btnCode.titleLabel?.textAlignment = .center

                          
                            do {
                                let realm = try Realm()
                                try realm.write {
                                    let vouchersResults: Results<Voucher>? = self.realm.objects(Voucher.self)
                                    if(vouchersResults?.count ?? 0 > 0){
                                        self.realm.delete(vouchersResults!)
                                    }
                                    //save new voucher
                                    realm.add(voucher,update: .modified)
                                }
                                
                              
                                
                            } catch let error as NSError {
                                print(error)
                            }
                         
                        
                        }
                        
                      
                        
                        case .failure(let encodingError):
                         var statusCode:Int
                            if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                                // no internet connection
                                statusCode = 1
                            }
                            else{
                                statusCode = result.response?.statusCode ?? 404
                        }
                        self.showErrorView(errorCode: statusCode)
                    }
                    }
                case .failure(let encodingError):
                    var statusCode:Int
                    if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                        // no internet connection
                        statusCode = 1
                    }
                    else{
                        statusCode = result.response?.statusCode ?? 404
                    }
                    if(statusCode == 401 )
                    {
                        APIClient.getAuthToken(){ result in
                            
                            switch result.result {
                                
                            case .success:
                                
                                Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                                self.fetchVoucher()
                                
                            case .failure:
                                
                                print("do something when token refresh fails")
                            }
                        }
                    }
                    else{
                        self.showErrorView(errorCode: statusCode)
                    }
                }
        }
    }

    @IBAction func shareCode(_ sender: Any) {
        DispatchQueue.main.async {
            //Set the default sharing message.
            
            
            let message = "\(Defaults.getUser()?.name! ?? "") has sent you 1 free day of Songa. Download, play and share over 2.5 million fresh local hits and international music. Use code \(self.code) to redeem your free day or click the following link to install Songa https://play.google.com/store/apps/details?id=com.songa.music "
                
            
                let objectsToShare = [message] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
                self.present(activityVC, animated: true, completion: nil)
            }
    }
}
