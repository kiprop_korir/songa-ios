//
//  QRCodeScannerViewController.swift
//  Songa
//
//  Created by Collins Korir on 10/1/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

class QRCodeScannerViewController: UIViewController {
       
            @IBOutlet weak var scannerView: QRScannerView! {
                didSet {
                    scannerView.delegate = self
                }
            }
            @IBOutlet weak var scanButton: UIButton! {
                didSet {
                    scanButton.setTitle("STOP", for: .normal)
                }
            }
            
            var qrData: String? = nil {
                didSet {
                    if qrData != nil {
                        self.performSegue(withIdentifier: "detailSeuge", sender: self)
                    }
                }
            }
            
            override func viewDidLoad() {
                super.viewDidLoad()
            }
            
         
            override func viewWillAppear(_ animated: Bool) {
                super.viewWillAppear(animated)

                if !scannerView.isRunning {
                    scannerView.startScanning()
                }
            }
            
            override func viewWillDisappear(_ animated: Bool) {
                super.viewWillDisappear(animated)
                if !scannerView.isRunning {
                    scannerView.stopScanning()
                }
            }

            @IBAction func scanButtonAction(_ sender: UIButton) {
                scannerView.isRunning ? scannerView.stopScanning() : scannerView.startScanning()
                let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
                sender.setTitle(buttonTitle, for: .normal)
            }
        }


        extension QRCodeScannerViewController: QRScannerViewDelegate {
            func qrScanningDidStop() {
                let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
                scanButton.setTitle(buttonTitle, for: .normal)
            }
            
            func qrScanningDidFail() {
               Toast.show(message: "Scanning Failed. Please try again", controller: self)
            }
            
            func qrScanningSucceededWithCode(_ str: String?) {
                
                 let data = str!.data(using: .utf8)!
                do {
                    if let jsonObject = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
                    {
                     //TO-DO : get this i a better way
                        if let s = jsonObject["code_short"] as? String {
                        self.qrData = s
                        }
                        else {
                            Toast.show(message: "Invalid QR Code", controller: self)
                        }
                    } else {
                        print("bad json")
                    }
                } catch let error as NSError {
                    print(error)
                }
                
            }
            
            
            
        }


        extension QRCodeScannerViewController {
            override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//                if segue.identifier == "detailSeuge", let viewController = segue.destination as? DetailViewController {
//                    viewController.qrData = self.qrData
//                }
}
}
          
