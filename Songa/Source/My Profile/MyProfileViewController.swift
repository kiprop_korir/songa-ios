//
//  MyProfileViewController.swift
//  Songa
//
//  Created by Collins Korir on 1/19/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController {

    @IBOutlet weak var lblTitleAccountStatus: UILabel!
    @IBOutlet weak var lblTitleDOB: UILabel!
    @IBOutlet weak var lblTitlePhone: UILabel!
    @IBOutlet weak var lblTitleEmail: UILabel!
    @IBOutlet weak var lblTitleUsername: UILabel!
    
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblDOB: UILabel!
    @IBOutlet weak var lblAccountStatus: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblUsername.text = Defaults.getUser()?.name
        lblEmail.text =  Defaults.getUser()?.email
        lblPhone.text =  Defaults.getUser()?.phone
        lblDOB.text =  Defaults.getUser()?.dob
        lblAccountStatus.text = Defaults.getUserSubscription().state
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
