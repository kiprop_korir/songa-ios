//
//  RedeemVoucherViewController.swift
//  Songa
//
//  Created by Collins Korir on 9/27/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

class RedeemVoucherViewController: UIViewController {

    @IBOutlet weak var tfVoucherCode: SearchTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        tfVoucherCode.placeHolderColor = UIColor.darkGray
        tfVoucherCode.addDoneCancelToolbar()
       
    }
    @IBAction func redeemVoucher(_ sender: Any) {
        
        let codeShort = tfVoucherCode.text ?? ""
        if(codeShort.isEmpty){
            tfVoucherCode.isError(baseColor: UIColor.red as! CGColor, numberOfShakes: 3, revert: true)
        }
        else {
            redeem( codeShort: codeShort)
        }
    }
    func redeem(codeShort:String ){
        //self.showLoadingView()
    APIClient.getCampaigns { result in
    
      
            switch result.result {
            case .success(let value):
                
                APIClient.redeemVoucher(campaignID: value[0].id, codeShort: codeShort) { result in
    
                    switch result.result {
                    case .success(let voucherResponse):
                        
                        self.applyVoucher(campaignID: value[0].id, promoCode: voucherResponse.promoCode ?? "", gifterID: voucherResponse.gifterID ?? "", redemptionID: voucherResponse.redemptionID ?? "")
                        
                    case .failure(let encodingError):
                        var statusCode:Int
                        if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                            // no internet connection
                            statusCode = 1
                        }
                        else{
                            statusCode = result.response?.statusCode ?? 404
                        }
                        self.showErrorView(errorCode: statusCode)
                    }
                }
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            
                            self.redeem(codeShort: self.tfVoucherCode.text ?? "")
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
    }
    
    func applyVoucher( campaignID: String, promoCode: String, gifterID: String, redemptionID: String){
        
        APIClient.applyVoucher( campaignID: campaignID, promoCode: promoCode, gifterID: gifterID, redemptionID: redemptionID) { result in
            
            switch result.result {
            case .success(let value):
                
                //TO DO:- Check the defaults save sub section
               
                Defaults.saveUserSubscription(accountId: value.accountId ?? "", billingFrequency: value.billingFrequency ?? "", externalKey: value.externalKey ?? "", planId: value.planId ?? "", productId: value.productId ?? "", productPlanType: value.productPlanType ?? "", productPrice: "5", state: value.state ?? "", planName: value.planName ?? "", paymentStatus: value.paymentStatus ?? "", message: value.message ?? "", nextBillingDate: value.nextBillingDate ?? 0)
                
                self.showMessage(message: value.message ?? "")
                
            case .failure(let encodingError):
                
                self.showMessage(message: "Invalid Voucher ID")
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
    }
    
    func showMessage(message:String){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Redeem Voucher", message: message, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                alert.dismiss(animated: true, completion: nil)
              
            }))
            self.present(alert, animated: false, completion: nil);
        }
    }
    
    @IBAction func scanQRCode(_ sender: Any) {
    }
}
