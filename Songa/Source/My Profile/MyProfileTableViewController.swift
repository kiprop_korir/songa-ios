//
//  MyProfileTableViewController.swift
//  Songa
//
//  Created by Collins Korir on 1/19/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import AccountKit
import RealmSwift

class MyProfileTableViewController: UITableViewController,UIPickerViewDataSource, UIPickerViewDelegate {
    let realm = try! Realm()
    @IBOutlet var tvProfile: UITableView!
    var _accountKit: AccountKitManager!
    @IBOutlet weak var lblUserLoggedIn: UILabel!
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.languages[row]
    }
    

    let sections = ["Information","My Account","Vouchers","Look and Feel","App Information"]
    var imgMenu:UIImageView! =  nil
    
    let items = [["Notifications", "Leaderboard"],["Profile", "Subscriptions","Account"],["Refer a friend", "Redeem voucher"],[ "Send Feedback","Enable dark theme"],[ "Terms and Conditions", "Privacy Policy", "App Version",]]
    var languages:[String] = ["English","Kiswahili"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        lblUserLoggedIn.text = "You are logged in as \(Defaults.getUser()?.name ?? "")"
        lblUserLoggedIn.textColor = UIColor.colorAccent 
        //remove separator form tableview
        //tvProfile.separatorStyle = UITableViewCell.SeparatorStyle.none
        //tvProfile.allowsSelection = false
        
        
        // initialize Account Kit
        if _accountKit == nil {
            _accountKit = AccountKitManager(responseType: .accessToken)
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
               tableView.reloadData()
    }
    
    
    @IBAction func logOut(_ sender: Any) {
        DispatchQueue.main.async {
            let logoutAlert = UIAlertController(title: "Log Out", message: "Are you sure you want to log out?", preferredStyle: UIAlertController.Style.alert)
            
            logoutAlert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: { (action: UIAlertAction!) in
            }))
            logoutAlert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action: UIAlertAction!) in
                self.logOutUser()
            }))
           
            self.present(logoutAlert, animated: false, completion: nil);
            
        }
    }
    
    func logOutUser() {
        
        //log out from AccountKit
        _accountKit.logOut()
        Defaults.clearAllData()
        let storyboard = UIStoryboard(name: "SplashScreen", bundle: nil)
        self.view.window?.rootViewController = storyboard.instantiateInitialViewController()
        self.view.window?.makeKeyAndVisible()
    }
    
    
    @objc func switchValueDidChange(sender:UISwitch!) {
        (sender.isOn) ? ThemeManager.applyTheme(theme: .darkTheme)  : ThemeManager.applyTheme(theme: .lightTheme)
        
        //reload views

        let windows = UIApplication.shared.windows
        for window in windows {
            for view in window.subviews {
                view.removeFromSuperview()
                window.addSubview(view)
            }
        }
        

        tableView.reloadData()
        //post notification that theme has changed
        NotificationCenter.default.post(name: Notification.Name.themeChanged, object: nil)
        
    }
    


    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
          return self.sections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
      return items[section].count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
         return self.sections[section]
    }
    
    

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var returnedCell: UITableViewCell? = nil


         if(indexPath.section == 0 && indexPath.row == 0 ){
            
            ///put a notification count on  notifications
            
                let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
            cell.textLabel?.text = self.items[indexPath.section][indexPath.row]
                           imgMenu = UIImageView(frame: CGRect(x: tableView.frame.size.width - 40, y: 12, width: 20, height: 20));
                           imgMenu.image = #imageLiteral(resourceName: "ic_detail").withRenderingMode(.alwaysTemplate)
                          imgMenu.tintColor = UIColor.lightGray
                           cell.addSubview(imgMenu);
                           cell.contentView.backgroundColor = ThemeManager.currentTheme().primaryColor
                             
                let count = RoundEdgesButton(frame: CGRect(x: tableView.frame.size.width - 75, y: 12, width: 20, height: 20))
                   count.backgroundColor = UIColor.red
                   count.cornerRadius = 10
                   
                   let notificationsCount = realm.objects(SongaNotification.self).filter("isRead == false").count
                   
                   if(notificationsCount>0){
                       count.setTitle(String(notificationsCount), for: .normal)
                       count.titleLabel?.font = .systemFont(ofSize: 12)
                       cell.addSubview(count)
                }
                   
                
                returnedCell = cell
                   
            
        }
        
        else if(indexPath.section == 3){
            //profile settings
        
            if(indexPath.row == 1 ){
                let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
                cell.textLabel?.text = self.items[indexPath.section][indexPath.row]
                let switchTheme = UISwitch(frame: CGRect(x: tableView.frame.size.width - 60, y: 5, width: 24, height: 24));
                
                let theme = Defaults.getSelectedTheme()
                
                (theme == 1) ? switchTheme.setOn(false, animated: false) : switchTheme.setOn(true, animated: false)
                
                switchTheme.onTintColor = UIColor.colorAccent
                
                // check subviews and remove switch to avoid multiplelayering of swicth after tableview reload
                for subview in cell.subviews{
                    if let themeSwitch = subview as? UISwitch {
                        themeSwitch.removeFromSuperview();
                    }
                }
                
                cell.addSubview(switchTheme)
                
                
                switchTheme.addTarget(self, action: #selector(self.switchValueDidChange), for: .valueChanged)
                
                cell.contentView.backgroundColor = ThemeManager.currentTheme().primaryColor
                
                returnedCell = cell
            }
            else {
            

            
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
            cell.textLabel?.text = self.items[indexPath.section][indexPath.row]
            imgMenu = UIImageView(frame: CGRect(x: tableView.frame.size.width - 40, y: 12, width: 20, height: 20));
            imgMenu.image = #imageLiteral(resourceName: "ic_detail").withRenderingMode(.alwaysTemplate)
            imgMenu.tintColor = UIColor.lightGray
            cell.addSubview(imgMenu);
            cell.contentView.backgroundColor = ThemeManager.currentTheme().primaryColor
            returnedCell = cell
        }
        }
        else {
        
            switch indexPath.row {
                
           
            default:
          
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath)
                cell.textLabel?.text = self.items[indexPath.section][indexPath.row]
                imgMenu = UIImageView(frame: CGRect(x: tableView.frame.size.width - 40, y: 12, width: 20, height: 20));
                imgMenu.image = #imageLiteral(resourceName: "ic_detail").withRenderingMode(.alwaysTemplate)
               imgMenu.tintColor = UIColor.lightGray
                cell.addSubview(imgMenu);
                cell.contentView.backgroundColor = ThemeManager.currentTheme().primaryColor
                returnedCell = cell
            }
        }
        
        return returnedCell!
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
 

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

    
//    override func viewDidAppear(_ animated: Bool) {
//        tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height: tableView.contentSize.height)
//    }
//
    
   
    
    override func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
       
        let cell = tableView.cellForRow(at: indexPath)
        if cell != nil {
            cell?.contentView.backgroundColor = ThemeManager.currentTheme().secondaryColor
        }
    }
    
    override func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        if cell != nil {
            cell?.contentView.backgroundColor = ThemeManager.currentTheme().primaryColor
        }

    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
            
        case 0:
            switch indexPath.row {
            case 0:
                //go to profile
                self.performSegue(withIdentifier: "segueNotifications", sender: nil)
            case 1:
                self.performSegue(withIdentifier: "segueLeaderboard", sender: nil)
                
            default:
                break
            }
        case 1:
            
            switch indexPath.row {
            case 0:
                //go to profile
                self.performSegue(withIdentifier: "segueProfile", sender: nil)
            case 1:
                self.performSegue(withIdentifier: "segueSubscriptions", sender: nil)
            case 2:
                self.performSegue(withIdentifier: "segueAccounts", sender: nil)
                
            default:
                break
            }
            
          case 2:
            
            switch indexPath.row {
            case 0:
                //go to profile
                self.performSegue(withIdentifier: "segueReferFriend", sender: nil)
            case 1:
                self.performSegue(withIdentifier: "segueRedeemVoucher", sender: nil)
              default:
                break
            }
            
        case 3:
            
            if indexPath.row == 0{
          
                self.performSegue(withIdentifier: "segueSendFeedback", sender: nil)
            
            }
            
        case 4:
            
            
            switch indexPath.row {
            
                
            case 0:
                self.performSegue(withIdentifier: "segueTermsAndConditions", sender: nil)
                
            case 1:
                self.performSegue(withIdentifier: "seguePrivacyPolicy", sender: nil)
                
            case 2:
                
                DispatchQueue.main.async {
                    
                    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String

                    
                    let logoutAlert = UIAlertController(title: "App Version", message: "You are using Songa version \(appVersion ?? "1.0.0")", preferredStyle: UIAlertController.Style.alert)
            
                logoutAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                }))
                self.present(logoutAlert, animated: false, completion: nil);
                    
                }
                
          
                
            default:
                break
            }
        
        default:
            break
        }
   
            
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:28))
        let label = UILabel(frame: CGRect(x:16, y:5, width:tableView.frame.size.width, height:28))
        
        
           label.text = self.sections[section];
      
        label.textColor = UIColor.colorAccent
        label.font = UIFont.boldSystemFont(ofSize: 20)
        view.addSubview(label);
        view.backgroundColor = ThemeManager.currentTheme().secondaryColor
        
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
}
