//
//  ProfileViewController.swift
//  Songa
//
//  Created by Collins Korir on 2/22/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import RealmSwift

class ProfileViewController: UIViewController {

    @IBOutlet weak var viewNotifications: UIView!
    @IBOutlet weak var btnSearch: UIButton!
        let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //add tap gestires to top bar
        setUpNavBar()
    }
 
    
    @objc func setUpNavBar(){
      
        
        let searchButton = UIBarButtonItem(image: UIImage(imageLiteralResourceName: "ic_search").withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: #selector(self.searchClicked))
        
        self.navigationItem.rightBarButtonItem = searchButton
        self.navigationItem.rightBarButtonItem?.tintColor = ThemeManager.currentTheme().barTint
        
    }
    
    @objc func notificationsViewClicked(){
        performSegue(withIdentifier: "segueNotifications", sender: self)
    }
    
    @objc func searchClicked(){
        performSegue(withIdentifier: "segueSearch", sender: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setUpNavBar),
                                               name: Notification.Name.notificationReceived,
                                               object: nil)
    }
    @IBAction func back(_ sender: Any) {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }

}

