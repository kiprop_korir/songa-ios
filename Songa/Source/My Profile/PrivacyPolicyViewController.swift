//
//  PrivacyPolicyViewController.swift
//  Songa
//
//  Created by Collins Korir on 9/14/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import WebKit

class PrivacyPolicyViewController: UIViewController ,WKNavigationDelegate{

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var loadingIndicatior: UIActivityIndicatorView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        webView.navigationDelegate = self
        loadingIndicatior.startAnimating()
    loadingIndicatior.color = . colorAccent
        // Do any additional setup after loading the view.
        let url = URL (string: SongaInfo.privacyPolicy)
        let requestObj = URLRequest(url: url!)
        webView.load(requestObj)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goBack(_ sender: Any) {
  self.navigationController?.popViewController(animated: true)
        
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadingIndicatior.stopAnimating()
        loadingIndicatior.isHidden = true
    }
}

