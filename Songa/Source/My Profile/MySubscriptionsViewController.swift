//
//  MySubscriptionsViewController.swift
//  Songa
//
//  Created by Collins Korir on 1/19/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

class MySubscriptionsViewController: UIViewController {

    @IBOutlet weak var lblPlan: UILabel!
    @IBOutlet weak var lblExpiryTitle: SongaAccentTextLabel!
    @IBOutlet weak var lblExpiry: UILabel!
    @IBOutlet weak var lblPlanTitle: SongaAccentTextLabel!
    @IBOutlet weak var btnUpgrade: RoundEdgesButton!
    @IBOutlet weak var btnSubscribe: RoundEdgesButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Subscriptions"
        getSubscription()
        
    }
    

    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func displayDetails(){
        
        
        if(Defaults.getUserSubscription().state == Defaults.CANCELLED){
       
            btnUpgrade.isHidden = true
            btnSubscribe.isHidden = false
         lblPlanTitle.isHidden = true
            lblExpiryTitle.isHidden = true
            lblPlan.text = Defaults.getPaymentPlan()

        }
        else if (Defaults.getUserSubscription().state == Defaults.BLOCKED){
            
            lblPlan.text = Defaults.getUserSubscription().message
            lblExpiryTitle.isHidden = true
            lblExpiry.isHidden = true
            btnUpgrade.isHidden = true
            
         
            if(Defaults.isAuthorizationRejected()){
                btnSubscribe.isHidden = false
                     btnSubscribe.setTitle("Listen to radio", for: .normal)
            }
            else {
                btnSubscribe.isHidden = false
                btnSubscribe.setTitle("Pay Now", for: .normal) 
            }
            }
        
        else {
           let subName = Defaults.getUserSubscription().planName
            let price = Defaults.getUserSubscription().productPrice
            lblPlan.text = "You are currently subscribed to \(subName ?? "") at KES \(price ?? "")."
            lblExpiryTitle.isHidden = false
            lblExpiry.isHidden = false
            lblExpiry.text = Defaults.getSubscriptionExpiryDate()
            btnSubscribe.isHidden = true
            initUpgradeButton()
        }
        
        
    }

    func  getSubscription() {
    
        self.showLoadingView()
        APIClient.getUserSubscription(externalKey: (Defaults.getUser()?.id)!){ result in
            
            self.dismissLoadingView()
        
            switch result.result {
            case .success(let value):
                
                Defaults.saveUserSubscription(accountId: value.accountId ?? "", billingFrequency: value.billingFrequency ?? "", externalKey: value.externalKey ?? "", planId: value.planId ?? "", productId: value.productId ?? "", productPlanType: value.productPlanType ?? "", productPrice:  "5", state: value.state ?? "", planName: value.planName ?? "", paymentStatus: value.paymentStatus ?? "", message: value.message ?? "", nextBillingDate: value.nextBillingDate ?? 0)
                
              self.displayDetails()
            case .failure:
                
                let statusCode = result.response?.statusCode ?? 404
                
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.getSubscription()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                        self.showErrorView(errorCode: statusCode)
                }
            }
        }
    }
    
    func triggerPayment(){
        self.showLoadingView()
        APIClient.payNow(externalKey: (Defaults.getUser()?.id)!){ result in
        
            self.dismissLoadingView()
            switch result.result {
            case .success:
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Success", message: "Your request is being processed", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                }))
                self.present(alert, animated: false, completion: nil);
                
            }
                
                 case .failure:
                
                    let alert = UIAlertController(title: "Failed", message: "An error occurred while processing your request", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                    }))
                    self.present(alert, animated: false, completion: nil);
                
            }
        }
    }
    
    func initUpgradeButton(){
        if(Defaults.getUserSubscription().productPlanType == Defaults.STANDARD){
            btnUpgrade.isHidden = false
        }
        else {
            btnUpgrade.isHidden = true
        }
    }
    
    @IBAction func subscribe(_ sender: Any) {
   
        
        if(Defaults.getUserSubscription().state == Defaults.CANCELLED){
            //billing act
            // TODO: Show this VC cleanly
            let storyboard = UIStoryboard(name: "Sign Up", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "choosePlan")
            self.show(vc, sender: self)
        }
        else if(Defaults.isAuthorizationRejected()){
            //go home to radio
            let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
            let vc = storyboard.instantiateInitialViewController() as! MainViewController
            vc.selectedTab = 3
            self.view.window?.rootViewController = vc
        }
        else if (!Defaults.isPaymentPending()){
         triggerPayment()
        }
    }
}
