//
//  CancelSubscriptionViewController.swift
//  Songa
//
//  Created by Collins Korir on 6/17/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

class CancelSubscriptionViewController: UIViewController, UICollectionViewDelegate , UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {
    
    @IBOutlet weak var tfComments: UITextView!
    var cancellationReason = ""
    var cancellationComment = ""
    
    @IBOutlet weak var collectionView: UICollectionView!
    let reasons = ["The library of songs is small","I do not like most of the songs","The service is too expensive","I am just taking a break","I experienced technical issues","Other"]
    let images = [UIImage.init(named: "ic_reason_library"),UIImage.init(named: "ic_reason_not_like_songs"),UIImage.init(named: "ic_reason_expensive"),UIImage.init(named: "ic_reason_break"),UIImage.init(named: "ic_reason_technical"),UIImage.init(named: "ic_reason_other")]
    
    override func viewDidLoad() {
        collectionView.delegate = self
        collectionView.dataSource = self
        
        tfComments.addDoneCancelToolbar()
     NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
}

        @objc func keyboardWillShow(sender: NSNotification) {
            self.view.frame.origin.y -= 200
        }
        @objc func keyboardWillHide(sender: NSNotification) {
            self.view.frame.origin.y += 200
        }
    
    
    
    @IBAction func keepSonga(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelSubscription(_ sender: Any) {
        //check if reason and comments are provided
        
        cancellationComment = tfComments.text
        if(cancellationReason == "")
        {
            Toast.show(message: "Please select a reason for cancelling your subscription", controller: self)
        }
        else if(cancellationComment == "") {
            Toast.show(message: "Please provide more details on your reason for cancellation", controller: self)
        }
        else {
            
            //TODO:- Report cancellation reason to Firebase
            self.cancelUserSubscription()
        }
          
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var selectedIndex = Int()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
         //remove all selected selcetions
        for n in 0...5 {
            let cell = self.collectionView?.cellForItem(at: IndexPath(row: n, section: 0))  as! CancellationCollectionViewCell
            
            cell.backgroundColor = ThemeManager.currentTheme().primaryColor
        }
        
        //indicate the selected cell
        
        let cell = self.collectionView?.cellForItem(at: IndexPath(row: sender.selectedIndex , section: 0))  as! CancellationCollectionViewCell
        
        cell.backgroundColor = UIColor.colorAccent
        self.cancellationReason = reasons[sender.selectedIndex]
    }
    
    
    private func cancelUserSubscription(){
             showLoadingView()
           APIClient.cancelUserSubscription(){ result in

            self.dismissLoadingView()
               switch result.result {
               case .success( let code):
                   
               let subResponse = result.result.value
                
                if let value = subResponse {
                    //save subscription
                                   Defaults.saveUserSubscription(accountId: value.accountId ?? "", billingFrequency: value.billingFrequency ?? "", externalKey: value.externalKey ?? "", planId: value.planId ?? "", productId: value.productId ?? "", productPlanType: value.productPlanType ?? "", productPrice: "", state: value.state ?? "", planName: value.planName ?? "", paymentStatus: value.paymentStatus ?? "", message: value.message ?? "", nextBillingDate: value.nextBillingDate ?? 0)
                                 
                   DispatchQueue.main.async {
                       let logoutAlert = UIAlertController(title: "Success", message: "Your subscription has been cancelled successfully.", preferredStyle: UIAlertController.Style.alert)
                       
                       logoutAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                        self.dismiss(animated: true, completion: nil)
                       }))
                    
                    logoutAlert.show(self, sender: self)
                       
                   }
                }
                else {
                    DispatchQueue.main.async {
                        let logoutAlert = UIAlertController(title: "Failed", message: "We are unable to cancel your subscription at this time, please try again later.", preferredStyle: UIAlertController.Style.alert)
                        
                        logoutAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                         self.dismiss(animated: true, completion: nil)
                        }))
                     
                     logoutAlert.show(self, sender: self)
                        
                    }
                }
                   
               case .failure(let encodingError):
                   
                   if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                       // no internet connection
                       Toast.show(message: "We are unable to get an internet connection, please try again", controller: self)
                   }
                   else{
                       Toast.show(message: "Oops, something went wrong", controller: self)
                   }
                   
               }
           }
       
       }
      
    
    
    //MARL: - Delegate datasource methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reasons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //handle playlists cellForItemAt
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cancellationCollectionViewCell", for: indexPath) as! CancellationCollectionViewCell
        
        cell.lblReason.text = reasons[indexPath.row]
        cell.ivReason.image = images[indexPath.row]
        cell.ivReason.tintColor = ThemeManager.currentTheme().textColor
        
        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        
        tappy.selectedIndex = indexPath.row
        cell.addGestureRecognizer(tappy)
        cell.layer.cornerRadius = 10
        cell.layer.masksToBounds = true
        
        return cell
        
        
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout Delegate Method
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size : CGSize? = nil
        
        if collectionView == self.collectionView {
            
            let padding: CGFloat =  20
            let collectionViewWidth = collectionView.frame.size.width - padding
            
            size = CGSize(width: collectionViewWidth/2, height: 150)
            
        }
        return size!
        
    }
    
  
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }
}

