//
//  LeaderboardTableViewController.swift
//  Songa
//
//  Created by Collins Korir on 9/25/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import Alamofire

class LeaderboardTableViewController: UITableViewController {

    var stats: [NSArray]? = []
    var userPosition = 0
    var userPoints = 0
    override func viewDidLoad() {
        super.viewDidLoad()
            fetchLeaderboard()
        self.tableView.separatorStyle = .none
        self.tableView.allowsSelection = false
        
        self.title = "Leaderboard"
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return stats?.count ?? 0
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "statCell", for: indexPath) as! StatsCell
        //get name initials only
        let name =  stats?[indexPath.row].object(at: 0) as? String
        let initial = name?.prefix(1)
        var secondName = String()
        if(name?.components(separatedBy: " ").count ?? 0 > 1){
            secondName = (name?.components(separatedBy: " ")[1])!
            cell.lblName.text = "\(initial ?? ""). \(secondName )"
        }
        else {
            cell.lblName.text = name
        }
        cell.lblPoints.text = "\(stats?[indexPath.row].object(at: 1) as! Int) pts"
        cell.lblIndex.text = String(indexPath.row + 1)
        // Add a separator below each cell
        let horizontalGap = 15.0 as CGFloat
        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
        cell.addSubview(seperatorView)
        
        if(name == Defaults.getUser()?.name){
            cell.backgroundView?.backgroundColor = UIColor.colorAccent
        }
        
        
        return cell
    }
    
    //add footer for table positiion
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        
        let lblPosition = UILabel(frame: CGRect(x: 8, y: 8, width: 100, height: 20))
        let lblName = UILabel(frame: CGRect(x: 108, y: 8, width: tableView.frame.size.width - 300, height: 20))
        
        let lblPoints = UILabel(frame: CGRect(x: 108, y: tableView.frame.size.width - 300, width: tableView.frame.size.width - 200, height: 20))
        
        
        lblPosition.text = String(self.userPosition)
        lblPoints.text = String(self.userPoints)
        
        let name = Defaults.getUser()?.name
        let initial = name?.prefix(1)
        var secondName = String()
        if(name?.components(separatedBy: " ").count ?? 0 > 1){
            secondName = (name?.components(separatedBy: " ")[1])!
            lblName.text = "\(initial ?? ""). \(secondName )"
        }
        else {
            lblName.text = name
        }
       
        footerView.addSubview(lblPoints)
        footerView.addSubview(lblPosition)
        footerView.addSubview(lblName)
        
        footerView.backgroundColor = .fadedRed
        
        return footerView
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 70
    }

    func fetchUserPosition(){
        APIClient.getUserLeaderboardPosition(){
            response in
            
            print("huku")
            print(response.response)
            print(response.request)
            print(response.response)
            switch response.result {
                       case .success(let value):
                        
                        self.tableView.reloadData()
                        
                        self.userPosition = value[0].position ?? 0
                        self.userPoints = value[0].plays ?? 0
            
                    
                case .failure(let encodingError):
                    var statusCode:Int
                    if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                        // no internet connection
                        statusCode = 1
                    }
                    else{
                        statusCode = response.response?.statusCode ?? 404
                    }
                    if(statusCode == 401 )
                    {
                        APIClient.getAuthToken(){ result in
                            
                            switch result.result {
                                
                            case .success:
                                
                                Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                                self.fetchLeaderboard()
                                
                            case .failure:
                                
                                print("do something when token refresh fails")
                            }
                        }
                    }
                    else{
                        self.showErrorView(errorCode: statusCode)
                    }
                }
            }
    }
    
    func fetchLeaderboard(){
        self.showLoadingView()
        
        let headers = [
            
            "CF-Access-Client-Id" : "f2eb71a71a2d489121273492c7271ce7.access.songamusic.com",
            "CF-Access-Client-Secret" : "75c93016e4b304986e9c29316aa2317d54ee81023efa29f3e1ed2998ef12ef7e"
        ]
        
        Alamofire.request( "https://metabase.songamusic.com/api/public/card/c82a1337-c0e9-4541-bbfb-0154097e7350/query", method: .get, headers: headers)
            .responseJSON()  { response in
            
            
            self.dismissLoadingView()
            switch response.result {
            case .success(let value):
                
                
                    
                     let json = value as? [String: Any]
                        //print("JSON: \(json)") // serialized json response
                     let data = json!["data"] as? [String: Any]
                        
                    self.stats = data!["rows"] as? [NSArray]
                     
                     //self.fetchUserPosition()
           
               
               
                
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = response.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.fetchLeaderboard()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
        
    }
}

class StatsCell:UITableViewCell {
    
    @IBOutlet weak var lblPoints: UILabel!
    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var lblName: UILabel!

}
