//
//  ArtistOverviewTableViewController.swift
//  Songa
//
//  Created by Collins Korir on 2/28/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import SDWebImage
import XLPagerTabStrip
import Foundation
import  Alamofire
import AlamofireObjectMapper
import SDWebImage
import RealmSwift
import  MarqueeLabel

class ArtistOverviewViewController: UIViewController , IndicatorInfoProvider {
    
    @IBOutlet weak var tvTracks: UITableView!
    @IBOutlet weak var cvAlbums: UICollectionView!
     let realm = try! Realm()
    var tracks : [Track] = []
    var albums: [Album] = []
    
    var playlistID : String = ""
     var bio: String = ""
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "OVERVIEW")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get instance of PlaylistViewController
        
        tvTracks.delegate =  self
        tvTracks.dataSource =  self
        cvAlbums.delegate =  self
        cvAlbums.dataSource =  self
     
        //remove separator form tableview
        tvTracks.separatorStyle = UITableViewCell.SeparatorStyle.none
        tvTracks.allowsSelection = false
        
        }

    
    func isTrackDownloaded (trackID:String) -> Bool {
        let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(trackID)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
        else {
            return false
        }
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var tracks = Array<Track>()
        var currrentPlayingIndex = Int()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
        
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
        
    }
    class optionsTapGesture: UITapGestureRecognizer {
        var track = Track()
    }
    
    @objc func optionsTapped(_ sender: optionsTapGesture) {
       let track = sender.track
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
                
                let headerView = UIView()
                alertController.view.addSubview(headerView)
                headerView.translatesAutoresizingMaskIntoConstraints = false
                headerView.topAnchor.constraint(equalTo: alertController.view.topAnchor, constant: 8).isActive = true
                headerView.rightAnchor.constraint(equalTo: alertController.view.rightAnchor, constant: -10).isActive = true
                headerView.leftAnchor.constraint(equalTo: alertController.view.leftAnchor, constant: 10).isActive = true
                headerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
                
                let rect = CGRect(x: 8, y: 8, width: 80, height: 80)
                let trackCover = UIImageView(frame: rect)
                
                let url : String
                if ( track.artwork.count > 0){
                    url = track.artwork[0].small
                }
                else
                {    url = "no_cover"
                }
                trackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
                
                
                let lblTrackTitle = MarqueeLabel(frame: CGRect(x: 100, y: 22, width: alertController.view.bounds.size.width - 150 , height: 22))
                lblTrackTitle.font = UIFont.boldSystemFont(ofSize: 20)
                lblTrackTitle.speed = MarqueeLabel.SpeedLimit.duration(10)
                lblTrackTitle.animationDelay = 2
                lblTrackTitle.fadeLength = 10
                
                
                let lblArtist = MarqueeLabel(frame: CGRect(x: 100 , y: 53, width: alertController.view.bounds.size.width - 150 , height: 22))
                lblArtist.font = UIFont.systemFont(ofSize: 20)
                lblArtist.textColor = UIColor.lightGray
                lblArtist.speed = MarqueeLabel.SpeedLimit.duration(10)
                lblArtist.animationDelay = 2
                lblArtist.fadeLength = 10
                
                lblTrackTitle.text = track.title
                lblArtist.text = track.artistName
                headerView.addSubview(trackCover)
                headerView.addSubview(lblTrackTitle)
                headerView.addSubview(lblArtist)
                
                
                alertController.view.translatesAutoresizingMaskIntoConstraints = false
                alertController.view.heightAnchor.constraint(equalToConstant: 350).isActive = true
                
                let downloadAction = UIAlertAction(title: "Download", style: .default) { (action) in
                    self.startDownload(track: track)
                }
                let shareAction = UIAlertAction(title: "Share", style: .default) { (action) in
                    self.share(artist:track.artistName , link: "https//open.songamusic.com/track/\(track.id)")
                }
                
                let addToPlaylistAction = UIAlertAction(title: "Add to Playlist", style: .default) { (action) in
                    print("selection")
                }
               
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                alertController.addAction(downloadAction)
                alertController.addAction(shareAction)
                alertController.addAction(addToPlaylistAction)
    
                
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadCollectionViewData),
                                               name: Notification.Name.ArtistHasLoaded,
                                               object: nil)
    }
    @objc func reloadCollectionViewData(_ notification: Notification)  {
        
        tracks = notification.userInfo?["tracks"] as! Array <Track>
        albums = notification.userInfo?["albums"] as! Array <Album>
         bio = notification.userInfo?["bio"] as! String
    
        tvTracks.reloadData()
        cvAlbums.reloadData()
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
       
        
        if let vc = segue.destination as? AlbumPageViewController {
        //get the current selected cell
        if let cell = sender as? UICollectionViewCell,
            let indexPath = self.cvAlbums.indexPath(for: cell) {
            vc.albumID = self.albums[indexPath.row].id
          }
        }
        else {
            let vc = segue.destination as! ArtistPageViewController
                    vc.artistID = sender as! String
            }
        }
    }
    
    extension ArtistOverviewViewController: UITableViewDelegate , UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return tracks.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as! TrackTableCell
            let url : String
            
            if ( tracks[indexPath.row].artwork.count > 0){
                
                url =  tracks[indexPath.row].artwork[0].small
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = tracks[indexPath.row].title
            cell.lblArtist.text = tracks[indexPath.row].artistName
            cell.lblTrackIndex.text = String(indexPath.row + 1)
            
            // Add a separator below each cell
            let horizontalGap = 15.0 as CGFloat
            // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
            let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
            seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
            cell.addSubview(seperatorView)
            
            
            
            let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
            
            let tapOptions = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
            tapOptions.track = tracks[indexPath.row]
            
            cell.btnOptions.addGestureRecognizer(tapOptions)
             cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
            
            //add rounded edge
            cell.layer.cornerRadius =  5
            
            if(!isTrackDownloaded(trackID: tracks[indexPath.row].id)){
                cell.ivDownloaded.isHidden = true
            }
            
            
            
            tappy.tracks = tracks
            tappy.currrentPlayingIndex = indexPath.row
            cell.addGestureRecognizer(tappy)
            
            return cell
        }
        
        
    }
    
    extension ArtistOverviewViewController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return  albums.count
            
        }
        
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "albumCell", for: indexPath) as! AlbumCell
            
            let url : String
            
            if ( albums[indexPath.row].artwork.count > 0){
                
                url =  albums[indexPath.row].artwork[0].medium
                
            }
            else
            {
                //might be a blank array
                url = "no_cover"
                
            }
            
            //set the genre image using sd web image library asycnhronously
            
            cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            cell.lblTitle.text = albums[indexPath.row].title
        
            //add rounded edge
            cell.layer.cornerRadius =  5
            
            return cell
            
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         
               return   CGSize(width: 160, height: 194)
            
        }
        
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            
            return 5
        }
}
