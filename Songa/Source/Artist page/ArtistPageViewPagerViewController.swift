//
//  ArtistPageViewPagerViewController.swift
//  Songa
//
//  Created by Collins Korir on 9/7/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class ArtistPageViewPagerViewController: ButtonBarPagerTabStripViewController {
    let colorAccent = UIColor(named: "textColorHeader" )
    
    var playlistID:String = ""
    
    override func viewDidLoad() {
        
        
        
        // change selected bar color
        //settings.style.buttonBarBackgroundColor = UIColor(named: "colorBackground" )
        settings.style.buttonBarItemBackgroundColor = UIColor(named: "colorPrimary" )
        settings.style.selectedBarBackgroundColor = colorAccent!
        settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 14)
        settings.style.buttonBarBackgroundColor = UIColor.colorPrimary
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = colorAccent
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .white
            newCell?.label.textColor = self?.colorAccent
        }
        super.viewDidLoad()
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let child_1 = UIStoryboard(name: "ArtistPage", bundle: nil).instantiateViewController(withIdentifier: "overview")
      
        return [child_1]
    }
    
}
    



