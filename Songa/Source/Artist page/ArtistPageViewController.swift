//
//  ViewController.swift
//  Songa
//
//  Created by Collins Korir on 8/10/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import Foundation
import  Alamofire
import AlamofireObjectMapper
import SDWebImage
import RealmSwift
import SDWebImage
import MarqueeLabel

class ArtistPageViewController: BaseViewController {
    @IBOutlet weak var lblArtistTitle: UILabel!
    @IBOutlet weak var btnPlayAll: RoundEdgesButton!
    @IBOutlet weak var btnMix: RoundEdgesButton!
    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var ivCoverBg: UIImageView!
    @IBOutlet weak var lblBio: UILabel!
    @IBOutlet weak var viewPage: UIView!
    @IBOutlet weak var tvTracks: UITableView!
    @IBOutlet weak var cvAlbums: UICollectionView!
    @IBOutlet weak var lblAlbums: UILabel!
    @IBOutlet weak var lblFollowers: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var artistID : String = ""
    var isPodcast : Bool = false
         var bio: String = ""
    var tracks : [Track] = []
       var albums : [Album] = []
     let realm = try! Realm()
    
     
     var artist: ArtistResponse?  = nil
    
    var infos: ArtistPageViewPagerViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Artist"
        loadArtist()
        tvTracks.delegate =  self
        tvTracks.dataSource =  self
        cvAlbums.delegate =  self
        cvAlbums.dataSource =  self
        
        //remove separator form tableview
        tvTracks.separatorStyle = UITableViewCell.SeparatorStyle.none
        tvTracks.allowsSelection = false
        
    }
    
    
    func isTrackDownloaded (trackID:String) -> Bool {
        let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(trackID)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
        else {
            return false
        }
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var tracks = Array<Track>()
        var currrentPlayingIndex = Int()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
//        if(isPodcast){
//     NotificationCenter.default.post(name: Notification.Name.startPodcastPlay,
//                                          object: nil,
//                                          userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
//
//        }
//        else {
            NotificationCenter.default.post(name: Notification.Name.startPlay,
                                                 object: nil,
                                                 userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
        //}
    }
    class optionsTapGesture: UITapGestureRecognizer {
        var track = Track()
    }
    
    @objc func optionsTapped(_ sender: optionsTapGesture) {
        let track = sender.track
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
            
            let headerView = UIView()
            alertController.view.addSubview(headerView)
            headerView.translatesAutoresizingMaskIntoConstraints = false
            headerView.topAnchor.constraint(equalTo: alertController.view.topAnchor, constant: 8).isActive = true
            headerView.rightAnchor.constraint(equalTo: alertController.view.rightAnchor, constant: -10).isActive = true
            headerView.leftAnchor.constraint(equalTo: alertController.view.leftAnchor, constant: 10).isActive = true
            headerView.heightAnchor.constraint(equalToConstant: 100).isActive = true
            
            let rect = CGRect(x: 8, y: 8, width: 80, height: 80)
            let trackCover = UIImageView(frame: rect)
            
            let url : String
            if ( track.artwork.count > 0){
                url = track.artwork[0].small
            }
            else
            {    url = "no_cover"
            }
            trackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
            
            
            let lblTrackTitle = MarqueeLabel(frame: CGRect(x: 100, y: 22, width: alertController.view.bounds.size.width - 150 , height: 22))
            lblTrackTitle.font = UIFont.boldSystemFont(ofSize: 20)
            lblTrackTitle.speed = MarqueeLabel.SpeedLimit.duration(10)
            lblTrackTitle.animationDelay = 2
            lblTrackTitle.fadeLength = 10
            
            
            let lblArtist = MarqueeLabel(frame: CGRect(x: 100 , y: 53, width: alertController.view.bounds.size.width - 150 , height: 22))
            lblArtist.font = UIFont.systemFont(ofSize: 20)
            lblArtist.textColor = UIColor.lightGray
            lblArtist.speed = MarqueeLabel.SpeedLimit.duration(10)
            lblArtist.animationDelay = 2
            lblArtist.fadeLength = 10
            
            lblTrackTitle.text = track.title
            lblArtist.text = track.artistName
            headerView.addSubview(trackCover)
            headerView.addSubview(lblTrackTitle)
            headerView.addSubview(lblArtist)
            
            
            alertController.view.translatesAutoresizingMaskIntoConstraints = false
            alertController.view.heightAnchor.constraint(equalToConstant: 350).isActive = true
            
            let downloadAction = UIAlertAction(title: "Download", style: .default) { (action) in
                self.startDownload(track: track)
            }
            let shareAction = UIAlertAction(title: "Share", style: .default) { (action) in
                self.share(artist:track.artistName , link: "https//open.songamusic.com/track/\(track.id)")
            }
            
            let addToPlaylistAction = UIAlertAction(title: "Add to Playlist", style: .default) { (action) in
                self.performSegue(withIdentifier: "segueAddToPlaylist", sender: self)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(downloadAction)
            alertController.addAction(shareAction)
            alertController.addAction(addToPlaylistAction)
            
            
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        FirebaseUtil.shared.getArtistFollowCount(artistID: artistID)
               
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadCollectionViewData),
                                               name: Notification.Name.ArtistHasLoaded,
                                               object: nil)
            NotificationCenter.default.addObserver(self,
                                                    selector: #selector(showFollowCount),
                                                    name: Notification.Name.artistFollowCount
                ,
                                                    object: nil)
        NotificationCenter.default.addObserver(self,
                                                    selector: #selector(showFollowCount),
                                                    name: Notification.Name.artistFollowCountModified
                ,
                                                    object: nil)
        }
        
        @objc func showFollowCount(_ notification: Notification){
           let followers = notification.userInfo?["count"] as! Int?
                 var desc = ""
                 if followers! > 0 {
                     desc = "Followers"
                 }
                 else {
                     desc =
                     "Follower"
                 }
            lblFollowers.text =  "\(String(describing: followers!) ) \(desc)"
         
        }
    @objc func reloadCollectionViewData(_ notification: Notification)  {
        
        tracks = notification.userInfo?["tracks"] as! Array <Track>
        albums = notification.userInfo?["albums"] as! Array <Album>
        bio = notification.userInfo?["bio"] as! String
        
        tvTracks.reloadData()
        cvAlbums.reloadData()
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        
        if let vc = segue.destination as? AlbumPageViewController {
            //get the current selected cell
            if let cell = sender as? UICollectionViewCell,
                let indexPath = self.cvAlbums.indexPath(for: cell) {
                vc.albumID = self.albums[indexPath.row].id
            }
        }
        else if let vc = segue.destination as? ArtistDescriptionViewController {
                vc.playlistDescription = artist?.bio ?? ""
        }
           
                else if let destinationNavigationController = segue.destination as? UINavigationController {
                           let targetController = destinationNavigationController.topViewController as! AddToPlaylistViewController
                    targetController.track = sender as? Track
                       }

    
    }
    
    @objc func retry(){
        self.dismissErrorView()
        loadArtist()
    }
    
    func isArtistFavourited() -> Bool {
        let queryResults: Results<FavouriteArtist>? = realm.objects(FavouriteArtist.self).filter("id == \"\(artistID)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
        else {
            return false
        }
    }
    
    func loadArtist()  {
        
        self.showLoadingView()

        
        APIClient.getArtist(artistId: artistID){ result in
            self.dismissLoadingView()
            
          
            switch result.result {
            case .success(let value):
                
                self.artist = value
                
                if(self.isArtistFavourited()){
                    self.btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite_filled"), for: .normal)
                }
                
                if let tracks =  value.tracks {
                    self.tracks = tracks
                }
                if let albums =  value.albums {
                    self.albums = albums
                }
                self.bio = ""
                if let des =  value.bio {
                    self.bio =  des
                    self.lblBio.text = des
                   
                }
                else {
                    self.lblBio.text = ""
                    self.btnMore.isHidden = true
                    
                }
                
                if self.albums.count == 0 {
                   self.lblAlbums.isHidden = true
                }
                    
               
                
                if let coverImageURL =  value.artwork?[safe: 0]?.original {
                    
                    self.ivCover.sd_setImage(with: URL(string: coverImageURL), placeholderImage: #imageLiteral(resourceName: "empty_album"))
                    self.ivCoverBg.sd_setImage(with: URL(string: coverImageURL), placeholderImage: #imageLiteral(resourceName: "empty_album"))
                    
                }
                
                let name = value.name!
                
                
                self.lblArtistTitle.text =  name
                self.lblArtistTitle.textColor = .red
                self.lblBio.textColor = .white
                self.btnShare.imageView?.tintColor = .white
                
                self.ivCoverBg.blurImage()
                
            self.cvAlbums.reloadData()
                self.tvTracks.reloadData()
                
                
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.loadArtist()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
    }
    
    
    
    ///IB Actions
    

    @IBAction func share(_ sender: Any) {
        self.share(artist:artist?.name ?? "Artist name" , link: "https//open.songamusic.com/playlist/\(artist?.id  ?? "")")
    }
    
    @IBAction func playAll(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": self.artist?.tracks as Any ,"currentPlayingIndex": 0])
    }
    @IBAction func mix(_ sender: Any) {
        //to do shuffle the indexes
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": self.artist?.tracks as Any ,"currentPlayingIndex": 3])
    }
   
    @IBAction func favourite(_ sender: Any) {
        
        let favouritedArtist: FavouriteArtist = FavouriteArtist()
                    favouritedArtist.id = artist!.id!
                    
                    
                    let artistObject = Artist()
                   
                        
                    for artwork in artist!.artwork! {
                       artistObject.artwork.append(artwork)
                    }
                    
                    for track in artist!.tracks! {
                        artistObject.tracks.append(track)
                    }
                    artistObject.name = (artist?.name)!
                    artistObject.id = artist!.id!

                   
                    favouritedArtist.artist = artistObject
        
        if(!isArtistFavourited()){
             
            do {
                 let realm = try Realm()
                 try realm.write {
                     realm.add(favouritedArtist,update: .modified)
                     btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite_filled"), for: .normal)
                 }
             
             } catch let error as NSError {
                 print(error)
             }
            
            //subsribe user to topic on firebase
                  FirebaseUtil.shared.subscribeToTopic(topic: artist!.name!)
                  FirebaseUtil.shared.followArtist(artist: artistObject, toUnfollow: false)
            
             }else {
                 //unfavourite artist
                           
                           let queryResults: Results<FavouriteArtist>? = realm.objects(FavouriteArtist.self).filter("id == \"\(artistID)\"")
                         
                           do {
                               let realm = try Realm()
                               try realm.write {
                                   realm.delete((queryResults?[0])!)
                                   btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite"), for: .normal)
                               }
                               
                               
                           } catch let error as NSError {
                               
                               print(error)
                               
                           }
            //subsribe user to topic on firebase
                         FirebaseUtil.shared.unsubscribeToTopic(topic: artist!.name!)
                         FirebaseUtil.shared.followArtist(artist: artistObject, toUnfollow: true)
                           }
    }
}


extension ArtistPageViewController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  albums.count
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "albumCell", for: indexPath) as! AlbumCell
        
        let url : String
        
        if ( albums[indexPath.row].artwork.count > 0){
            
            url =  albums[indexPath.row].artwork[0].medium
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        //set the genre image using sd web image library asycnhronously
        
        cell.ivCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = albums[indexPath.row].title
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return   CGSize(width: 160, height: 194)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 5
    }
}
extension ArtistPageViewController: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as! TrackTableCell
        let url : String
        
        if ( tracks[indexPath.row].artwork.count > 0){
            
            url =  tracks[indexPath.row].artwork[0].small
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = tracks[indexPath.row].title
        cell.lblArtist.text = tracks[indexPath.row].artistName
        cell.lblTrackIndex.text = String(indexPath.row + 1)
        
        cell.lblTrackIndex.textColor = ThemeManager.currentTheme().textColorSecondary
            cell.lblArtist.textColor = ThemeManager.currentTheme().textColorSecondary
          cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        
        // Add a separator below each cell
        let horizontalGap = 15.0 as CGFloat
        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
        cell.addSubview(seperatorView)
        
        
        
        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        
        let tapOptions = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
        tapOptions.track = tracks[indexPath.row]
        
        cell.btnOptions.addGestureRecognizer(tapOptions)
      
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
        if(isTrackDownloaded(trackID: tracks[indexPath.row].id)){
            cell.ivDownloaded.isHidden = false
        }
        
        
        
        tappy.tracks = tracks
        tappy.currrentPlayingIndex = indexPath.row
        cell.addGestureRecognizer(tappy)
        
        return cell
    }
    
    
}


