//
//  AlbumPageViewController.swift
//  Songa
//
//  Created by Collins Korir on 7/26/18.
//  Copyright © 2018 Collins Korir. All rights reserved.
//

import UIKit
import Alamofire
import UIKit
import XLPagerTabStrip
import RealmSwift

class AlbumPageViewController: BaseViewController {

    @IBOutlet weak var viewDownload: UIView!
    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var ivCover: UIImageView!
    @IBOutlet weak var ivCoverBg: UIImageView!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblAlbumTitle: UILabel!
    @IBOutlet weak var lblTrackCount: UILabel!
    @IBOutlet weak var tvTracks: UITableView!
    @IBOutlet weak var lblDownload: UILabel!
    @IBOutlet weak var ivDownload: UIImageView!
    @IBOutlet weak var btnShare: UIButton!
    
    let headers = APIurls.Headers
    var album: AlbumResponse?  = nil
    
    var albumID : String = ""
    var tracks : [Track] = []
    let albumTracksAPI = APIurls.album
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title =  "Album"
        tvTracks.delegate = self
        tvTracks.dataSource =  self
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.downloadAll))
        viewDownload.addGestureRecognizer(tap)
        
        //remove separator form tableview
        tvTracks.separatorStyle = UITableViewCell.SeparatorStyle.none
        tvTracks.allowsSelection = false
        
        loadAlbum()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
      {
              
              if let vc = segue.destination as? ArtistPageViewController
              {
                  vc.artistID = sender as! String
              }
              else if let destinationNavigationController = segue.destination as? UINavigationController {
                         let targetController = destinationNavigationController.topViewController as! AddToPlaylistViewController
                  targetController.track = sender as? Track
                     }
          
      }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //add retry observer
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(retry),
                                               name: Notification.Name.retryClicked,
                                               object: nil)
    }
    @objc func retry(){
        self.dismissErrorView()
        loadAlbum()
    }
    
    func isTrackDownloaded (trackID:String) -> Bool {
        let queryResults: Results<DownloadedTrack>? = realm.objects(DownloadedTrack.self).filter("id == \"\(trackID)\"")
        
        if(queryResults?.count ?? 0 >= 1){
            return true
        }
        else {
            return false
        }
    }
    
    class cellTapGesture: UITapGestureRecognizer {
        var tracks = Array<Track>()
        var currrentPlayingIndex = Int()
    }
    
    @objc func cellTapped(_ sender: cellTapGesture) {
        
        
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": sender.tracks ,"currentPlayingIndex": sender.currrentPlayingIndex])
        
    }
    class optionsTapGesture: UITapGestureRecognizer {
        var track = Track()
    }
    @objc func optionsTapped(_ sender: optionsTapGesture) {
        showOptionsController(track: sender.track)
    }

    
    
    
    func loadAlbum()  {
  
        self.showLoadingView()
        
        APIClient.getAlbum(albumId: albumID){ result in
            
         
            self.dismissLoadingView()
            switch result.result {
            case .success(let value):
                
                self.album = value
                self.tracks =  value.tracks!
                self.navigationItem.title =  value.title
                
                
                if let coverImageURL =  value.artwork?[safe: 0]?.original {
                    
                    self.ivCover.sd_setImage(with: URL(string: coverImageURL), placeholderImage: #imageLiteral(resourceName: "empty_album"))
                    self.ivCoverBg.sd_setImage(with: URL(string: coverImageURL), placeholderImage: #imageLiteral(resourceName: "empty_album"))
                    
                }
                
                
                
                if let trackCount = value.trackCount {
                    
                    if(trackCount == 1){
                        self.lblTrackCount.text = "\(trackCount) Track"
                    }
                    else {
                        self.lblTrackCount.text = "\(trackCount) Tracks"
                        
                    }
                }
                
                if let albumTitle = value.title {
                    self.lblAlbumTitle.text =  albumTitle
                }
                
                if let albumArtist = value.artistName {
                    self.lblArtistName.text =  albumArtist
                }
                
                self.lblAlbumTitle.textColor =  .red
                self.lblArtistName.textColor = .white
                self.btnShare.imageView?.tintColor = .white
                self.ivDownload.tintColor = .white
                self.lblDownload.textColor = .white
                self.btnFavourite.imageView?.tintColor = ThemeManager.currentTheme().textColor
                self.lblTrackCount.textColor = .white
                self.ivCoverBg.blurImage()
                
                self.tvTracks.reloadData()
                
                
            case .failure(let encodingError):
                var statusCode:Int
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    statusCode = 1
                }
                else{
                    statusCode = result.response?.statusCode ?? 404
                }
                if(statusCode == 401 )
                {
                    APIClient.getAuthToken(){ result in
                        
                        switch result.result {
                            
                        case .success:
                            
                            Defaults.putString(key: Defaults.accessTokenKey, value: (result.value?.accessToken)!)
                            self.loadAlbum()
                            
                        case .failure:
                            
                            print("do something when token refresh fails")
                        }
                    }
                }
                else{
                    self.showErrorView(errorCode: statusCode)
                }
            }
        }
    }
    
    @objc func downloadAll(){
        
        
    }
 
    
    ///IB Actions
    
    
    @IBAction func playAll(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": self.album!.tracks as Any ,"currentPlayingIndex": 0])
    }
    @IBAction func mix(_ sender: Any) {
        //to do shuffle the indexes
        let randomIndex = Int.random(in: 0 ..< tracks.count - 1)
        NotificationCenter.default.post(name: Notification.Name.startPlay,
                                        object: nil,
                                        userInfo:["tracks": self.album!.tracks as Any ,"currentPlayingIndex": randomIndex])
        
    }
    @IBAction func favourite(_ sender: Any) {
        
        let favouritedAlbum: FavouriteAlbum = FavouriteAlbum()
        favouritedAlbum.id = album!.id!
        
        
        let albumObject = Album()
        
        
        for artwork in album!.artwork! {
            albumObject.artwork.append(artwork)
        }
        
        for track in album!.tracks! {
            albumObject.tracks.append(track)
        }
        albumObject.title = (album?.title)!
        albumObject.id = album!.id!
        
        
        favouritedAlbum.album = albumObject
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(favouritedAlbum,update: .modified)
                btnFavourite.setImage(#imageLiteral(resourceName: "ic_favourite_filled"), for: .normal)
            }
            
        } catch let error as NSError {
            print(error)
        }
    }
    
    @IBAction func share(_ sender: Any) {
        self.share(artist: (self.album?.title!)! , link: "https//open.songamusic.com/playlist/\(album!.id  ?? "")")
    }
    
    
}

extension AlbumPageViewController: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "trackCell", for: indexPath) as! TrackTableCell
        let url : String
        
        if ( tracks[indexPath.row].artwork.count > 0){
            
            url =  tracks[indexPath.row].artwork[0].medium
            
        }
        else
        {
            //might be a blank array
            url = "no_cover"
            
        }
        
        print(tracks[indexPath.row].artistName)
        print(tracks[indexPath.row].title)
        cell.ivTrackCover.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "no_cover"))
        cell.lblTitle.text = tracks[indexPath.row].title
        cell.lblArtist.text = tracks[indexPath.row].artistName
        cell.lblTrackIndex.text = String(indexPath.row + 1)
        cell.lblTrackIndex.textColor = ThemeManager.currentTheme().textColorSecondary
        cell.lblArtist.textColor = ThemeManager.currentTheme().textColorSecondary
        cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        
        
        // Add a separator below each cell
        let horizontalGap = 15.0 as CGFloat
        // As you want to have equal gaping in left & right side, you have to position the view's origin.x to the constant and have to have the width minus the double of that constant.
        let seperatorView = UIView(frame: CGRect(x: horizontalGap, y: cell.frame.size.height - 1, width: cell.frame.size.width - horizontalGap * 2.0, height: 1))
        seperatorView.backgroundColor = ThemeManager.currentTheme().primaryColor
        cell.addSubview(seperatorView)
        
        
        
        let tappy = cellTapGesture(target: self, action: #selector(cellTapped(_:)))
        
        let tapOptions = optionsTapGesture(target: self, action: #selector(optionsTapped(_:)))
        tapOptions.track = tracks[indexPath.row]
        
        cell.btnOptions.addGestureRecognizer(tapOptions)
         cell.btnOptions.imageView?.tintColor = ThemeManager.currentTheme().textColorSecondary
        
        //add rounded edge
        cell.layer.cornerRadius =  5
        
        if(isTrackDownloaded(trackID: tracks[indexPath.row].id)){
            cell.ivDownloaded.isHidden = false
        }
        
        
        
        tappy.tracks = tracks
        tappy.currrentPlayingIndex = indexPath.row
        cell.addGestureRecognizer(tappy)
        
        return cell
    }
    
    
}

