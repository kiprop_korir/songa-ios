//
//  BillingPlansViewController.swift
//  Songa
//
//  Created by Collins Korir on 1/28/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class BillingPlansViewController: UIViewController {

 
    @IBOutlet weak var btnSubscribe: RoundEdgesButton!
    @IBOutlet weak var cvPlans: UICollectionView!
    @IBOutlet weak var lblPlanName: UILabel!
    @IBOutlet weak var lblPlanPrice: UILabel!
    @IBOutlet weak var lblPlanDescription: UILabel!
    @IBOutlet weak var btnGold: RoundEdgesButton!
    @IBOutlet weak var btnPremium: RoundEdgesButton!
    var storedOffsets = [Int: CGFloat]()
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var dailyPlans:[Plan] = []
    var monthlyPlans:[Plan] = []
    var weeklyPlans:[Plan] = []
    var selectedPlan: Plan? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cvPlans.delegate = self
        cvPlans.dataSource = self
        btnSubscribe.isEnabled = false
      getBillingPlans()
        cvPlans.backgroundColor = .white
        
        
    }
    
    @IBAction func subscribe(_ sender: Any) {
              
        activityIndicator.isHidden = false
        
        self.createSubscription()
                  
    }
    
    private func getBillingPlans(){
     activityIndicator.isHidden = false
         
        APIClient.getPlans(){ result in
  
            
            switch result.result {
            case .success(let value):
                
                self.activityIndicator.isHidden = true
                for plan in value
                    
                {
                    if(plan.billingFrequency == "DAILY") {
                        self.dailyPlans.append(plan)
            
                    }
                    else if (plan.billingFrequency == "WEEKLY") {
                        self.weeklyPlans.append(plan)
                    }
                    else {
                        self.monthlyPlans.append(plan)
                    }
                    
                }
                
                self.dailyPlans.reverse()
                self.weeklyPlans.reverse()
                self.monthlyPlans.reverse()
                //set default selected plan
                
                if(self.dailyPlans.count > 0){
                    self.selectedPlan = self.dailyPlans[1]}
                self.btnSubscribe.isEnabled = true
                
                
                self.cvPlans.reloadData()
                
                //by default gold is selected
               // self.selectPlan(plan: self.premiumPlan!)
                
            case .failure(let encodingError):
                
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    Toast.show(message: "We are unable to get an internet connection, please try again", controller: self)
                }
                else{
                    Toast.show(message: "Oops, something went wrong", controller: self)
                }
                
            }
        }
        
        
    }
        
//        func selectPlan (plan:Plan){
//
//            if(plan.productPlanType == "STANDARD") {
//                //select gold
//                //by default gold is selected
//                self.btnPremium.backgroundColor = UIColor.fadedRed
//                self.btnGold.backgroundColor = UIColor.textColorPrimary
//               self.lblPlanDescription.text = "Local, African and selected International"
//                self.selectedPlan = goldPlan
//            }
//            else {
//                self.btnGold.backgroundColor = UIColor.fadedRed
//                self.btnPremium.backgroundColor = UIColor.textColorPrimary
//                 self.lblPlanDescription.text = "Local, African and All International"
//             self.selectedPlan = premiumPlan
//            }
//
//            self.lblPlanName.text = plan.planName
//            let price = String(plan.planPrice!)
//            self.lblPlanPrice.text = ("\(price) /-")
//        }
    
    
    private func createSubscription(){
              
        APIClient.createUserSubscription(planName: selectedPlan!.planName! , planID: selectedPlan!.id!){ result in
            
            self.activityIndicator.isHidden = true
            switch result.result {
            case .success(let value):
                
                //save subscription
                 Defaults.saveUserSubscription(accountId: value.accountId ?? "", billingFrequency: value.billingFrequency ?? "", externalKey: value.externalKey ?? "", planId: value.planId ?? "", productId: value.productId ?? "", productPlanType: value.productPlanType ?? "", productPrice: "", state: value.state ?? "", planName: value.planName ?? "", paymentStatus: value.paymentStatus ?? "", message: value.message ?? "", nextBillingDate: value.nextBillingDate ?? 0)
                
                //show success dialog
                DispatchQueue.main.async {
                    let logoutAlert = UIAlertController(title: "", message: "Your subscription is being processed.", preferredStyle: UIAlertController.Style.alert)
                    
                    logoutAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
                        logoutAlert.dismiss(animated: true, completion: nil)
                        self.performSegue(withIdentifier: "segueSubscriptionProcessing", sender: nil)
                    }))
                    self.present(logoutAlert, animated: false, completion: nil);
                }
                
            case .failure(let encodingError):
                
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    Toast.show(message: "We are unable to get an internet connection, please try again", controller: self)
                }
                else{
                    Toast.show(message: "Oops, something went wrong", controller: self)
                }
                
            }
        }
    
    }
   
//    @IBAction func goldSelected(_ sender: Any) {
//        selectPlan(plan: goldPlan!)
//    }
//    @IBAction func premiumSelected(_ sender: Any) {
//        selectPlan(plan: premiumPlan!)
//    }
//
//    @IBAction func `continue`(_ sender: Any) {
//       //get selected plan
//        let planName = selectedPlan?.planName
//        let billingMethod = APIurls.getKillbillPaymentPluginName
//
//        DispatchQueue.main.async {
//            let logoutAlert = UIAlertController(title: "Billing", message: "You will receive a pop up to begin your 10 days free trial. Select 1 to confirm. You will have a few seconds to confirm.", preferredStyle: UIAlertController.Style.alert)
//
//            logoutAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction!) in
//                logoutAlert.dismiss(animated: true, completion: nil)
//                  self.createSubscription(planName: planName!, billingMethod: billingMethod)
//            }))
//            self.present(logoutAlert, animated: false, completion: nil);
//        }
//    }
    
}
extension BillingPlansViewController: UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      
         var returnedCell: UICollectionViewCell? = nil
   
        switch indexPath.row {
        case 0:
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "planCell", for: indexPath) as! PlanCollectionViewCell
            cell.lblTitle.text = "Daily"
            cell.layer.cornerRadius = 8;
                  cell.clipsToBounds  =  true
            //cell.setTableViewDataSourceDelegate(self, forRow: indexPath.row)
            //cell.tableViewOffset = storedOffsets[indexPath.row] ?? 0
           returnedCell = cell
        
        case 1:
              let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "planCell", for: indexPath) as! PlanCollectionViewCell
        cell.lblTitle.text = "Weekly"
            cell.layer.cornerRadius = 8;
                  cell.clipsToBounds  =  true
              //cell.tvPlans.tag = 1
            //cell.setTableViewDataSourceDelegate(self, forRow: indexPath.row)
            //cell.tableViewOffset = storedOffsets[indexPath.row] ?? 0
           returnedCell = cell
        
    case 2:
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "planCell", for: indexPath) as! PlanCollectionViewCell
    cell.lblTitle.text = "Monthly"
            cell.layer.cornerRadius = 8;
                  cell.clipsToBounds  =  true
            //cell.setTableViewDataSourceDelegate(self, forRow: indexPath.row)
           // cell.tableViewOffset = storedOffsets[indexPath.row] ?? 0
            
            returnedCell = cell
       default:
      print("")
        }
         
             
        return returnedCell!
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        guard let cell = cell as? PlanCollectionViewCell else { return }
        //cell.tvPlans.tag = 1
                   cell.setTableViewDataSourceDelegate(self, forRow: indexPath.row)
                   cell.tableViewOffset = storedOffsets[indexPath.row] ?? 0
    }
//  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//           guard let cell = cell as? RadioPodcastCell else { return }
//
//           cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
//           cell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
//           //cell.lblPodcastTitle.text = curatedPodcasts[indexPath.row].title
//           //cell.lblPodcastDescription.text = curatedPodcasts[indexPath.row].podcastDescription
//           cell.lblPodcastDescription.textColor = ThemeManager.currentTheme().textColorSecondary
//
//       }
     func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
             
             guard let collectionViewCell = cell as? PlanCollectionViewCell else { return }
             
             storedOffsets[indexPath.row] = collectionViewCell.tableViewOffset
         }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
            
            let collectionViewWidth = collectionView.bounds.width
            let collectionViewHeight = collectionView.bounds.height
            
           //return CGSize(width: collectionViewWidth - 70, height: collectionViewHeight)
        return CGSize(width: collectionViewWidth, height: collectionViewHeight)
            
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
   
            return 10

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
            return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
       
    }
}



extension BillingPlansViewController: UITableViewDelegate , UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        switch tableView.tag {
            case 0:
            return dailyPlans.count
            case 1:
            return weeklyPlans.count
            case 2:
            return monthlyPlans.count
              default:
            return 0
               }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        switch indexPath.row{
                   case 0:
                   return 40
                   case 1:
                   return 40
                   case 2:
                   return 90
                     default:
                   return 40
                      }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        var returnedCell: UITableViewCell? = nil
        //todo:- find a  better way to compare instead of converting to string
        switch tableView.tag {
        case 0:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "planCell", for: indexPath) as! PlanTableViewCell
            cell.lblTitle.text = dailyPlans[indexPath.row].planName
            cell.ivChecked.tintColor = .colorSecondary
             
            if(dailyPlans[indexPath.row].planName == self.selectedPlan!.planName){
                cell.ivChecked.tintColor = .colorAccent
            }
            

            returnedCell = cell
       
        case 1:
            
          
            let cell = tableView.dequeueReusableCell(withIdentifier: "planCell", for: indexPath) as! PlanTableViewCell
            cell.lblTitle.text = weeklyPlans[indexPath.row].planName
            cell.ivChecked.tintColor = .colorSecondary
            
            if(weeklyPlans[indexPath.row].planName == self.selectedPlan!.planName){
                       cell.ivChecked.tintColor = .colorAccent
                   }
        returnedCell = cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "planCell", for: indexPath) as! PlanTableViewCell
            cell.lblTitle.text = monthlyPlans[indexPath.row].planName
            cell.ivChecked.tintColor = .colorSecondary
            if(monthlyPlans[indexPath.row].planName == self.selectedPlan!.planName){
                       cell.ivChecked.tintColor = .colorAccent
                   }
           
        returnedCell = cell
          default:
         print("")
           }
        returnedCell!.selectionStyle = .none
        
 
        return returnedCell!
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // cvPlans.reloadData()
         switch tableView.tag {
               case 0:
                
               
                   self.selectedPlan = dailyPlans[indexPath.row]
               case 1:
                    self.selectedPlan = weeklyPlans[indexPath.row]
               case 2:
                  self.selectedPlan = monthlyPlans[indexPath.row]
            
                 default:
                print("")
                  }
        
        cvPlans.reloadData()
        
               
    }
    
   
}

