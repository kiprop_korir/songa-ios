//
//  PlanTableViewCell.swift
//  Songa
//
//  Created by Collins Korir on 10/2/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//


import UIKit

class PlanTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var ivChecked: UIImageView!
  
}
