//
//  SubscriptionResultViewController.swift
//  Songa
//
//  Created by Collins Korir on 1/30/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import Alamofire

class SubscriptionResultViewController: UIViewController {

    @IBOutlet weak var btnRetry: RoundEdgesButton!
    @IBOutlet weak var lblEnjoy: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var ivStatus: UIImageView!
    @IBOutlet weak var btnSkip: RoundEdgesButton!
    var message:String?
    var success:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        processResult()
    }

        @IBAction func skip(_ sender: Any) {
            Defaults.putBoolean(key: Defaults.hasSelectedPlansKey, value: true)
            goHome()
        }
        
    @IBAction func retry(_ sender: Any) {
//        if (EntitlementUtil.state == EntitlementUtil.BLOCKED)
//        refreshAccount()
//        else
//        EventBus.getDefault().post(FragmentTransitionEvent(1)) //go back to plans screen
//    }
        if(Defaults.getUserSubscription().state == Defaults.BLOCKED){
            refreshAccount(userID: Defaults.getUser()!.id!)
        }
        else {
            //go back to plans screen
            let storyboard = UIStoryboard(name: "SignUp", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "choosePlan") as UIViewController
            self.view.window?.rootViewController = initialViewController
            self.view.window?.makeKeyAndVisible()
        }
    }
    
    @objc func instantiateHome(sender : Timer){
            goHome()
        }
        
        func goHome()  {
            
            //set app theme
                  
                  let theme = Defaults.getSelectedTheme()
                 
                  (theme == 1) ? ThemeManager.applyTheme(theme: .lightTheme) : ThemeManager.applyTheme(theme: .darkTheme)
                  
            
            let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
            self.view.window?.rootViewController = storyboard.instantiateInitialViewController()
            self.view.window?.makeKeyAndVisible()
        }
    
    func refreshAccount(userID:String){
        
        Toast.show(message: "Refreshing account", controller: self)
        let headers = APIurls.Headers
        
        Alamofire.request(APIurls.refreshAccount + userID, method: .post ,encoding: JSONEncoding.default ,headers: headers ).responseObject() { (response: DataResponse<SubscriptionResponse>) in
            let loginResponse = response.result.value
            
            if let value = loginResponse {
                
                //hide loading dialog
                
                //save subscription
                Defaults.saveUserSubscription(accountId: value.accountId ?? "", billingFrequency: value.billingFrequency ?? "", externalKey: value.externalKey ?? "", planId: value.planId ?? "", productId: value.productId ?? "", productPlanType: value.productPlanType ?? "", productPrice: "", state: value.state ?? "", planName: value.planName ?? "", paymentStatus: value.paymentStatus ?? "", message: value.message ?? "", nextBillingDate: value.nextBillingDate ?? 0)
              
                self.message = value.message ?? value.reason
                self.success = true
            }
            else {
                
                //user still doesn't have a billing plan
                self.message = "An error occurred while processing your request"
                self.success = false
            }
     
            self.processResult()
        }
    }
    
    func processResult(){
        lblMessage.text = message
        if(success!){
            ivStatus.image = #imageLiteral(resourceName: "ic_success")
            btnRetry.isHidden = true
            btnSkip.isHidden = true
            Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.instantiateHome(sender:)), userInfo: nil, repeats: false)

        }
        else {
            ivStatus.image = #imageLiteral(resourceName: "ic_failed")
            ivStatus.tintColor = UIColor.textColorPrimary
            lblEnjoy.isHidden = true
        }
    }
    
}
