//
//  BillingInfoViewController.swift
//  Songa
//
//  Created by Collins Korir on 1/24/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

class BillingInfoViewController: UIViewController {

    @IBOutlet weak var lblDescription: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    func getRegistrationDate() -> Date{
        
        let dateCreated =  Defaults.getUser()?.createdAt
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.locale = Locale(identifier: "en_US_POSIX")
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        
        return dateFormatterGet.date(from: dateCreated!)!
    }

    
    @IBAction func seePlans(_ sender: Any) {
     self.performSegue(withIdentifier: "segueBillingPlans", sender: nil)
    }
}
