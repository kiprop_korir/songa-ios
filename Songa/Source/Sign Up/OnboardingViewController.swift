//
//  OnboardingViewController.swift
//  Songa
//
//  Created by Kiprop Korir on 07/01/2019.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import Alamofire
import AccountKit
import AVKit


class OnboardingViewController: UIViewController,AKFViewControllerDelegate{

    @IBOutlet weak var cvOnboarding: UICollectionView!
    @IBOutlet weak var btnJoin: RoundEdgesButton!
    @IBOutlet weak var lblJoin: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnLogIn: RoundEdgesButton!
    @IBOutlet weak var videoContainer: UIView!
    @IBOutlet weak var stackButtons: UIStackView!
    
    @IBOutlet weak var ivSongaLogo: UIImageView!
    
    var player: AVPlayer?
    var layer: AVPlayerLayer?
    
    var _accountKit: AccountKitManager!
    var loggedInUser : RAGUser? = nil

    var signInToken : String = ""
    var authToken : String = ""
    let CLIENT_ID : String = Constants.FB_CLIENT_ID
    let CLIENT_SECRET : String = Constants.FB_CLIENT_SECRET
    let GRANT_TYPE : String = Constants.GRANT_TYPE
    
    var images: [UIImage] = [#imageLiteral(resourceName: "onboarding_one"),#imageLiteral(resourceName: "onboarding_two"),#imageLiteral(resourceName: "onboarding_three"),#imageLiteral(resourceName: "onboarding_four")]
    
    var descriptions: [String] = ["OVER TWO MILLION TRACKS","UNLIMITED DOWNLOADS","DATA FREE STREAMING ON DOWNLOADS","FROM KES 5 DAILY"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        lblDescription.font =  UIFont(name:"Bebas Neue",size:22)
        cvOnboarding.dataSource = self
        cvOnboarding.delegate = self
        cvOnboarding.backgroundColor = .clear
        
        self.pageControl.numberOfPages = images.count
        
         
        btnLogIn.addTarget(self, action: #selector(logIn), for: .touchUpInside)
        
        // initialize Account Kit
        if _accountKit == nil {
            _accountKit = AccountKitManager(responseType: .accessToken)
        }
        
        playIntroVideo()
 
 }
    
    func prepareLoginViewController(loginViewController: AKFViewController) {
        loginViewController.delegate = self
        //UI Theming - Optional
        loginViewController.uiManager = SkinManager(skinType: .classic, primaryColor: UIColor.colorAccent  , backgroundImage: UIImage(named: "account_kit_bg") , backgroundTint: BackgroundTint.white, tintIntensity: 0.1)
    }

    func playIntroVideo(){

        self.videoContainer.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height )
              self.videoContainer.layoutIfNeeded()
              self.view.layoutSubviews()
                     self.viewDidAppear(true)
        // get the path string for the video from assets
        let videoString:String? = Bundle.main.path(forResource: "onboarding_clip", ofType: "mp4")
        guard let unwrappedVideoPath = videoString else {
            return}
        
        // convert the path string to a url
        let videoUrl = URL(fileURLWithPath: unwrappedVideoPath)
        
        // initialize the video player with the url
        self.player = AVPlayer(url: videoUrl)
        
        // create a video layer for the player
         layer = AVPlayerLayer(player: player)
        
        // make the layer the same size as the container view
        //layer.frame. = videoContainer.frame.height
        
        layer!.frame = CGRect(x: 0, y: 0, width: videoContainer.frame.width, height: videoContainer.frame.height)
        // make the video fill the layer as much as possible while keeping its aspect size
        layer!.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        // add the layer to the container view
        videoContainer.layer.addSublayer(layer!)
        
        player?.play()
        
       self.player?.addObserver(self, forKeyPath: "rate", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //let guide = view.area
        layer!.frame = self.view.frame
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "rate" {
            if let playRate = self.player?.rate {
                if playRate == 0.0 {
              //play stopped
                    lblJoin.isHidden = false
                    lblDescription.isHidden = false
                    pageControl.isHidden = false
                    cvOnboarding.isHidden = false
                    stackButtons.isHidden = false
                    ivSongaLogo.isHidden = true
        
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
         videoContainer.updateConstraints()
    }
            
            
    @objc func logIn(){
        //get token

        APIClient.getAuthToken(){ result in
            
        
            switch result.result {
            case .success(let value):
                
                self.authToken = value.accessToken!
                
                Defaults.putString(key: Defaults.accessTokenKey, value: value.accessToken!)
                
                // launch account kit
                if self._accountKit?.currentAccessToken != nil {
                    // if the user is already logged in, go to the main screen
                    // ...
                    
                    //select user billing plan
                    let hasLoggedIn = Defaults.getBoolean(key: Defaults.hasLoggedInKey)
                    let hasSelectedPlans = Defaults.getBoolean(key: Defaults.hasSelectedPlansKey)
                    
                    
                    
                    if(hasLoggedIn ?? false && hasSelectedPlans ?? false){
                        //go to main home VC
                        self.instantiateHome()
                    }
                    else {
                        
                        //check users subscription status
                        
                        //take user to select plans
                        
                        self.checkSubscription(userID: (Defaults.getUser()?.id)!)
                        
                    }
                    
                }
                else {
                    // Show the login screen
                    
                    self.loginWithPhone()
                }

            case .failure(let encodingError):
                
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                  Toast.show(message: "We are unable to get an internet connection, please try again", controller: self)
                }
                else{
                  Toast.show(message: "Oops, something went wrong", controller: self)
                }
               
            }
        }
    }


 
    
    func loginWithPhone(){
        let inputState = UUID().uuidString
        let vc = (_accountKit?.viewControllerForPhoneLogin(with: nil, state: inputState))!
        vc.isSendToFacebookEnabled = true
        self.prepareLoginViewController(loginViewController: vc)
        self.present(vc as UIViewController, animated: true, completion: nil)
    }
    
    func instantiateBillingPlansSelection(){
        self.performSegue(withIdentifier: "segueBillingInfo", sender: nil)
    }
    
    @objc func checkSubscription (userID:String){

        APIClient.getUserSubscription(externalKey: userID){ result in
            
            switch result.result {
            case .success(let value):
                   //save subscription
                  Defaults.saveUserSubscription(accountId: value.accountId ?? "", billingFrequency: value.billingFrequency ?? "", externalKey: value.externalKey ?? "", planId: value.planId ?? "", productId: value.productId ?? "", productPlanType: value.productPlanType ?? "", productPrice: "", state: value.state ?? "", planName: value.planName ?? "", paymentStatus: value.paymentStatus ?? "", message: value.message ?? "", nextBillingDate: value.nextBillingDate ?? 0)
                if(Defaults.isEligible()){
                    Defaults.putBoolean(key: Defaults.hasSelectedPlansKey, value: true)
                    
                    //go to home screen
                    //self.instantiateHome()
                    self.instantiateBillingPlansSelection()
                }
                else {
                    //user doesn't have a suitable billing plan
                    self.instantiateBillingPlansSelection()
                }
                
            case .failure(let encodingError):
                
                if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                    // no internet connection
                    Toast.show(message: "We are unable to get an internet connection, please try again", controller: self)
                }
                else{
                    //killbill subscription not found, create new subscription
                    self.instantiateBillingPlansSelection()
                }
            }
        }
    }
    
    func instantiateHome(){
        
        //set up app theme
                     
                     let theme = Defaults.getSelectedTheme()
                    
                     (theme == 1) ? ThemeManager.applyTheme(theme: .lightTheme) : ThemeManager.applyTheme(theme: .darkTheme)
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        self.view.window?.rootViewController = storyboard.instantiateInitialViewController()
        self.view.window?.makeKeyAndVisible()
    }
    func viewController(_ viewController: (UIViewController & AKFViewController), didCompleteLoginWith accessToken: AKFAccessToken, state: String) {
        //success
        //get users phone number from account kit
        
        _accountKit.requestAccount({ account, error in
            //update user phone number
           let phoneNumber = (account?.phoneNumber?.stringRepresentation())!
            
            //create RAG user using new Auth
            
            APIClient.msisdnLogin(msisdn: phoneNumber){ result in
                
                
                switch result.result {
                case .success(let value):
                    
                    Defaults.putBoolean(key: Defaults.hasLoggedInKey, value: true)
                    
                    //create account on UserDefaults
                    Defaults.saveUser(id: value.user?.id ?? "", name: value.user?.name ?? "", email: value.user?.email ?? "", phone: String(value.user?.phoneNumber?.suffix(12) ?? ""), city: value.user?.city ?? "", country: value.user?.country ?? "", dob: value.user?.dob ?? "", gender: value.user?.gender ?? "", createdAt: value.user?.createdAt ?? "")
                    
                    self.checkSubscription(userID: (value.user?.id)!)
    
                 case .failure(let encodingError):
                    
                    if let err = encodingError as? URLError, err.code == .notConnectedToInternet {
                        // no internet connection
                        Toast.show(message: "We are unable to get an internet connection, please try again", controller: self)
                    }
                    else{
                        Toast.show(message: "Oops, something went wrong", controller: self)
                    }
                    
                }
           }
    })
}
            

    
    
    func viewController(_ viewController: (UIViewController & AKFViewController), didCompleteLoginWith code: String, state: String) {
        
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController), didFailWithError error: Error) {
        // ... implement appropriate error handling ...
        print("\(String(describing: viewController)) did fail with error: \(error.localizedDescription)")
    }
    
    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)) {
        // ... handle user cancellation of the login process .
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension OnboardingViewController: UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "onboardingCell", for: indexPath) as! OnboardingCollectionViewCell
   
        cell.ivOnboarding.image = images[indexPath.row]
        cell.backgroundColor = .clear
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
            
            let collectionViewWidth = collectionView.bounds.width
            let collectionViewHeight = collectionView.bounds.height
            
           return CGSize(width: collectionViewWidth, height: collectionViewHeight)
            
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
   
            return 0

    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            self.pageControl.currentPage = indexPath.row
        self.lblDescription.text = descriptions[indexPath.row]
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
            return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
       
    }
}
