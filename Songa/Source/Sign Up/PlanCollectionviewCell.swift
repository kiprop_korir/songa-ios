//
//  PlanCollectionviewCell.swift
//  Songa
//
//  Created by Collins Korir on 10/1/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

class PlanCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet fileprivate weak var  tvPlans: UITableView!
    
}

extension PlanCollectionViewCell {
    
    func setTableViewDataSourceDelegate<D: UITableViewDataSource & UITableViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        tvPlans.delegate = dataSourceDelegate
        tvPlans.dataSource = dataSourceDelegate
        
        
        
        
        
        
        
        print("tag set \(row)")
        
        
        tvPlans.separatorStyle = .none
        tvPlans.tag = row
        tvPlans.reloadData()
        
        
    }
    
    var tableViewOffset: CGFloat {
         set { tvPlans.contentOffset.x = newValue }
         get { return tvPlans.contentOffset.x }
     }
      
}
