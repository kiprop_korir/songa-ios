//
//  SubscriptionProcessingViewController.swift
//  Songa
//
//  Created by Collins Korir on 1/30/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit
import AVKit
import Alamofire

class SubscriptionProcessingViewController: UIViewController {
    
       // TODO: Get that methodology from firebase that automatically skips the video when account is created
    
   @IBOutlet weak var videoContainer: UIView!
    @IBOutlet weak var progressBar: UIProgressView!
    var player: AVPlayer?
    var timer: Timer?
    var poseDuration = 22
    var indexProgressBar = 0
    var currentPoseIndex = 0
    var message: String?
    var success:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //super.viewDidLayoutSubviews()
        playWaitingVideo()
    
        // start the timer
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(setProgressBar), userInfo: nil, repeats: true)
        
    }
    
   @objc func setProgressBar()
    {
        if currentPoseIndex < poseDuration
        {
            // use poseDuration - 1 so that you display 22 steps of the the progress bar, from 0...21
            
        progressBar.progress = Float(currentPoseIndex) / Float(poseDuration - 1)
        
        // increment the counter
        currentPoseIndex += 1
            
        }
        else {
            timer?.invalidate()
            checkSubscriptionStatus(userID: (Defaults.getUser()?.id)!)
        }
    }
        
    func playWaitingVideo(){
        
        //resize view
        self.videoContainer.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height )
        self.videoContainer.layoutIfNeeded()
        self.view.layoutSubviews()
               self.viewDidAppear(true)
            // get the path string for the video from assets
            let videoString:String? = Bundle.main.path(forResource: "loading_screen_video", ofType: "mp4")
            guard let unwrappedVideoPath = videoString else {
                return}
            
            // convert the path string to a url
            let videoUrl = URL(fileURLWithPath: unwrappedVideoPath)
            
            // initialize the video player with the url
            self.player = AVPlayer(url: videoUrl)
            
            // create a video layer for the player
            let layer: AVPlayerLayer = AVPlayerLayer(player: player)
            
            // make the layer the same size as the container view
            layer.frame = videoContainer.bounds
            
            // make the video fill the layer as much as possible while keeping its aspect size
            layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            
            // add the layer to the container view
            videoContainer.layer.addSublayer(layer)
            
            player?.play()
            
            self.player?.addObserver(self, forKeyPath: "rate", options: NSKeyValueObservingOptions(rawValue: 0), context: nil)
            
        }
    
    func  checkSubscriptionStatus(userID:String){
   
        APIClient.getUserSubscription(externalKey: userID){ result in
            
            switch result.result {
            case .success(let value):
                //save subscription
                   Defaults.saveUserSubscription(accountId: value.accountId ?? "", billingFrequency: value.billingFrequency ?? "", externalKey: value.externalKey ?? "", planId: value.planId ?? "", productId: value.productId ?? "", productPlanType: value.productPlanType ?? "", productPrice: "", state: value.state ?? "", planName: value.planName ?? "", paymentStatus: value.paymentStatus ?? "", message: value.message ?? "", nextBillingDate: value.nextBillingDate ?? 0)
                
                Defaults.putBoolean(key: Defaults.hasSelectedPlansKey, value: true)
                
                self.message = value.message ?? value.reason
                self.success = true
                
            case .failure:
                
                //user still doesn't have a billing plan
                self.message = "An error occurred while processing your request"
                self.success = false
                
            }
            
            self.performSegue(withIdentifier: "segueBillingProcessingResult", sender: nil)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "rate" {
            if let playRate = self.player?.rate {
                if playRate == 0.0 {
                //play stopped, play vid again
                    playWaitingVideo()
                   
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! SubscriptionResultViewController
        vc.message = self.message
        vc.success = self.success
    }
        
    }
    

