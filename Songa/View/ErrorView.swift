//
//  ErrorView.swift
//  Songa
//
//  Created by Collins Korir on 2/27/19.
//  Copyright © 2019 Collins Korir. All rights reserved.
//

import UIKit

class ErrorView: UIView {

    @IBOutlet var content: UIView!
    @IBOutlet weak var btnRetry: RoundEdgesButton!
    @IBOutlet weak var tvError: UILabel!
    @IBOutlet weak var ivError: UIImageView!

    override init(frame: CGRect) {
        super .init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        commonInit()
    }
    
    private func commonInit(){
      Bundle.main.loadNibNamed("ErrorView", owner: self, options: nil)
        addSubview(content)
        content.frame = bounds
        content.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    }
        
    @IBAction func retryRequest(_ sender: Any) {
    NotificationCenter.default.post(name: Notification.Name.retryClicked,
                                        object: nil)
    }
    
    
}
