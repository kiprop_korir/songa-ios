# songa-ios
The Songa app written in Swift for the iPhone iOS

## What features do you get?

- Music streaming
- Videos
- Local and international FM radio
- Downloads
- Smart content recommendation

## How to fork on XCode
Clone an existing project > Copy and paste this link "https://github.com/Radioafricagroup/songa-ios/" 
